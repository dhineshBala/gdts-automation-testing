from Profile_functions.Profile_general_functions import ProfileGeneralFunctions
import time

class WarmwasserSetting(ProfileGeneralFunctions):
    """
    This is a helping class for the test case 'UHI-07 -- Warmwasser Einstellungen' with get, set and verify functions.

    """
    context = ProfileGeneralFunctions.WARMWASSER
    
    prof_info = ProfileGeneralFunctions.profile_info["Warmwasser"]
    Ww_sperre = prof_info["WW_Sperre"]
    TD_info = prof_info["Nacherwarm/Desinfek"]

    def expected_screen_name(self):
        """
        Returns screen name from de.common.json

        Returns
        -------
        'string'
            Warmwasser screen name
        """
        return self._content["WARMWATER"]

    def get_Ww_Ist_temp_from_WPM(self):
        """
        Returns 'Warmwasser' Ist temperature from WPM

        Returns
        -------
        'float'
            Ww 'Ist' temp
        """
        return self.tcp.get_values_from_analog_registers(self.regs_analog["E_Ww_Fuehl"], 10)
    
    def get_Ww_Soll_temp_from_WPM(self):
        """
        Returns 'Warmwasser' Soll temperature from WPM

        Returns
        -------
        'float'
            Ww 'Soll' temp
        """
        return self.tcp.get_values_from_integer_registers(self.regs_integer["P_WW_SOLL"])
    
    def get_Ww_Soll_Anz_temp_from_WPM(self):
        """
        Returns 'Warmwasser' Soll temperature from WPM

        Returns
        -------
        'float'
            Ww 'Soll' temp
        """
        return self.tcp.get_values_from_integer_registers(self.regs_integer["Ww_Soll_Anz"])

    def get_WwSperre_min_temp_from_WPM(self):
        """
        Returns 'Warmwasser Sperre' Minimum temperature from WPM

        Returns
        -------
        'int'
            Ww 'Sperre' Min temp
        """
        return self.tcp.get_values_from_integer_registers(self.regs_integer["P_WW_MIN_TEMP"])

    def get_WwTherDes_Soll_temp_from_WPM(self):
        """
        Returns 'Warmwasser Thermische Desinfektion' Soll temperature from WPM

        Returns
        -------
        'int'
            'Thermische Desinfektion' Soll temp
        """
        return self.tcp.get_values_from_integer_registers(self.regs_integer["P_WW_TD_SOLL"])

    def check_WwSperre_screen(self, test_case, element_ID, step):
        """
        Verifies 'Warmwasser' screen inside tile by checking IST, SOLL temperatures and frontend elements

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        element_ID
            An element ID for the particular screen number
        step
            step number from the test case document
        """
        screen_name = self.expected_screen_name()
        self.check_screen(test_case, screen_name, element_ID, self._content["CURRENT"], element_ID,
                          self.get_Ww_Ist_temp_from_WPM(), self._content["TARGET"], self.get_Ww_Soll_temp_from_WPM(), step)

    def set_WW_Soll_temp_in_UHI(self, test_case, element_ID, step):
        """
        Set 'Soll' temperatures for Warmwasser
        
        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        element_ID
            An element ID for the particular screen number
        step
            step number from the test case document
        """
        soll_temp = self.prof_info["Soll_temp"]
        self.set_temp_slider(test_case, self.context, soll_temp, element_ID, step)

    def choose_WwSperre_days(self, test_case, step):
        """
        Select 'Ww sperre' days from *'profiles_info.json'* file
        
        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        run_days = self.Ww_sperre["Running_days"]
        self.choose_days(test_case + " --> Ww Sperre ", run_days, step)

    def get_WwSperre_profile_times_from_WPM(self):
        """
        Returns Ww Sperre times (Zeit1, Zeit2)
        
        Returns
        -------
        'int' tuple
            Returns ST_H1, ST_M1, END_H1, END_M1, ST_H2, ST_M2, END_H2, END_M2
        """
        reg_index = "P_WW_SP_ST_H1"
        ST_H1 = self.tcp.get_values_from_integer_registers(
            self.regs_integer[reg_index])
        reg_index = "P_WW_SP_ST_M1"
        ST_M1 = self.tcp.get_values_from_integer_registers(
            self.regs_integer[reg_index])
        reg_index = "P_WW_SP_END_H1"
        END_H1 = self.tcp.get_values_from_integer_registers(
            self.regs_integer[reg_index])
        reg_index = "P_WW_SP_END_M1"
        END_M1 = self.tcp.get_values_from_integer_registers(
            self.regs_integer[reg_index])
        reg_index = "P_WW_SP_ST_H2"
        ST_H2 = self.tcp.get_values_from_integer_registers(
            self.regs_integer[reg_index])
        reg_index = "P_WW_SP_ST_M2"
        ST_M2 = self.tcp.get_values_from_integer_registers(
            self.regs_integer[reg_index])
        reg_index = "P_WW_SP_END_H2"
        END_H2 = self.tcp.get_values_from_integer_registers(
            self.regs_integer[reg_index])
        reg_index = "P_WW_SP_END_M2"
        END_M2 = self.tcp.get_values_from_integer_registers(
            self.regs_integer[reg_index])
        return ST_H1, ST_M1, END_H1, END_M1, ST_H2, ST_M2, END_H2, END_M2


    def get_WwSperre_days_from_WPM(self):
        """
        Returns 'Ww Sperre' running days from WPM registers

        Returns
        -------
        'int' tuple
            Returns Mo, Di, ... So int value
        """
        MO = self.tcp.get_values_from_integer_registers(self.regs_integer["P_WW_SP_MO"])
        DI = self.tcp.get_values_from_integer_registers(self.regs_integer["P_WW_SP_DI"])
        MI = self.tcp.get_values_from_integer_registers(self.regs_integer["P_WW_SP_MI"])
        DO = self.tcp.get_values_from_integer_registers(self.regs_integer["P_WW_SP_DO"])
        FR = self.tcp.get_values_from_integer_registers(self.regs_integer["P_WW_SP_FR"])
        SA = self.tcp.get_values_from_integer_registers(self.regs_integer["P_WW_SP_SA"])
        SO = self.tcp.get_values_from_integer_registers(self.regs_integer["P_WW_SP_SO"])

        return MO, DI, MI, DO, FR, SA, SO

    def verify_WwSperre_days(self, test_case, step):
        """
        Verify 'Ww Sperre' actual days against expected days

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        WPM_days = self.get_WwSperre_days_from_WPM()
        UHI_days = self.Ww_sperre["Running_days"]
        self.verify_intervals_with_WPM(test_case, WPM_days, UHI_days, step)

        # Deselect the Wochenverlauf for future test
        self.choose_WwSperre_days(test_case, step)

    def verify_WwSperre_times(self, test_case, step):
        """
        Verify 'Ww Sperre' actual times against expected times

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        UHI_times = self.get_time_intervals_from_UHI(test_case, step)
        WPM_times = self.get_WwSperre_profile_times_from_WPM()
        
        self.verify_profile_times(test_case, self.context, WPM_times, UHI_times, step)

    def verify_WwSperre_temps_in_days_screen(self, test_case, step):
        """
        Verify 'Ww Sperre' temperatures (Soll, Min Ww.Temp) in running days screen
        
        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        self.verify_Refernz_temp_in_days_screen(test_case, self.WARMWASSER_SPERRE,  self.get_Ww_Soll_temp_from_WPM(), step)
        self.verify_Profil_temp_in_days_screen(test_case, self.WARMWASSER_SPERRE, self.get_WwSperre_min_temp_from_WPM(), step)

    def choose_WwTherDes_days(self, test_case, step):
        """
        Select 'Ww Thermische Desinfektion' days from JSON
        
        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        run_days = self.TD_info["Running_days"]
        self.choose_Thermische_days(test_case + " --> Therm.Desinfek ", run_days, step)

    def verify_WwTherDes_times(self, test_case, step):
        """
        Verify 'Ww Thermische Desinfektion' actual times against expected times

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        UHI_ST_H1, UHI_ST_M1 = self.zeit_start_1(test_case, step)
        WPM_ST_H1 = self.tcp.get_values_from_integer_registers(
            self.regs_integer["P_WW_TD_ST_H1"])
        WPM_ST_M1 = self.tcp.get_values_from_integer_registers(
            self.regs_integer["P_WW_TD_ST_M1"])
        self.compare_results(test_case, WPM_ST_H1, UHI_ST_H1, "Thermische Desinfektion Hour 1 mismatch", step)
        self.compare_results(test_case, WPM_ST_M1, UHI_ST_M1, "Thermische Desinfektion Mins 1 mismatch", step)

    def get_WwTherDes_days_from_WPM(self):
        """
        Returns 'Ww Thermische Desinfektion' running days from WPM registers

        Returns
        -------
        'int' tuple
            Returns Mo, Di, ... So int value
        """
        MO = self.tcp.get_values_from_integer_registers(self.regs_integer["P_WW_TD_MO"])
        DI = self.tcp.get_values_from_integer_registers(self.regs_integer["P_WW_TD_DI"])
        MI = self.tcp.get_values_from_integer_registers(self.regs_integer["P_WW_TD_MI"])
        DO = self.tcp.get_values_from_integer_registers(self.regs_integer["P_WW_TD_DO"])
        FR = self.tcp.get_values_from_integer_registers(self.regs_integer["P_WW_TD_FR"])
        SA = self.tcp.get_values_from_integer_registers(self.regs_integer["P_WW_TD_SA"])
        SO = self.tcp.get_values_from_integer_registers(self.regs_integer["P_WW_TD_SO"])

        return MO, DI, MI, DO, FR, SA, SO

    def verify_WwTherDes_days(self, test_case, step):
        """
        Verify 'Ww Thermische Desinfektion' actual days against expected days

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        WPM_days = self.get_WwTherDes_days_from_WPM()
        UHI_days = self.TD_info["Running_days"]
        self.verify_Thermische_intervals_with_WPM(
            test_case, WPM_days, UHI_days, step)
        time.sleep(3)

        # Deselect the Wochenverlauf for future test
        self.choose_WwTherDes_days(test_case, step)

    def verify_WwTherDes_temps_in_days_screen(self, test_case, step):
        """
        Verify 'Ww Thermische Desinfektion' temperatures (Soll) in running days screen

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        self.verify_Profil_temp_in_days_screen(test_case, self.THERMISCHE_DESINFEKTION, self.get_WwTherDes_Soll_temp_from_WPM(), step)

    def choose_Zirkulation_days(self, test_case, step):
        """
        Select 'Ww Zirkulation' days from 'profiles_info.json' file
        
        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        run_days = self.prof_info["Zirkulation"]["Running_days"]
        self.choose_days(test_case + " --> Zirkulation ", run_days, step)

    def get_Zirkulation_times_from_WPM(self):
        """
        Returns `Zirkulation` times from WPM Registers

        Returns
        -------
        'int' tuple
            Returns ST_H1, ST_M1, END_H1, END_M1, ST_H2, ST_M2, END_H2, END_M2
        """
        reg_index = "P_WW_ZIRK_ST_H1"
        ST_H1 = self.tcp.get_values_from_integer_registers(
            self.regs_integer[reg_index])
        reg_index = "P_WW_ZIRK_ST_M1"
        ST_M1 = self.tcp.get_values_from_integer_registers(
            self.regs_integer[reg_index])
        reg_index = "P_WW_ZIRK_END_H1"
        END_H1 = self.tcp.get_values_from_integer_registers(
            self.regs_integer[reg_index])
        reg_index = "P_WW_ZIRK_END_M1"
        END_M1 = self.tcp.get_values_from_integer_registers(
            self.regs_integer[reg_index])
        reg_index = "P_WW_ZIRK_ST_H2"
        ST_H2 = self.tcp.get_values_from_integer_registers(
            self.regs_integer[reg_index])
        reg_index = "P_WW_ZIRK_ST_M2"
        ST_M2 = self.tcp.get_values_from_integer_registers(
            self.regs_integer[reg_index])
        reg_index = "P_WW_ZIRK_END_H2"
        END_H2 = self.tcp.get_values_from_integer_registers(
            self.regs_integer[reg_index])
        reg_index = "P_WW_ZIRK_END_M2"
        END_M2 = self.tcp.get_values_from_integer_registers(
            self.regs_integer[reg_index])
        return ST_H1, ST_M1, END_H1, END_M1, ST_H2, ST_M2, END_H2, END_M2

    def get_Zirkulation_days_from_WPM(self):
        """
        Returns 'Ww Zirkulation' days from WPM registers

        Returns
        -------
        'int' tuple
            Returns Mo, Di, ... So int value
        """
        MO = self.tcp.get_values_from_integer_registers(self.regs_integer["P_WW_ZIRK_MO"])
        DI = self.tcp.get_values_from_integer_registers(self.regs_integer["P_WW_ZIRK_DI"])
        MI = self.tcp.get_values_from_integer_registers(self.regs_integer["P_WW_ZIRK_MI"])
        DO = self.tcp.get_values_from_integer_registers(self.regs_integer["P_WW_ZIRK_DO"])
        FR = self.tcp.get_values_from_integer_registers(self.regs_integer["P_WW_ZIRK_FR"])
        SA = self.tcp.get_values_from_integer_registers(self.regs_integer["P_WW_ZIRK_SA"])
        SO = self.tcp.get_values_from_integer_registers(self.regs_integer["P_WW_ZIRK_SO"])

        return MO, DI, MI, DO, FR, SA, SO

    def verify_Zirkulation_with_WPM(self, test_case, step):
        """
        Verify 'Ww Zirkulation' setup with WPM

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        WPM_times = self.get_Zirkulation_times_from_WPM()
        UHI_times = self.get_time_intervals_from_UHI(test_case, step)
        self.verify_profile_times(test_case, self._content["CIRCULATION"], WPM_times, UHI_times, step)

        WPM_days = self.get_Zirkulation_days_from_WPM()
        UHI_days = self.prof_info["Zirkulation"]["Running_days"]
        self.verify_intervals_with_WPM(test_case, WPM_days, UHI_days, step)

        # Deselect the days from UHI
        self.choose_Zirkulation_days(test_case, step)

    def check_Reset_Max_WWTemp_button(self, test_case, step):
        """ check `Haus` tile for ** Reset Warmwasser-Maximaltemperatur ** button """
        Reset_btn = self.findElementCSS(test_case, self._buttons["btn_WW_Reset"], step)
        if Reset_btn is None:
            self.compare_true_result(test_case, False, "Reset Warmwasser-Maximaltemperatur button is not presented", step)
        else:
            return Reset_btn
    
    def Reset_WW_Max_temp(self, test_case, step):
        """ Activate `Reset WW Max temp` buttton in Haus screen """
        Reset_btn = self.check_Reset_Max_WWTemp_button(test_case, step)
        Reset_btn.click()
        self.get_click_from_text_area(test_case, self._content["RESET_MAX_WW_TEMP_CONFIRM"], step)

    def verify_Reset_WW_Max_temp_with_WPM(self, test_case, step):
        """ Read `P_WW_RES_MAX` Digital register """
        WPM = self.get_status_from_coils(self.regs_digital["P_WW_RES_MAX"])
        # This step has to be checked manually
        # self.compare_true_result(test_case, WPM, "Warmwasser-Maximaltemperatur Reset is not setup in WPM", step)


        