from Profile_functions.Profile_general_functions import ProfileGeneralFunctions

class SilentMode(ProfileGeneralFunctions):
    SILENT_MODE = "Silent Mode"
    SILENT_MODE_DAYS = ProfileGeneralFunctions.profile_info["Silent_mode"]["Running_days"]

    def setup_silent_mode_UHI(self, test_case, step):
        """
        Select 'Silent Mode' days from 'profiles_info.json' file
        
        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        self.get_click_from_text_area(
                test_case, self._content["SILENT_MODE"], step)
        self.click_Edit_without_ripple_text(test_case, step)
        # change time
        self.change_time_bars_inside_profile(test_case, step)
        self.click_Fertig_without_ripple_text(test_case, step)
        self.choose_days(test_case + self.SILENT_MODE, self.SILENT_MODE_DAYS, step)
        self.click_left_arrow(test_case, 0, step)

    def get_silent_mode_times_from_WPM(self):
        """ Reads `Silent mode` registers from Modbus and returns as 'int' tuple """
        ST_H1 = self.get_values_from_integer_registers(self.regs_integer["P_EC_ABS_ST_H1"])
        ST_M1 = self.get_values_from_integer_registers(self.regs_integer["P_EC_ABS_ST_M1"])
        END_H1 = self.get_values_from_integer_registers(self.regs_integer["P_EC_ABS_END_H1"])
        END_M1 = self.get_values_from_integer_registers(self.regs_integer["P_EC_ABS_END_M1"])
        ST_H2 = self.get_values_from_integer_registers(self.regs_integer["P_EC_ABS_ST_H2"])
        ST_M2 = self.get_values_from_integer_registers(self.regs_integer["P_EC_ABS_ST_M2"])
        END_H2 = self.get_values_from_integer_registers(self.regs_integer["P_EC_ABS_END_H2"])
        END_M2 = self.get_values_from_integer_registers(self.regs_integer["P_EC_ABS_END_M2"])
        return ST_H1, ST_M1, END_H1, END_M1, ST_H2, ST_M2, END_H2, END_M2

    def get_silent_mode_days_from_WPM(self):
        """ Reads `Silent mode` registers from Modbus and returns as 'int' tuple """
        Mo = self.get_values_from_integer_registers(self.regs_integer["P_EC_ABS_Mo"])
        Di = self.get_values_from_integer_registers(self.regs_integer["P_EC_ABS_Di"])
        Mi = self.get_values_from_integer_registers(self.regs_integer["P_EC_ABS_Mi"])
        Do = self.get_values_from_integer_registers(self.regs_integer["P_EC_ABS_Do"])
        Fr = self.get_values_from_integer_registers(self.regs_integer["P_EC_ABS_Fr"])
        Sa = self.get_values_from_integer_registers(self.regs_integer["P_EC_ABS_Sa"])
        So = self.get_values_from_integer_registers(self.regs_integer["P_EC_ABS_SO"])
        return Mo, Di, Mi, Do, Fr, Sa, So

    def verify_silent_mode(self, test_case, step):
        """ verify `Silent mode` UHI values with WPM values and logs fails in `Failed.log` """
        self.get_click_from_text_area(
                test_case, self._content["SILENT_MODE"], step)
        WPM_time = self.get_silent_mode_times_from_WPM()
        UHI_time = self.get_time_intervals_from_UHI(test_case, step)
        self.verify_profile_times(
            test_case, self.SILENT_MODE, WPM_time, UHI_time, step)

        WPM_days = self.get_silent_mode_days_from_WPM()
        UHI_days = self.SILENT_MODE_DAYS
        self.verify_intervals_with_WPM(test_case, WPM_days, UHI_days, step)
        self.choose_days(test_case + self.SILENT_MODE, self.SILENT_MODE_DAYS, step)
        self.click_left_arrow(test_case, 0, step)


                    
                    
                    
                    
                    
                    
