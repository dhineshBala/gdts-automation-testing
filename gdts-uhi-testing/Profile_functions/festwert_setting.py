from Profile_functions.Profile_general_functions import ProfileGeneralFunctions
import time, copy

class FestwertSettings(ProfileGeneralFunctions):
    """
    This is a supporting class for test case 'UHI-15 -- Festwert Settings' and it is derived from 'ProfileGeneralFunctions' where all common profile functions are defined.
    
    """
    prof_info = ProfileGeneralFunctions.profile_info["Festwert_info"]

    def expected_screen_name(self, HK):
        """
        Returns screen name from JSON

        Parameters
        ----------
        HK
            Targeted 'Heizkreis' number for the specific step

        Returns
        -------
        'string'
            Returns from 'profiles_info.json' file
        """
        HK = "HK" + str(HK)
        return self.prof_info[HK]["HK_name"]

    def set_Soll_temp(self, test_case, context, HK, element_ID, step):
        """
        set Soll temperatures of HK 1 & 2
        
        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        context 
            Key words to differentiate functions like Festwert, Raumregeler
        HK
            Targeted 'Heizkreis' number for the specific step
        element_ID
            Element number for the targeted screen (0, 1, 2, 3, ...)
        step
            step number from the test case document
        """
        HK = "HK" + str(HK)
        soll_temp = self.prof_info[HK]["Soll_temp"]
        self.set_temp_slider(test_case, context, soll_temp, element_ID, step)
    
    def choose_Festwert_days(self, test_case, HK, step):
        """
        Select running days from JSON
        
        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        HK
            Targeted 'Heizkreis' number for the specific step
        step
            step number from the test case document
        """
        HK = "HK" + str(HK)
        self.choose_days(test_case, self.prof_info[HK]["Running_days"], step)

    def verify_Festwert_screens(self, test_case, context, HK, element_ID, step):
        """
        Steps to verify Festwert screens of HK1 and HK2

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        context 
            Key words to differentiate functions like Festwert, Raumregeler
        HK
            Targeted 'Heizkreis' number for the specific step
        element_ID
            Element number for the targeted screen (0, 1, 2, 3, ...)
        step
            step number from the test case document
        """
        screen_name = self.expected_screen_name(HK)
        FWR_Ist_temp = self.get_Heizkreis_Ist_temp_from_WPM(HK)
        FWR_mode, FWR_wert = self.get_Heizkreis_mode_from_WPM(HK)
        FWR_Ref_temp = self.get_Festwert_Ref_temp_from_WPM(HK)
        copy_FWR_Ref_temp = copy.deepcopy(FWR_Ref_temp)
        
        # check if profile time is now
        if self.is_any_HK_profile_active(HK):
            FWR_Ref_temp = FWR_Ref_temp + FWR_wert

        self.check_screen(test_case, screen_name, element_ID, self._content["CURRENT"],
                            element_ID, FWR_Ist_temp, self._content["TARGET"], FWR_Ref_temp, step)
        self.click_down_triangle(test_case, element_ID, step)
        self.click_right_arrow_by_name(test_case, self._content["WEEK_OVERVIEW"], step)
        # Verify Absenkung / Anhebung mode profile times 
        self.verify_Heizkreis_profile_times_with_WPM(test_case, HK, FWR_mode, step)
        # Verify Soll temperature displayed at Wochenverlauf screen
        self.verify_Refernz_temp_in_days_screen(test_case, self.FESTWERT, copy_FWR_Ref_temp, step)
        # Verify Profile temperature displayed at Wochenverlauf screen
        self.verify_Profil_temp_in_days_screen(test_case, self.FESTWERT, FWR_wert , step)
        # Verify running days with WPM
        self.verify_Festwert_run_days(test_case, HK, FWR_mode, step)
        self.choose_Festwert_days(test_case, HK, step)
        self.click_left_arrow(test_case, element_ID, step)
        self.click_less_option_button(test_case, step)
        
    def verify_Festwert_run_days(self, test_case, HK, mode, step):
        """
        Steps to verify *Festwert* 'Wochenvorlauf' with WPM values

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        HK
            Targeted 'Heizkreis' number for the specific step
        mode
            'Abenkung / Anhebung' text to identify the verification variables
        element_ID
            Element number for the targeted screen (0, 1, 2, 3, ...)
        step
            step number from the test case document
        """
        WPM_days = self.get_Heizkreis_run_days_from_WPM(HK, mode)
        UHI_days = self.prof_info["HK" + str(HK)]["Running_days"]
        self.verify_intervals_with_WPM(test_case, WPM_days, UHI_days, step)

    def setup_Festwert_values(self, test_case, HK, step):
        """ Setup all `Festwert` values in the Heating circuits """
        element_ID = 0
        self.set_Soll_temp(test_case, self.FESTWERT, HK, element_ID, step)
        self.click_down_triangle(test_case, element_ID, step)
        if self.create_profile_Home_screen(test_case, 0, step):
            self.click_down_triangle(test_case, element_ID, step)
        self.click_right_arrow_by_name(test_case,
                                       self._content["WEEK_OVERVIEW"], step)
        self.choose_Festwert_days(test_case, HK, step)
        self.click_left_arrow(test_case, 0, step)
        self.click_less_option_button(test_case, step)

    