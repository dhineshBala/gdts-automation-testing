from Profile_functions.Profile_general_functions import ProfileGeneralFunctions
import time


class RaumreglerSettings(ProfileGeneralFunctions):
    """
    This is a supporting class for test case 'UHI-13 -- Raumregelung Settings' and it is derived from 'ProfileGeneralFunctions' where all common profile functions are defined.

    """
    prof_info = ProfileGeneralFunctions.profile_info["Raumregulung_info"]

    def expected_screen_name(self, room_no):
        """
        Returns screen name from JSON

        Parameters
        ----------
        room_no
            Targeted room number in the format of 50, 51, 52, 53 ... 69

        Returns
        -------
        'string'
            Returns from 'profiles_info.json' file
        """
        room = "RAUM_" + str(room_no)
        return self.prof_info[room]["Room_name"]

    def verify_screen_count(self, test_case, expected_count, step):
        """
        Verify total number of Room screens inside Raumregulung tile

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        expected_count
            An expected count of Room screens from WPM config
        step
            step number from the test case document
        """
        actual = self.get_Raumregulung_screen_count(test_case, step)
        self.compare_results(test_case, expected_count,
                             actual, "Raumregulung Screen count mismatch inside Haus tiles", step)

    def get_Ist_temp_from_WPM(self, room_no):
        """
        Returns Ist temperatures of RAUM_50 .. to .. 69

        Parameters
        ----------
        room_no
            Targeted room number in the format of 50, 51, 52, 53 ... 69

        Returns
        -------
        'float'
            Returns WPM registers
        """
        regs_index = "E_T_Raum_" + str(room_no)
        ist_temp = self.tcp.get_values_from_integer_registers(
            self.regs_integer[regs_index]) / 10
        return ist_temp

    def get_Soll_temp_from_WPM(self, room_no):
        """
        Returns Soll temperatures of RAUM_50 .. to .. 69

        Parameters
        ----------
        room_no
            Targeted room number in the format of 50, 51, 52, 53 ... 69

        Returns
        -------
        'float'
            Returns WPM registers
        """
        regs_index = "P_Soll_Raum_" + str(room_no)
        soll_temp = self.tcp.get_values_from_analog_registers(
            self.regs_analog[regs_index], 10)
        return soll_temp

    def get_Profile_temp_from_WPM(self, room_no):
        """
        Returns `Profile` temperatures of RAUM_50 .. to .. 69

        Parameters
        ----------
        room_no
            Targeted room number in the format of 50, 51, 52, 53 ... 69

        Returns
        -------
        'float'
            Returns WPM registers
        """
        regs_index = "P_Soll_ZP_Raum_" + str(room_no)
        prof_temp = self.tcp.get_values_from_analog_registers(
            self.regs_analog[regs_index], 10)
        return prof_temp

    def set_soll_temp_in_UHI(self, test_case, room_no, element_ID, step):
        """
        set Soll temperatures of RAUM_50 .. to .. 69

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        room_no
            Targeted room number in the format of 50, 51, 52, 53 ... 69
        element_ID
            Element number for the targeted screen (0, 1, 2, 3, ...)
        step
            step number from the test case document
        """
        time.sleep(2)
        Room = "RAUM_" + str(room_no)
        soll_temp = self.prof_info[Room]["Soll_temp"]
        self.set_temp_slider(test_case, self.RAUMREGLER,
                             soll_temp, element_ID, step)

    def get_Ventilstellung_from_WPM(self, room_no):
        """
        Returns 'Ventilstellung' of RAUM_50 .. to .. 69

        Parameters
        ----------
        room_no
            Targeted room number in the format of 50, 51, 52, 53 ... 69

        Returns
        -------
        'string'
            returns zu/auf from registers
        """
        reg_index = "A_thT_Ventil_Raum_" + str(room_no)
        if self.tcp.get_status_from_coils(self.regs_digital[reg_index]):
            return self._content["OPEN"]
        else:
            return self._content["CLOSED"]

    def get_Communication_status_from_WPM(self, room_no):
        """
        Returns 'Communication' status of RAUM_50 .. to .. 69

        Parameters
        ----------
        room_no
            Targeted room number in the format of 50, 51, 52, 53 ... 69

        Returns
        -------
        'string'
            returns ja/nein from registers
        """
        reg_index = "thT_status_" + str(room_no)
        result = self.tcp.get_values_from_integer_registers(
            self.regs_integer[reg_index])
        if result == 2:
            return self._content["YES"]
        else:
            return self._content["NO"]

    def get_Fenster_status_from_WPM(self, room_no):
        """
        Returns 'Fenster' status of RAUM_50 .. to .. 69

        Parameters
        ----------
        room_no
            Targeted room number in the format of 50, 51, 52, 53 ... 69

        Returns
        -------
        'string'
            returns ja/nein from registers
        """
        reg_index = "thT_status_" + str(room_no)
        if self.tcp.get_values_from_integer_registers(self.regs_integer[reg_index]) == 4:
            return self._content["YES"]
        else:
            return self._content["NO"]

    def get_humidity_from_WPM(self, room_no):
        """
        Returns Humidity of RAUM_50 .. to .. 69

        Parameters
        ----------
        room_no
            Targeted room number in the format of 50, 51, 52, 53 ... 69

        Returns
        -------
        'float'
            returns *Humidity* value from registers
        """
        regs_index = "E_H_Raum_" + str(room_no)
        humidity = self.tcp.get_values_from_integer_registers(
            self.regs_integer[regs_index]) / 10
        return humidity

    def get_profile_times_from_WPM(self, room_no):
        """
        Returns profile times (Zeit1, Zeit2) of RAUM_50 .. to .. 69

        Parameters
        ----------
        room_no
            Targeted room number in the format of 50, 51, 52, 53 ... 69

        Returns
        -------
        'int' tuple
            return value: ST_H1, ST_M1, END_H1, END_M1, ST_H2, ST_M2, END_H2, END_M2
        """
        reg_index = "P_ZP_Raum_" + str(room_no) + "_ST_H1"
        ST_H1 = self.tcp.get_values_from_integer_registers(
            self.regs_integer[reg_index])
        reg_index = "P_ZP_Raum_" + str(room_no) + "_ST_M1"
        ST_M1 = self.tcp.get_values_from_integer_registers(
            self.regs_integer[reg_index])
        reg_index = "P_ZP_Raum_" + str(room_no) + "_END_H1"
        END_H1 = self.tcp.get_values_from_integer_registers(
            self.regs_integer[reg_index])
        reg_index = "P_ZP_Raum_" + str(room_no) + "_END_M1"
        END_M1 = self.tcp.get_values_from_integer_registers(
            self.regs_integer[reg_index])
        reg_index = "P_ZP_Raum_" + str(room_no) + "_ST_H2"
        ST_H2 = self.tcp.get_values_from_integer_registers(
            self.regs_integer[reg_index])
        reg_index = "P_ZP_Raum_" + str(room_no) + "_ST_M2"
        ST_M2 = self.tcp.get_values_from_integer_registers(
            self.regs_integer[reg_index])
        reg_index = "P_ZP_Raum_" + str(room_no) + "_END_H2"
        END_H2 = self.tcp.get_values_from_integer_registers(
            self.regs_integer[reg_index])
        reg_index = "P_ZP_Raum_" + str(room_no) + "_END_M2"
        END_M2 = self.tcp.get_values_from_integer_registers(
            self.regs_integer[reg_index])
        return ST_H1, ST_M1, END_H1, END_M1, ST_H2, ST_M2, END_H2, END_M2

    def verify_Room_profile_times_with_WPM(self, test_case, room_no, step):
        """
        Verify Raumregelung profile actual times against expected times

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        room_no
            Targeted room number in the format of 50, 51, 52, 53 ... 69
        step
            step number from the test case document
        """
        time.sleep(2)
        UHI_times = self.get_time_intervals_from_UHI(test_case, step)
        WPM_times = self.get_profile_times_from_WPM(room_no)
        room_name = "RAUM_" + str(room_no)
        self.verify_profile_times(
            test_case, room_name, WPM_times, UHI_times, step)

    def choose_Raumregler_days(self, test_case, room_no, step):
        """
        Select running days from JSON

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        room_no
            Targeted room number in the format of 50, 51, 52, 53 ... 69
        step
            step number from the test case document
        """
        room_no = "RAUM_" + str(room_no)
        self.choose_days(
            test_case, self.prof_info[room_no]["Running_days"], step)

    def get_Raumregler_run_days_from_WPM(self, room_no):
        """
        Returns 'Raumregler' running days from WPM registers

        Parameters
        ----------
        room_no
            Targeted room number in the format of 50, 51, 52, 53 ... 69

        Returns
        -------
        'int' tuple
            returns 0, 1, 2 and 3
        """
        Mo = "P_ZP_Raum_" + str(room_no) + "_MO"
        Di = "P_ZP_Raum_" + str(room_no) + "_DI"
        Mi = "P_ZP_Raum_" + str(room_no) + "_MI"
        Do = "P_ZP_Raum_" + str(room_no) + "_DO"
        Fr = "P_ZP_Raum_" + str(room_no) + "_FR"
        Sa = "P_ZP_Raum_" + str(room_no) + "_SA"
        So = "P_ZP_Raum_" + str(room_no) + "_SO"

        MO = self.tcp.get_values_from_integer_registers(self.regs_integer[Mo])
        DI = self.tcp.get_values_from_integer_registers(self.regs_integer[Di])
        MI = self.tcp.get_values_from_integer_registers(self.regs_integer[Mi])
        DO = self.tcp.get_values_from_integer_registers(self.regs_integer[Do])
        FR = self.tcp.get_values_from_integer_registers(self.regs_integer[Fr])
        SA = self.tcp.get_values_from_integer_registers(self.regs_integer[Sa])
        SO = self.tcp.get_values_from_integer_registers(self.regs_integer[So])

        return MO, DI, MI, DO, FR, SA, SO

    def verify_Raumregler_run_days(self, test_case, room_no, step):
        """
        Verifies 'Raumregler' running days with WPM registers values

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        room_no
            Targeted room number in the format of 50, 51, 52, 53 ... 69
        step
            step number from the test case document
        """
        WPM_days = self.get_Raumregler_run_days_from_WPM(room_no)
        UHI_days = self.prof_info["RAUM_" + str(room_no)]["Running_days"]
        self.verify_intervals_with_WPM(test_case, WPM_days, UHI_days, step)

    def setup_Raumregler_Soll_temp(self, test_case, Rooms, room_no, step):
        """
        Steps to setup Raumregler Soll temp of Rooms 50 to 69

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        HK
            Targeted 'Heizkreis' number for the specific step
        room_no
            Targeted room number in the format of 50, 51, 52, 53 ... 69
        step
            step number from the test case document
        """
        self.set_soll_temp_in_UHI(test_case, room_no, 0, step)

    def setup_Raumregler_run_days(self, test_case, target_screen, target_tile, HK, room_no, step):
        """
        Steps to setup Raumregler running days of Rooms 50 to 69

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        target_screen
            The screen where the action is performed (Home, Settings, EasyOn, Analysis)
        target_tile
            The tile where the test steps are performed (Warmwasser, Haus, Netzwerk, Software update and so on.)
        HK
            Targeted 'Heizkreis' number for the specific step
        room_no
            Targeted room number in the format of 50, 51, 52, 53 ... 69
        step
            step number from the test case document
        """
        self.click_down_triangle(test_case, 0, step)
        self.click_right_arrow_by_name(test_case,
                                       self._content["WEEK_OVERVIEW"], step)
        if self.create_profile_Home_screen(test_case, 0, step):
            self.click_right_arrow_by_name(test_case,
                                       self._content["WEEK_OVERVIEW"], step)
        self.choose_Raumregler_days(test_case, room_no, step)
        self.click_left_arrow(test_case, 0, step)
        self.click_less_option_button(test_case, step)

    def verify_Raumregler_temps_in_days_screen(self, test_case, room_no, step):
        """
        Verify 'Raumregler' temperatures (Soll, Referenz) in running days screen.

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        self.verify_Refernz_temp_in_days_screen(
            test_case, self.RAUMREGLER, self.get_Soll_temp_from_WPM(room_no), step)
        self.verify_Profil_temp_in_days_screen(
            test_case, self.RAUMREGLER, self.get_Profile_temp_from_WPM(room_no), step)

    def verify_Raumregler_run_days_steps(self, test_case, target_screen, target_tile, HK, room_no, step):
        """
        Steps to verify Raumregler screens of Rooms 50 to 69

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        target_screen
            The screen where the action is performed (Home, Settings, EasyOn, Analysis)
        target_tile
            The tile where the test steps are performed (Warmwasser, Haus, Netzwerk, Software update and so on.)
        HK
            Targeted 'Heizkreis' number for the specific step
        room_no
            Targeted room number in the format of 50, 51, 52, 53 ... 69
        step
            step number from the test case document
        """
        screen_name = self.expected_screen_name(room_no)
        self.check_screen(test_case, screen_name, 0, self._content["CURRENT"], 0, self.get_Ist_temp_from_WPM(
            room_no), self._content["TARGET"], self.get_Soll_temp_from_WPM(room_no), step)
        self.click_down_triangle(test_case, 0, step)
        self.click_right_arrow_by_name(test_case,
                                       self._content["WEEK_OVERVIEW"], step)
        self.verify_Room_profile_times_with_WPM(test_case, room_no, step)
        self.verify_Raumregler_temps_in_days_screen(test_case, room_no, step)
        self.verify_Raumregler_run_days(test_case, room_no, step)
        self.choose_Raumregler_days(test_case, room_no, step)
        self.click_left_arrow(test_case, 0, step)
        self.click_less_option_button(test_case, step)
