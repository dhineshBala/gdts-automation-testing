from src.General_functions import GeneralFunctions
from selenium.webdriver.common.action_chains import ActionChains
import time
import json
import os
from src.logger import pass_log

PASS = pass_log.info
TEMP_UNITS = "° °C K"


class ProfileGeneralFunctions(GeneralFunctions):
    """
    This class defines most of the common functions that needed for verifying profile actual results against
    expected results. This is derived from base class "GeneralFunctions".

    """
    HEIZKURVE = "Heizkurve"
    FESTWERT = "Festwert"
    RAUMREGLER = "Raumregler"
    ANHEBUNG = "Anhebung"
    ABSENKUNG = "Absenkung"
    WARMWASSER = "Warmwasser"
    THERMISCHE_DESINFEKTION = "Thermische Desinfektion"
    WARMWASSER_SPERRE = "Warmwasser Sperre"

    _content = GeneralFunctions()._content

    json_dir = GeneralFunctions().json_dir
    with open(os.path.join(json_dir, 'profiles_info.json'), encoding='utf-8') as info:
        profile_info = json.load(info)

    screen_info = GeneralFunctions().screen_info["profile"]
    _text = screen_info["text"]
    _buttons = screen_info["buttons"]
    _slider = screen_info["slider"]
    _setting = screen_info["setting_info"]

    def read_Ist_value(self, test_case, element_ID, step):
        """
        Returns *'IST'* value from Haus/Raumregelung/Warmwasser tiles.

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        element_ID
            Required element number/ID for the particular scenario
        step
            step number from the test case document

        Returns
        -------
        'string'
            Ist temperature from screen
        """
        value = self.findElementsCSS(test_case, self._text["target"], step)[
            element_ID].text
        return value.strip(TEMP_UNITS)

    def read_Soll_value(self, test_case, element_ID, step):
        """
        Returns *'SOLL'* value from Haus/Raumregelung/Warmwasser tiles.

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        element_ID
            Required element number/ID for the particular scenario
        step
            step number from the test case document

        Returns
        -------
        'string'
            Soll temperature from screen
        """
        value = self.findElementsCSS(test_case, self._text["current"], step)[
            element_ID].text
        return value.strip(TEMP_UNITS)

    def read_humidity_value(self, test_case, element_ID, step):
        """
        Returns *'Humidity'* value from 'Raumregelung' tiles.

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        element_ID
            Required element number/ID for the particular scenario
        step
            step number from the test case document

        Returns
        -------
        'string'
            Humidity from Raumregeler tiles
        """
        value = self.findElementsCSS(test_case, self._text["humidity"], step)[
            element_ID].text
        return value.strip("%")

    def check_screen(self, test_case, screen_name, screen_name_ele_ID, Ist_name, element_ID, Ist_value, Soll_name, Soll_value, step):
        """
        Verifying *'Haus / Raumregeler / Warmwasser'* screens with expected results

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        screen_name
            Expected 'screen name' from *profiles_info.json* file
        screen_name_ele_ID
            Element number, if there are more than one screens available from *Haus / Raumregeler* tiles
        Ist_name
            Expected text in 'Ist' value area from *de.common.json* file
        element_ID
            Required element number/ID for the particular scenario
        Ist_value
            Expected 'Ist' value from reading *Modbus* registers
        Soll_name
            Expected text in 'Soll' value area from *de.common.json* file
        Soll_value
            Expected 'Soll' value from reading *Modbus* registers
        step
            step number from the test case document

        """
        self.compare_results(test_case, screen_name, self.findElementsCSS(test_case,
                                                                          self._text["screen_name"], step)[screen_name_ele_ID].text, screen_name + " is not presented in screen title", step)
        # check Ist and Soll values are as same as with WPM
        for ele in range(2):
            description = self.findElementsCSS(test_case,
                                               self._text["description"], step)
            if ele == 0:
                temp = self.read_Ist_value(test_case, element_ID, step)
                Ist_value_UHI = float(''.join(temp))
                self.compare_results(test_case, Ist_name, description[element_ID*2].text,
                                     "'Ist' name is wrong in screen: " + screen_name, step)
                self.compare_results(test_case, 0, Ist_value - Ist_value_UHI,
                                     "'Ist' value is wrong in screen: " + screen_name, step)

            if ele == 1:
                temp = self.read_Soll_value(test_case, element_ID, step)
                Soll_value_UHI = float(''.join(temp))
                self.compare_results(test_case, Soll_name, description[(element_ID*2)+1].text,
                                     "'Soll' name is wrong in screen: " + screen_name,  step)
                self.compare_results(test_case, 0, Soll_value - Soll_value_UHI,
                                     "The difference between UHI and WPM 'Soll' values is != 0: " + screen_name, step)
        # check temperature slider is presented
        self.verify_scale_is_displayed(
            test_case, screen_name, element_ID, step)

        # check downarrow is presented
        self.verify_triangle_is_displayed(
            test_case, screen_name, element_ID, step)

    def verify_scale_is_displayed(self, test_case, screen_name, element_ID, step):
        """
        Verifies if 'Temp' slider is presented in particular screen

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        screen_name
            Expected 'screen name' from *profiles_info.json* file
        element_ID
            Required element number/ID for the particular scenario
        step
            step number from the test case document
        """
        self.compare_true_result(test_case, self.findElementsCSS(test_case, self._slider["vertical"], step)[element_ID].is_displayed(
        ), screen_name + ": vertical scale is not presented in UHI screen", step)

    def verify_triangle_is_displayed(self, test_case, screen_name, element_ID, step):
        """
        Verifies if 'Triangle' icon (middle down) is presented in particular screen

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        screen_name
            Expected 'screen name' from *profiles_info.json* file
        element_ID
            Required element number/ID for the particular scenario
        step
            step number from the test case document
        """
        self.compare_true_result(test_case, self.findElementsCSS(test_case, self._buttons["triangle"], step)[element_ID].is_displayed(
        ), screen_name + ": Down triangle is not presented in UHI screen", step)

    def action_slider(self, slider, offset_x, offset_y, step):
        """
        It is a helping function of 'set_temp_slider()' to reduce redundency

        Parameters
        ----------
        slider
            Targeted *'slider'* element from Web frontend
        offset_x
            'x' direction offset to slide
        offset_y
            'y' direction offset to slide
        step
            step number from the test case document
        """
        action = ActionChains(self.browser)
        action.click_and_hold(slider).move_by_offset(
            offset_x, offset_y).release().perform()

    def set_temp_slider(self, test_case, context, soll_temp, element_ID, step):
        """
        Setup desired *'Temperatures/Heizkurve'* values via the slider available in Haus screens

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        context
            One of the following is used as *'context'* here (Raumregeler, Warmwasser, Heizkurve, Festwert) to find out middle value from the slider
        soll_temp
            Expected 'Soll' value from *profiles_info.json* file
        element_ID
            Required element number/ID for the particular scenario
        step
            step number from the test case document
        """
        slider = self.findElementsCSS(test_case,
                                      self._slider["vertical"], step)[element_ID]

        if context == self.RAUMREGLER:
            mid_temp = 22.5
        if context == self.WARMWASSER:
            mid_temp = 45.0
        if context == self.HEIZKURVE:
            mid_temp = 0.0
        if context == self.FESTWERT:
            mid_temp = 36.0

        offset_x = 0
        offset_y = 0

        while True:
            if soll_temp < mid_temp:
                if float(self.read_Soll_value(test_case, element_ID, step)) == float(soll_temp):
                    break
                self.action_slider(slider, offset_x, offset_y, step)
                if offset_y == 125:
                    break
                offset_y += 4
            else:
                if float(self.read_Soll_value(test_case, element_ID, step)) == float(soll_temp):
                    break
                self.action_slider(slider, offset_x, -offset_y, step)
                offset_y += 4
                if offset_y == 125:
                    break

    def click_down_triangle(self, test_case, element_ID, step):
        """
        Click on the *'Triangle'* icon (middle down) is presented in particular screen inside Haus settings screens

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        element_ID
            Required element number/ID for the particular scenario
        step
            step number from the test case document
        """
        time.sleep(2)
        return self.findElementsCSS(test_case, self._buttons["triangle"], step)[element_ID].click()

    def click_right_arrow(self, test_case, element_ID, step):
        """
        Click on the *'Right arrow'* is presented in particular screen inside Haus settings screens (like Wochenvorlauf, Warmwassersperren, ..)

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        element_ID
            Required element number/ID for the particular scenario
        step
            step number from the test case document
        """
        return self.findElementsCSS(test_case, self._buttons["right_arrow"], step)[element_ID].click()

    def verify_text_from_element_row(self, test_case, expected_text, screen_name, step):
        """
        Verifies if expected text from *de.common.json* are presented in paricular screen

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        expected_text
            Expected row text from *de.common.json* file   
        screen_name
            Expected 'screen name' from *profiles_info.json* file
        step
            step number from the test case document
        """
        for r in self.findElementsCSS(test_case, self._text["param_row"], step):
            if r.find_element_by_css_selector(self._text["name"]).text == expected_text:
                PASS(expected_text + " is presented in " +
                     screen_name + " at step = " + str(step))

    def select_Zeit1_Wochenvorlauf(self, test_case, day_row, step):
        """
        choose the `Zeit 1` for given day """
        Day = self.select_Days_Wochenvorlauf(test_case, day_row, step)
        try:
            Day.find_elements_by_class_name("interval")[0].click()
        except:
            day = Day.find_element_by_class_name("element-text-area").text
            self.compare_true_result(
                test_case, False, day + "--> Z1 failed to click in UHI", step)

    def select_Zeit2_Wochenvorlauf(self, test_case, day_row, step):
        """
        choose the `Zeit 2` for given day """
        Day = self.select_Days_Wochenvorlauf(test_case, day_row, step)
        try:
            Day.find_elements_by_class_name("interval")[2].click()
        except:
            day = Day.find_element_by_class_name("element-text-area").text
            self.compare_true_result(
                test_case, False, day + "--> Z2 failed to click in UHI", step)

    def select_Days_Wochenvorlauf(self, test_case, day_row, step):
        """ """
        return self.findElementsClass(test_case, "element-interval-setter", step)[day_row]

    def choose_days(self, test_case, func, step):
        """
        Select 'Wochenvorlauf' days based on *'profiles_info.json'* file

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        func
            Targeted function like *Warmwasser Sperren, Thermische Desinfektion, Aussentemp, ..* from 'profiles_info.json' file
        step
            step number from the test case document
        """
        time.sleep(2)
        if func["Mo"]["Z1"] == "yes":
            self.select_Zeit1_Wochenvorlauf(test_case, 0, step)
        if func["Mo"]["Z2"] == "yes":
            self.select_Zeit2_Wochenvorlauf(test_case, 0, step)
        if func["Di"]["Z1"] == "yes":
            self.select_Zeit1_Wochenvorlauf(test_case, 1, step)
        if func["Di"]["Z2"] == "yes":
            self.select_Zeit2_Wochenvorlauf(test_case, 1, step)
        if func["Mi"]["Z1"] == "yes":
            self.select_Zeit1_Wochenvorlauf(test_case, 2, step)
        if func["Mi"]["Z2"] == "yes":
            self.select_Zeit2_Wochenvorlauf(test_case, 2, step)
        if func["Do"]["Z1"] == "yes":
            self.select_Zeit1_Wochenvorlauf(test_case, 3, step)
        if func["Do"]["Z2"] == "yes":
            self.select_Zeit2_Wochenvorlauf(test_case, 3, step)
        if func["Fr"]["Z1"] == "yes":
            self.select_Zeit1_Wochenvorlauf(test_case, 4, step)
        if func["Fr"]["Z2"] == "yes":
            self.select_Zeit2_Wochenvorlauf(test_case, 4, step)
        if func["Sa"]["Z1"] == "yes":
            self.select_Zeit1_Wochenvorlauf(test_case, 5, step)
        if func["Sa"]["Z2"] == "yes":
            self.select_Zeit2_Wochenvorlauf(test_case, 5, step)
        if func["So"]["Z1"] == "yes":
            self.select_Zeit1_Wochenvorlauf(test_case, 6, step)
        if func["So"]["Z2"] == "yes":
            self.select_Zeit2_Wochenvorlauf(test_case, 6, step)

    def select_Wochenvorlauf_TherDes(self, test_case, day_row, step):
        """
        choose the `Zeit 1` for given day """
        Day = self.select_Days_Wochenvorlauf(test_case, day_row, step)
        try:
            Day.find_element_by_class_name("interval").click()
        except:
            day = Day.find_element_by_class_name("element-text-area").text
            self.compare_true_result(
                test_case, False, day + "--> Z1 failed to click in UHI", step)

    def choose_Thermische_days(self, test_case, func, step):
        """
        Select Thermische Desinfektion 'Wochenvorlauf' days based on *'profiles_info.json'* file

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        func
            Targeted function (Thermische Desinfektion) from 'profiles_info.json' file
        step
            step number from the test case document
        """
        time.sleep(2)
        if func["Mo"] == "yes":
            self.select_Wochenvorlauf_TherDes(test_case, 0, step)
        if func["Di"] == "yes":
            self.select_Wochenvorlauf_TherDes(test_case, 1, step)
        if func["Mi"] == "yes":
            self.select_Wochenvorlauf_TherDes(test_case, 2, step)
        if func["Do"] == "yes":
            self.select_Wochenvorlauf_TherDes(test_case, 3, step)
        if func["Fr"] == "yes":
            self.select_Wochenvorlauf_TherDes(test_case, 4, step)
        if func["Sa"] == "yes":
            self.select_Wochenvorlauf_TherDes(test_case, 5, step)
        if func["So"] == "yes":
            self.select_Wochenvorlauf_TherDes(test_case, 6, step)

    def days_int_to_str(self, integer):
        """
        Convert 'int' from *Modbus* registers to 'str' (Z1, Z2, J, N) for verification

        Parameters
        ----------
        integer
            Actual *read* value from Modbud registers 
        """
        if integer == 0:
            return "J"
        if integer == 1:
            return "N"
        if integer == 2:
            return "Z1"
        if integer == 3:
            return "Z2"

    def time_UHI(self, test_case, step):
        """
        This is a supporting function for getting *'time'* from UHI

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """

        _times = self.screen_info["times"]
        hours = self.findElementCSS(
            test_case, _times["hour"], step).find_elements_by_css_selector(_times["label"])
        mins = self.findElementCSS(
            test_case, _times["mins"], step).find_elements_by_css_selector(_times["label"])
        return hours, mins

    def zeit_start_1(self, test_case, step):
        """
        Returns profile times from UHI *'Wochenverlauf'* view in format *'ST_H1, ST_M1'*

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document

        Returns
        -------
        'int' tuple
            Start time 1 (H1, M1) of profiles 
        """
        hours, mins = self.time_UHI(test_case, step)
        s_H1 = ''.join(hours[0].text)
        s_M1 = ''.join(mins[0].text)
        return int(s_H1), int(s_M1)

    def zeit_end_1(self, test_case, step):
        """
        Returns profile times from UHI *'Wochenverlauf'* view in format *'END_H1, END_M1'*

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document

        Returns
        -------
        'int' tuple
            End time 1 (H1, M1) of profiles 
        """
        hours, mins = self.time_UHI(test_case, step)
        e_H1 = ''.join(hours[1].text)
        e_M1 = ''.join(mins[1].text)
        return int(e_H1), int(e_M1)

    def zeit_start_2(self, test_case,  step):
        """
        Returns profile times from UHI *'Wochenverlauf'* view in format *'ST_H2, ST_M2'*

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document

        Returns
        -------
        'int' tuple
            Start time 2 (H2, M2) of profiles 
        """
        hours, mins = self.time_UHI(test_case, step)
        s_H2 = ''.join(hours[2].text)
        s_M2 = ''.join(mins[2].text)
        return int(s_H2), int(s_M2)

    def zeit_end_2(self, test_case, step):
        """
        Returns profile times from UHI *'Wochenverlauf'* view in format *'END_H1, END_M1'*

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document

        Returns
        -------
        'int' tuple
            End time 2 (H2, M2) of profiles 
        """
        hours, mins = self.time_UHI(test_case, step)
        e_H2 = ''.join(hours[3].text)
        e_M2 = ''.join(mins[3].text)
        return int(e_H2), int(e_M2)

    def get_time_intervals_from_UHI(self, test_case, step):
        """
        Returns `UHI Time` intervals reading from Web frontend

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document

        Returns
        -------
        'int' tuple
            Retruns UHI times when there is `TWO` time intervals in the screen.
        """
        UHI_ST_H1, UHI_ST_M1 = self.zeit_start_1(test_case, step)
        UHI_END_H1, UHI_END_M1 = self.zeit_end_1(test_case, step)
        UHI_ST_H2, UHI_ST_M2 = self.zeit_start_2(test_case, step)
        UHI_END_H2, UHI_END_M2 = self.zeit_end_2(test_case, step)
        return UHI_ST_H1, UHI_ST_M1, UHI_END_H1, UHI_END_M1, UHI_ST_H2, UHI_ST_M2, UHI_END_H2, UHI_END_M2

    def verify_Refernz_temp_in_days_screen(self, test_case, context, expected, step):
        """
        Verify temp in 'Wochenverlauf' **screen all days**

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        expected
            Expected Referennz / Heizkurve (int) *'temperature'* to verify with UHI value
        step
            step number from the test case document
        """
        for tex in self.findElementsCSS(test_case, self._text["refer_temp"], step):
            tex = tex.text.strip(TEMP_UNITS)
            if context is self.RAUMREGLER:
                self.compare_results(test_case, expected, float(''.join(tex)
                                                                ), context + " Refernztemperatur values in days screen are wrong", step)
            else:
                self.compare_results(test_case, expected, int(
                    tex), context + " Refernztemperatur values in days screen are wrong", step)

    def verify_Profil_temp_in_days_screen(self, test_case, context, expected, step):
        """
        Verify temp inside 'Wochenverlauf' **screen boxes**

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        expected
            Expected Soll (int) *'temperature'* to verify with UHI value
        step
            step number from the test case document
        """

        for t in self.findElementsCSS(test_case, self._text["active"], step):
            tex = t.find_elements_by_css_selector(
                self._text["area"])[0].text.strip(TEMP_UNITS)
            if context is self.RAUMREGLER:
                self.compare_results(test_case, expected, float(''.join(tex)
                                                                ), context + " Profile temperature inside boxes in days screen are wrong", step)
            else:
                self.compare_results(test_case, expected, int(
                    tex), context + " Profile temperature inside boxes in days screen are wrong", step)

    def get_values_from_variable(self, func, day, Z1, Z2):
        """
        Returns *'Wochenverlauf'* integer value based on 'profiles_info.json' **except** Thermische Desinfektion

        Parameters
        ----------
        func
            Targeted function like *Warmwasser Sperren, Aussentemp, ..* from 'profiles_info.json' file
        day 
            Days in format of "Mo, Di, Mi ... So" for locating keys in 'json'
        Z1
            Time interval '1' from 'json'
        Z2 
            Time interval '2' from 'json'

        Returns
        -------
        'int' 
            0, 1, 2 or 3 based on 'yes' from 'json'
        """
        if func[day][Z1] == "yes" and func[day][Z2] == "yes":
            return 0, 0
        elif func[day][Z1] == "yes":
            return 2, 3
        elif func[day][Z2] == "yes":
            return 3, 2
        else:
            return 1, 1

    def get_Thermische_values_from_variable(self, func, day):
        """
        Returns *'Wochenverlauf'* integer value based on 'profiles_info.json' for *Thermische Desinfektion*

        Parameters
        ----------
        func
            Targeted function (Thermische Desinfektion) from 'profiles_info.json' file
        day 
            Days in format of "Mo, Di, Mi ... So" for locating keys in 'json'

        Returns
        -------
        'int' 
            0 or 1 based on 'yes' from 'json'
        """
        if func[day] == "yes":
            return 0
        else:
            return 1

    def verify_intervals_with_WPM(self, test_case, WPM_days, UHI_days, step):
        """
        Verifies *'Wochenverlauf'* UHI values with WPM values by comparing them. In somecases, the Z1 and Z2 changes its position in
        WPM, therfore 'Wochenverlauf' also interchanged between Z1 and Z2 (numerically, 2 or 3). To avoid this fail, this function 
        compares if any of days (Mo, Di, .. So) is selected by either Z1 or Z2 then it compares with both Z1 and Z2 to make it pass 
        in WPM.

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        WPM_days
            It is a collective *'int tuple'* information of 'Wochenvorlauf' from Modbus registers
        UHI_days
            It is a collective *'int tuple'* information of 'Wochenvorlauf' from UHI
        step
            step number from the test case document
        """
        Mo, Di, Mi, Do, Fr, Sa, So = WPM_days
        exp_1, exp_2 = self.get_values_from_variable(
            UHI_days, "Mo", "Z1", "Z2")
        self.compare_A_or_B_results(
            test_case, exp_1, exp_2, Mo, "Wochenverlauf mismatched for 'Montag'", step)
        exp_1, exp_2 = self.get_values_from_variable(
            UHI_days, "Di", "Z1", "Z2")
        self.compare_A_or_B_results(
            test_case, exp_1, exp_2, Di, "Wochenverlauf mismatched for 'Dienstag'", step)
        exp_1, exp_2 = self.get_values_from_variable(
            UHI_days, "Mi", "Z1", "Z2")
        self.compare_A_or_B_results(
            test_case, exp_1, exp_2, Mi, "Wochenverlauf mismatched for 'Mittwoch'", step)
        exp_1, exp_2 = self.get_values_from_variable(
            UHI_days, "Do", "Z1", "Z2")
        self.compare_A_or_B_results(
            test_case, exp_1, exp_2, Do, "Wochenverlauf mismatched for 'Donnerstag'", step)
        exp_1, exp_2 = self.get_values_from_variable(
            UHI_days, "Fr", "Z1", "Z2")
        self.compare_A_or_B_results(
            test_case, exp_1, exp_2, Fr, "Wochenverlauf mismatched for 'Freitag'", step)
        exp_1, exp_2 = self.get_values_from_variable(
            UHI_days, "Sa", "Z1", "Z2")
        self.compare_A_or_B_results(
            test_case, exp_1, exp_2, Sa, "Wochenverlauf mismatched for 'Samstag'", step)
        exp_1, exp_2 = self.get_values_from_variable(
            UHI_days, "So", "Z1", "Z2")
        self.compare_A_or_B_results(
            test_case, exp_1, exp_2, So, "Wochenverlauf mismatched for 'Sonntag'", step)

    def verify_Thermische_intervals_with_WPM(self, test_case, WPM_days, UHI_days, step):
        """
        Verifies 'Thermische Desinfektion' (only) UHI values against WPM values by comparing them.

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        WPM_days
            It is a collective *'int tuple'* information of 'Wochenvorlauf' from Modbus registers
        UHI_days
            It is a collective *'int tuple'* information of 'Wochenvorlauf' from UHI
        step
            step number from the test case document
        """
        Mo, Di, Mi, Do, Fr, Sa, So = WPM_days
        self.compare_results(test_case, Mo, self.get_Thermische_values_from_variable(
            UHI_days, "Mo"), "Thermische/Nacherwarmung Wochenverlauf mismatched for 'Montag'", step)
        self.compare_results(test_case, Di, self.get_Thermische_values_from_variable(
            UHI_days, "Di"), "Thermische/Nacherwarmung Wochenverlauf mismatched for 'Dienstag'", step)
        self.compare_results(test_case, Mi, self.get_Thermische_values_from_variable(
            UHI_days, "Mi"), "Thermische/Nacherwarmung Wochenverlauf mismatched for 'Mittwoch'", step)
        self.compare_results(test_case, Do, self.get_Thermische_values_from_variable(
            UHI_days, "Do"), "Thermische/Nacherwarmung Wochenverlauf mismatched for 'Donnerstag'", step)
        self.compare_results(test_case, Fr, self.get_Thermische_values_from_variable(
            UHI_days, "Fr"), "Thermische/Nacherwarmung Wochenverlauf mismatched for 'Freitag'", step)
        self.compare_results(test_case, Sa, self.get_Thermische_values_from_variable(
            UHI_days, "Sa"), "Thermische/Nacherwarmung Wochenverlauf mismatched for 'Samstag'", step)
        self.compare_results(test_case, So, self.get_Thermische_values_from_variable(
            UHI_days, "So"), "Thermische/Nacherwarmung Wochenverlauf mismatched for 'Sonntag'", step)

    def click_Thermische_box(self, test_case, index, step):
        """
        Click on the specified *'Wochenvorlauf'* box presented in 'Thermische Desinfektion' profile screen

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        index
            Frontend 'element' index for particular boxes
        step
            step number from the test case document
        """
        time.sleep(1)
        try:
            element = "//div[@data-test-index='" + str(index) + "']"
            return self.findElementXpath(test_case, element, step).click()
            # return self.findElementsId(test_case, , step).click()
        except:
            self.failed_choose_Thermische_days(test_case, int(index), step)

    def failed_choose_Thermische_days(self, test_case, index, step):
        """
        Logs the failed to choose 'Wochenvorlauf' for *'Thermische Desinfektion'* in *Failed.log*

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        index
            Frontend 'element' index for particular boxes
        step
            step number from the test case document
        """
        if index == 0:
            self.compare_true_result(
                test_case, False, "'Mo' failed to click in UHI", step)
        if index == 3:
            self.compare_true_result(
                test_case, False, "'Di' failed to click in UHI", step)
        if index == 2:
            self.compare_true_result(
                test_case, False, "'Mi' failed to click in UHI", step)
        if index == 1:
            self.compare_true_result(
                test_case, False, "'Do' failed to click in UHI", step)
        if index == 4:
            self.compare_true_result(
                test_case, False, "'Fr' failed to click in UHI", step)
        if index == 5:
            self.compare_true_result(
                test_case, False, "'Sa' failed to click in UHI", step)
        if index == 6:
            self.compare_true_result(
                test_case, False, "'So' failed to click in UHI", step)

    def get_screen_name(self, test_case, element_ID, step):
        """
        Returns 'screen name' of any particular screen from *'profiles_info.json'* file

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        element_ID
            Required element number/ID for the particular scenario
        step
            step number from the test case document
        """
        element_ID = element_ID * 2
        return self.findElementsCSS(test_case, self._text["name"], step)[0].text

    def get_Raumregulung_screen_count(self, test_case, step):
        """
        Returns *'Raumregeler'* screen count based on number of "Humidity" values available from UHI

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        return int(len(self.findElementsCSS(test_case, self._setting["icon_humidity"], step)))

    def select_RTC_Schnellheizen(self, test_case, screen_name, element_ID, schnellHeiz_time, step):
        """
        Setup *'Raumregeler Schnellheizen'* in the UHI screens based on 'profiles_info.json' file and
        returns the *'Schnellheizen'* time

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        screen_name
            Expected 'screen name' from *profiles_info.json* file
        element_ID
            Required element number/ID for the particular scenario
        schnellHeiz_time
            Read from *profiles_info.json* file to setup in UHI
        step
            step number from the test case document
        """
        slider = self.findElementsCSS(test_case,
                                      self._slider["range_slider"], step)[element_ID]
        length = slider.size["width"]
        offset_x = -(length+200)
        offset_y = 0

        self.action_slider(slider, offset_x, offset_y, step)

        if schnellHeiz_time == 20:
            offset_x = 0
            self.action_slider(slider, offset_x, offset_y, step)
            self.verify_toggle_is_ON(test_case, screen_name, element_ID, step)
            return schnellHeiz_time
        if schnellHeiz_time == 40:
            offset_x = 10
            self.action_slider(slider, offset_x, offset_y, step)
            self.verify_toggle_is_ON(test_case, screen_name, element_ID, step)
            return schnellHeiz_time
        if schnellHeiz_time == 60:
            offset_x = length + 200
            self.action_slider(slider, offset_x, offset_y, step)
            self.verify_toggle_is_ON(test_case, screen_name, element_ID, step)
            return schnellHeiz_time

    def verify_title_of_screen(self, test_case, screen_name, element_ID, step):
        """
        Verifies *'Screen name'* of any particular screen in UHI with expected result

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        screen_name
            Expected 'screen name' from *profiles_info.json* file
        element_ID
            Required element number/ID for the particular scenario
        step
            step number from the test case document
        """
        actual_screen_name = self.findElementsCSS(test_case, self._text["title_bar"], step)[
            element_ID].find_element_by_css_selector(self._text["title"]).text
        if not actual_screen_name == screen_name:
            self.compare_true_result(
                test_case, False, screen_name + ": screen title is wrong", step)

    def verify_toggle_is_ON(self, test_case, screen_name, element_ID, step):
        """
        Verifies if a particular *'toggle button'* is ON on a specific screen in UHI

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        screen_name
            Expected 'screen name' from *profiles_info.json* file
        element_ID
            Required element number/ID for the particular scenario
        step
            step number from the test case document
        """
        button = self.findElementsCSS(test_case, self._text["toggle_row"], step)[element_ID].find_element_by_css_selector(
            self.buttons["toggle_button"])
        if int(button.value_of_css_property(self.css_prop["margin_left"])[0]) == 0:
            self.compare_true_result(
                test_case, False, screen_name + " toggle failed to switch ON", step)

    def get_Heizkreis_Ist_temp_from_WPM(self, HK):
        """
        Returns **Heizkurve/Festwert** 'IST' temp from WPM

        Parameters
        ----------
        HK 
            The *'Heizkreis number'* in which the 'Ist' temperature wanted

        Returns
        -------
        'float'
            Ist temperature
        """
        regs_index = "E_HK" + str(HK) + "_Temp"
        return self.tcp.get_values_from_analog_registers(self.regs_analog[regs_index], 10)

    def get_Heizkreis_Soll_temp_from_WPM(self, HK):
        """
        Returns **Heizkurve/Festwert** 'SOLL' temp from WPM

        Parameters
        ----------
        HK 
            The *'Heizkreis number'* in which the 'Soll' temperature wanted

        Returns
        -------
        'int'
            Soll temperature
        """
        reg_index = "HK" + str(HK) + "_Soll_Temp"
        Soll_temp = int(self.tcp.get_values_from_integer_registers(
            self.regs_integer[reg_index]) / 10)
        return Soll_temp

    def get_Festwert_Ref_temp_from_WPM(self, HK):
        """
        Returns **Festwert** 'Referenztemperatur' temp from WPM

        Parameters
        ----------
        HK 
            The *'Heizkreis number'* in which the 'Referenztemperatur' wanted

        Returns
        -------
        'int'
            Referenztemperatur
        """
        reg_index = "P_HK" + str(HK) + "_FWR_SOLL"
        ref_temp = self.tcp.get_values_from_integer_registers(
            self.regs_integer[reg_index])
        return ref_temp

    def get_Heizkurve_value_from_WPM(self, HK):
        """
        Convert and returns **Heizkurve** 'values' from WPM. 

        Parameters
        ----------
        HK 
            The *'Heizkreis number'* in which the 'Heizkurve' value wanted

        Returns
        -------
        'int'
            Heizkurve value
        """
        reg_index = "P_HK" + str(HK) + "_WK"
        HK_value = self.tcp.get_values_from_integer_registers(
            self.regs_integer[reg_index])
        return self.Wpm_info["Heizkurve_index"][str(HK_value)]

    def get_Heizkreis_mode_from_WPM(self, HK):
        """
        Returns *'Absenkung/Anhebung'* mode and Wert from WPM Modbus Registers

        Parameters
        ----------
        HK 
            The *'Heizkreis number'* in which the 'Heating' mode wanted

        Returns
        -------
        'string', 'int' tuple
            Heating mode, value from WPM

        """
        reg_index = "P_HK" + str(HK) + "_ABS_DIFF"
        Abs_wert = self.tcp.get_values_from_integer_registers(
            self.regs_integer[reg_index])
        reg_index = "P_HK" + str(HK) + "_ANH_DIFF"
        Anh_wert = self.tcp.get_values_from_integer_registers(
            self.regs_integer[reg_index])

        if Abs_wert != 0:
            return self.ABSENKUNG, -Abs_wert
        elif Anh_wert != 0 or Anh_wert == 0:
            return self.ANHEBUNG, Anh_wert

    def get_Heizkreis_profil_times_from_WPM(self, HK, mode):
        """
        Returns 'Heizkurve / Festwert' profile times for 'Annebung / Absenkung' mode from WPM in format 'ST_H1, ST_M1, END_H1, END_M1, ST_H2, ST_M2, END_H2, END_M2'

        Parameters
        ----------
        HK 
            The *'Heizkreis number'* in which the 'Profile' time from WPM wanted
        mode
            Heating mode (Absenkung/Anhebung) as input

        Returns
        -------
        'int' tuple
            **WPM** time in format of *ST_H1, ST_M1, END_H1, END_M1, ST_H2, ST_M2, END_H2, END_M2*
        """

        if mode == self.ABSENKUNG:
            reg_index = "P_HK" + str(HK) + "_ABS_ST_H1"
            ST_H1 = self.tcp.get_values_from_integer_registers(
                self.regs_integer[reg_index])
            reg_index = "P_HK" + str(HK) + "_ABS_ST_M1"
            ST_M1 = self.tcp.get_values_from_integer_registers(
                self.regs_integer[reg_index])
            reg_index = "P_HK" + str(HK) + "_ABS_END_H1"
            END_H1 = self.tcp.get_values_from_integer_registers(
                self.regs_integer[reg_index])
            reg_index = "P_HK" + str(HK) + "_ABS_END_M1"
            END_M1 = self.tcp.get_values_from_integer_registers(
                self.regs_integer[reg_index])
            reg_index = "P_HK" + str(HK) + "_ABS_ST_H2"
            ST_H2 = self.tcp.get_values_from_integer_registers(
                self.regs_integer[reg_index])
            reg_index = "P_HK" + str(HK) + "_ABS_ST_M2"
            ST_M2 = self.tcp.get_values_from_integer_registers(
                self.regs_integer[reg_index])
            reg_index = "P_HK" + str(HK) + "_ABS_END_H2"
            END_H2 = self.tcp.get_values_from_integer_registers(
                self.regs_integer[reg_index])
            reg_index = "P_HK" + str(HK) + "_ABS_END_M2"
            END_M2 = self.tcp.get_values_from_integer_registers(
                self.regs_integer[reg_index])
            return ST_H1, ST_M1, END_H1, END_M1, ST_H2, ST_M2, END_H2, END_M2

        if mode == self.ANHEBUNG:
            reg_index = "P_HK" + str(HK) + "_ANH_ST_H1"
            ST_H1 = self.tcp.get_values_from_integer_registers(
                self.regs_integer[reg_index])
            reg_index = "P_HK" + str(HK) + "_ANH_ST_M1"
            ST_M1 = self.tcp.get_values_from_integer_registers(
                self.regs_integer[reg_index])
            reg_index = "P_HK" + str(HK) + "_ANH_END_H1"
            END_H1 = self.tcp.get_values_from_integer_registers(
                self.regs_integer[reg_index])
            reg_index = "P_HK" + str(HK) + "_ANH_END_M1"
            END_M1 = self.tcp.get_values_from_integer_registers(
                self.regs_integer[reg_index])
            reg_index = "P_HK" + str(HK) + "_ANH_ST_H2"
            ST_H2 = self.tcp.get_values_from_integer_registers(
                self.regs_integer[reg_index])
            reg_index = "P_HK" + str(HK) + "_ANH_ST_M2"
            ST_M2 = self.tcp.get_values_from_integer_registers(
                self.regs_integer[reg_index])
            reg_index = "P_HK" + str(HK) + "_ANH_END_H2"
            END_H2 = self.tcp.get_values_from_integer_registers(
                self.regs_integer[reg_index])
            reg_index = "P_HK" + str(HK) + "_ANH_END_M2"
            END_M2 = self.tcp.get_values_from_integer_registers(
                self.regs_integer[reg_index])
            return ST_H1, ST_M1, END_H1, END_M1, ST_H2, ST_M2, END_H2, END_M2

    def verify_profile_times(self, test_case, mode, WPM_times, UHI_times, step):
        """
        Verify profile actual times against expected times with WPM. This function is also verifies the Profile times if the WPM times are swapped 
        with UHI times.

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        mode
            Heating mode (Absenkung/Anhebung) as input
        WPM_time
            It is a collective *'int tuple'* information of profile 'times' from Modbus registers
        UHI_days
            It is a collective *'int tuple'* information of profile 'times' from UHI
        step
            step number from the test case document

        """
        WPM_ST_H1, WPM_ST_M1, WPM_END_H1, WPM_END_M1, WPM_ST_H2, WPM_ST_M2, WPM_END_H2, WPM_END_M2 = WPM_times
        UHI_ST_H1, UHI_ST_M1, UHI_END_H1, UHI_END_M1, UHI_ST_H2, UHI_ST_M2, UHI_END_H2, UHI_END_M2 = UHI_times

        if WPM_ST_H1 == UHI_ST_H1:
            self.compare_results(
                test_case, WPM_ST_H1, UHI_ST_H1, mode + "--> Zeit 1, Start Hour mismatch", step)
            self.compare_results(
                test_case, WPM_ST_M1, UHI_ST_M1, mode + "--> Zeit 1, Start Mins mismatch", step)
            self.compare_results(
                test_case, WPM_END_H1, UHI_END_H1, mode + "--> Zeit 1, End Hour mismatch", step)
            self.compare_results(
                test_case, WPM_END_M1, UHI_END_M1, mode + "--> Zeit 1, End Mins mismatch", step)
            self.compare_results(
                test_case, WPM_ST_H2, UHI_ST_H2, mode + "--> Zeit 2, Start Hour mismatch", step)
            self.compare_results(
                test_case, WPM_ST_M2, UHI_ST_M2, mode + "--> Zeit 2, Start Mins mismatch", step)
            self.compare_results(
                test_case, WPM_END_H2, UHI_END_H2, mode + "--> Zeit 2, End Hour mismatch", step)
            self.compare_results(
                test_case, WPM_END_M2, UHI_END_M2, mode + "--> Zeit 2, End Mins mismatch", step)

        if WPM_ST_H1 == UHI_ST_H2:
            self.compare_results(
                test_case, WPM_ST_H1, UHI_ST_H2, mode + "--> Zeit 2, Start Hour mismatch", step)
            self.compare_results(
                test_case, WPM_ST_M1, UHI_ST_M2, mode + "--> Zeit 2, Start Mins mismatch", step)
            self.compare_results(
                test_case, WPM_END_H1, UHI_END_H2, mode + "--> Zeit 2, End Hour mismatch", step)
            self.compare_results(
                test_case, WPM_END_M1, UHI_END_M2, mode + "--> Zeit 2, End Mins mismatch", step)
            self.compare_results(
                test_case, WPM_ST_H2, UHI_ST_H1, mode + "--> Zeit 1, Start Hour mismatch", step)
            self.compare_results(
                test_case, WPM_ST_M2, UHI_ST_M1, mode + "--> Zeit 1, Start Mins mismatch", step)
            self.compare_results(
                test_case, WPM_END_H2, UHI_END_H1, mode + "--> Zeit 1, End Hour mismatch", step)
            self.compare_results(
                test_case, WPM_END_M2, UHI_END_M1, mode + "--> Zeit 1, End Mins mismatch", step)

    def verify_Heizkreis_profile_times_with_WPM(self, test_case, HK, mode, step):
        """
        Verify 'Heizkreis/Festwert' profile actual times against expected times

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        HK 
            The *'Heizkreis number'* in which the 'Profile' time from WPM wanted
        mode
            Heating mode (Absenkung/Anhebung) as input
        step
            step number from the test case document

        """
        time.sleep(2)
        UHI_times = self.get_time_intervals_from_UHI(test_case, step)
        WPM_times = self.get_Heizkreis_profil_times_from_WPM(HK, mode)
        self.verify_profile_times(test_case, mode, WPM_times, UHI_times, step)

    def get_Heizkreis_run_days_from_WPM(self, HK, mode):
        """
        Returns 'Heizkreis/Festwert' *Wochenvorlauf* from WPM in 'int' tuple

        Parameters
        ----------
        HK 
            The *'Heizkreis number'* in which the 'Wochenvorlauf' from WPM wanted
        mode
            Heating mode (Absenkung/Anhebung) as input

        Returns
        -------
        'int' tuple
            Heizkreis *'Wochenvorlauf'* from WPM
        """
        if mode == self.ANHEBUNG:
            mode = "ANH"
        if mode == self.ABSENKUNG:
            mode = "ABS"
        reg_index = "P_HK" + str(HK) + "_" + mode + "_MO"
        Mo = self.tcp.get_values_from_integer_registers(
            self.regs_integer[reg_index])
        reg_index = "P_HK" + str(HK) + "_" + mode + "_DI"
        Di = self.tcp.get_values_from_integer_registers(
            self.regs_integer[reg_index])
        reg_index = "P_HK" + str(HK) + "_" + mode + "_MI"
        Mi = self.tcp.get_values_from_integer_registers(
            self.regs_integer[reg_index])
        reg_index = "P_HK" + str(HK) + "_" + mode + "_DO"
        Do = self.tcp.get_values_from_integer_registers(
            self.regs_integer[reg_index])
        reg_index = "P_HK" + str(HK) + "_" + mode + "_FR"
        Fr = self.tcp.get_values_from_integer_registers(
            self.regs_integer[reg_index])
        reg_index = "P_HK" + str(HK) + "_" + mode + "_SA"
        Sa = self.tcp.get_values_from_integer_registers(
            self.regs_integer[reg_index])
        reg_index = "P_HK" + str(HK) + "_" + mode + "_SO"
        So = self.tcp.get_values_from_integer_registers(
            self.regs_integer[reg_index])
        return Mo, Di, Mi, Do, Fr, Sa, So

    def Edit_Heizkreise_name(self, test_case, HK, step):
        """
        Rename 'Heizkreis' names inside profile from *profiles_info.json* file

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        HK 
            The *'Heizkreis number'* in which the 'Heizkreis' name from 'profiles_info.json' wanted
        step
            step number from the test case document
        """
        HK_name = self.profile_info["AussenTemp_info"]["HK" +
                                                       str(HK)]["HK_name"]
        time.sleep(1)
        self.click_Anpassen_text(test_case, step)
        time.sleep(1)
        self.click_Edit_icon(test_case, HK-1, step)
        time.sleep(1)
        self.Edit_text_box(test_case, "HK"+str(HK), HK_name, step)
        time.sleep(1)
        self.click_scrollable_area(test_case, step)

    def Edit_Raumregeler_name(self, test_case, HK, Room, step):
        """
        Rename 'Raumregeler' names inside profile from *profiles_info.json* file

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        HK 
            The *'Heizkreis number'* in which the 'Heizkreis' name from 'profiles_info.json' wanted
        step
            step number from the test case document
        """
        Rooms = 0
        if self._config.P_HK1_REG() == 2:
            Rooms = self._config.P_HK1_RT_thT_Anz()
        if HK == 2:
            HK = Rooms + 1
            Rooms = self._config.P_HK2_RT_thT_Anz()

        self.click_Anpassen_text(test_case, step)
        time.sleep(1)

        for R in range(Rooms):
            Room_name = self.profile_info["Raumregulung_info"]["RAUM_" +
                                                               str(Room)]["Room_name"]
            # self.click_Edit_text(test_case, step)
            time.sleep(1)
            self.click_Edit_icon(test_case, HK-1, step)
            time.sleep(1)
            self.Edit_text_box(test_case, "RAUM_"+str(Room), Room_name, step)
            HK += 1
            Room += 1
        
        self.click_Anpassen_text(test_case, step)
        time.sleep(1)
        self.click_scrollable_area(test_case, step)

    def rename_HK_and_RTC(self, test_case, step):
        """
        Renaming *'Heizkreis'* and *'Raumregeler'* names inside 'Individulle Anpassungen'

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        if self._config.P_HK1_REG() < 2 or self._config.P_HK2_REG() < 2:
            self.click_right_arrow_by_name(
                test_case, self._content["HEATING_CIRCUITS"], step)
            if self._config.P_HK_1() > 0 and self._config.P_HK1_REG() < 2:
                HK = 1
                self.Edit_Heizkreise_name(test_case, HK, step)

            if self._config.P_HK_2() > 0 and self._config.P_HK2_REG() < 2:
                HK = 2
                self.Edit_Heizkreise_name(test_case, HK, step)
            self.click_left_arrow(test_case, 0, step)

        if self._config.P_HK_1() > 0 and self._config.P_HK1_REG() == 2:
            self.click_right_arrow_by_name(
                test_case, self._content["ROOMS"], step)
            if self._config.P_HK1_REG() == 2:
                HK, Room = 1, 50
                self.Edit_Raumregeler_name(test_case, HK, Room, step)
            if self._config.P_HK_2() > 0 and self._config.P_HK2_REG() == 2:
                HK, Room = 2, 60
                self.Edit_Raumregeler_name(test_case, HK, Room, step)
            self.click_left_arrow(test_case, 0, step)

        if self._config.P_HK_2() > 0 and self._config.P_HK1_REG() < 2 and self._config.P_HK2_REG() == 2:
            self.click_right_arrow_by_name(
                test_case, self._content["ROOMS"], step)
            HK, Room = 2, 60
            self.Edit_Raumregeler_name(test_case, HK, Room, step)
            self.click_left_arrow(test_case, 0, step)

    def check_if_profile_exists_with_same_name(self, test_case, profile_name, step):
        """
        Checks the new *'profile'* name is already existed from the list and returns True and 
        element ID for the same `profile_name`.

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        profile_name
            Targeted name of the profile
        step
            step number from the test case document

        Returns
        -------
        'Boolean', 'int' tuple
            Returns True, element ID of same profile name
        """
        ele = 0
        for tex in self.findElementsCSS(test_case, self.texT["area_name"], step):
            if tex.text == profile_name:
                return True, ele
                break
            ele += 1
        return False, ele

    def get_total_profiles_count(self, test_case, step):
        """
        Returns available *'Profiles'* count from the list view

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document

        Returns
        -------
        'int'
            Profiles count
        """
        return int(len(self.findElementsCSS(test_case, self.texT["area_name"], step)))

    def check_if_profile_only_one_with_same_name(self, test_case, same_name_status, step):
        """
        Check if *there are only one profile* is avaiable with same name as "profile_info.json" file and if yes,
        this will create a *'Dummy'* profile for testing purpose.

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        if self.get_total_profiles_count(test_case, step) == 1 and same_name_status:
            self.create_profile(
                test_case, self.profile_info["dummy_profile"], step)

    def create_profile(self, test_case, profile_name, step):
        """
        Create a *'Profile'* in the profiles page with given 'profile_name'

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        profile_name
            Targeted name of the new profile
        step
            step number from the test case document
        """
        from selenium.webdriver.common.keys import Keys
        time.sleep(1)
        self.get_click_from_text_area(
            test_case, self._content["ADD_PROFILE"], step)
        time.sleep(1)
        prof_text = self.findElementId(
            test_case, self.buttons["add_profile_ID"], step)
        prof_text.click()
        prof_text.send_keys(profile_name)
        prof_text.send_keys(Keys.RETURN)
        self.waitByCss(test_case, self.buttons["btn"], 60, step)
        # verify success message
        expected_text = self._content["PROFILE_SUCCESS"]
        actual_text = self.get_text_from_element(
            test_case, self.texT["value"], step)
        self.compare_results(test_case, expected_text, actual_text,
                             "Success message on profile creation is wrong", step)
        self.findElementCSS(test_case, self.buttons["btn"], step).click()
        self.wait_browser(test_case, 1, step)

    def steps_to_create_profile(self, test_case, profile_name, step):
        """
        Steps to create a *'Profile'* in the profiles page based on 'profiles_info.json' file by considering
        same profile names, last profile with same profile names and so on.

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        same_name_status, ele = self.check_if_profile_exists_with_same_name(
            test_case, profile_name, step)

        self.check_if_profile_only_one_with_same_name(
            test_case, same_name_status, step)

        # check if profile name is same and more than 1 available
        if same_name_status and self.get_total_profiles_count(test_case, step) != 1:
            time.sleep(1)
            # self.click_Edit_text(test_case, step)
            self.click_Anpassen_text(test_case, step)
            time.sleep(1)
            self.click_Delete_icon(test_case, ele, step)
            self.click_X_icon(test_case, 0, step)
            time.sleep(60)
        time.sleep(1)

        self.create_profile(test_case, profile_name, step)

    def select_created_profile(self, test_case, profile_name, step):
        """
        Choose the created `profile` name passed to this function call

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        profile_name
            Targeted profile name from 'profiles_info.json' file
        step
            step number from the test case document
        """
        ele = 0
        for tex in self.findElementsCSS(test_case, self.texT["device_row"], step):
            if tex.find_element_by_css_selector("div.text").text == profile_name:
                tex.find_element_by_css_selector("div.text").click()
                self.wait_browser(test_case, 6, step)
                self.findElementsCSS(test_case, self.buttons["right_arrow"], step)[
                    ele].click()
                time.sleep(2)
                break
            ele += 1

    def read_temp_from_horizontal_slider(self, test_case, step):
        """
        Returns `Temperature` over the "Horizontal" slider inside the profiles

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document

        Returns
        -------
        'float'
            Set temperature from horizontal slider
        """
        temp = self.findElementCSS(test_case, self.texT["value"], step).text
        return float(temp.strip(TEMP_UNITS))

    def set_temp_slider_inside_profile(self, test_case, context, element_ID, soll_temp, step):
        """
        Setup desired *'Temperatures/Heizkurve'* values via the slider available inside **Individuelle Anpassungen** screen

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        context
            One of the following is used as *'context'* here (Raumregeler, Warmwasser, Heizkurve, Festwert) to find out middle value from the slider
        soll_temp
            Expected 'Soll' value from *profiles_info.json* file
        element_ID
            Required element number/ID for the particular scenario
        step
            step number from the test case document
        """
        slider = self.findElementsCSS(test_case,
                                      self._slider["horizontal"], step)[element_ID]

        if context == self.THERMISCHE_DESINFEKTION:
            if self._config.Fun_Code_Biv():
                mid_temp = 77.0
            else:
                mid_temp = 40.0

        offset_x = 0
        offset_y = 0

        while True:
            if soll_temp < mid_temp:
                if self.read_temp_from_horizontal_slider(test_case, step) == float(soll_temp):
                    break
                self.action_slider(slider, -offset_x, offset_y, step)
                if offset_y == 125:
                    break
                offset_x += 4

            else:
                if self.read_temp_from_horizontal_slider(test_case, step) == float(soll_temp):
                    break
                self.action_slider(slider, offset_x, offset_y, step)
                if offset_y == 125:
                    break
                offset_x += 4

    def get_temp_from_bars(self, test_case, element_ID, step):
        """
        Returns `temperature` from UHI besides the slide **bars** class

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        element_ID
            Desired element ID for reading `temperature` from UHI (Min. WW temp, Ww Soll temp)
        step
            step number from the test case document
        """
        temp = self.findElementsCSS(test_case, "div.temperature", step)[
            element_ID].text
        return float(temp.strip(TEMP_UNITS))

    def set_temp_via_bars(self, test_case, context, soll_temp, element_ID, step):
        """
        Setup desired `temperature` in bars slider inside profile screens like Ww Sperre, Raumregeler, HK.

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        context
            One of the following is used as *'context'* here (Warmwasser Sperre, Warmwasser, Heizkurve, Festwert) to find out middle value from the slider
        soll_temp
            Expected 'Soll' value from *profiles_info.json* file
        element_ID
            Desired element ID for reading `temperature` from UHI (Min. WW temp, Ww Soll temp)
        step
            step number from the test case document
        """
        time.sleep(1)
        bar = self.findElementCSS(test_case, "div.bars", step)
        offset_x = 0
        offset_y = 0

        if context == self.WARMWASSER_SPERRE:
            mid_temp = 35.0
        if context == self.RAUMREGLER:
            mid_temp = 22.5
        if context == self.HEIZKURVE:
            mid_temp = 0.0
        if context == self.FESTWERT:
            mid_temp = 37.0

        while True:
            if soll_temp < mid_temp:
                if self.get_temp_from_bars(test_case, element_ID, step) == float(soll_temp):
                    break
                self.action_slider(bar, -offset_x, offset_y, step)
                if offset_y == 125:
                    break
                offset_x += 4
            else:
                if self.get_temp_from_bars(test_case, element_ID, step) == float(soll_temp):
                    break
                self.action_slider(bar, offset_x, offset_y, step)
                if offset_y == 125:
                    break
                offset_x += 4

    def change_time_bars_inside_profile(self, test_case, step):
        """
        Changes the time randomly of `Profiles` inside "Individuelle Anpassungen" screen

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        time.sleep(1)
        start = self.findElementsCSS(
            test_case, "div#start.interval__handle.handle-left", step)
        end = self.findElementsCSS(
            test_case, "div#end.interval__handle.handle-right", step)
        start_0 = start[0].find_element_by_css_selector("div.interval__handle__inner")
        end_0 = end[0].find_element_by_css_selector("div.interval__handle__inner")
        start_1 = start[1].find_element_by_css_selector("div.interval__handle__inner")
        end_1 = end[1].find_element_by_css_selector("div.interval__handle__inner")

        self.action_slider(start_0, self.get_rand_int(-100, -20), 0, step)
        self.action_slider(end_0, self.get_rand_int(20, 100), 0, step)
        self.action_slider(start_1, self.get_rand_int(-100, -20), 0, step)
        self.action_slider(end_1, self.get_rand_int(20, 100), 0, step)

    def change_temp_slider_values(self, test_case, context, up_temp, down_temp, step):
        """
        Changes the `upper` (Min Ww temp, Anhebung/Absenkung) and `lower` (Ww Soll temp, Heizkurve, Referenztemperatur) temperature values based on 'profiles_info.json' files

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        context
            One of the following is used as *'context'* here (Warmwasser Sperre, Warmwasser, Heizkurve, Festwert) to find out middle value from the slider
        up_temp 
            Desired `upper` (Min Ww temp, Anhebung/Absenkung) values 
        down_temp 
            Desired `lower` (Ww Soll temp, Heizkurve, Referenztemperatur) values 
        step
            step number from the test case document
        """
        # change lower temp and upper temp of any temperature slider inside profiles screen
        time.sleep(1)
        up_slider = self.findElementCSS(test_case, "div.label2", step)
        down_slider = self.findElementCSS(test_case, "div.label1", step)

        down_slider.click()
        time.sleep(1)
        self.set_temp_via_bars(
            test_case, context, down_temp, -1, step)

        up_slider.click()
        time.sleep(1)
        self.set_temp_via_bars(
            test_case, context, up_temp, -2, step)

    def setup_Ww_profile(self, test_case, step):
        """
        Setup the desired `Warmwasser` profile values and verify them against WPM values by comparing them.

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        from Profile_functions.warmwasser_setting import WarmwasserSetting
        Ww = WarmwasserSetting()

        # Open 'Thermische Desinfektion" under Ww profile
        self.click_dropdown_by_title1(
            test_case, self._content["WARMWATER"], step)

        if self._config.Fun_Code_Biv():
            self.click_right_arrow_by_name(
                test_case, self._content["THERMAL_DESINFECTION"], step)

        if not self._config.Fun_Code_Biv():
            self.click_right_arrow_by_name(
                test_case, self._content["REHEATING"], step)

        self.click_Edit_without_ripple_text(test_case, step)


        ### Thermische Desinfektionen ####
        action = self.action
        bar = self.findElementId(test_case, "move", step)
        action.click_and_hold(bar).move_to_element_with_offset(
            bar, 50, 0).release().perform()
        time.sleep(2)
        # change temp of TD via slider
        if self._config.Fun_Code_Biv():
            TD_Soll = Ww.TD_info["Soll_temp_Biv"]
        else:
            TD_Soll = Ww.TD_info["Soll_temp"]
        self.set_temp_slider_inside_profile(
            test_case, self.THERMISCHE_DESINFEKTION, 0, TD_Soll, step)
        self.click_Fertig_without_ripple_text(test_case, step)
        # choose 'Wochenvorlauf' of TD
        Ww.choose_WwTherDes_days(test_case, step)
        self.click_left_arrow(test_case, 0, step)

        #### Warmwasser Sperre ####
        self.click_dropdown_by_title1(
            test_case, self._content["WARMWATER"], step)
        self.click_right_arrow_by_name(
            test_case, self._content["THERMAL_LOCK"], step)
        self.click_Edit_without_ripple_text(test_case, step)
        # change time of Ww Sperre
        self.change_time_bars_inside_profile(test_case, step)
        # change Min.Ww temp and Ww Soll temp
        self.change_temp_slider_values(
            test_case, self.WARMWASSER_SPERRE, Ww.Ww_sperre["Min_WW_temp"], Ww.prof_info["Soll_temp"], step)
        self.click_Fertig_without_ripple_text(test_case, step)
        # choose 'Wochenvorlauf' of Ww Sperre
        Ww.choose_WwSperre_days(test_case, step)
        self.click_left_arrow(test_case, 0, step)

        #### Ww Zirkulation ####
        if self._config.get_Ww_Zirkulation_status() == 2:
            self.click_dropdown_by_title1(
                test_case, self._content["WARMWATER"], step)
            self.click_right_arrow_by_name(
                test_case, "Zir­ku­la­ti­on", step)
            self.click_Edit_without_ripple_text(test_case, step)
            # change time
            self.change_time_bars_inside_profile(test_case, step)
            self.click_Fertig_without_ripple_text(test_case, step)
            Ww.choose_Zirkulation_days(test_case, step)
            self.click_left_arrow(test_case, 0, step)

    def verify_Ww_profile(self, test_case, step):
        """
        Verfiy the setup `Warmwasser` profile values against WPM values by comparing them.

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        from Profile_functions.warmwasser_setting import WarmwasserSetting
        Ww = WarmwasserSetting()

        # Verify 'Thermische Desinfektion" under Ww profile
        self.click_dropdown_by_title1(
            test_case, self._content["WARMWATER"], step)
        if self._config.Fun_Code_Biv():
            self.click_right_arrow_by_name(
                test_case, self._content["THERMAL_DESINFECTION"], step)
        if not self._config.Fun_Code_Biv():
            self.click_right_arrow_by_name(
                test_case, self._content["REHEATING"], step)
        Ww.verify_WwTherDes_times(test_case, step)
        Ww.verify_WwTherDes_temps_in_days_screen(test_case, step)
        Ww.verify_WwTherDes_days(test_case, step)
        self.click_left_arrow(test_case, 0, step)

        # Verify 'Warmwasser Sperre" under Ww profile
        self.click_dropdown_by_title1(
            test_case, self._content["WARMWATER"], step)
        self.click_right_arrow_by_name(
            test_case, self._content["THERMAL_LOCK"], step)
        Ww.verify_WwSperre_times(test_case, step)
        Ww.verify_WwSperre_temps_in_days_screen(test_case, step)
        Ww.verify_WwSperre_days(test_case, step)
        self.click_left_arrow(test_case, 0, step)

        # Verify 'Zirkulation' under Ww profile
        if self._config.get_Ww_Zirkulation_status() == 2:
            self.click_dropdown_by_title1(
                test_case, self._content["WARMWATER"], step)
            self.click_right_arrow_by_name(
                test_case, "Zir­ku­la­ti­on", step)
            Ww.verify_Zirkulation_with_WPM(test_case, step)
            self.click_left_arrow(test_case, 0, step)

    def setup_Raumregeler_profile(self, test_case, step):
        """
        Setup the desired `Raumregeler` profile values and verify them against WPM values by comparing them.

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        from Profile_functions.raumregler_setting import RaumreglerSettings
        rtc = RaumreglerSettings()

        def support_setup_function(room_no):
            self.click_dropdown_by_title1(
                test_case, self._content["ROOMS"], step)
            Room_info = rtc.prof_info["RAUM_"+str(room_no)]
            Room_name = Room_info["Room_name"]
            test_case_info = test_case + " " + \
                Room_name + "(RAUM_" + str(room_no) + ")"
            self.click_right_arrow_by_name(test_case_info, Room_name, step)
            self.click_Edit_without_ripple_text(test_case_info, step)
            self.change_time_bars_inside_profile(test_case_info, step)
            self.change_temp_slider_values(
                test_case_info, self.RAUMREGLER, Room_info["Soll_temp"], Room_info["Referenz_temp"], step)
            self.click_Fertig_without_ripple_text(test_case_info, step)
            self.choose_days(test_case_info, Room_info["Running_days"], step)
            self.click_left_arrow(test_case_info, 0, step)

        if self.is_HK1_RTC():
            room_no = 50
            for HK1 in range(self._config.P_HK1_RT_thT_Anz()):
                support_setup_function(room_no)
                room_no += 1

        if self.is_HK2_RTC():
            room_no = 60
            for HK2 in range(self._config.P_HK2_RT_thT_Anz()):
                support_setup_function(room_no)
                room_no += 1

    def verify_Raumregeler_profile(self, test_case, step):
        """
        Verify the desired `Raumregeler` profile values against WPM values by comparing them.

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        from Profile_functions.raumregler_setting import RaumreglerSettings
        rtc = RaumreglerSettings()

        def support_verify_function(room_no):
            self.click_dropdown_by_title1(
                test_case, self._content["ROOMS"], step)
            Room_info = rtc.prof_info["RAUM_"+str(room_no)]
            Room_name = Room_info["Room_name"]
            test_case_info = test_case + " " + \
                Room_name + "(RAUM_" + str(room_no) + ")"
            self.click_right_arrow_by_name(test_case_info, Room_name, step)
            rtc.verify_Room_profile_times_with_WPM(
                test_case_info, room_no, step)
            rtc.verify_Raumregler_temps_in_days_screen(
                test_case_info, room_no, step)
            rtc.verify_Raumregler_run_days(test_case_info, room_no, step)
            rtc.choose_Raumregler_days(test_case_info, room_no, step)
            self.click_left_arrow(test_case_info, 0, step)

        if self._config.P_HK1_REG() == 2:
            room_no = 50
            for HK1 in range(self._config.P_HK1_RT_thT_Anz()):
                support_verify_function(room_no)
                room_no += 1

        if self._config.P_HK2_REG() == 2:
            room_no = 60
            for HK2 in range(self._config.P_HK2_RT_thT_Anz()):
                support_verify_function(room_no)
                room_no += 1

    def setup_and_verify_Heizkreise_profile(self, test_case, step):
        """
        Setup the desired `Aussentemperature` profile values and verify them against WPM values by comparing them.

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        from Profile_functions.aussentemp_setting import AussenTempSettings
        from Profile_functions.festwert_setting import FestwertSettings
        Aussen = AussenTempSettings()
        fwr = FestwertSettings()

        def support_setup_function(context, HK):

            self.click_dropdown_by_title1(
                test_case, self._content["HEATING_CIRCUITS"], step)

            if context == self.HEIZKURVE:
                HK_info = Aussen.prof_info["HK" + str(HK)]
                HK_name = HK_info["HK_name"]
                test_case_info = test_case + " " + \
                    HK_name + "(HK_" + str(HK) + ")"
                self.click_right_arrow_by_name(test_case_info, HK_name, step)
                self.click_Edit_without_ripple_text(test_case_info, step)
                self.change_time_bars_inside_profile(test_case_info, step)
                self.change_temp_slider_values(
                    test_case_info, self.HEIZKURVE, HK_info["Soll_wert"], HK_info["Heizkurve"], step)
                self.click_Fertig_without_ripple_text(test_case_info, step)
                self.choose_days(test_case_info, HK_info["Running_days"], step)

            if context == self.FESTWERT:
                FWR_info = fwr.prof_info["HK" + str(HK)]
                FWR_name = FWR_info["HK_name"]
                test_case_info = test_case + " " + \
                    FWR_name + "(HK_" + str(HK) + ")"
                self.click_right_arrow_by_name(test_case_info, FWR_name, step)
                self.click_Edit_without_ripple_text(test_case_info, step)
                self.change_time_bars_inside_profile(test_case_info, step)
                self.change_temp_slider_values(
                    test_case_info, context, FWR_info["Soll_temp"], FWR_info["Refernztemperatur"], step)
                self.click_Fertig_without_ripple_text(test_case_info, step)
                self.choose_days(
                    test_case_info, FWR_info["Running_days"], step)

            self.click_left_arrow(test_case, 0, step)

        def support_verify_function(context, HK):

            self.click_dropdown_by_title1(
                test_case, self._content["HEATING_CIRCUITS"], step)
            HK_info = Aussen.prof_info["HK" + str(HK)]
            HK_name = HK_info["HK_name"]
            test_case_info = test_case + " " + HK_name + "(HK_" + str(HK) + ")"
            self.click_right_arrow_by_name(test_case, HK_name, step)

            if context == self.HEIZKURVE:
                HK_mode, HK_wert = self.get_Heizkreis_mode_from_WPM(HK)
                HK_value = self.get_Heizkurve_value_from_WPM(HK)
                self.verify_Heizkreis_profile_times_with_WPM(
                    test_case_info, HK, HK_mode, step)
                self.verify_Refernz_temp_in_days_screen(
                    test_case_info, self.HEIZKURVE, HK_value, step)
                self.verify_Profil_temp_in_days_screen(
                    test_case_info, self.HEIZKURVE, HK_wert, step)
                Aussen.verify_Heizkurve_run_days(
                    test_case_info, HK, HK_mode, step)
                Aussen.choose_Heizkurve_days(test_case_info, HK, step)

            if context == self.FESTWERT:
                FWR_mode, FWR_wert = self.get_Heizkreis_mode_from_WPM(HK)
                FWR_Ref_temp = self.get_Festwert_Ref_temp_from_WPM(HK)
                self.verify_Heizkreis_profile_times_with_WPM(
                    test_case_info, HK, FWR_mode, step)
                self.verify_Refernz_temp_in_days_screen(
                    test_case_info, self.FESTWERT, FWR_Ref_temp, step)
                self.verify_Profil_temp_in_days_screen(
                    test_case_info, self.FESTWERT, FWR_wert, step)
                fwr.verify_Festwert_run_days(
                    test_case_info, HK, FWR_mode, step)
                fwr.choose_Festwert_days(test_case_info, HK, step)

            self.click_left_arrow(test_case, 0, step)

        if self._config.P_HK1_REG() == 0:
            support_setup_function(self.HEIZKURVE, 1)

        if self._config.P_HK2_REG() == 0:
            support_setup_function(self.HEIZKURVE, 2)

        if self._config.P_HK1_REG() == 1:
            support_setup_function(self.FESTWERT, 1)

        if self._config.P_HK2_REG() == 1:
            support_setup_function(self.FESTWERT, 2)

        self.wait_browser(test_case, 5, step)

        if self._config.P_HK1_REG() == 0:
            support_verify_function(self.HEIZKURVE, 1)

        if self._config.P_HK2_REG() == 0:
            support_verify_function(self.HEIZKURVE, 2)

        if self._config.P_HK1_REG() == 1:
            support_verify_function(self.FESTWERT, 1)

        if self._config.P_HK2_REG() == 1:
            support_verify_function(self.FESTWERT, 2)

    def creat_profile_for_testing_Home_screen(self, test_case, step):
        """
        This is a special function to create a profile before proceeding the testing of Home screen settings
        options like Warmwasser, Raumregeler, Haus tiles.

        """
        target_screen = self._content["SETTINGS"]
        target_tile = self._content["INDIVIDUAL_ADAPTIONS"]

        self.initial_steps(test_case, target_screen, target_tile)
        self.rename_HK_and_RTC(test_case, step)

        # select "Systemprofile erstellen" right arrow
        self.click_right_arrow_by_name(
            test_case, self._content["CREATE_PROFILE"], step)

        profile_name = self.profile_info["profile_name"]
        self.steps_to_create_profile(test_case, profile_name, step)
        # self.select_created_profile(test_case, profile_name, step)
        self.browser.refresh()

    def special_profile_steps(self, test_case, target_screen, target_tile, step):
        """
        ** Deprecated : UHI version more than 2.3.1 **

        To avoid fails in Haus tiles options when there are more than 2 screens inside one tile. This follows,
        1. Refreshing the browser.
        2. Open "Individual Anpassungen" tile and select profile if its not already selected.
        3. Reach the target page to change the parameters.

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        target_screen
            The screen where the action is performed (Home, Settings, EasyOn, Analysis)
        target_tile
            The tile where the test steps are performed (Warmwasser, Haus, Netzwerk, Software update and so on.)
        step
            step number from the test case document
        """
        self.browser.get(self.uhi_url)
        self._1st_step(test_case, step)
        step += 1

        self.waitByCss(test_case, self.buttons["info"], 15, step)
        self.wait_browser(test_case, 3, step)
        self._2nd_step(test_case, self._content["SETTINGS"], step)
        step += 1

        self._3rd_step(test_case, self._content["INDIVIDUAL_ADAPTIONS"], step)
        self.findElementsCSS(
            test_case, self.buttons["right_arrow"], step)[-1].click()
        time.sleep(1)
        profile_name = self.profile_info["profile_name"]
        self.select_created_profile(test_case, profile_name, step)
        self.click_left_arrow(test_case, 0, step)
        self.click_left_arrow(test_case, 0, step)
        time.sleep(1)
        self.click_close_icon(test_case, step)
        time.sleep(1)
        self.findElementsCSS(test_case, self.buttons["indicator"], step)[
            1].click()

        # 3rd step
        self._2nd_step(test_case, target_screen, step)
        time.sleep(2)
        self._3rd_step(test_case, target_tile, step)
        time.sleep(2)
        step += 1

    def is_profile_valid(self, test_case, step):
        """ returns `True` if no profile is active / invalid """
        try:
            return self.browser.find_element_by_css_selector(self.texT["no_active_profile"])
        except:
            return None

    def create_profile_Home_screen(self, test_case, element_ID, step):
        """ 
        creates a `Profile` at Home screen when the current profile is invalid 

        Return
        ------
        True
            If the profile is Invalid/Inactive
        """
        if self.is_profile_valid(test_case, step) is not None:
            self.get_click_from_text_area(
                test_case, self._content["NO_ACTIVE_PROFILE_INFO_ACTION"], step)
            profile_name = self.profile_info["profile_name"]
            self.steps_to_create_profile(test_case, profile_name, step)
            self.click_left_arrow(test_case, 0, step)
            return True

    def click_less_option_button(self, test_case, step):
        """ returns a click `Weniger Optionen` from the Haus screens """
        time.sleep(2)
        return self.findElementCSS(test_case, self.buttons["btn_less_option"], step).click()

    def is_any_HK_profile_active(self, HK):
        """ return `True` if `HK1 Profile times` (Zeit 1 / Zeit 2) is current time in Absenkung / Anhebung"""
        if self.get_status_from_coils(self.regs_digital["HK" + str(HK) + "_Abs1"]) or self.get_status_from_coils(self.regs_digital["HK" + str(HK) + "_Abs2"]) or \
                self.get_status_from_coils(self.regs_digital["HK" + str(HK) + "_Anh1"]) or self.get_status_from_coils(self.regs_digital["HK" + str(HK) + "_Anh2"]):
            return True
        else:
            return False
