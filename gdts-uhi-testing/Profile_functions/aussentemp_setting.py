from Profile_functions.Profile_general_functions import ProfileGeneralFunctions
import time, copy

class AussenTempSettings(ProfileGeneralFunctions):
    """
    This is a supporting class for test case 'UHI-16 -- Aussentemperatur Settings' in the Haus tile and it is derived from 'ProfileGeneralFunctions' where all common profile functions are defined.
    
    """
    prof_info = ProfileGeneralFunctions.profile_info["AussenTemp_info"]
    HK_index = ProfileGeneralFunctions().Wpm_info["Heizkurve_index"]

    def expected_screen_name(self, HK):
        """
        Returns screen name from JSON

        Parameters
        ----------
        HK
            Targeted 'Heizkreis' number for the specific step

        Returns
        -------
        'string'
            Returns from 'profiles_info.json' file
        """
        HK = "HK" + str(HK)
        return self.prof_info[HK]["HK_name"]

    def set_Soll_temp(self, test_case, context, HK, element_ID, step):
        """
        set Soll temperatures of HK 1 & 2
        
        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        context 
            Key words to differentiate functions like Festwert, Raumregeler
        HK
            Targeted 'Heizkreis' number for the specific step
        element_ID
            Element number for the targeted screen (0, 1, 2, 3, ...)
        step
            step number from the test case document
        """
        HK = "HK" + str(HK)
        soll_wert = self.prof_info[HK]["Soll_wert"]
        self.set_temp_slider(test_case, context, soll_wert, element_ID, step)
    
    def choose_Heizkurve_days(self, test_case, HK, step):
        """
        Select running days from JSON
        
        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        HK
            Targeted 'Heizkreis' number for the specific step
        step
            step number from the test case document
        """
        HK = "HK" + str(HK)
        self.choose_days(test_case, self.prof_info[HK]["Running_days"], step)

    def verify_Heizkurve_screens(self, test_case, context, HK, element_ID, step):
        """
        Steps to verify Festwert screens of HK1 and HK2

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        context 
            Key words to differentiate functions like Festwert, Raumregeler
        HK
            Targeted 'Heizkreis' number for the specific step
        element_ID
            Element number for the targeted screen (0, 1, 2, 3, ...)
        step
            step number from the test case document
        """
        screen_name = self.expected_screen_name(HK)
        HK_Ist_temp = self.get_Heizkreis_Ist_temp_from_WPM(HK)
        HK_mode, HK_wert = self.get_Heizkreis_mode_from_WPM(HK)
        HK_value = self.get_Heizkurve_value_from_WPM(HK)
        HK_value_Ref = copy.deepcopy(HK_value)
        
        # check if the profile time is now for Soll value in Haus screen
        if self.is_any_HK_profile_active(HK):
            HK_value = HK_value + HK_wert

        self.check_screen(test_case, screen_name, element_ID, self._content["SYSTEM"],
                            element_ID, HK_Ist_temp, self._content["HEATING_CURVE"], HK_value, step)

        self.click_down_triangle(test_case, element_ID, step)
        self.click_right_arrow_by_name(test_case, self._content["WEEK_OVERVIEW"], step)
        
        # Verify Absenkung / Anhebung mode profile times 
        self.verify_Heizkreis_profile_times_with_WPM(test_case, HK, HK_mode, step)
        
        # Verify Soll temperature displayed at Wochenverlauf screen
        self.verify_Refernz_temp_in_days_screen(test_case, context, HK_value_Ref, step)

        # Verify Profile temperature displayed at Wochenverlauf screen
        self.verify_Profil_temp_in_days_screen(test_case, context, HK_wert , step)

        # Verify running days with WPM
        self.verify_Heizkurve_run_days(test_case, HK, HK_mode, step)
        
        self.choose_Heizkurve_days(test_case, HK, step)
        self.click_left_arrow(test_case, element_ID, step)
        self.click_less_option_button(test_case, step)
        
    def verify_Heizkurve_run_days(self, test_case, HK, mode, step):
        """
        Steps to verify *Aussentemperatur* 'Wochenvorlauf' with WPM values

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        HK
            Targeted 'Heizkreis' number for the specific step
        mode
            'Abenkung / Anhebung' text to identify the verification variables
        element_ID
            Element number for the targeted screen (0, 1, 2, 3, ...)
        step
            step number from the test case document
        """
        WPM_days = self.get_Heizkreis_run_days_from_WPM(HK, mode)
        UHI_days = self.prof_info["HK" + str(HK)]["Running_days"]
        self.verify_intervals_with_WPM(test_case, WPM_days, UHI_days, step)

    def setup_Heizkurve_values(self, test_case, context, HK, element_ID, step):
        """
        Setup `Aussentemperatur` values in the Haus screens """
        self.set_Soll_temp(test_case, context, HK, element_ID, step)
        self.click_down_triangle(test_case, element_ID, step)
        if self.create_profile_Home_screen(test_case, element_ID, step):
            self.click_down_triangle(test_case, element_ID, step)
        self.click_right_arrow_by_name(test_case,
                                        self._content["WEEK_OVERVIEW"], step)
        self.choose_Heizkurve_days(test_case, HK, step)
        self.click_left_arrow(test_case, element_ID, step)
        self.click_less_option_button(test_case, step)
        
        