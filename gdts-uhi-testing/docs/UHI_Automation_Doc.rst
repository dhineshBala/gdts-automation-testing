Welcome to UHI Selenium Automation Documentation
================================================
This is the complete documentation of the project **"31138 -- UHI Frontend Automation"** by Dhinesh, Bala.
This document contains all the classes, methods that are used for this project and one can find the complete
explanation about it. 



.. toctree::
   :maxdepth: 2
   
   ./src/introduction
   ./src/getting_started
   ./src/test_data_setup
   ./src/Define_init
   ./src/Define_functions
   ./src/general_functions
   ./src/read_modbus
   ./src/WPM_config
   ./Profile_functions/Profile_general_functions
   ./src/test_cases
   


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
