json : User Input
=================

This 'json' file contains the **User input** like Bildschrim Sperre zeit, Betriebsmodus, Anheizprogramm and Funktionssperren for test execution. This can be updated / changed in the path ``path/to/gdts-uhi-testing/json-files/user_input.json``.

.. code-block:: json
	
	{
		"Bildschrim_Sperre": {
			"Zeit": "1 min"
		},
		"Home":{
			"Betriebsmodus":{
				"Automatik": {
					"Mode_status": "Off",
					"Avail_modes":["All", "Automatik", "Urlaub", "Party"],
					"Operating_mode": "Party",
					"Heating_limit": 22,
					"Cooling_limit": 28,
					"time_limit": 3
				},
				"Manuell":{
					"Mode_status": "On",
					"Avail_modes":["All", "Winter", "Sommer", "Urlaub", "Party", "Heizstab", "Kuehlen"],
					"Operating_mode": "All"
				}
				
			}
		},
		"Anheizprogramm":{
			"Max_temp": 27,
			"Belegreifheizen":{
				"Zeitdauer_Halten": 21,
				"Aufheizen":{
					"temp_differenz": 5,
					"Zeit_dauer": 22
				},
				"Abheizen":{
					"temp_differenz": 7,
					"Zeit_dauer": 24
				}
			}
		},
		"Funktionssperren":{
			"Warmwasser":"gesperrt",
			"Schwimmbad":"gesperrt",
			"Bivalent":"gesperrt",
			"Regenerativ":"gesperrt",
			"Aktiv_Kuehlen":"gesperrt",
			"Passiv_Kuehlen":"gesperrt"
		}
	}