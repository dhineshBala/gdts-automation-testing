json : Profile Info
===================

This 'json' file contains all the **Profile** info like Zeitintervall, Wochenvorlauf, Temperatures, Heizkreis and Raumregeler names. This can be updated / changed in the path ``path/to/gdts-uhi-testing/json-files/profiles_info.json``.

.. code-block:: json
	
	{
		"profile_name": "testProfile",
		"dummy_profile": "Dummy",
		"Warmwasser": {
			"Soll_temp": 50.0,
			"WW_Sperre": {
				"Min_WW_temp": 30.0,
				"Running_days": {
					"Mo": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Di": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Mi": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Do": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Fr": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Sa": {
						"Z1": "yes",
						"Z2": "no"
					},
					"So": {
						"Z1": "yes",
						"Z2": "yes"
					}
				}
			},
			"Nacherwarm/Desinfek": {
				"Soll_temp": 55.0,
				"Soll_temp_Biv": 79.0,
				"Running_days": {
					"Mo": "yes",
					"Di": "yes",
					"Mi": "no",
					"Do": "yes",
					"Fr": "no",
					"Sa": "yes",
					"So": "yes"
				}
			},
			"Zirkulation":{
				"Zeit_programm": {
					"Mo": {
						"Z1": "no",
						"Z2": "no"
					},
					"Di": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Mi": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Do": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Fr": {
						"Z1": "yes",
						"Z2": "yes"
					},
					"Sa": {
						"Z1": "yes",
						"Z2": "no"
					},
					"So": {
						"Z1": "yes",
						"Z2": "yes"
					}
				}
			}
		},
		"AussenTemp_info":{
			"HK1":{
				"HK_name":"1. Floor",
				"Soll_wert": 5.0,
				"Heizkurve": 3.0,
				"Running_days": {
					"Mo": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Di": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Mi": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Do": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Fr": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Sa": {
						"Z1": "yes",
						"Z2": "no"
					},
					"So": {
						"Z1": "yes",
						"Z2": "yes"
					}
				}
			},
			"HK2":{
				"HK_name":"2. Floor",
				"Soll_wert": -5.0,
				"Heizkurve": 2.0,
				"Running_days": {
					"Mo": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Di": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Mi": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Do": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Fr": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Sa": {
						"Z1": "yes",
						"Z2": "no"
					},
					"So": {
						"Z1": "yes",
						"Z2": "yes"
					}
				}

			}
		},
		"Festwert_info":{
			"HK1":{
				"HK_name":"1. Floor",
				"Soll_temp": 36.0,
				"Refernztemperatur": 25.0,
				"Running_days": {
					"Mo": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Di": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Mi": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Do": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Fr": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Sa": {
						"Z1": "yes",
						"Z2": "no"
					},
					"So": {
						"Z1": "yes",
						"Z2": "yes"
					}
				}
			},
			"HK2":{
				"HK_name":"2. Floor",
				"Soll_temp": 25.0,
				"Refernztemperatur": 35.0,
				"Running_days": {
					"Mo": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Di": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Mi": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Do": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Fr": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Sa": {
						"Z1": "yes",
						"Z2": "no"
					},
					"So": {
						"Z1": "yes",
						"Z2": "yes"
					}
				}

			}
		},
		"Raumregulung_info": {
			"RAUM_50": {
				"SchnellHeiz_time": 20,
				"Room_name": "Room 1",
				"Soll_temp": 23.0,
				"Referenz_temp": 30.0,
				"Profile_times": {
					"Zeit1_st": "08:00",
					"Zeit1_ed": "12:00",
					"Zeit2_st": "14:00",
					"Zeit3_ed": "16:00"
				},
				"Running_days": {
					"Mo": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Di": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Mi": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Do": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Fr": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Sa": {
						"Z1": "yes",
						"Z2": "no"
					},
					"So": {
						"Z1": "yes",
						"Z2": "yes"
					}
				}
			},
			"RAUM_51": {
				"Room_name": "Room 2",
				"Soll_temp": 23.0,
				"Referenz_temp": 30.0,
				"Profile_times": {
					"Zeit1_st": "08:00",
					"Zeit1_ed": "12:00",
					"Zeit2_st": "14:00",
					"Zeit3_ed": "16:00"
				},
				"Running_days": {
					"Mo": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Di": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Mi": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Do": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Fr": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Sa": {
						"Z1": "yes",
						"Z2": "no"
					},
					"So": {
						"Z1": "yes",
						"Z2": "yes"
					}
				}
			},
			"RAUM_52": {
				"Room_name": "Room 3",
				"Soll_temp": 25.0,
				"Referenz_temp": 30.0,
				"Profile_times": {
					"Zeit1_st": "08:00",
					"Zeit1_ed": "12:00",
					"Zeit2_st": "14:00",
					"Zeit3_ed": "16:00"
				},
				"Running_days": {
					"Mo": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Di": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Mi": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Do": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Fr": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Sa": {
						"Z1": "yes",
						"Z2": "no"
					},
					"So": {
						"Z1": "yes",
						"Z2": "yes"
					}
				}
			},
			"RAUM_53": {
				"Room_name": "Room 4",
				"Soll_temp": 25.0,
				"Referenz_temp": 30.0,
				"Profile_times": {
					"Zeit1_st": "08:00",
					"Zeit1_ed": "12:00",
					"Zeit2_st": "14:00",
					"Zeit3_ed": "16:00"
				},
				"Running_days": {
					"Mo": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Di": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Mi": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Do": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Fr": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Sa": {
						"Z1": "yes",
						"Z2": "no"
					},
					"So": {
						"Z1": "yes",
						"Z2": "yes"
					}
				}
			},
			"RAUM_54": {
				"Room_name": "Room 5",
				"Soll_temp": 25.0,
				"Referenz_temp": 30.0,
				"Profile_times": {
					"Zeit1_st": "08:00",
					"Zeit1_ed": "12:00",
					"Zeit2_st": "14:00",
					"Zeit3_ed": "16:00"
				},
				"Running_days": {
					"Mo": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Di": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Mi": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Do": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Fr": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Sa": {
						"Z1": "yes",
						"Z2": "no"
					},
					"So": {
						"Z1": "yes",
						"Z2": "yes"
					}
				}
			},
			"RAUM_55": {
				"Room_name": "Room 6",
				"Soll_temp": 25.0,
				"Referenz_temp": 30.0,
				"Profile_times": {
					"Zeit1_st": "08:00",
					"Zeit1_ed": "12:00",
					"Zeit2_st": "14:00",
					"Zeit3_ed": "16:00"
				},
				"Running_days": {
					"Mo": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Di": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Mi": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Do": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Fr": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Sa": {
						"Z1": "yes",
						"Z2": "no"
					},
					"So": {
						"Z1": "yes",
						"Z2": "yes"
					}
				}
			},
			"RAUM_56": {
				"Room_name": "Room 7",
				"Soll_temp": 25.0,
				"Referenz_temp": 30.0,
				"Profile_times": {
					"Zeit1_st": "08:00",
					"Zeit1_ed": "12:00",
					"Zeit2_st": "14:00",
					"Zeit3_ed": "16:00"
				},
				"Running_days": {
					"Mo": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Di": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Mi": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Do": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Fr": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Sa": {
						"Z1": "yes",
						"Z2": "no"
					},
					"So": {
						"Z1": "yes",
						"Z2": "yes"
					}
				}
			},
			"RAUM_57": {
				"Room_name": "Room 8",
				"Soll_temp": 25.0,
				"Referenz_temp": 30.0,
				"Profile_times": {
					"Zeit1_st": "08:00",
					"Zeit1_ed": "12:00",
					"Zeit2_st": "14:00",
					"Zeit3_ed": "16:00"
				},
				"Running_days": {
					"Mo": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Di": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Mi": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Do": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Fr": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Sa": {
						"Z1": "yes",
						"Z2": "no"
					},
					"So": {
						"Z1": "yes",
						"Z2": "yes"
					}
				}
			},
			"RAUM_58": {
				"Room_name": "Room 9",
				"Soll_temp": 25.0,
				"Referenz_temp": 30.0,
				"Profile_times": {
					"Zeit1_st": "08:00",
					"Zeit1_ed": "12:00",
					"Zeit2_st": "14:00",
					"Zeit3_ed": "16:00"
				},
				"Running_days": {
					"Mo": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Di": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Mi": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Do": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Fr": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Sa": {
						"Z1": "yes",
						"Z2": "no"
					},
					"So": {
						"Z1": "yes",
						"Z2": "yes"
					}
				}
			},
			"RAUM_59": {
				"Room_name": "Room 10",
				"Soll_temp": 25.0,
				"Referenz_temp": 30.0,
				"Profile_times": {
					"Zeit1_st": "08:00",
					"Zeit1_ed": "12:00",
					"Zeit2_st": "14:00",
					"Zeit3_ed": "16:00"
				},
				"Running_days": {
					"Mo": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Di": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Mi": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Do": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Fr": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Sa": {
						"Z1": "yes",
						"Z2": "no"
					},
					"So": {
						"Z1": "yes",
						"Z2": "yes"
					}
				}
			},
			"RAUM_60": {
				"Room_name": "Room 11",
				"Soll_temp": 23.0,
				"Referenz_temp": 30.0,
				"Profile_times": {
					"Zeit1_st": "08:00",
					"Zeit1_ed": "12:00",
					"Zeit2_st": "14:00",
					"Zeit3_ed": "16:00"
				},
				"Running_days": {
					"Mo": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Di": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Mi": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Do": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Fr": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Sa": {
						"Z1": "yes",
						"Z2": "no"
					},
					"So": {
						"Z1": "yes",
						"Z2": "yes"
					}
				}
			},
			"RAUM_61": {
				"Room_name": "Room 12",
				"Soll_temp": 23.0,
				"Referenz_temp": 30.0,
				"Profile_times": {
					"Zeit1_st": "08:00",
					"Zeit1_ed": "12:00",
					"Zeit2_st": "14:00",
					"Zeit3_ed": "16:00"
				},
				"Running_days": {
					"Mo": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Di": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Mi": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Do": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Fr": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Sa": {
						"Z1": "yes",
						"Z2": "no"
					},
					"So": {
						"Z1": "yes",
						"Z2": "yes"
					}
				}
			},
			"RAUM_62": {
				"Room_name": "Room 13",
				"Soll_temp": 25.0,
				"Referenz_temp": 30.0,
				"Profile_times": {
					"Zeit1_st": "08:00",
					"Zeit1_ed": "12:00",
					"Zeit2_st": "14:00",
					"Zeit3_ed": "16:00"
				},
				"Running_days": {
					"Mo": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Di": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Mi": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Do": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Fr": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Sa": {
						"Z1": "yes",
						"Z2": "no"
					},
					"So": {
						"Z1": "yes",
						"Z2": "yes"
					}
				}
			},
			"RAUM_63": {
				"Room_name": "Room 14",
				"Soll_temp": 25.0,
				"Referenz_temp": 30.0,
				"Profile_times": {
					"Zeit1_st": "08:00",
					"Zeit1_ed": "12:00",
					"Zeit2_st": "14:00",
					"Zeit3_ed": "16:00"
				},
				"Running_days": {
					"Mo": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Di": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Mi": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Do": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Fr": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Sa": {
						"Z1": "yes",
						"Z2": "no"
					},
					"So": {
						"Z1": "yes",
						"Z2": "yes"
					}
				}
			},
			"RAUM_64": {
				"Room_name": "Room 15",
				"Soll_temp": 25.0,
				"Referenz_temp": 30.0,
				"Profile_times": {
					"Zeit1_st": "08:00",
					"Zeit1_ed": "12:00",
					"Zeit2_st": "14:00",
					"Zeit3_ed": "16:00"
				},
				"Running_days": {
					"Mo": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Di": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Mi": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Do": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Fr": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Sa": {
						"Z1": "yes",
						"Z2": "no"
					},
					"So": {
						"Z1": "yes",
						"Z2": "yes"
					}
				}
			},
			"RAUM_65": {
				"Room_name": "Room 16",
				"Soll_temp": 25.0,
				"Referenz_temp": 30.0,
				"Profile_times": {
					"Zeit1_st": "08:00",
					"Zeit1_ed": "12:00",
					"Zeit2_st": "14:00",
					"Zeit3_ed": "16:00"
				},
				"Running_days": {
					"Mo": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Di": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Mi": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Do": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Fr": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Sa": {
						"Z1": "yes",
						"Z2": "no"
					},
					"So": {
						"Z1": "yes",
						"Z2": "yes"
					}
				}
			},
			"RAUM_66": {
				"Room_name": "Room 17",
				"Soll_temp": 25.0,
				"Referenz_temp": 30.0,
				"Profile_times": {
					"Zeit1_st": "08:00",
					"Zeit1_ed": "12:00",
					"Zeit2_st": "14:00",
					"Zeit3_ed": "16:00"
				},
				"Running_days": {
					"Mo": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Di": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Mi": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Do": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Fr": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Sa": {
						"Z1": "yes",
						"Z2": "no"
					},
					"So": {
						"Z1": "yes",
						"Z2": "yes"
					}
				}
			},
			"RAUM_67": {
				"Room_name": "Room 18",
				"Soll_temp": 25.0,
				"Referenz_temp": 30.0,
				"Profile_times": {
					"Zeit1_st": "08:00",
					"Zeit1_ed": "12:00",
					"Zeit2_st": "14:00",
					"Zeit3_ed": "16:00"
				},
				"Running_days": {
					"Mo": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Di": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Mi": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Do": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Fr": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Sa": {
						"Z1": "yes",
						"Z2": "no"
					},
					"So": {
						"Z1": "yes",
						"Z2": "yes"
					}
				}
			},
			"RAUM_68": {
				"Room_name": "Room 19",
				"Soll_temp": 25.0,
				"Referenz_temp": 30.0,
				"Profile_times": {
					"Zeit1_st": "08:00",
					"Zeit1_ed": "12:00",
					"Zeit2_st": "14:00",
					"Zeit3_ed": "16:00"
				},
				"Running_days": {
					"Mo": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Di": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Mi": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Do": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Fr": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Sa": {
						"Z1": "yes",
						"Z2": "no"
					},
					"So": {
						"Z1": "yes",
						"Z2": "yes"
					}
				}
			},
			"RAUM_69": {
				"Room_name": "Room 20",
				"Soll_temp": 25.0,
				"Referenz_temp": 30.0,
				"Profile_times": {
					"Zeit1_st": "08:00",
					"Zeit1_ed": "12:00",
					"Zeit2_st": "14:00",
					"Zeit3_ed": "16:00"
				},
				"Running_days": {
					"Mo": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Di": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Mi": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Do": {
						"Z1": "yes",
						"Z2": "no"
					},
					"Fr": {
						"Z1": "no",
						"Z2": "yes"
					},
					"Sa": {
						"Z1": "yes",
						"Z2": "no"
					},
					"So": {
						"Z1": "yes",
						"Z2": "yes"
					}
				}
			}
		}
	}