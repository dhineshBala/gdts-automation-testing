json : Registers Index
======================

This 'json' file have all the regsiters index from '.2cf' file of WPM software. This can be updated / changed in the path ``path/to/gdts-uhi-testing/json-files/All_registers.json``. 

To see the 'json' files, please choose from below,

.. toctree::
   :maxdepth: 2
   
   Analog
   Digital
   Integer
   