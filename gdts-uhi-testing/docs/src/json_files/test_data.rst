json : Test Data
================

This 'json' file contains all the **test data** for the test execution like UHI Url, pCO Card IP, API Urls and so on. This can be updated / changed in the path ``path/to/gdts-uhi-testing/json-files/test_data.json``.

.. code-block:: json
	
	{
	  "UrlInfo": {
		"Url": "<<UHI-ip-address>>",
		"prefix": "http://",
		"suffix": "/?system=uhi"
	  },
	  "pCoInfo": {
		"host": "<<pCO-ip-address>>",
		"port": "502",
		"offset": "5001"
	  },
	  "IoT_info": {
		"staging": "gdts-uhi-staging-iot.azure-devices.net",
		"production": "GDTS-UHI-Default.azure-devices.net"
	  },
	  "APIservices": {
		"uhi_ver": "/api/system/version",
		"uhi_update_check": "/api/system/update/check"
	  },
	  "Wlan_info": {
		"IP": "192.168.1.100",
		"SSID": "GDTS Hotspot",
		"Passwort": "@systemm"
	  },
	  "WpmInfo": {
		"WPM_index": {
		  "0": "",
		  "1": "a",
		  "2": "b",
		  "3": "c",
		  "4": "d",
		  "5": "e",
		  "6": "f",
		  "7": "g",
		  "8": "h",
		  "9": "i",
		  "10": "j",
		  "11": "k",
		  "12": "l",
		  "13": "m",
		  "14": "n",
		  "15": "o",
		  "16": "p",
		  "17": "q",
		  "18": "r",
		  "19": "s",
		  "20": "t",
		  "21": "u",
		  "22": "v",
		  "23": "w",
		  "24": "x",
		  "25": "y",
		  "26": "z"
		},
		"Heizkurve_index": {
		  "0": -19,
		  "1": -18,
		  "2": -17,
		  "3": -16,
		  "4": -15,
		  "5": -14,
		  "6": -13,
		  "7": -12,
		  "8": -11,
		  "9": -10,
		  "10": -9,
		  "11": -8,
		  "12": -7,
		  "13": -6,
		  "14": -5,
		  "15": -4,
		  "16": -3,
		  "17": -2,
		  "18": -1,
		  "19": 0,
		  "20": 1,
		  "21": 2,
		  "22": 3,
		  "23": 4,
		  "24": 5,
		  "25": 6,
		  "26": 7,
		  "27": 8,
		  "28": 9,
		  "29": 10,
		  "30": 11,
		  "31": 12,
		  "32": 13,
		  "33": 14,
		  "34": 15,
		  "35": 16,
		  "36": 17,
		  "37": 18,
		  "38": 19
		},
		"Betriebsmodus": {
		  "0": "Sommer",
		  "1": "Winter",
		  "2": "Urlaub",
		  "3": "Party",
		  "4": "Heizstab",
		  "5": "Kuehlen"
		}
	  }
	}