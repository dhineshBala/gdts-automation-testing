.. include:: <isonum.txt>
.. _getting_started:

***************
Getting started
***************
This project uses UHI and WPM with NWPM card as Hardware setup. The overall architecture with complete scope of this project is shown below.

.. image:: ../images/Scope.png

The individual sections are explained further below.

.. _prerequisite_hardware:

Pre-requisite : Hardware
========================
This testing can be performed by using local computer connected with the network as same as UHI and WPM (connected by pCO Web card). The testing setup is shown below.

.. image:: ../images/HW_setup.png

Once the setup is done, start the UHI and follow the instructions.
	
	* Disable 'Firewall' settings in UHI (hasselfree access via browser),
		
		* Add ``firewall_enabled: 'false'`` line at the end of `'local.yml'`::
			
			> sudo nano /usr/local/uhi/config/local.yml
		
	* Verify Modbus RTU communication between WPM and UHI (default **System M** setup)

	* Insert pCO 1 (or) 2 webcard to the WPM and enable `Modbus Extended` mode 
		
		* In WPM, 
		
			Menu + Enter |rarr| Diagnose (Menu + Modus) |rarr| Info Diagnose |rarr| Kommunik. |rarr| BMS1 |rarr| MODBUS EXT |rarr| OK
			
		* In pCO (get the pCO Web IP from PGD by, Esc + Enter |rarr| OTHER INFORMATION |rarr| PCOWEB/NET CONFIG |rarr| PCOWEB settings |rarr| IP Address)
			
			- Go to ``http://ip-address/config/adminpage.html`` page
			- Navigate to Configuration |rarr| pCO Com |rarr| Protocol |rarr| Modbus Extended, then `Submit` to save the changes.
			
			
.. _prerequisite_software:

Pre-requisite : Software
========================
	* Clone the latest automation script using,
	
		* ``git clone git@bitbucket.org:dhineshBala/gdts-automation-testing.git``
  
	* Download the latest `Selenium <https://www.seleniumhq.org/>`_ Webdriver inside the working (cloned) directory
	* Install the latest `Python 3 <https://www.python.org/>`_ 
	* Install pip3 (for managing dependicies)
	* Install dependicies via 'pip install -r requirements.txt'


Test Execution
==============
	* Before test execution, user should setup the UHI and pCO Card IPs in ``path/to/gdts-uhi-testing/json-files/test_data.json`` under "UrlInfo" and "pCoInfo" objects. 
	
	* Once the setup is done, test execution is performed by,
		
		* **In Windows**,
		
			* Run ``run_win.bat`` 
		
		* **In Linux**,
		
			* Run ``run_linux.sh`` 
		
		After exeucting the automation script, the results are generated as HTML document and stored in under ``path/to/gdts-uhi-testing/Test_Reports/*``.

Important info / Issues:
-------------------------------
	* Changing times inside profiles is not working at the moment. Please test this case in Manual mode.
				