***************
Test Data Setup
***************

Test data are in the format of '.json' files. The main purpose of 'json' file is to configured / changed by anyone. Also, this requires no programming knowledge and these files are located under ``path/to/gdts-uhi-testing/json-files/*``.

.. toctree::
   :maxdepth: 2
   
   json_files/test_run
   json_files/test_data
   json_files/user_input
   json_files/profiles_info
   json_files/All_registers
   json_files/de.common
   json_files/elements_info
   
   
Important Info:
---------------
Before executing the test automation script, the UHI and pCO card IPs should be setup in any case. Please refer to **json : Test Data** link from below. The IPs should be enetered in between the double quotes like "192.168.16.223".
