.. _introduction:

************
Introduction
************
This project is about automating the Universal Hardware Interface (referred as **UHI**) testing method with help of `Selenium <https://www.seleniumhq.org/>`_ framework in `Python <https://www.python.org/>`_. 
The user instructions are generated using Selenium in the web browser and compared these expected result with Wärmepumpe Manager (referred as **WPM**) actual results by reading Modbus Registers and Coils. 
The Python `Unittest <https://docs.python.org/3/library/unittest.html>`_ framework is used to generate results in the format of `XML <https://www.w3schools.com/xml/xml_whatis.asp>`_ for better visualization. 
This report is more focused on explaining the Python functions from the automation script along with the logic that have been implemented for verifying the results. 
This document can be also referred as finding the logics behind the UHI and WPM functions. 

.. _concept:

Concept
=======

UHI(Universal Hardware Interface) is the Master of controlling the heat pump in **System M** which *Write/Read* the values from the Modbus registers /
coils from WPM(Wärmepumpe Manager). The UHI can be accessed in the
web browser via http port (80) and setup the user inputs. 
The user inputs are configured via json file and this can be edited by anyone. These json inputs
are converted in to browser instructions using Selenium framework. Once the
instructions are successfully set, the automation script waits for 30 seconds to synchronize
UHI values to WPM values. After the successful communication, the WPM
values are read from **pCO Web card** (via Modbus TCP/IP) using `pymodbusTCP <https://pymodbustcp.readthedocs.io/en/latest/#>`_
library then it is scaled if necessary.


.. image:: ../images/Overview.png


   