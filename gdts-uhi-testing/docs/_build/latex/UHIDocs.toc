\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Introduction}{3}{chapter.1}% 
\contentsline {section}{\numberline {1.1}Concept}{3}{section.1.1}% 
\contentsline {chapter}{\numberline {2}Getting started}{5}{chapter.2}% 
\contentsline {section}{\numberline {2.1}Pre-requisite : Hardware}{5}{section.2.1}% 
\contentsline {section}{\numberline {2.2}Pre-requisite : Software}{6}{section.2.2}% 
\contentsline {section}{\numberline {2.3}Test Execution}{7}{section.2.3}% 
\contentsline {subsection}{\numberline {2.3.1}Important info / Issues:}{7}{subsection.2.3.1}% 
\contentsline {chapter}{\numberline {3}Test Data Setup}{9}{chapter.3}% 
\contentsline {section}{\numberline {3.1}Important Info:}{9}{section.3.1}% 
\contentsline {chapter}{\numberline {4}py : \_\_init\_\_}{11}{chapter.4}% 
\contentsline {chapter}{\numberline {5}py : Function Definitions}{13}{chapter.5}% 
\contentsline {chapter}{\numberline {6}py : General Functions}{19}{chapter.6}% 
\contentsline {chapter}{\numberline {7}py : Read Modbus Function}{31}{chapter.7}% 
\contentsline {chapter}{\numberline {8}py : W\IeC {\"a}rmepumpe Config}{33}{chapter.8}% 
\contentsline {chapter}{\numberline {9}py : Profile Functions}{35}{chapter.9}% 
\contentsline {section}{\numberline {9.1}py : Aussentemperatur Settings}{47}{section.9.1}% 
\contentsline {section}{\numberline {9.2}py : Festwert Settings}{48}{section.9.2}% 
\contentsline {section}{\numberline {9.3}py : Ramregeler Settings}{49}{section.9.3}% 
\contentsline {section}{\numberline {9.4}py : Warmwasser Settings}{52}{section.9.4}% 
\contentsline {chapter}{\numberline {10}py : Test Cases}{57}{chapter.10}% 
\contentsline {section}{\numberline {10.1}py : Anheizprogramm}{60}{section.10.1}% 
\contentsline {section}{\numberline {10.2}py : Anlagendaten}{64}{section.10.2}% 
\contentsline {section}{\numberline {10.3}py : Betriebsdaten}{65}{section.10.3}% 
\contentsline {section}{\numberline {10.4}py : Betriebsmodus}{67}{section.10.4}% 
\contentsline {section}{\numberline {10.5}py : Date and Time}{70}{section.10.5}% 
\contentsline {section}{\numberline {10.6}py : Funktionskontroll}{72}{section.10.6}% 
\contentsline {section}{\numberline {10.7}py : Funktionssperren}{75}{section.10.7}% 
\contentsline {section}{\numberline {10.8}py : Language and Region}{77}{section.10.8}% 
\contentsline {section}{\numberline {10.9}py : Laufzeit and Taktungen}{78}{section.10.9}% 
\contentsline {section}{\numberline {10.10}py : Bildschrim Sperre}{80}{section.10.10}% 
\contentsline {section}{\numberline {10.11}py : Systemzustand}{81}{section.10.11}% 
\contentsline {section}{\numberline {10.12}py : W\IeC {\"a}rmemenge}{81}{section.10.12}% 
\contentsline {chapter}{\numberline {11}Indices and tables}{83}{chapter.11}% 
\contentsline {chapter}{Python Module Index}{85}{section*.406}% 
