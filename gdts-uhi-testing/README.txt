Welcome to UHI Automation Testing with Selenium Project.
-------------------------------------------------------

Clone the latest automation script using,
	
	- git clone git@bitbucket.org:dhineshBala/gdts-automation-testing.git

The complete documentation in HTML format can be found here, 
	-> path\to\gdts-uhi-testing\docs\_build\html\UHI_Automation_Doc
Please make sure your are opening this file in browser (it's a HTML file). 

The same in PDF format can be found in 
	-> path\to\gdts-uhi-testing\UHI Automation Documentation.pdf.
	
Pre-requisite : Hardware
	This testing can be performed by using local computer connected with the network as same as UHI and WPM (connected by pCO Web card). 
	Once the setup is done, start the UHI and follow the instructions.
		-> Disable 'Firewall' settings in UHI (hasselfree access via browser),
			- Add firewall_enabled: 'false' line at the end of 'local.yml':
				> sudo nano /usr/local/uhi/config/local.yml
		-> Verify Modbus RTU communication between WPM and UHI (default System M setup)
		-> Insert pCO 1 (or) 2 webcard to the WPM and enable Modbus Extended mode

	- In WPM,
		Menu + Enter -> Diagnose (Menu + Modus) -> Info Diagnose -> Kommunik. -> BMS1 -> MODBUS EXT -> OK

	- In pCO (get the pCO Web IP from PGD by, Esc + Enter -> OTHER INFORMATION -> PCOWEB/NET CONFIG ->PCOWEB settings->IP Address)
		* Go to http://ip-address/config/adminpage.html page
		* Navigate to Configuration -> pCO Com -> Protocol -> Modbus Extended, then Submit to save the changes.

Pre-requisite : Software
	-> Download the latest Selenium Webdriver inside the working (cloned) directory
	-> Install the latest Python 3.
	-> Install pip3 (for managing dependicies)
	-> Install dependicies via 'pip install -r requirements.txt'

Test Execution
	-> Before test execution, user should setup the UHI and pCO Card IPs in path/to/gdts-uhi-testing/json-files/test_data.json under 'UrlInfo' and 'pCoInfo' objects.
	-> Once the setup is done, test execution is performed by,
		- In Windows,
			* Run run_win.bat
		- In Linux,
			* Run run_linux.sh
	After exeucting the automation script, the results are generated as HTML document and stored in under path/to/gdts-uhi-testing/Test_Reports/*.

Important info / Issues:
	-> Changing times inside profiles is not working at the moment. Please test this case in Manual mode.

Have fun with this Automation Testing!!
