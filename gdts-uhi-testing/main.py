import unittest
import json
import os
import HtmlTestRunner


json_dir = os.path.join(os.getcwd(), 'json-files')
with open(os.path.join(json_dir, 'test_run.json'), encoding='utf-8') as info:
    test_info = json.load(info)
avail_testCases = test_info["Available_testcase"]
avail_testSuites = test_info["Available_testsuites"]
suite_info = test_info["suite_info"]

def get_userInput():
    u_input = input("Welcome to 'UHI Automation Testing', Please enter \
        'TC' for executing a test case (or) \
            'TS' for executing a test suite (or) \
                'All' to execute all test cases: ")
    if u_input == "TC":
        print("You have entered: " + u_input +
              ", the available 'test cases' are, ")
        for opt in avail_testCases:
            print(opt)
        tc_input = input(
            "Please enter a 'test case' name from above options: ")
        for opt in avail_testCases:
            if opt == tc_input:
                confirm = input("You have entered: " + tc_input +
                                ", If you want to continue, please enter [y]: ")
                if confirm == 'y':
                    return tc_input
                else:
                    print("Test execution is interrupted, Please try again.")
                    break
        else:
            print("Entered 'Test Case' name is not available. Please try again.")
    elif u_input == "TS":
        print("You have entered: " + u_input +
              ", the available 'test suites' are, ")
        for opt in avail_testSuites:
            print(opt)
        ts_input = input(
            "Please enter a 'test suite' name from above options: ")
        for opt in suite_info:
            if opt == ts_input:
                print("You have entered 'Test Suite' name: " + ts_input +
                      ", This contains following 'test cases', ")
                for opt in suite_info[ts_input]:
                    print(opt)
                confirm = input(
                    "If you want to run this suite, please enter [y] to continue: ")
                if confirm == 'y':
                    return ts_input
                else:
                    print("Test execution is interrupted, Please try again.")
                    break
        else:
            print("Entered 'Test Suite' name is not available, please try again.")
    elif u_input == "All":
        all_input = input("You have entered: " + u_input +
                          ". This will execute all the 'test cases'. If you want to continue, please enter [y]: ")
        if all_input == 'y':
            return u_input
        else:
            print("Test execution is interrupted, Please try again.")
    else:
        print("The entered option is not available. Please try again.")

user_input = get_userInput()
if not user_input == None:
    from src.test_cases import DefTestCases

    #### Test case order from excel ####
    def suite_order():
        suite = unittest.TestSuite()
        for tc in avail_testCases:
            suite.addTest(DefTestCases(tc))
        return suite

    #### Individual screens test cases ####
    def suite_Settings():
        suite = unittest.TestSuite()
        for setting in suite_info["suite_Settings"]:
            suite.addTest(DefTestCases(setting))
        return suite

    def suite_Home():
        suite = unittest.TestSuite()
        for home in suite_info["suite_Home"]:
            suite.addTest(DefTestCases(home))
        return suite

    def suite_Analyse():
        suite = unittest.TestSuite()
        for analyse in suite_info["suite_Analyse"]:
            suite.addTest(DefTestCases(analyse))
        return suite

    def suite_EasyOn():
        suite = unittest.TestSuite()
        for easyOn in suite_info["suite_EasyOn"]:
            suite.addTest(DefTestCases(easyOn))
        return suite

    #### Profiles test cases ####
    def suite_Profile():
        suite = unittest.TestSuite()
        for profile in suite_info["suite_Profile"]:
            suite.addTest(DefTestCases(profile))
        return suite

    #### Anheizprogramm test cases ####
    def suite_Anheizprogramm():
        suite = unittest.TestSuite()
        for anheiz in suite_info["suite_Anheizprogramm"]:
            suite.addTest(DefTestCases(anheiz))
        return suite

    suites = {
        'suite_Settings': suite_Settings,
        'suite_Home': suite_Home,
        'suite_Analyse': suite_Analyse,
        'suite_EasyOn': suite_EasyOn,
        'suite_Profile': suite_Profile,
        'suite_Anheizprogramm': suite_Anheizprogramm
    }

    #### Specific test case ####
    def suite_individual(test_case):
        suite = unittest.TestSuite()
        suite.addTest(DefTestCases(test_case))
        return suite

    if __name__ == "__main__":
        runner = HtmlTestRunner.HTMLTestRunner(output="Test_Reports")
        if user_input == "All":
            runner.run(suite_order())
        for opt in avail_testSuites:
            if opt == user_input:
                runner.run(suites[user_input]())
        for opt in avail_testCases:
            if opt == user_input:
                runner.run(suite_individual(user_input))