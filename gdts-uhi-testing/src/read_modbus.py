from src.Define_functions import Define_functions
from pyModbusTCP.client import ModbusClient
from pyModbusTCP import utils
import time
import json, os


class ReadModbusTCP(Define_functions):
    """
    This is a class for Reading *Modbus* Anlalog, Integer registers and Digital coils. In this class, the modbus values are read via `pymodbusTCP <https://pymodbustcp.readthedocs.io/en/latest/#>`_
    library.

    * :py:meth:`src.read_modbus.ReadModbusTCP.connect_tcp`
    * :py:meth:`src.read_modbus.ReadModbusTCP.read_analog_registers`
    * :py:meth:`src.read_modbus.ReadModbusTCP.read_integer_registers`
    * :py:meth:`src.read_modbus.ReadModbusTCP.read_digital`
    * :py:meth:`src.read_modbus.ReadModbusTCP.get_values_from_analog_registers`
    * :py:meth:`src.read_modbus.ReadModbusTCP.get_values_from_integer_registers`
    * :py:meth:`src.read_modbus.ReadModbusTCP.get_status_from_coils`

    """
    json_dir = os.path.join(Define_functions().path_to_dir, 'json-files')
    
    with open(os.path.join(json_dir, 'test_data.json'), encoding='utf-8') as data:
        _data = json.load(data)
    pCO_info = _data["pCoInfo"]

    SERVER_HOST = pCO_info["host"]
    SERVER_PORT = pCO_info["port"]
    OFFSET = pCO_info["offset"]

    def connect_tcp(self):
        """
        Make a connection to Modbus via TCP/IP protocol with given host and port in the 'test_data.json' file under "pCoInfo" object.

        Returns
        -------
        Boolean
        """
        try:
            return  ModbusClient(host=self.SERVER_HOST, port=self.SERVER_PORT,
                             auto_open=True, auto_close=True)
        except ValueError:
            print("Error with host or port params")

    def read_analog_registers(self, data_type, register_ID, factor):
        """
        Returns an Analog value from given register ID with factor

        Parameters
        ----------
        data_type
            Data type as an index of the register ID (i.e., A102)
        register_ID
            Desired register ID for Modbus read request
        factor
            Factoring scale for the particular read value

        Returns
        -------
        'float'
            Modbus register value
        """
        c = self.connect_tcp()
        if data_type == "A":
            value = c.read_input_registers(int(register_ID), reg_nb=1)
            return int(value[0]) / int(factor)

    def read_integer_registers(self, data_type, register_ID):
        """
        Returns an Integer value from given register ID 

        Parameters
        ----------
        data_type
            Data type as an index of the register ID (i.e., I502)
        register_ID
            Desired register ID for Modbus read request

        Returns
        -------
        'int'
            Modbus register value
        """
        c = self.connect_tcp()
        if data_type == "I":
            register_ID = int(register_ID) + int(self.OFFSET)
            value = c.read_input_registers(int(register_ID), reg_nb=1)
            return value[0]

    def read_digital(self, data_type, coil_ID):
        """
        Returns a Boolean value from given coil ID 
        
        Parameters
        ----------
        data_type
            Data type as an index of the register ID (i.e., D201)
        coil_ID
            Desired coil ID for Modbus read request

        Returns
        -------
        'Boolean'
        """
        if data_type == "D":
            c = self.connect_tcp()
            value = c.read_coils(int(coil_ID), bit_nb=1)
            return value[0]

    def write_register(self, data_type, register_ID, register_value):
        c = self.connect_tcp()
        if data_type == "A":
            return c.write_single_register(register_ID, register_value)

    def get_values_from_analog_registers(self, vars, factor):
        """
        Returns an Analog value for given object from "All_registers.json" file
        
        Parameters
        ----------
        vars
            Targeted object from "All_registers.json" file
        factor
            Scale to get desired value

        Returns
        -------
        'float'
            Modbus Registers
        """
        
        return self.read_analog_registers(vars[0], vars[1:], factor)

    def get_values_from_integer_registers(self, vars):
        """
        Returns an integer value for given object from "All_registers.json" file
        
        Parameters
        ----------
        vars
            Targeted object from "All_registers.json" file

        Returns
        -------
        'int'
            Modbus Registers
        """
        return self.read_integer_registers(vars[0], vars[1:])

    def get_status_from_coils(self, vars):
        """
        Returns a Boolean value for given object from "All_registers.json" file
        
        Parameters
        ----------
        vars
            Targeted object from "All_registers.json" file

        Returns
        -------
        'Boolean'
        """
        return self.read_digital(vars[0], vars[1:]) 

if __name__ == "__main__":
    tcp = ReadModbusTCP()
    print(tcp.read_digital("D", 1613))