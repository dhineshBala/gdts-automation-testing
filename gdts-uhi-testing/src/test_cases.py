from Profile_functions.Profile_general_functions import ProfileGeneralFunctions
import unittest
import time
import random
import requests
import timeit
import os
import socket
from datetime import datetime
from src.logger import pass_log
from src.logger import fail_log
from time import ctime

PASS = pass_log.info
FAIL = fail_log.error
_config = ProfileGeneralFunctions()._config
_prof = ProfileGeneralFunctions()

skip_msg = " test case is skipped because of WPM config"

_content = ProfileGeneralFunctions()._content
HOME = _content["HOME"]
ANALYSIS = _content["ANALYSIS"]
SETTINGS = _content["SETTINGS"]
EASYON = _content["EASYON"]


class DefTestCases(ProfileGeneralFunctions):
    """
    This class contains collective test steps of all test cases in the right order to execute. This class is
    derived from 'General_functions' class which contains almost all the instructions to perform this testing.
    Also, there are more 'Helping' classes are derived in future to reduce the complexity of the script
    and easy for maintainability.

    * :py:meth:`src.test_cases.DefTestCases.setUpClass`
    * :py:meth:`src.test_cases.DefTestCases.setUp`
    * :py:meth:`src.test_cases.DefTestCases.tearDown`
    * :py:meth:`src.test_cases.DefTestCases.tearDownClass`
    * :py:meth:`src.test_cases.DefTestCases.test_network`
    * :py:meth:`src.test_cases.DefTestCases.test_date_and_time`
    * :py:meth:`src.test_cases.DefTestCases.test_sprache_region`
    * :py:meth:`src.test_cases.DefTestCases.test_kontroll_heizstab`
    * :py:meth:`src.test_cases.DefTestCases.test_Kontrolle_Mischer_Ventile`
    * :py:meth:`src.test_cases.DefTestCases.test_Funktionskontrolle_Pumpe`
    * :py:meth:`src.test_cases.DefTestCases.test_Warmwasser_Einstellungen`
    * :py:meth:`src.test_cases.DefTestCases.test_software_update`
    * :py:meth:`src.test_cases.DefTestCases.test_screen_lock`
    * :py:meth:`src.test_cases.DefTestCases.test_wireless_hotspot`
    * :py:meth:`src.test_cases.DefTestCases.test_laufzeit_und_taktungen`
    * :py:meth:`src.test_cases.DefTestCases.test_anlagendaten`
    * :py:meth:`src.test_cases.DefTestCases.test_Raumregler_Einstellungen`
    * :py:meth:`src.test_cases.DefTestCases.test_Festwert_Einstellungen`
    * :py:meth:`src.test_cases.DefTestCases.test_Aussentemperatur_Einstellungen`
    * :py:meth:`src.test_cases.DefTestCases.test_Funktionsheizen`
    * :py:meth:`src.test_cases.DefTestCases.test_Belegreifheizen`
    * :py:meth:`src.test_cases.DefTestCases.test_Waermemenge`
    * :py:meth:`src.test_cases.DefTestCases.test_Funktionssperren`
    * :py:meth:`src.test_cases.DefTestCases.test_Betriebsmodus`
    * :py:meth:`src.test_cases.DefTestCases.test_Betriebsdaten`
    * :py:meth:`src.test_cases.DefTestCases.test_Systemzustand`
    * :py:meth:`src.test_cases.DefTestCases.test_Profile`

    """
    @classmethod
    def setUpClass(self):
        """
        This method is called only once before the 'test execution' started. In our case, we are using this function to open the Web browser for the 
        automation testing.

        """
        # generic variables
        self.base_url = self.Url_info["Url"]
        # text for search
        self._content = _content
        self._api = ProfileGeneralFunctions()._data["APIservices"]

    def setUp(self):
        """
        This method is called before each test case is executed in "DefTestCases" class. This is helpful to refresh the browser to continue the test
        execution.

        """
        self.browser.get(self.uhi_url)
        self.assertEqual(self.browser.title, "System M")

    def tearDown(self):
        pass

    @classmethod
    def tearDownClass(self):
        """
        This method is called only once before the 'test execution' completed. In our case, we are using this function to close the Web browser for the 
        automation testing.

        """
        self.browser.close()

    def test_network(self):
        """
        This function executes the following test steps within UHI "Network" tiles with help of test automation and records the failed test steps in **Failed.log**

        .. raw:: html

                   <iframe src="D:\\Project\\31138_AutomationTesting\\gdts-automation-testing\\gdts-uhi-testing\\docs\\_test_cases\\test_network.html" height="345px" width="100%"></iframe>
        """
        from Helping_functions.Netzwerk import TestNetzwerk
        test_case = "UHI-01: 'Netzwerk'"
        target_screen = SETTINGS
        target_tile = self._content["NETWORK"]
        IPV4 = "IPv4"
        IPV6 = "IPv6"

        step = self.initial_steps(test_case, target_screen, target_tile)
        
        try:
            host_ip = socket.gethostbyname(self.base_url)
        except:
            host_ip = self.base_url

        self.wait_browser(test_case, 1, step)

        # verify IPv4
        self.click_dropdown_by_title1(test_case, IPV4, step)
        TestNetzwerk().verify_IPv4(test_case, host_ip, step)

        # verify IPv6
        self.click_dropdown_by_title1(test_case, IPV6, step)
        TestNetzwerk().verify_IPv6(test_case, step)

        # verify Netwerk-Analyse
        self.click_dropdown_by_title1(
            test_case, _content["NETWORK_ANALYSIS"], step)
        TestNetzwerk().verify_Netzwerk_status(test_case, step)

        step += 1

        # verify tile status
        self.click_close_icon(test_case, step)
        self.status_tile(test_case, target_screen,
                         target_tile, "OK", step)

    def test_date_and_time(self):
        """
                This function executes the following test steps and verifies the "Date and Time" values with *WPM* with help of test automation and records the failed test steps in **Failed.log**

        .. raw:: html

                   <iframe src="D:\\Project\\31138_AutomationTesting\\gdts-automation-testing\\gdts-uhi-testing\\docs\\_test_cases\\test_date_and_time.html" height="345px" width="100%"></iframe>

        * :py:class:`Helping_functions.date_and_time.DateAndTime`
                """
        from Helping_functions.date_and_time import DateAndTime
        test_case = "UHI-02: 'Datum und Uhrzeit'"
        target_screen = SETTINGS
        target_tile = _content["DATE_AND_TIME"]
        dat = DateAndTime()

        step = self.initial_steps(test_case, target_screen, target_tile)

        dat.test_Automatik_timezone(test_case, step)
        
        ##### Old version #####
        # # switching to 'Automatik' mode
        # dat.switch_to_Automatik_date_time_mode(test_case, step)

        # active_mode = dat.get_active_element_from_UHI(test_case, step).text
        # if active_mode == _content["AUTO"]:
        #     dat.get_Automatik_date_time_from_ntp(test_case, step)
        #     self.wait_browser(test_case, 20, step)

        #     dat.verify_WPM_date(test_case, active_mode, step)
        #     dat.verify_WPM_time(test_case, active_mode, step)

        # # switching to "Manuell" mode
        # dat.switch_to_Manuell_date_time_mode(test_case, step)

        # active_mode = dat.get_active_element_from_UHI(test_case, step).text
        # if dat.get_active_element_from_UHI(test_case, step).text == _content["MANUAL"]:
        #     dat.set_and_verify_Manuell_time(test_case, active_mode)

        # dat.switch_to_Automatik_date_time_mode(test_case, step)

    def test_sprache_region(self):
        """
                This function executes the following test steps within *UHI* "language" tile and verify the changes are applied to all over UHI with help of test automation and records the failed test steps in **Failed.log**

        .. raw:: html

                   <iframe src="D:\\Project\\31138_AutomationTesting\\gdts-automation-testing\\gdts-uhi-testing\\docs\\_test_cases\\test_sprache_region.html" height="345px" width="100%"></iframe>
                """
        from Helping_functions.language_and_region import LanguageAndRegion
        test_case = "UHI-03: 'Sprache und Region'"
        target_screen = SETTINGS
        target_tile = _content["LANGUAGE_REGION_AND_APPEARANCE"]
        lar = LanguageAndRegion()

        step = self.initial_steps(test_case, target_screen, target_tile)

        # open the dropdown
        # self.click_dropdown_by_title1(test_case, _content["LANGUAGE"], step)

        # UHI languages
        count = 0
        for ele in self.findElementsCSS(test_case, lar.lang_und_regio["inactive"], step):
            lar.select_languages_from_list(test_case, count, "English", step)
            # lar.select_languages_from_list(test_case, count, "Polski", step)
            # lar.select_languages_from_list(test_case, count, "Français", step)
            # lar.select_languages_from_list(test_case, count, "Italiano", step)
            count += 1

        # select back to Deutsch for future testing
        count = 0
        for ele in self.findElementsCSS(test_case, lar.lang_und_regio["inactive"], step):
            lar.select_languages_from_list(test_case, count, "Deutsch", step)
            count += 1

    def test_kontroll_heizstab(self):
        """
                This function executes the following test steps regarding "Funktionskontroll Heizstab" and verifies the values with *WPM* with help of test automation and records the failed test steps in **Failed.log**	

        .. raw:: html

                   <iframe src="D:\\Project\\31138_AutomationTesting\\gdts-automation-testing\\gdts-uhi-testing\\docs\\_test_cases\\test_kontroll_heizstab.html" height="345px" width="100%"></iframe>
                """
        from Helping_functions.function_control import FunctionControl
        test_case = "UHI-04: 'Kontrolle Heizstab'"
        target_screen = ANALYSIS
        target_tile = "Kon­troll­funk­tio­nen Heiz­stab"
        fc = FunctionControl()

        step = self.initial_steps(test_case, target_screen, target_tile)

        self.waitByCss(test_case, self.buttons["toggle_btn"], 20, step)
        time.sleep(3)

        # check Heizstab based on config
        fc.check_options_from_config(
            test_case, not fc.Fun_Code_Biv(), "E10.1 E-Heiz­stab", step)
        fc.check_options_from_config(
            test_case, fc.Fun_Code_Biv(), "E10.2-3 Bi­va­lent", step)
        step += 1

        # 5 check on PGD
        self.wait_browser(test_case, 6, step)

        fc.verify_results(test_case, True, fc.Funkt_Kontr_Aktiv(
        ), "Funktionkontroll is not activated in WPM", step)
        fc.verify_results(test_case, True, fc.P_FK_ZWE(
        ), "E10.1 E-Heiz­stab is not activated in WPM", step)
        fc.verify_results(test_case, fc.Fun_Code_Biv(), fc.P_FK_ZWE(
        ), "E10.2 11_NO2 is not activated in WPM", step)

        factor = fc.P_FK_Zeit_Ein() + 30
        self.wait_browser(test_case, factor/5, step)
        step += 1

        fc.verify_toggle_after_time(test_case, step)

    def test_Kontrolle_Mischer_Ventile(self):
        """
                This function executes the following test steps regarding "Funktionskontroll Mischer und Ventile" and verifies the values with *WPM* with help of test automation and records the failed test steps in **Failed.log**	

        .. raw:: html

                   <iframe src="D:\\Project\\31138_AutomationTesting\\gdts-automation-testing\\gdts-uhi-testing\\docs\\_test_cases\\test_Kontrolle_Mischer_Ventile.html" height="345px" width="100%"></iframe>
                """
        from Helping_functions.function_control import FunctionControl
        test_case = "UHI-05: 'Kontrolle Mischer/Ventile'"
        target_screen = ANALYSIS
        target_tile = "Kon­troll­funk­tio­nen Mi­scher und Ven­ti­le"
        fc = FunctionControl()

        step = self.initial_steps(test_case, target_screen, target_tile)

        # wait untill element is visible
        self.waitByCss(test_case, self.buttons["toggle_btn"], 20, step)
        time.sleep(3)

        # Check no of Heizkreis to know the Mischers
        fc.check_options_from_config(
            test_case, _config.Fun_Code_Hk1(), _content["P_FK_Mi_Hk1"], step)
        fc.check_options_from_config(
            test_case, _config.Fun_Code_Hk2(), _content["P_FK_Mi_Hk2"], step)
        fc.check_options_from_config(
            test_case, _config.Fun_Code_Biv(), _content["P_FK_Mi_Biv"], step)
        fc.check_options_from_config(
            test_case, _config.Fun_Code_Reg(), _content["P_FK_Mi_Reg"], step)
        step += 1

        self.wait_browser(test_case, 6, step)

        fc.verify_results(test_case, _config.Fun_Code_Hk1(), fc.HK1_Msicher_status(
        ), "Mi­scher HK 1 M21.1 is not activated in WPM", step)
        fc.verify_results(test_case, _config.Fun_Code_Hk2(), fc.HK2_Msicher_status(
        ), "Mi­scher HK 2 M21.2 is not activated in WPM", step)
        fc.verify_results(test_case, _config.Fun_Code_Biv(), fc.Biv_Mischer_status(
        ), "Mi­scher Bivalent M26 is not activated in WPM", step)
        fc.verify_results(test_case, _config.Fun_Code_Reg(), fc.Reg_Mischer_status(
        ), "Mi­scher Regenerativ is not activated in WPM", step)

        factor = fc.get_max_Mischerlaufzeit_WPM() * 2 * 60
        self.wait_browser(test_case, factor/5, step)

        if self.get_screen_saver_clock_status(test_case, step):
            self.initial_steps(test_case, target_screen, target_tile)
        step += 1

        fc.verify_toggle_after_time(test_case, step)

    def test_Funktionskontrolle_Pumpe(self):
        """
                This function executes the following test steps regarding "Funktionskontroll Pumpe" and verifies the values with *WPM* with help of test automation and records the failed test steps in **Failed.log**	

        .. raw:: html

                   <iframe src="D:\\Project\\31138_AutomationTesting\\gdts-automation-testing\\gdts-uhi-testing\\docs\\_test_cases\\test_Funktionskontrolle_Pumpe.html" height="345px" width="100%"></iframe>
                """
        from Helping_functions.function_control import FunctionControl
        test_case = "UHI-06: 'Funktionskontrolle Pumpe'"
        target_screen = ANALYSIS
        target_tile = "Kon­troll­funk­tio­nen Pum­pe"
        fc = FunctionControl()

        step = self.initial_steps(test_case, target_screen, target_tile)

        # wait untill element is visible
        self.waitByCss(test_case, self.buttons["toggle_btn"], 20, step)
        time.sleep(3)

        # Check no of Heizkreis to know the Mischers
        fc.check_options_from_config(
            test_case, True, "Er­zeu­ger­krei­s­pum­pe M16", step)
        fc.check_options_from_config(
            test_case, _config.Fun_Code_Hk1(), "Heiz­krei­s­pum­pe HK 1 M13", step)
        fc.check_options_from_config(
            test_case, _config.Fun_Code_Hk2(), "Heiz­krei­s­pum­pe HK 2 M15", step)
        fc.check_options_from_config(
            test_case, _config.Fun_Code_Ww(), "Warm­was­ser­la­de­pum­pe M18", step)
        if _config.get_Ww_Zirkulation_status() == 2:
            fc.check_options_from_config(
                test_case, True, "Zir­ku­la­ti­ons­pum­pe M24", step)
        fc.check_options_from_config(
            test_case, True, "Ven­ti­la­tor M2", step)

        time.sleep(fc.P_FK_Zeit_Ein() - 10)
        self.close_error_popup(test_case, step)

        fc.verify_results(test_case, True, fc.P_FK_ZUP(
        ), "Er­zeu­ger­krei­s­pum­pe M16 is not activated in WPM", step)
        fc.verify_results(test_case, _config.Fun_Code_Hk1(), fc.P_FK_HUP1(
        ), "Heiz­krei­s­pum­pe HK 1 M13 is not activated in WPM", step)
        fc.verify_results(test_case, _config.Fun_Code_Hk2(), fc.P_FK_HUP2(
        ), "Heiz­krei­s­pum­pe HK 2 M15 is not activated in WPM", step)
        fc.verify_results(test_case, _config.Fun_Code_Ww(), fc.P_FK_WUP(
        ), "Warm­was­ser­la­de­pum­pe M18 is not activated in WPM", step)
        if _config.P_WW() == 2:
            fc.verify_results(test_case, True, fc.P_FK_ZWUP(
            ), "Zir­ku­la­ti­ons­pum­pe M24 is not activated in WPM", step)
        fc.verify_results(test_case, True, fc.P_FK_VEN(
        ), "Ven­ti­la­tor M2 is not activated in WPM", step)

        factor = fc.P_FK_Zeit_Ein() + 60
        self.wait_browser(test_case, factor/5, step)
        step += 1

        fc.verify_toggle_after_time(test_case, step)

    @unittest.skipIf(_config.Fun_Code_Ww() == False, "Warmwasser Einstellungen" + skip_msg)
    def test_Warmwasser_Einstellungen(self):
        """
        This function executes the following test steps regarding "Warmwasser tile settings" and verifies the values with *WPM* with help of test automation and records the failed test steps in **Failed.log**. Also this method will be skipped if the *WPM* config doesn't contain Warmwasser in Funktionscode.	

        .. raw:: html

                   <iframe src="D:\\Project\\31138_AutomationTesting\\gdts-automation-testing\\gdts-uhi-testing\\docs\\_test_cases\\test_Warmwasser_Einstellungen.html" height="345px" width="100%"></iframe>
                """
        from Profile_functions.warmwasser_setting import WarmwasserSetting
        test_case = "UHI-07: 'Warmwasser Einstellungen'"
        target_screen = HOME
        target_tile = self._content["WARMWATER"]
        Ww = WarmwasserSetting()

        if _config.Fun_Code_Ww:
            self.creat_profile_for_testing_Home_screen(test_case, 1)
            self.wait_browser(test_case, 12, 3)
            step = self.initial_steps(
                test_case, target_screen, target_tile)

            element_ID = 0
            Ww.set_WW_Soll_temp_in_UHI(test_case, element_ID, step)
            step += 1

            # click on down triangle
            if _prof.click_down_triangle(test_case, element_ID, step):
                self.click_down_triangle(test_case, element_ID, step)

            _prof.create_profile_Home_screen(test_case, element_ID, step)

            # # verify the options
            time.sleep(2)

            sub_opt1 = self._content["THERMAL_LOCK"]
            # self._content["THERMAL_DESINFECTION"]
            sub_opt2 = "Ther­mi­sche Des­in­fek­ti­on"
            # self._content["CIRCULATION"] # "Zirkulation" text is wrong in "de.common.json" file
            sub_opt3 = "Zir­ku­la­ti­on"
            sub_opt4 = self._content["REHEATING"]

            _prof.verify_text_from_element_row(
                test_case, sub_opt1, target_tile, step)
            _prof.verify_text_from_element_row(
                test_case, sub_opt2, target_tile, step)

            # warmwasser sperre
            self.click_right_arrow_by_name(test_case, sub_opt1, step)
            Ww.choose_WwSperre_days(test_case, step)
            self.click_back_by_header_title(test_case, sub_opt1, step)

            # Zirkulation
            if _config.get_Ww_Zirkulation_status() == 2:
                self.click_right_arrow_by_name(test_case, sub_opt3, step)
                Ww.choose_Zirkulation_days(test_case, step)
                self.click_back_by_header_title(test_case, sub_opt3, step)

            # Reset Maximal Warmwasser Temperatur
            Ww.Reset_WW_Max_temp(test_case, step)

            # Zeitliche Nacherwarmung
            if not _config.Fun_Code_Biv():
                self.click_right_arrow_by_name(test_case, sub_opt4, step)
                Ww.choose_WwTherDes_days(test_case, step)
                self.wait_browser(test_case, 10, step)

            # verify Max WW Reset
            Ww.verify_Reset_WW_Max_temp_with_WPM(test_case, step)

            # Thermische Desinfek
            time.sleep(1)
            if _config.Fun_Code_Biv():
                self.click_right_arrow_by_name(test_case, sub_opt2, step)
                Ww.choose_WwTherDes_days(test_case, step)
                self.wait_browser(test_case, 10, step)

                # Verify Thermische Desinfektion
                Ww.verify_WwTherDes_times(test_case, step)
                Ww.verify_WwTherDes_temps_in_days_screen(test_case, step)
                Ww.verify_WwTherDes_days(test_case, step)
                self.click_back_by_header_title(test_case, sub_opt2, step)

            # Verify Zeitliche Nacherwarmung
            if not _config.Fun_Code_Biv():
                Ww.verify_WwTherDes_times(test_case, step)
                Ww.verify_WwTherDes_temps_in_days_screen(test_case, step)
                Ww.verify_WwTherDes_days(test_case, step)
                self.click_back_by_header_title(test_case, sub_opt4, step)

            # verify WW Sperre
            self.click_right_arrow_by_name(test_case, sub_opt1, step)
            Ww.verify_WwSperre_temps_in_days_screen(test_case, step)
            Ww.verify_WwSperre_times(test_case, step)
            Ww.verify_WwSperre_days(test_case, step)
            self.click_back_by_header_title(test_case, sub_opt1, step)

            # verify Zirkulation
            if _config.get_Ww_Zirkulation_status() == 2:
                self.click_right_arrow_by_name(test_case, sub_opt3, step)
                Ww.verify_Zirkulation_with_WPM(test_case, step)
                self.click_back_by_header_title(test_case, sub_opt3, step)

            # verify Warmwasser screen
            self.click_less_option_button(test_case, step)
            time.sleep(2)
            Ww.check_WwSperre_screen(test_case, element_ID, step)
        else:
            PASS(test_case + skip_msg)
            FAIL(test_case + skip_msg)

    def test_software_update(self):
        """
                This function executes the following test steps within *UHI* "Software Update" tile and verify if 'Software' update is available with help of test automation and records the failed test steps in **Failed.log**

        .. raw:: html

                   <iframe src="D:\\Project\\31138_AutomationTesting\\gdts-automation-testing\\gdts-uhi-testing\\docs\\_test_cases\\test_software_update.html" height="345px" width="100%"></iframe>
                """

        test_case = "UHI-08: 'Software Update'"
        target_screen = SETTINGS
        target_tile = "Soft­ware Up­date"

        step = self.initial_steps(test_case, target_screen, target_tile)

        uhi_ver = self.get_text_from_row(
            test_case, "Ver­si­on", step).strip(" OS ")
        uhi_uptd_date = self.get_text_from_row(test_case, "Da­tum", step)

        # get version from api
        json_data = self.get_response_from_api(
            "UHI version", self._api["uhi_ver"], step)
        api_uhi_ver = json_data["uhi"]["version"]
        api_uhi_uptd_date = json_data["uhi"]["date"]["formatted"]
        if os.name == "nt":  # detect if os is windows
            de_format = datetime.strptime(
                api_uhi_uptd_date, '%Y-%m-%dT%H:%M:%S').strftime('%#d.%#m.%Y')
        else:  # linux and others
            de_format = datetime.strptime(
                api_uhi_uptd_date, '%Y-%m-%dT%H:%M:%S').strftime('%-d.%-m.%Y')

        self.compare_results(
            test_case, api_uhi_ver[:4], uhi_ver, "Software version in UHI and API are not same", step)
        self.compare_results(test_case, de_format, uhi_uptd_date,
                             "Software updated date in UHI and API are not same", step)

        # 4 case 1: software up to date
        self.findElementCSS(
            test_case, self.buttons["update_btn"], step).click()
        self.waitByCss(test_case, self.buttons["btn"], 15, step)
        json_data = self.get_response_from_api(
            "Update check", self._api["uhi_update_check"], step)
        check_update = json_data["available"]
        if check_update == False:
            for t in self.findElementsCSS(test_case, self.texT["value"], step):
                if t.text == "Ihr Sys­tem ist be­reits auf dem ak­tu­el­len Stand.":
                    PASS("UHI is up-to-date with version: " + str(api_uhi_ver))
        if check_update == True:
            avail_ver = json_data["latest"]["version"]
            PASS("An update is available for UHI to version: " +
                 str(avail_ver) + " , Please carry on update manually")
        step += 1

    def test_screen_lock(self):
        """
                This function executes the following test steps within *UHI* "Bildschrim Sperre" tile and verify the Automatik lock time in UHI with help of test automation and records the failed test steps in **Failed.log**

        .. raw:: html

                   <iframe src="D:\\Project\\31138_AutomationTesting\\gdts-automation-testing\\gdts-uhi-testing\\docs\\_test_cases\\test_screen_lock.html" height="345px" width="100%"></iframe>
                """
        from Helping_functions.screen_lock import ScreenLock
        test_case = "UHI-09: 'Bildschirm Sperre'"
        target_screen = SETTINGS
        target_tile = "Bild­schirm Sper­re"
        sl = ScreenLock()

        step = self.initial_steps(test_case, target_screen, target_tile)

        lock_time = sl.select_lock_time(test_case, step)
        self.click_close_icon(test_case, step)

        self.status_tile(test_case, target_screen,
                         target_tile, lock_time, step)
        sl.check_auto_lock_time(test_case, lock_time, step)

        ### Vergessen is removed from UHI screen ###
        # sl._4th_step(test_case, target_screen, target_tile, step)

        # # 8 click on Logout
        # self.findElementCSS(test_case, self.buttons["info"], step).click()
        # self.compare_results(test_case, True, self.findElementClass(test_case,
        #                                                             "clock", step).is_displayed(), "Clock is not displayed after user locked the screen", step)
        # step += 1

        # # 9 click on clock
        # self.tapClock(test_case, step)
        # step += 1

        # # 10 Klick auf Vergessen oben links.
        # for e in self.findElementsCSS(test_case, self._text["area"], step):
        #     if e.text == "Ver­ges­sen":
        #         e.click()
        #         time.sleep(2)
        #         forgot_text = self.findElementCSS(test_case,
        #                                           self.texT["value"], step).text
        # self.compare_results(test_case, 'Das Ent­sperr­mus­ter ist ein "M".',
        #                      forgot_text, "Forgot warning message is not as expected", step)
        # time.sleep(1)
        # self.findElementCSS(test_case, self.buttons["btn"], step).click()
        # step += 1

        # # 11 unlock UHI
        # self.unlockUserScreen(test_case, step)

    def test_wireless_hotspot(self):
        """
                This function executes the following test steps within *UHI* "Wireless Hotspot" tile and check the credentials are displayed correct with help of test automation and records the failed test steps in **Failed.log**

        .. raw:: html

                   <iframe src="D:\\Project\\31138_AutomationTesting\\gdts-automation-testing\\gdts-uhi-testing\\docs\\_test_cases\\test_wireless_hotspot.html" height="345px" width="100%"></iframe>
                """
        test_case = "UHI-10: 'Wireless Hotspot'"
        target_screen = SETTINGS
        target_tile = "Wire­less Hot­spot"
        Wlan = self._data["Wlan_info"]

        step = self.initial_steps(test_case, target_screen, target_tile)

        self.tap_button(test_case, self.buttons["wifi_button"], step)
        time.sleep(8)

        self.compare_results(test_case, Wlan["IP"], self.get_text_from_row(test_case,
                                                                           "IP", step), "IP is wrong with JSON", step)
        self.compare_results(test_case, Wlan["SSID"], self.get_text_from_row(test_case,
                                                                             "SSID", step), "SSID is wrong with JSON", step)
        self.compare_results(test_case, Wlan["Passwort"], self.get_text_from_row(test_case,
                                                                                 "Pass­wort", step), "Passwort is wrong with JSON", step)
        time.sleep(3)

        self.click_close_icon(test_case, step)
        time.sleep(1)

        self.status_tile(test_case, target_screen,
                         target_tile, "An", step)

        self._3rd_step(test_case, target_tile, step)
        self.tap_button(test_case, self.buttons["wifi_button"], step)
        time.sleep(5)

        self.click_close_icon(test_case, step)
        time.sleep(1)

        self.status_tile(test_case, target_screen,
                         target_tile, "Aus", step)

    def test_laufzeit_und_taktungen(self):
        """
                This function executes the following test steps within *UHI* "Laufzeiten und Taktungen" tile and check the values displayed in UHI are as same as with WPM with help of test automation and records the failed test steps in **Failed.log**

        .. raw:: html

                   <iframe src="D:\\Project\\31138_AutomationTesting\\gdts-automation-testing\\gdts-uhi-testing\\docs\\_test_cases\\test_laufzeit_und_taktungen.html" height="345px" width="100%"></iframe>
                """
        from Helping_functions.laufzeit_and_taktungen import LaufzeitUndTaktungen
        test_case = "UHI-11: 'Laufzeit und Taktungen'"
        target_screen = ANALYSIS
        target_tile = "Lauf­zei­ten und Tak­tun­gen"
        lnt = LaufzeitUndTaktungen()

        step = self.initial_steps(test_case, target_screen, target_tile)

        self.compare_results(
            test_case, "Lauf­zei­ten", self.findElementsCSS(test_case,
                                                            self.texT["section_title"], step)[0].text, "Lauf­zei­ten is not presented", step)
        self.compare_results(
            test_case, "Tak­tun­gen", self.findElementsCSS(test_case,
                                                           self.texT["section_title"], step)[1].text, "Tak­tun­gen is not presented", step)
        step += 1

        # Laufzeiten
        lnt.verify_laufzeit_taktungen(
            test_case, "Ver­dich­ter 1", lnt.BS_Vd1(), step)
        lnt.verify_laufzeit_taktungen(
            test_case, "Ven­ti­la­tor", lnt.BS_PupVen(), step)
        lnt.verify_laufzeit_taktungen(
            test_case, "2. Wär­me­er­zeu­ger", lnt.BS_ZWE(), step)

        # Taktungen
        lnt.verify_laufzeit_taktungen(
            test_case, "Hei­zen", lnt.BS_Vd1_SV_H(), step)

        if _config.Fun_Code_Ww():
            lnt.verify_sameName_laufzeit(
                test_case, "Warm­was­ser", lnt.BS_WUP(), step)
            lnt.verify_sameName_taktungen(
                test_case, "Warm­was­ser", lnt.BS_Vd1_SV_WW(), step)

        if _config.P_HK_1() > 1 or _config.P_HK_2() > 1:
            lnt.verify_laufzeit_taktungen(
                test_case, "Küh­len", lnt.BS_Vd1_SV_K(), step)

    def test_anlagendaten(self):
        """
                This function executes the following test steps within *UHI* "Anlagendaten" tile and check the values displayed in UHI are as same as with WPM with help of test automation and records the failed test steps in **Failed.log**

        .. raw:: html

                   <iframe src="D:\\Project\\31138_AutomationTesting\\gdts-automation-testing\\gdts-uhi-testing\\docs\\_test_cases\\test_anlagendaten.html" height="345px" width="100%"></iframe>
                """
        from Helping_functions.anlagen_daten import AnlagenDaten
        test_case = "UHI-12: 'Anlagendaten'"
        target_screen = ANALYSIS
        target_tile = "An­la­gen­da­ten"
        ad = AnlagenDaten()

        step = self.initial_steps(test_case, target_screen, target_tile)

        UHI_ver, serial_no, WPM_ver, KKR_ver, WQMF_ver = ad.get_uhi_versions_info(test_case,
                                                                                  step)

        self.compare_results(test_case, ad.get_uhi_version_from_api(
            step), UHI_ver, "UHI version is wrong", step)

        sn1, sn2, sn3 = ad.get_serial_no_from_WPM()
        self.compare_results(
            test_case, sn1, int(serial_no[0:4]), "Serial Nummer 0-4 is wrong", step)
        self.compare_results(
            test_case, sn2, int(serial_no[4:8]), "Serial Nummer 4-8 is wrong", step)
        self.compare_results(
            test_case, sn3, int(serial_no[8:12]), "Serial Nummer 8-12 is wrong", step)

        self.compare_results(
            test_case, ad.get_WPM_ver_from_WPM(), WPM_ver, "WPM version is wrong", step)
        self.compare_results(
            test_case, ad.get_KKR_ver_from_WPM(), KKR_ver, "KKR version is wrong", step)
        self.compare_results(
            test_case, ad.get_WQMF_ver_from_WPM(), float(WQMF_ver), "WQMF version is wrong", step)

    @unittest.skipIf(not _config.is_any_RTC(), "UHI-13 Raumregler Einstellungen" + skip_msg)
    def test_Raumregler_Einstellungen(self):
        """
                This function executes the following test steps to setup "Raumregler" tile and check the values displayed in UHI are as same as with WPM with help of test automation and records the failed test steps in **Failed.log**

        .. raw:: html

                   <iframe src="D:\\Project\\31138_AutomationTesting\\gdts-automation-testing\\gdts-uhi-testing\\docs\\_test_cases\\test_Raumregler_Einstellungen.html" height="345px" width="100%"></iframe>
                """
        from Profile_functions.raumregler_setting import RaumreglerSettings
        test_case = "UHI-13: 'Raumregler Einstellungen'"
        target_screen = HOME
        target_tile = self._content["ROOM_REGULATION"]

        rtc = RaumreglerSettings()

        if _config.is_any_RTC():
            self.creat_profile_for_testing_Home_screen(test_case, 1)
            self.wait_browser(test_case, 12, 3)
            step = self.initial_steps(
                test_case, target_screen, target_tile)
            
            HK1_Anz = _config.P_HK1_RT_thT_Anz()
            if _config.is_HK1_RTC():
                room_no = 50
                for Rooms in range(HK1_Anz):
                    screen_name = test_case + " => " + \
                        rtc.expected_screen_name(room_no)
                    rtc.setup_Raumregler_Soll_temp(
                        screen_name, Rooms, room_no, step)
                    rtc.setup_Raumregler_run_days(
                        screen_name, target_screen, target_tile, Rooms, room_no, step)
                    rtc.slide_haus_screen(screen_name, 0, step)
                    room_no += 1

            HK2_Anz = _config.P_HK2_RT_thT_Anz()
            if _config.is_HK2_RTC():
                room_no = 60
                for Rooms in range(HK1_Anz, HK1_Anz + HK2_Anz):
                    screen_name = test_case + " => " + rtc.expected_screen_name(room_no)
                    rtc.setup_Raumregler_Soll_temp(
                        screen_name, Rooms, room_no, step)
                    rtc.setup_Raumregler_run_days(
                        screen_name, target_screen, target_tile, Rooms, room_no, step)
                    rtc.slide_haus_screen(screen_name, 0, step)
                    room_no += 1

            self.wait_browser(test_case, 6, step)

            # verify temperatures and running days
            if _config.is_HK1_RTC():
                room_no = 50
                for Rooms in range(HK1_Anz):
                    screen_name = test_case + " => " + \
                        rtc.expected_screen_name(room_no)
                    rtc.verify_Raumregler_run_days_steps(
                        screen_name, target_screen, target_tile, Rooms, room_no, step)
                    rtc.slide_haus_screen(screen_name, 0, step)
                    room_no += 1

            # verify temperatures and running days
            if _config.is_HK2_RTC():
                room_no = 60
                for Rooms in range(HK1_Anz, HK1_Anz + HK2_Anz):
                    screen_name = test_case + " => " + \
                        rtc.expected_screen_name(room_no)
                    rtc.verify_Raumregler_run_days_steps(
                        screen_name, target_screen, target_tile, Rooms, room_no, step)
                    rtc.slide_haus_screen(screen_name, 0, step)
                    room_no += 1
        else:
            PASS(test_case + skip_msg)
            FAIL(test_case + skip_msg)

    @unittest.skipIf(not _config.is_any_Festwert(), "UHI-15 Festwert Einstellugen" + skip_msg)
    def test_Festwert_Einstellungen(self):
        """
                This function executes the following test steps to setup "Raumregler" tile and check the values displayed in UHI are as same as with WPM with help of test automation and records the failed test steps in **Failed.log**

        .. raw:: html

                   <iframe src="D:\\Project\\31138_AutomationTesting\\gdts-automation-testing\\gdts-uhi-testing\\docs\\_test_cases\\test_Festwert_Einstellungen.html" height="345px" width="100%"></iframe>
                """
        from Profile_functions.festwert_setting import FestwertSettings
        test_case = "UHI-15: 'Festwert Einstellungen'"
        target_screen = HOME
        target_tile = self._content["HOUSE"]
        context = "Festwert"
        fwr = FestwertSettings()

        if _config.is_any_Festwert():
            self.creat_profile_for_testing_Home_screen(test_case, 1)
            self.wait_browser(test_case, 12, 3)
            step = self.initial_steps(
                test_case, target_screen, target_tile)
            if _config.is_HK1_Festwert():
                element_ID, HK = 0, 1
                screen_name = test_case + " => " + fwr.expected_screen_name(HK)
                fwr.setup_Festwert_values(screen_name, HK, step)

            if _config.is_HK2_Festwert():
                element_ID, HK = 0, 2
                screen_name = test_case + " => " + fwr.expected_screen_name(HK)
                self.slide_haus_screen(screen_name, element_ID, step)
                fwr.setup_Festwert_values(screen_name, HK, step)
                if _config.is_HK1_Festwert():
                    self.slide_haus_screen(screen_name, element_ID, step)

            self.wait_browser(test_case, 6, step)

            # verify temperatures and running days
            if _config.is_HK1_Festwert():
                element_ID, HK = 0, 1
                screen_name = test_case + " => " + fwr.expected_screen_name(HK)
                fwr.verify_Festwert_screens(
                    screen_name, context, HK, element_ID, step)

            if _config.is_HK2_Festwert():
                element_ID, HK = 0, 2
                screen_name = test_case + " => " + fwr.expected_screen_name(HK)
                if _config.is_HK1_Festwert():
                    self.slide_haus_screen(screen_name, element_ID, step)
                fwr.verify_Festwert_screens(
                    screen_name, context, HK, element_ID, step)

        else:
            PASS(test_case + skip_msg)
            FAIL(test_case + skip_msg)

    @unittest.skipIf(not _config.is_any_AussenTemp(), "UHI-16 Aussentemperatur Einstellugen" + skip_msg)
    def test_Aussentemperatur_Einstellungen(self):
        """
                This function executes the following test steps to setup "Raumregler" tile and check the values displayed in UHI are as same as with WPM with help of test automation and records the failed test steps in **Failed.log**

        .. raw:: html

                   <iframe src="D:\\Project\\31138_AutomationTesting\\gdts-automation-testing\\gdts-uhi-testing\\docs\\_test_cases\\test_Aussentemperatur_Einstellungen.html" height="345px" width="100%"></iframe>
                """
        from Profile_functions.aussentemp_setting import AussenTempSettings
        test_case = "UHI-16: 'Außentemperatur Einstellungen'"
        target_screen = HOME
        target_tile = self._content["HOUSE"]
        context = self.HEIZKURVE
        aus = AussenTempSettings()

        if _config.is_any_AussenTemp():
            self.creat_profile_for_testing_Home_screen(test_case, 1)
            
            self.wait_browser(test_case, 12, 3)

            step = self.initial_steps(
                test_case, target_screen, target_tile)
            if _config.is_HK1_AussenTemp():
                element_ID, HK = 0, 1
                screen_name = test_case + " => " + aus.expected_screen_name(HK)
                aus.setup_Heizkurve_values(screen_name, context, HK, element_ID, step)

            if _config.is_HK2_AussenTemp():
                element_ID, HK = 0, 2
                self.slide_haus_screen(test_case, element_ID, step)
                screen_name = test_case + " => " + aus.expected_screen_name(HK)
                aus.setup_Heizkurve_values(screen_name, context, HK, element_ID, step)
                if _config.is_HK1_AussenTemp():
                    self.slide_haus_screen(screen_name, element_ID, step)

            self.wait_browser(test_case, 6, step)

            # verify temperatures and running days
            if _config.is_HK1_AussenTemp():
                element_ID, HK = 0, 1
                aus.verify_Heizkurve_screens(
                    test_case, context, HK, element_ID, step)

            if _config.P_HK_2() >= 1 and _config.P_HK2_REG() == 0:
                element_ID, HK = 0, 2
                if _config.is_HK1_AussenTemp():
                    self.slide_haus_screen(test_case, element_ID, step)
                aus.verify_Heizkurve_screens(
                    test_case, context, HK, element_ID, step)

        else:
            PASS(test_case + skip_msg)
            FAIL(test_case + skip_msg)

    def test_Funktionsheizen(self):
        """
                This function executes the following test steps to setup "Funktionsheizen" option and check the setup values in UHI are as same as with WPM with help of test automation and records the failed test steps in **Failed.log**

        .. raw:: html

                   <iframe src="D:\\Project\\31138_AutomationTesting\\gdts-automation-testing\\gdts-uhi-testing\\docs\\_test_cases\\test_Funktionsheizen.html" height="345px" width="100%"></iframe>
                """
        from Helping_functions.Anheizprogramm import AnheizProgramm
        test_case = "UHI-17: 'Funktionsheizen'"
        target_screen = EASYON
        target_tile = self._content["HEATING_PROGRAM"]

        Anheiz = AnheizProgramm()

        step = self.initial_steps(test_case, target_screen, target_tile)

        # 3 verify Funktionsheizen, Belegreifheizen options are presented
        self.check_option_from_row_text(
            test_case, Anheiz.opt_Funktionsheizen, step)
        self.check_option_from_row_text(
            test_case, Anheiz.opt_Belegreifheizen, step)
        step += 1

        # 4 Select the Funktionsheizen
        Anheiz.click_right_arrow(test_case, 0, step)
        Anheiz.check_Funktionsheizen_screen(test_case, step)
        step += 1

        # 5 Click on the right arrow next to max. Temperature.
        Anheiz.click_right_arrow(test_case, 0, step)
        step += 1

        # 6 Setting the temperature via + and - function.
        Anheiz.set_Anheizprogramm_Max_temp(test_case, step)
        Anheiz.click_back_by_header_title(test_case, Anheiz.opt_MaxTemp, step)
        step += 1

        # 7 Activate WW./schwimmbadbereitung over the slider
        Anheiz.activate_WwBereitung(test_case, step)
        step += 1

        # 8 Start heating program via the slider
        Anheiz.start_Anheizprogramm(test_case, step)
        step += 2

        # 10 Check whether the settings have been accepted correctly in the WPM. 
        Anheiz.verify_Funktionsheizen_in_WPM(test_case, step)

        # 11 Stop heating program via the slider in the UHI manually
        Anheiz.stop_Anheizprogramm(test_case, step)

        self.wait_browser(test_case, 8, step)

        Anheiz.verify_Funktionsheizen_in_WPM(test_case, step)

    def test_Belegreifheizen(self):
        """
                This function executes the following test steps to setup "Belegreifheizen" option and check the setup values in UHI are as same as with WPM with help of test automation and records the failed test steps in **Failed.log**

        .. raw:: html

                   <iframe src="D:\\Project\\31138_AutomationTesting\\gdts-automation-testing\\gdts-uhi-testing\\docs\\_test_cases\\test_Belegreifheizen.html" height="345px" width="100%"></iframe>
                """
        from Helping_functions.Anheizprogramm import AnheizProgramm
        test_case = "UHI-18: 'Belegreifheizen'"
        target_screen = EASYON
        target_tile = self._content["HEATING_PROGRAM"]

        Anheiz = AnheizProgramm()

        step = self.initial_steps(test_case, target_screen, target_tile)

        # 3 verify Funktionsheizen, Belegreifheizen options are presented
        self.check_option_from_row_text(
            test_case, Anheiz.opt_Funktionsheizen, step)
        self.check_option_from_row_text(
            test_case, Anheiz.opt_Belegreifheizen, step)
        step += 1

        # 4 Select the Funktionsheizen
        Anheiz.click_right_arrow(test_case, 1, step)
        Anheiz.check_Funktionsheizen_screen(test_case, step)
        step += 1

        # setup 'Belegreifheizen' options
        Anheiz.setup_Belegreifheizen(test_case, step)

        self.wait_browser(test_case, 6, step)

        Anheiz.verify_Belegreifheizen(test_case, step)

    def test_Waermemenge(self):
        """
                This function executes the following test steps to read  "Warmemenge" from WPM and verifies the values with *UHI* with help of test automation and records the failed test steps in **Failed.log**

        .. raw:: html

                   <iframe src="D:\\Project\\31138_AutomationTesting\\gdts-automation-testing\\gdts-uhi-testing\\docs\\_test_cases\\test_Waermemenge.html" height="345px" width="100%"></iframe>
                """
        from Helping_functions.warmemenge import Warmemenge
        test_case = "UHI-19: 'Wärmemenge'"
        target_screen = ANALYSIS
        target_tile = self._content["QUANTITIES_OF_HEAT"]
        Wm = Warmemenge()

        step = self.initial_steps(test_case, target_screen, target_tile)

        WPM_Ww = 0

        if _config.P_WW():
            UHI_Ww = (Wm.get_text_from_row(
                test_case, _content["WARMWATER"], step)).strip(" kWh")
            WPM_Ww = Wm.get_Warmwasser_value_from_WPM()
            Wm.compare_results(
                test_case, WPM_Ww, UHI_Ww, "Warmwasser value mismatch", step)

        UHI_Hz = (Wm.get_text_from_row(
            test_case, _content["HEAT"], step)).strip(" kWh")
        UHI_total = (Wm.get_text_from_row(
            test_case, _content["TOTAL"], step)).strip(" kWh")

        WPM_Hz = Wm.get_Heizen_value_from_WPM()
        WPM_total = int(WPM_Hz) + int(WPM_Ww)

        Wm.compare_results(
            test_case, WPM_Hz, UHI_Hz, "Heizen value mismatch", step)
        Wm.compare_results(
            test_case, WPM_total, int(UHI_total), "Gesamt value mismatch", step)

    def test_Funktionssperren(self):
        """
                This function executes the following test steps to setup "Funktionssperren" options based on 'user_input.json' and verifies with WPM with help of test automation and records the failed test steps in **Failed.log**

        .. raw:: html

                   <iframe src="D:\\Project\\31138_AutomationTesting\\gdts-automation-testing\\gdts-uhi-testing\\docs\\_test_cases\\test_Funktionssperren.html" height="345px" width="100%"></iframe>
                """
        from Helping_functions.Funktionssperren import FunktionsSperren
        test_case = "UHI-20: 'Funktionssperren'"
        target_screen = EASYON
        target_tile = self._content["FUNCTION_LOCKS"]
        FunkSperre = FunktionsSperren()

        step = self.initial_steps(test_case, target_screen, target_tile)

        # activate 'Funktionssperren' based on available config
        FunkSperre.activate_Funktionssperren(test_case, step)
        step += 1

        self.wait_browser(test_case, 8, step)
        FunkSperre.close_error_popup(test_case, step)

        # verify activated 'Funktionssperren' with WPM
        FunkSperre.verify_Funktionssperren_with_WPM_after_activation(
            test_case, step)
        step += 1

        # deactivate 'Funktionssperren' based on available config
        FunkSperre.deactivate_Funktionssperren(test_case, step)
        step += 1

        time.sleep(40)
        FunkSperre.close_error_popup(test_case, step)

        # verify deactivated 'Funktionssperren' with WPM
        FunkSperre.verify_Funktionssperren_with_WPM_after_deactivation(
            test_case, step)

    def test_Betriebsmodus(self):
        """
                This function executes the following test steps to setup "Betriebsmodus" options in UHI based on 'user_input.json' and verifies the setup values with WPM with help of test automation and records the failed test steps in **Failed.log**

        .. raw:: html

                   <iframe src="D:\\Project\\31138_AutomationTesting\\gdts-automation-testing\\gdts-uhi-testing\\docs\\_test_cases\\test_Betriebsmodus.html" height="345px" width="100%"></iframe>
                """
        from Helping_functions.Betriebsmodus import BetriebsModus
        test_case = "UHI-21: 'Betriebsmodus'"
        target_screen = HOME
        target_tile = self._content["OPERATION_MODE"]
        modus = BetriebsModus()

        step = self.initial_steps(test_case, target_screen, target_tile)

        modus.test_Betriebsmodus_in_UHI(
            test_case, target_screen, target_tile, step)

    def test_Betriebsdaten(self):
        """
                This function executes the following test steps to verify "Betriebsdaten" values in UHI against WPM with help of test automation and records the failed test steps in **Failed.log**

        .. raw:: html

                   <iframe src="D:\\Project\\31138_AutomationTesting\\gdts-automation-testing\\gdts-uhi-testing\\docs\\_test_cases\\test_Betriebsdaten.html" height="345px" width="100%"></iframe>
                """
        from Helping_functions.Betriebsdaten import BetriebsDaten
        test_case = "UHI-23: 'Betriebsdaten'"
        target_screen = ANALYSIS
        target_tile = self._content["OPERATING_DATA"]
        Daten = BetriebsDaten()

        step = self.initial_steps(test_case, target_screen, target_tile)

        Daten.verify_Allgemeine_Betriebsdaten_with_WPM(test_case, step)
        Daten.verify_specific_HK_Betriebsdaten(test_case, step)

    def test_Systemzustand(self):
        """
                This function executes the following test steps to verify "Systemzustand" tile and also start 'Funktionsheizen' programm to verify the options displayed in UHI against WPM with help of test automation and records the failed test steps in **Failed.log**

        .. raw:: html

                   <iframe src="D:\\Project\\31138_AutomationTesting\\gdts-automation-testing\\gdts-uhi-testing\\docs\\_test_cases\\test_Systemzustand.html" height="345px" width="100%"></iframe>
                """
        from Helping_functions.Systemzustand import SystemZustand
        test_case = "UHI-24: 'Systemzustand'"
        target_screen = HOME
        system = SystemZustand()
        step = 1

        if system.get_status_of_Funktionsheizen():
            time.sleep(2)
            self._1st_step(test_case, step)
            self._2nd_step(test_case, target_screen, step)
            step += 1

        if not system.get_status_of_Funktionsheizen():
            system.activate_Funktionsheizen()
            time.sleep(2)
            self._2nd_step(test_case, target_screen, step)
            step += 1

        # Get the tile information
        system.verify_Systemzustand_tile(test_case, step)

    def test_Profile(self):
        """
                This function executes the following test steps to setup and verify profile values inside "Individuelle Anpassungen" tile UHI and compare the values against WPM with help of test automation and records the failed test steps in **Failed.log**

        .. raw:: html

                   <iframe src="D:\\Project\\31138_AutomationTesting\\gdts-automation-testing\\gdts-uhi-testing\\docs\\_test_cases\\test_Profile.html" height="345px" width="100%"></iframe>
                """
        from Profile_functions.silent_mode import SilentMode as SM
        test_case = "UHI-29: 'Profile'"
        target_screen = SETTINGS
        target_tile = _content["INDIVIDUAL_ADAPTIONS"]

        step = self.initial_steps(test_case, target_screen, target_tile)

        self.rename_HK_and_RTC(test_case, step)

        # # select "Systemprofile erstellen" right arrow
        self.click_right_arrow_by_name(
            test_case, _content["CREATE_PROFILE"], step)

        # # setup a profile from 'json'
        profile_name = self.profile_info["profile_name"]
        self.steps_to_create_profile(test_case, profile_name, step)

        # # select 'target' profile
        self.select_created_profile(test_case, profile_name, step)

        if self.is_Ww_true():
            self.setup_Ww_profile(test_case, step)

        if self.is_any_RTC():
            self.setup_Raumregeler_profile(test_case, step)

        SM().setup_silent_mode_UHI(test_case, step)
        
        if self.is_any_AussenTemp() or self.is_any_Festwert():
            self.setup_and_verify_Heizkreise_profile(test_case, step)
        
        if not self.is_any_AussenTemp() and not self.is_any_Festwert():
            self.wait_browser(test_case, 6, step)

        if self.is_any_RTC():
            self.verify_Raumregeler_profile(test_case, step)

        if self.is_Ww_true():
            self.verify_Ww_profile(test_case, step)
        
        SM().verify_silent_mode(test_case, step)

    def test_EasyOn(self):
        """
        This function executes the following test steps to setup "EasyOn" under `Einrichtung EasyOn` tile and verify profile values with UHI against WPM with help of test automation and records the failed test steps in **Failed.log**

        .. raw:: html

        <iframe src="D:\\Project\\31138_AutomationTesting\\gdts-automation-testing\\gdts-uhi-testing\\docs\\_test_cases\\test_Profile.html" height="345px" width="100%"></iframe>
        """
        from Helping_functions.EasyOn import EasyOn

        test_case = "UHI-35: 'EasyOn'"
        target_screen = EASYON
        target_tile = self._content["EASYON_SETUP"]
        EOn = EasyOn()

        step = self.initial_steps(test_case, target_screen, target_tile)

        active_config, active_since, activated_by = EOn.get_active_EasyOn(
            test_case, step)
        if activated_by == EOn._content_EOn["NO_ACTIVE_CONFIGURATION"]:
            PASS(test_case + "=>" + activated_by)
        else:
            PASS(test_case + "=>" + active_config +
                 " " + active_since + " " + activated_by)

        EOn.create_new_EasyOn(test_case, step)
