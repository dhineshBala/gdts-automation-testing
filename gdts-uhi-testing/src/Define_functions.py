from src.Define_init import Define_init
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from src.logger import pass_log, fail_log
from time import strftime
import random, unittest
import io, sys

PASS = pass_log.info
FAIL = fail_log.error



class Define_functions(unittest.TestCase):
    """
    This case consists of basic browser instructions like 'Finding' elements by ID, Class, Xpath and CSS. Also,
    this contains all of the 'Wait' functions based on all mentioned methods above. Most of the basic functions are defined for 'Selenium' browser instructions

    * :py:meth:`src.Define_functions.Define_functions.waitByClass`
    * :py:meth:`src.Define_functions.Define_functions.waitById`
    * :py:meth:`src.Define_functions.Define_functions.waitByXpath`
    * :py:meth:`src.Define_functions.Define_functions.waitByCss`
    * :py:meth:`src.Define_functions.Define_functions.waitByText`
    * :py:meth:`src.Define_functions.Define_functions.findElementsClass`
    * :py:meth:`src.Define_functions.Define_functions.findElementClass`
    * :py:meth:`src.Define_functions.Define_functions.findElementCSS`
    * :py:meth:`src.Define_functions.Define_functions.findElementsCSS`
    * :py:meth:`src.Define_functions.Define_functions.findElementId`
    * :py:meth:`src.Define_functions.Define_functions.findElementsXpath`
    * :py:meth:`src.Define_functions.Define_functions.findElementXpath`
    * :py:meth:`src.Define_functions.Define_functions.compare_results`
    * :py:meth:`src.Define_functions.Define_functions.compare_greater_than_results`
    * :py:meth:`src.Define_functions.Define_functions.compare_true_result`
    * :py:meth:`src.Define_functions.Define_functions.compare_false_result`
    * :py:meth:`src.Define_functions.Define_functions.assert_not_equal`
    * :py:meth:`src.Define_functions.Define_functions.compare_A_or_B_results`
    """
    init = Define_init()
    browser = init.browser
    path_to_dir = init.path_to_dir

    def waitByClass(self, test_case, className, waitTime, step):
        """
        Make the browser 'wait' until desired 'Class Name' is appeared in the browser

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description

        className
            Desired / Targeted element class from the frontend

        waitTime
            Required 'time' for the browser to wait

        step
            step number from the test case document

        """
        try:
            WebDriverWait(self.browser, waitTime).until(
                EC.visibility_of_element_located((By.CLASS_NAME, className)))
            return True
        except:
            pass
            # self.compare_true_result(test_case, False, "[Expected Fail] Wait by class is failed for " +
                                    #  className, step)

    def waitById(self, test_case, id, waitTime, step):
        """
        Make the browser 'wait' until desired 'element ID' is appeared in the browser

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description

        id
            Desired / Targeted element ID from the frontend

        waitTime
            Required 'time' for the browser to wait

        step
            step number from the test case document

        """
        try:
            WebDriverWait(self.browser, waitTime).until(
                EC.visibility_of_element_located((By.CLASS_NAME, id)))
            return True
        except:
            pass
            # self.compare_true_result(test_case, False, "[Expected Fail] Wait by ID is failed for " +
                                    #  id, step)

    def waitByXpath(self, test_case, xpath, waitTime, step):
        """
        Make the browser 'wait' until desired element 'xpath' is appeared in the browser

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description

        xpath
            Desired / Targeted element xpath from the frontend

        waitTime
            Required 'time' for the browser to wait

        step
            step number from the test case document

        """
        try:
            WebDriverWait(self.browser, waitTime).until(
                EC.visibility_of_element_located((By.XPATH, xpath)))
            return True
        except:
            pass
            # self.compare_true_result(test_case, False, "[Expected Fail] Wait by xPath is failed for " +
                                    #  xpath, step)

    def waitByCss(self, test_case, cssElem, waitTime, step):
        """
        Make the browser 'wait' until desired element 'CSS' is appeared in the browser

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description

        cssElem
            Desired / Targeted element CSS from the frontend

        waitTime
            Required 'time' for the browser to wait

        step
            step number from the test case document

        """
        try:
            WebDriverWait(self.browser, waitTime).until(
                EC.visibility_of_element_located((By.CSS_SELECTOR, cssElem)))
            return True
        except:
            pass
            # self.compare_true_result(test_case, False, "[Expected Fail] Wait by CSS is failed for " +
                                    #  cssElem, step)

    def waitByText(self, test_case, cssElem, waitTime, _text, step):
        """
        Make the browser 'wait' until desired 'text' appeared in particular 'CSS' element

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description

        cssElem
            Desired / Targeted element CSS from the frontend

        waitTime
            Required 'time' for the browser to wait

        _text
            Targeted 'text' in the desired CSS element

        step
            step number from the test case document

        """
        try:
            WebDriverWait(self.browser, waitTime).until(
                EC.text_to_be_present_in_element_value((By.CSS_SELECTOR, cssElem), _text))
            return True
        except:
            pass
            # self.compare_true_result(test_case, False, "[Expected Fail] Wait by text is failed for " +
                                    #  _text, step)

    def findElementsClass(self, test_case, className, step):
        """
        Find the targeted / desired element(s) by its class name from the browser frontend

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description

        className
            Desired / Targeted element(s) class name from the frontend

        step
            step number from the test case document

        """
        try:
            return self.browser.find_elements_by_class_name(className)
        except:
            self.compare_true_result(
                test_case, False, "Elements by 'Class' name --> " + className + " is not found", step)

    def findElementClass(self, test_case, className, step):
        """
        Find the targeted / desired element by its class name from the browser frontend

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description

        className
            Desired / Targeted element class name from the frontend

        step
            step number from the test case document

        """
        try:
            return self.browser.find_element_by_class_name(className)
        except:
            self.compare_true_result(
                test_case, False, "Elements by 'Class' name --> " + className + " is not found", step)

    def findElementCSS(self, test_case, cssName, step):
        """
        Find the targeted / desired element by its CSS name from the browser frontend

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description

        cssName
            Desired / Targeted element CSS name from the frontend

        step
            step number from the test case document

        """
        try:
            return self.browser.find_element_by_css_selector(cssName)
        except:
            self.compare_true_result(
                test_case, False, "Elements by 'CSS' element --> " + cssName + " is not found", step)


    def findElementsCSS(self, test_case, cssName, step):
        """
        Find the targeted / desired element(s) by its CSS name from the browser frontend

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description

        cssName
            Desired / Targeted element(s) CSS name from the frontend

        step
            step number from the test case document

        """
        try:
            return self.browser.find_elements_by_css_selector(cssName)
        except:
            self.compare_true_result(
                test_case, False, "Elements by 'CSS' element --> " + cssName + " is not found", step)

    def findElementId(self, test_case, id, step):
        """
        Find the targeted / desired element by its ID from the browser frontend

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description

        id
            Desired / Targeted element CSS name from the frontend

        step
            step number from the test case document

        """
        try:
            return self.browser.find_element_by_id(id)
        except:
            self.compare_true_result(
                test_case, False, "Elements by 'ID' element --> " + id + " is not found", step)

    def findElementsXpath(self, test_case, xPath, step):
        """
        Find the targeted / desired element(s) by its Xpath from the browser frontend

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description

        xPath
            Desired / Targeted element(s) xpath name from the frontend

        step
            step number from the test case document

        """
        try:
            return self.browser.find_elements_by_xpath(xPath)
        except:
            self.compare_true_result(
                test_case, False, "Elements by 'xpath' element --> " + xPath + " is not found", step)

    def findElementXpath(self, test_case, xPath, step):
        """
        Find the targeted / desired element by its Xpath from the browser frontend

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description

        xPath
            Desired / Targeted element xpath name from the frontend

        step
            step number from the test case document

        """
        try:
            return self.browser.find_element_by_xpath(xPath)
        except:
            self.compare_true_result(
                test_case, False, "Elements by 'xpath' element --> " + xPath + " is not found", step)

    def compare_results(self, test_case, expected, actual, error, step):
        """
        Verify 'actual' result against 'expected' results by comparing them exactly 

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description

        expected
            Result from the WPM / Known sources

        actual
            Result from the test execution

        error
            Reason for this particular step fails

        step
            Step number from the test case document

        """
        try:
            self.assertEqual(expected, actual, msg = test_case +
                     " failed at step = " + str(step) + " due to error: " + error + " (Actual(UHI): " + str(actual) + ", Expected(WPM): " + str(expected) + ")")
            PASS("'expected == actual' -> " + test_case +
                 " Passed at step = " + str(step) + ", Step info: " + error + "(error step, just for reference), (Actual (UHI): " + str(actual) + ", Expected (WPM): " + str(expected) + ")")
            print("<b style='color:green;'>[PASS]</b> Step info: " + error + "(reference), Actual (UHI): " + str(actual) + ", Expected (WPM): " + str(expected) + "<br>")
        except:
            FAIL("'expected != actual' -> " + test_case +
                 " failed at step = " + str(step) + " due to error: " + error + " (Actual(UHI): " + str(actual) + ", Expected(WPM): " + str(expected) + ")")
            print("<b style='color:red;'>[FAIL]</b> Step info: " + error + ", Actual (UHI): " + str(actual) + ", Expected (WPM): " + str(expected) + "<br>")

    def compare_greater_than_results(self, test_case, expected_bigger, actual_smaller, error, step):
        """
        Verify if 'actual' result is less or equal than 'expected' results (actual <= expected)

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description

        expected_bigger
            Result from the WPM / Known sources

        actual_smaller
            Result from the test execution

        error
            Reason for this particular step fails

        step
            Step number from the test case document

        """
        try:
            self.assertGreaterEqual(expected_bigger, actual_smaller, msg=test_case +
                 " failed at step = " + str(step) + " due to error: " + error + " (Actual: " + str(actual_smaller) + ", Expected: " + str(expected_bigger) + ")")
            PASS("'actual >= expected' -> " + test_case +
                 " Passed at step = " + error + "(error step, just for reference), (Actual (UHI): " + str(actual_smaller) + ", Expected (WPM): " + str(expected_bigger) + ")")
            print("<b style='color:green;'>[PASS]</b> Step info: " + error + "(reference), Actual (UHI): " + str(actual_smaller) + ", Expected (WPM): " + str(expected_bigger) + "<br>")
            
        except:
            FAIL("'expected >!= actual' -> " + test_case +
                 " failed at step = " + str(step) + " due to error: " + error + " (Actual: " + str(actual_smaller) + ", Expected: " + str(expected_bigger) + ")")
            print("<b style='color:red;'>[FAIL]</b> Step info: " + error + ", Actual (UHI): " + str(actual_smaller) + ", Expected (WPM): " + str(expected_bigger) + "<br>")

    def compare_true_result(self, test_case, actual, error, step):
        """
        Verify 'actual' result against 'True' condition.

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description

        actual
            'Actual' result from the test execution

        error
            Reason for this particular step fails

        step
            step number from the test case document

        """
        try:
            self.assertTrue(actual, msg=test_case +
                 " failed at step = " + str(step) + " due to error: " + error + " (Actual: " + str(actual) + ", Expected: " + str(True) + ")")
            PASS("'actual == True' -> " + test_case +
                 " Passed at step = " + error + "(error step, just for reference), (Actual (UHI): " + str(actual) + ", Expected (WPM): " + str(True) + ")")
            print("<b style='color:green;'>[PASS]</b> Step info: " + error + "(reference), Actual (UHI): " + str(actual) + ", Expected (WPM): " + str(True) + "<br>")
            return True
        except:
            FAIL("'expected != True' -> " + test_case +
                 " failed at step = " + str(step) + " due to error: " + error + " (Actual: " + str(actual) + ", Expected: " + str(True) + ")")
            print("<b style='color:red;'>[FAIL]</b> Step info: " + error + ", Actual (UHI): " + str(actual) + ", Expected (WPM): " + str(True) + "<br>")

    def compare_false_result(self, test_case, actual, error, step):
        """
        Verify 'actual' result against 'False' condition.

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description

        actual
            'Actual' result from the test execution

        error
            Reason for this particular step fails

        step
            step number from the test case document

        """
        try:
            self.assertFalse(actual, msg=test_case +
                 " failed at step = " + str(step) + " due to error: " + error + " (Actual: " + str(actual) + ", Expected: " + str(False) + ")")
            PASS("'actual == False' -> " + test_case +
                 " Passed at step = " + error + "(error step, just for reference), (Actual (UHI): " + str(actual) + ", Expected (WPM): " + str(False) + ")")
            print("<b style='color:green;'>[PASS]</b> Step info: " + error + "(reference), Actual (UHI): " + str(actual) + ", Expected (WPM): " + str(False) + "<br>")
            return False
        except:
            FAIL("'expected != True' -> " + test_case +
                 " failed at step = " + str(step) + " due to error: " + error + " (Actual: " + str(actual) + ", Expected: " + str(False) + ")")
            print("<b style='color:red;'>[FAIL]</b> Step info: " + error + ", Actual (UHI): " + str(actual) + ", Expected (WPM): " + str(False) + "<br>")

    def assert_not_equal(self, test_case, expected, actual, error, step):
        """
        Verify 'actual' result, if it is not equal to 'expected' results

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description

        expected
            Result from the WPM / Known sources

        actual
            Result from the test execution

        error
            Reason for this particular step fails

        step
            Step number from the test case document

        """
        try:
            self.assertNotEqual(expected, actual, msg=test_case +
                 " failed at step = " + str(step) + " due to error: " + error + " (Actual: " + str(actual) + ", Expected: " + str(expected) + ")")
        except:
            FAIL("'expected != actual' -> " + test_case +
                 " failed at step = " + str(step) + " due to error: " + error + " (Actual: " + str(actual) + ", Expected: " + str(expected) + ")")

    def compare_A_or_B_results(self, test_case, expected_1, expected_2, actual, error, step):
        """
        Verify 'actual' result against 'expected_1' (or) 'expected_2' results by comparing them. This is 
        used in verifying 'Wochenvorlauf' in profiles since UHI/WPM randomly switches Zeit1 and Zeit2 in 
        profiles.

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description

        expected_1
            Result from the WPM / Known sources
        
        expected_2
            Result from the WPM / Known sources

        actual
            Result from the test execution

        error
            Reason for this particular step fails

        step
            Step number from the test case document

        """
        if actual == expected_1:
            self.compare_results(test_case, expected_1, actual, error, step)
        elif actual == expected_2:
            self.compare_results(test_case, expected_2, actual, error, step)
        else:
            self.compare_true_result(test_case, False, error, step)

    def print_console(self, printText):
        output = io.StringIO()
        sys.stdout = output
        print(printText)
        sys.stdout = sys.__stdout__
        print('Cosole Output: ', output.getvalue())
        
