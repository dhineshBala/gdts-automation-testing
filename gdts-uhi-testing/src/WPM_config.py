from src.read_modbus import ReadModbusTCP
import os, json

class WPMconfig(ReadModbusTCP):
    """
    This is a class for getting Waermepumpe config from *Modbus* registers for testing purpose.

    """
    tcp = ReadModbusTCP()

    with open(os.path.join(tcp.json_dir, 'All_registers.json'), encoding='utf-8') as content:
        _index = json.load(content)
    regs_integer = _index["Integer"]
    regs_analog = _index["Analog"]
    regs_digital = _index["Digital"]

    def Fun_Code_Ww(self):
        return self.tcp.get_status_from_coils(self.regs_digital["Fun_Code_Ww"])
    def Fun_Code_Hk1(self):
        return self.tcp.get_status_from_coils(self.regs_digital["Fun_Code_Hk1"])
    def Fun_Code_Hk2(self):
        return self.tcp.get_status_from_coils(self.regs_digital["Fun_Code_Hk2"])
    def Fun_Code_KuehlAktiv(self):
        return self.tcp.get_status_from_coils(self.regs_digital["Fun_Code_KuehlAktiv"])
    def Fun_Code_Biv(self):
        return self.tcp.get_status_from_coils(self.regs_digital["Fun_Code_Biv"])
    def Fun_Code_Reg(self):
        return self.tcp.get_status_from_coils(self.regs_digital["Fun_Code_Reg"])
    def Fun_Code_Sw(self):
        return self.tcp.get_status_from_coils(self.regs_digital["Fun_Code_Sw"])
    def Fun_Code_KuehlPass(self):
        return self.tcp.get_status_from_coils(self.regs_digital["Fun_Code_KuehlPass"])
    
    def P_HK_1(self):
        return self.tcp.get_values_from_integer_registers(self.regs_integer["P_HK_1"])
    def P_HK1_REG(self):
        return self.tcp.get_values_from_integer_registers(self.regs_integer["P_HK1_REG"])
    def P_HK1_RT_thT_Anz(self):
        return self.tcp.get_values_from_integer_registers(self.regs_integer["P_HK1_RT_thT_Anz"])


    def P_HK_2(self):
        return self.tcp.get_values_from_integer_registers(self.regs_integer["P_HK_2"])
    def P_HK2_REG(self):
        return self.tcp.get_values_from_integer_registers(self.regs_integer["P_HK2_REG"])
    def P_HK2_RT_thT_Anz(self):
        return self.tcp.get_values_from_integer_registers(self.regs_integer["P_HK2_RT_thT_Anz"])

    
    def P_WW(self):
        """
        returns 'True' if Warmwasser is available in Config

        return value: True / False

        """
        return self.tcp.get_values_from_integer_registers(self.regs_integer["P_WW"])
        
    def P_SW(self):
        """
        returns 'True' if Swimmbad is available in Config

        return value: True / False
        
        """
        return self.tcp.get_values_from_integer_registers(self.regs_integer["P_SW"])

    def get_Ww_Zirkulation_status(self):
        """
        Returns `integer` (0 / 1 / 2) based on **Zirkulation** in the Warmwasser

        Returns
        -------
        'int'
            0, 1 or 2 based on WPM config
        """
        return self.tcp.get_values_from_integer_registers(self.regs_integer["P_WW_ZIRK"])

    def Betriebsweise(self):
        """
        returns 0-3 from registers (0 = monovalent, 1 = monoenergetisch, 2 = bivalent, 3 = regenerativ)

        return type: 'int'

        return value: 0 / 1 / 2 / 3

        """
        return self.tcp.get_values_from_integer_registers(self.regs_integer["P_BETR_WEISE"])

    def KuehlAktiv(self):
        """
        returns True or False based on 'Aktiv Kühlen' status

        return type: 'string'

        return value: True / False

        """
        return self.tcp.get_status_from_coils(self.regs_digital["P_KUEHLAKTIV"])

    def KuehlPassiv(self):
        """
        returns True or False based on 'Passiv Kühlen' status

        return type: 'string'

        return value: True / False

        """
        return self.tcp.get_status_from_coils(self.regs_digital["P_KUEHLPASSIV"])

    def Kuehlen_in_HK(self):
        """
        returns 'True' if any type of 'Kuehlen' is active in any 'HK'

        return type: 'boolean'

        """
        if self.P_HK_1() > 1 or self.P_HK_2() > 1:
            return True
        else:
            return False

    def is_any_RTC(self):
        """ 
        returns 'True' if there are any `Raumregelung` in heating circuits
        """
        if self.P_HK1_REG() == 2 or self.P_HK2_REG() == 2:
            return True
        else:
            return False

    def is_HK1_RTC(self):
        """ 
        returns 'True' if HK1 is Raumregelung
        """
        if self.P_HK_1() >= 1 and self.P_HK1_REG() == 2:
            return True
        else:
            return False

    def is_HK2_RTC(self):
        """ 
        returns 'True' if HK2 is Raumregelung
        """
        if self.P_HK_2() >= 1 and self.P_HK2_REG() == 2:
            return True
        else:
            return False

    def is_any_Festwert(self):
        """ 
        returns 'True' if there are any `Festwert` in Heating circuits
        """
        if self.P_HK1_REG() == 1 or self.P_HK2_REG() == 1:
            return True
        else:
            return False

    def is_HK1_Festwert(self):
        """ 
        returns 'True' if there HK1 is `Festwert`
        """
        if self.P_HK_1() >= 1 and self.P_HK1_REG() == 1:
            return True
        else:
            return False

    def is_HK2_Festwert(self):
        """ 
        returns 'True' if there HK2 is `Festwert`
        """
        if self.P_HK_2() >= 1 and self.P_HK2_REG() == 1:
            return True
        else:
            return False

    def is_any_AussenTemp(self):
        """ 
        returns 'True' if there are any `Aussentemperatur(Heizkurve)` in the Heating circuits
        """
        if self.P_HK1_REG() == 0 or self.P_HK2_REG() == 0:
            return True
        else:
            return False
    
    def is_HK1_AussenTemp(self):
        """ 
        returns 'True' if HK1 is `Aussentemperatur(Heizkurve)`
        """
        if self.P_HK_1() >= 1 and self.P_HK1_REG() == 0:
            return True
        else:
            return False

    def is_HK2_AussenTemp(self):
        """ 
        returns 'True' if HK2 is `Aussentemperatur(Heizkurve)`
        """
        if self.P_HK_2() >= 1 and self.P_HK2_REG() == 0:
            return True
        else:
            return False

    def is_Ww_true(self):
        """ 
        returns 'True' if `Warmwasser` is presented in Funcktionscode
        """
        if self.P_WW():
            return True
        else:
            return False
