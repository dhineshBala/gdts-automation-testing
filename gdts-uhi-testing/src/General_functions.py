from src.WPM_config import WPMconfig
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from src.logger import fail_log, pass_log
import time
import random
import json
import os
import requests


PASS = pass_log.info
FAIL = fail_log.error

_config = WPMconfig()


class GeneralFunctions(WPMconfig):
    """
    This class contains most of the common actions or workflows that have been performed in Web browser. The most common functions like unlocking UHI with 'Kundendienst' / 'installatuer' / 'M user', repeated steps after each test case run and so on. This is derived from 'Define_functions' class with most common steps to reduce redundency in automation script.

    * :py:meth:`src.General_functions.GeneralFunctions.tapClock`
    * :py:meth:`src.General_functions.GeneralFunctions.unlockUserScreen`
    * :py:meth:`src.General_functions.GeneralFunctions.unlockServiceScreen`
    * :py:meth:`src.General_functions.GeneralFunctions.unlock_P_screen`
    * :py:meth:`src.General_functions.GeneralFunctions.dragOptions`
    * :py:meth:`src.General_functions.GeneralFunctions.close_error_popup`
    * :py:meth:`src.General_functions.GeneralFunctions.click_close_icon`
    * :py:meth:`src.General_functions.GeneralFunctions.count_tiles`
    * :py:meth:`src.General_functions.GeneralFunctions.get_response_from_api`
    * :py:meth:`src.General_functions.GeneralFunctions.compare_count`
    * :py:meth:`src.General_functions.GeneralFunctions._1st_step`
    * :py:meth:`src.General_functions.GeneralFunctions._2nd_step`
    * :py:meth:`src.General_functions.GeneralFunctions._3rd_step`
    * :py:meth:`src.General_functions.GeneralFunctions.initial_steps`
    * :py:meth:`src.General_functions.GeneralFunctions.special_profile_steps`
    * :py:meth:`src.General_functions.GeneralFunctions.get_text_from_row`
    * :py:meth:`src.General_functions.GeneralFunctions.get_click_from_toggle_row`
    * :py:meth:`src.General_functions.GeneralFunctions.get_target_screen`
    * :py:meth:`src.General_functions.GeneralFunctions.get_tile_text`
    * :py:meth:`src.General_functions.GeneralFunctions.get_text_from_spinbox`
    * :py:meth:`src.General_functions.GeneralFunctions.get_text_from_element`
    * :py:meth:`src.General_functions.GeneralFunctions.get_size_of_elements`
    * :py:meth:`src.General_functions.GeneralFunctions.check_presence_of_element_from_text`
    * :py:meth:`src.General_functions.GeneralFunctions.check_presence_of_option_from_toggle_row_text`
    * :py:meth:`src.General_functions.GeneralFunctions.check_is_displayed`
    * :py:meth:`src.General_functions.GeneralFunctions.click_back_by_header_title`
    * :py:meth:`src.General_functions.GeneralFunctions.tap_button`
    * :py:meth:`src.General_functions.GeneralFunctions.status_tile`
    * :py:meth:`src.General_functions.GeneralFunctions.slide_haus_screen`
    * :py:meth:`src.General_functions.GeneralFunctions.click_right_arrow_by_name`
    * :py:meth:`src.General_functions.GeneralFunctions.check_option_from_row_text`
    * :py:meth:`src.General_functions.GeneralFunctions.check_option_from_toggle_row_text`
    * :py:meth:`src.General_functions.GeneralFunctions.get_toggle_button_status`
    * :py:meth:`src.General_functions.GeneralFunctions.get_UHI_values`
    * :py:meth:`src.General_functions.GeneralFunctions.set_temp_via_plus_minus_buttons`
    * :py:meth:`src.General_functions.GeneralFunctions.click_right_arrow`
    * :py:meth:`src.General_functions.GeneralFunctions.click_left_arrow`
    * :py:meth:`src.General_functions.GeneralFunctions.click_dropdown_by_title1`
    * :py:meth:`src.General_functions.GeneralFunctions.click_dropdown_by_title2`
    * :py:meth:`src.General_functions.GeneralFunctions.click_text_area_by_name`
    * :py:meth:`src.General_functions.GeneralFunctions.set_date_time_via_up_down_arrow`
    * :py:meth:`src.General_functions.GeneralFunctions.get_today_from_ntp`
    * :py:meth:`src.General_functions.GeneralFunctions.wait_for_verification`
    * :py:meth:`src.General_functions.GeneralFunctions.check_internet`
    * :py:meth:`src.General_functions.GeneralFunctions.click_Edit_text`
    * :py:meth:`src.General_functions.GeneralFunctions.click_Edit_icon`
    * :py:meth:`src.General_functions.GeneralFunctions.Edit_text_box`
    """
    _config = _config
    action = ActionChains(_config.browser)

    json_dir = _config.json_dir

    with open(os.path.join(json_dir, 'screen_info.json'), encoding='utf-8') as info:
        screen_info = json.load(info)
    Haus_info = screen_info["Haus_screen_setting"]

    Url_info = _config._data["UrlInfo"]
    Wpm_info = _config._data["WpmInfo"]

    with open(os.path.join(json_dir, 'de.common.json'), encoding='utf-8') as content:
        _content = json.load(content)

    # read 'user_input.json' file
    with open(os.path.join(json_dir, 'user_input.json'), encoding='utf-8') as _input:
        user_input = json.load(_input)

    with open(os.path.join(json_dir, 'elements_info.json'), encoding='utf-8') as info:
        elements = json.load(info)

    uhi_url = Url_info["prefix"] + \
        Url_info["Url"]+Url_info["suffix"]

    # elements from elements_info.json
    drag = elements["drag"]
    tiles = elements["tiles"]
    lock_slider = elements["lock_slider"]
    close_icon = elements["close_icon"]
    texT = elements["text"]
    buttons = elements["buttons"]
    css_prop = elements["css_property"]
    icon = elements["icon"]

    def tapClock(self, test_case, step):
        """
        Tapping on screen saver 'clock' before preceeding to unlock the UHI

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document

        """
        try:
            self.findElementCSS(
                test_case, self.elements["clock"], step).click()
        except:
            self.compare_true_result(
                test_case, False, "Clock is not available", step)

    def unlockUserScreen(self, test_case, step):
        """
        Unlock 'End user' screen with unlock pattern "M"

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        try:
            time.sleep(1)
            # follow M pattern to unlock the user options
            dots = self.findElementsClass(
                test_case, self.elements["pattern"], step)
            action = ActionChains(_config.browser)
            action.click_and_hold(on_element=dots[6])
            action.move_to_element(to_element=dots[3])
            action.move_to_element(to_element=dots[0])
            action.move_to_element(to_element=dots[4])
            action.move_to_element(to_element=dots[2])
            action.move_to_element(to_element=dots[5])
            action.move_to_element(to_element=dots[8])
            action.release()
            action.perform()
        except:
            self.compare_true_result(
                test_case, False, "Unlock 'M user' screen failed", step)

    def unlockServiceScreen(self, test_case, step):
        """
        Unlock 'Kundendienst' screen with unlock pattern "I" with password 'gdtskd'

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        try:
            # "I" pattern for service options and login credentials
            time.sleep(1)
            dots = self.findElementsClass(test_case,
                                          self.elements["pattern"], step)
            action = ActionChains(_config.browser)
            action.click_and_hold(on_element=dots[1])
            action.move_to_element(to_element=dots[4])
            action.move_to_element(to_element=dots[7])
            action.release()
            action.perform()
            time.sleep(2)
            servicePwd = self.browser.find_element_by_id("1")
            servicePwd.click()
            time.sleep(1)
            servicePwd.send_keys("gdtskd")
            time.sleep(1)
            servicePwd.send_keys(Keys.RETURN)
            self.wait_browser(test_case, 1, step)
            self.findElementCSS(
                test_case, self.elements["element_app_menu"], step).click()
            time.sleep(2)
        except:
            self.compare_true_result(
                test_case, False, "Unlock 'gdtskd' user screen failed", step)

    def unlock_P_screen(self, test_case, step):
        """
        Unlock 'Erstemeldungen' screen with unlock pattern "I" with password 'p'

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        try:
            time.sleep(1)
            dots = self.findElementsClass(test_case,
                                          self.elements["pattern"], step)
            action = ActionChains(_config.browser)
            action.click_and_hold(on_element=dots[1])
            action.move_to_element(to_element=dots[4])
            action.move_to_element(to_element=dots[7])
            action.release()
            action.perform()
            time.sleep(2)
            servicePwd = self.browser.find_element_by_id("1")
            servicePwd.click()
            time.sleep(1)
            servicePwd.send_keys("p")
            time.sleep(1)
            servicePwd.send_keys(Keys.RETURN)
            time.sleep(1)
        except:
            self.compare_true_result(
                test_case, False, "Unlock 'p' user screen failed", step)

    def dragOptions(self, test_case, fromOption, toOption, step):
        """
        Drag the desired screen from 'fromOption' to 'toOption'.

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        fromOption
            Drag screen start element (where to start)
        toOption
            Drag screen stop element (where to stop)
        step
            step number from the test case document
        """

        self.waitByCss(test_case, self.icon["home_heat"], 3, step)

        try:
            self.browser.find_elements_by_css_selector(
                self.icon["home_heat"]).click()
        except:
            pass

        try:
            startBtn = self.findElementCSS(test_case, fromOption, step)
            stopBtn = self.findElementCSS(test_case, toOption, step)
            action = ActionChains(_config.browser)
            action.click_and_hold(on_element=startBtn)
            action.move_to_element(to_element=stopBtn)
            action.release()
            action.perform()
        except:
            self.compare_true_result(
                test_case, False, "Drag options are not available", step)

    def click_scrollable_area(self, test_case, step):
        """ Returns a click on `scrollable area` element to minimize keypad after OS 2.10.1 """
        self.findElementCSS(
            test_case, self.elements["scrollable_area"], step).click()

    def close_error_popup(self, test_case, step):
        """
        Close the 'Sperren / Stoerungen / Fehler' popup appears while web automation.

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description

        step
            step number from the test case document
        """
        for q in self.browser.find_elements_by_css_selector(self.texT["area"]):
            if q.text == self._content["CONFIRM"]:
                try:
                    self.browser.find_element_by_css_selector(
                        self.elements["close_icon_css"]).click()
                except:
                    pass
                try:
                    self.browser.find_element_by_css_selector(
                        self.elements["close_icon"]).click()
                except:
                    pass
                try:
                    q.click()
                    time.sleep(2)
                except:
                    pass

    def click_close_icon(self, test_case, step):
        """
        click on 'close' icon on active screen.

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description

        step
            step number from the test case document
        """
        self.findElementCSS(
            test_case, self.elements["close_icon_css"], step).click()
        time.sleep(1)

    def click_close_icon_scroll_home(self, test_case, element_ID, step):
        """
        click on 'close' icon under *'scroll home'* element screen.

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description

        step
            step number from the test case document
        """
        self.findElementsCSS(test_case, self.elements["scroll_home"], step)[
            element_ID].find_element_by_css_selector(self.elements["close_icon_css"]).click()
        time.sleep(1)

    def count_tiles(self, test_case, screen, step):
        """
        Returns total number of 'tile' count in active screen

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description

        screen
            Active screen name on the Web browser

        step
            step number from the test case document

        Returns
        -------
        'int'
            Total tiles count in the specific screen


        """
        if screen == self._content["ANALYSIS"]:
            tot_tiles = self.findElementsCSS(test_case, self.elements["main_screens"], step)[
                0].find_elements_by_css_selector(self.tiles["analyse_setting"])
        if screen == self._content["SETTINGS"]:
            tot_tiles = self.findElementsCSS(test_case, self.elements["main_screens"], step)[
                1].find_elements_by_css_selector(self.tiles["analyse_setting"])
        if screen == self._content["HOME"]:
            tot_tiles = self.findElementsCSS(test_case, self.elements["main_screens"], step)[
                2].find_elements_by_css_selector(self.tiles["home"])
        if screen == self._content["EASYON"]:
            tot_tiles = self.findElementsCSS(test_case, self.elements["main_screens"], step)[
                3].find_elements_by_css_selector(self.tiles["easyon"])
        return len(tot_tiles)

    def get_response_from_api(self, opt, api_url, step):
        """
        Returns 'json' list from the specified 'API' request

        Parameters
        ----------
        api_url
            The suffix (after UHI 'IP' address) of 'API' url 

        opt
            'Context' where API requested from

        step
            step number from the test case document

        Return
        ------
        type: 'json' object

        """
        api = self.Url_info["prefix"] + self.Url_info["Url"] + api_url
        response = requests.get(api)
        if not response.status_code == 200:
            fail_log.error(opt + " '" + api + "' -> Request failed with code " +
                           response.status_code + " in step " + str(step))
        try:
            json_data = json.loads(response.text)
            return json_data
        except:
            fail_log.error(
                opt + " '" + api + "' JSON date is not available in step = " + str(step))

    def compare_count(self, test_case, target_screen, tot_tiles, step):
        """
        Verify 'Tiles' count for the specific screen by comparing against expected.

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description

        target_screen
            The screen where the action is performed (Home, Settings, EasyOn, Analysis)

        tot_tiles
            'Expected' total number of tiles (known value)

        step
            step number from the test case document

        """
        self.compare_results(test_case, int(self.count_tiles(test_case, target_screen, step)), int(tot_tiles), "Tiles in "
                             + target_screen + " page are not equal to " + tot_tiles, step)

    def _1st_step(self, test_case, step):
        """
        Executes "first" step of the test case which is common in most cases.
            1. Unlock 'UHI' with 'gdtskd' password

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        # 1 "I" pattern
        self.tapClock(test_case, step)
        self.unlockServiceScreen(test_case, step)
        self.waitByCss(test_case, self.buttons["info"], 15, step)
        self.wait_browser(test_case, 2, step)

    def _2nd_step(self, test_case, target_screen, step):
        """
        Executes "second" step of the test case which is common in most cases.
            2. Drag 'desired' screen from UHI and Verify the tiles count on the 
            specific 'screen' with expected result

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        target_screen
            The screen where the action is performed (Home, Settings, EasyOn, Analysis)
        step
            step number from the test case document
        """
        # 2 Open tiles in the target_screen page
        if target_screen == self._content["SETTINGS"]:
            self.dragOptions(
                test_case, self.drag["top"], self.drag["bottom"], step)
            self.wait_browser(test_case, 1, step)
            self.compare_count(
                test_case, target_screen, self.tiles["count_analyse_setting"], step)
        if target_screen == self._content["ANALYSIS"]:
            self.dragOptions(
                test_case, self.drag["left"], self.drag["right"], step)
            self.wait_browser(test_case, 2, step)
            self.compare_count(
                test_case, target_screen, self.tiles["count_analyse_setting"], step)
        if target_screen == self._content["EASYON"]:
            self.dragOptions(
                test_case, self.drag["bottom"], self.drag["top"], step)
            self.wait_browser(test_case, 1, step)
            self.compare_count(test_case, target_screen,
                               self.tiles["count_easyon"], step)
        if target_screen == self._content["HOME"]:
            self.dragOptions(
                test_case, self.drag["right"], self.drag["left"], step)
            self.wait_browser(test_case, 1, step)
            # self.compare_count(test_case, target_screen,
            #                    self.tiles["count_home"], step)

    def _3rd_step(self, test_case, target_tile, step):
        """
        Executes "third" step of the test case which is common in most cases.
            3. Click on 'desired' tile for the test execution

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        target_tile
            The tile where the test steps are performed (Warmwasser, Haus, Netzwerk, Software update and so on.)
        step
            step number from the test case document
        """
        # 3 click "software update"
        try:
            for t in self.findElementsCSS(test_case, self.tiles["title"], step):
                if t.text == target_tile:
                    while True:
                        if t.value_of_css_property("opacity") == "1":
                            break
                        # if not self.findElementsCSS(test_case, "div.icon-loading", step):
                        #     break
                    t.click()
            time.sleep(2)
        except:
            for tile in self.findElementsCSS(test_case, self.tiles["ripple_advanced"], step):
                if tile.find_element_by_css_selector(self.tiles["title"]).text == target_tile:
                    while True:
                        if not self.findElementsCSS(test_case, "div.icon-loading", step):
                            break
                        # if tile.value_of_css_property("opacity") == "1":
                        #     break
                    tile.click()
                time.sleep(2)

    def initial_steps(self, test_case, target_screen, target_tile):
        """
        Executes 1, 2, 3 steps of the test case which is common in most cases.

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        target_screen
            The screen where the action is performed (Home, Settings, EasyOn, Analysis)
        target_tile
            The tile where the test steps are performed (Warmwasser, Haus, Netzwerk, Software update and so on.)
        step
            step number from the test case document
        """
        PASS("-------------" + test_case + "-------------")
        FAIL("-------------" + test_case + "-------------")
        step = 1

        # 1 "I" pattern
        self._1st_step(test_case, step)
        step += 1

        # 2 Open tiles in the target_screen page
        self._2nd_step(test_case, target_screen, step)
        step += 1

        # wait until the tile is displayed
        while True:
            for t in self.findElementsCSS(test_case, self.tiles["title"], step):
                if t.text == target_tile:
                    break
            break

        # 3 click "software update"
        self._3rd_step(test_case, target_tile, step)
        step += 1

        return step

    def check_if_tile_loaded_based_on_status_info(self, test_case, step):
        return self.findElementsCSS(test_case, self.elements["tiles"]["value"], step)


    def get_text_from_row(self, test_case, text_title, step):
        """
        Returns a 'text', if **"text_title"** matches with 'div.element-text-row'-->'div.element-text-area.title'

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        text_title
            A 'text' from 'de.common.json' which used for searching in browser 
        step
            step number from the test case document
        """
        for r in self.findElementsCSS(test_case, self.texT["row"], step):
            if r.find_element_by_css_selector(self.texT["title"]).text == text_title:
                return r.find_element_by_css_selector(self.texT["value"]).text

    def get_click_from_toggle_row(self, test_case, button_name, step):
        """
        Returns a 'click' on 'div.element-toggle-row', if the **"button_name"** matches with 'div.element-text-area'

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        button_name
            A 'text' from 'de.common.json' which used for searching in browser 
        step
            step number from the test case document
        """
        time.sleep(3)
        for r in self.findElementsCSS(test_case, self.texT["toggle_row"], step):
            if r.find_element_by_css_selector(self.texT["area"]).text == button_name:
                if not self.get_toggle_button_status(test_case, button_name, step):
                    return r.find_element_by_css_selector(self.buttons["toggle_btn"]).click()

    def get_target_screen(self, target_screen):
        """
        Returns 'screen' element CSS name from 'elements_info.json' based on **"target screen"**

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        button_name
            A 'text' from 'de.common.json' which used for searching in browser 
        step
            step number from the test case document
        """
        if target_screen == self._content["SETTINGS"] or target_screen == self._content["ANALYSIS"]:
            return self.elements["tiles"]["analyse_setting"]
        if target_screen == self._content["EASYON"]:
            return self.elements["tiles"]["easyon"]
        if target_screen == self._content["HOME"]:
            return self.elements["tiles"]["home"]

    def get_tile_text(self, test_case, target_screen, tile_title, target_tile, tile_value, step):
        """
        Returns a 'tile_value', if **"target_tile"** matches with 'target_screen'-->'tile_title'

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        target_screen
            The screen where the action is performed (Home, Settings, EasyOn, Analysis)
        target_tile
            The tile where the test steps are performed (Warmwasser, Haus, Netzwerk, Software update and so on.)
        tile_value
            The 'text' value which we wanted to read from UHI (text.value)
        step
            step number from the test case document

        Returns
        -------
        'string'
            Text when tile name matches
        """
        for t in self.findElementsCSS(test_case, self.get_target_screen(target_screen), step):
            if t.find_element_by_css_selector(tile_title).text == target_tile:
                return t.find_element_by_css_selector(tile_value).text

    def get_text_from_spinbox(self, test_case, parent, child, child_no, step):
        """
        Returns a 'text' from *Spin-box* element based on "Parent -> Child" element name (date -> value, time -> value)

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        parent
            **Main** element of targeted spinbox
        child
            **sub** element of targeted *Main* element spinbox
        child_no
            **sub** element number from child element *array*
        step
            step number from the test case document

        Returns
        -------
        'string'
            Text of Parent->Child 'spinbox'
        """
        return self.findElementCSS(test_case, parent, step).find_elements_by_css_selector(child)[child_no].text

    def get_rand_int(self, start, end):
        """
        Returns `random` intergers between 'start' and 'end' limits

        Parameters
        ----------
        start
            Start number of random numbers
        end
            End number of random numbers

        Returns
        -------
        'int'
            Returns between 'start' and 'end' limits
        """
        return random.randint(start, end)

    def get_text_from_element(self, test_case, element, step):
        """
        Returns a 'text' from **element** CSS

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        element
            **Main** element CSS name of targeted text
        step
            step number from the test case document

        Returns
        -------
        'string'
            Text of passed element CSS name
        """
        return self.findElementCSS(test_case, element, step).text

    def get_size_of_elements(self, test_case, elements, step):
        """
        Returns the 'size' of array that matches **elements** CSS

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        elements
            **Main** element(s) CSS name of targeted element 
        step
            step number from the test case document

        Returns
        -------
        'int'
            Size of the array of passed 'elements' CSS name
        """
        return int(len(self.findElementsCSS(test_case, elements, step)))

    def check_presence_of_element_from_text(self, test_case, elements, _text, step):
        """
        Returns 'True', if any of the 'text' in elements matches with **"_text"**

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        elements
            'CSS' element(s) name which contains more than one text. 
        _text
            A 'text' from 'de.common.json' which used for searching in browser 
        step
            step number from the test case document

        Returns
        -------
        'Boolean'
            True if element.text == _text
        """
        for t in self.findElementsCSS(test_case, elements, step):
            if t.text == _text:
                return True
            else:
                return False

    def check_presence_of_option_from_toggle_row_text(self, test_case, expected_option, step):
        """
        Returns 'True', if **"expected_option"** is presented in 'div.element-toggle-row'-->'div.element-text-area'

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        expected_option
            A 'text' from 'de.common.json' which used for searching in browser 
        step
            step number from the test case document
        """
        for r in self.findElementsCSS(test_case, self.texT["toggle_row"], step):
            if r.find_element_by_css_selector(self.texT["area"]).text == expected_option:
                break
        else:
            self.compare_true_result(
                test_case, False, expected_option + " --> option is not available", step)

    def check_is_displayed(self, test_case, element, step):
        """
        Returns 'True', if **"element"** is displayed in the Web browser.

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        element
            A 'CSS' element name which needs to be checked. 
        step
            step number from the test case document
        """
        self.compare_true_result(test_case, self.findElementCSS(test_case, element, step).is_displayed(
        ), element + " is not presented in UHI screen", step)

    def click_back_by_header_title(self, test_case, title, step):
        """
        returns a 'click' on **"title"**, if 'title' matches with 'div.element-text-area.title'. 

        NOTE: Some of the "area.title" have same 'text' in more than one place like *Funktionsheizen*. In this
        case, this function may fail because it tries to click "1st" Funktionsheizen instead of "2nd" which 
        we wanted to click. So, one can use the function to click on specific left arrow in the screen

            click_left_arrow(element_ID, step)

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        title
            A 'text' from 'de.common.json' which used for searching in browser 
        step
            step number from the test case document
        """
        self._3rd_step(test_case, title, step)
        time.sleep(1)

    def tap_button(self, test_case, ele_btn, step):
        """
        Click on 'button' element when **ele_btn** element matches

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        ele_btn
            A *CSS* element name to tap on Web frontend
        step
            step number from the test case document
        """
        return self.findElementCSS(test_case, ele_btn, step).click()

    def status_tile(self, test_case, target_screen, target_tile, expected_status, step):
        """
        Verify if 'text' presented in tiles are as expected. 

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        target_screen
            The screen where the action is performed (Home, Settings, EasyOn, Analysis)
        target_tile
            The tile where the test steps are performed (Warmwasser, Haus, Netzwerk, Software update and so on.)
        expected_status
            A 'text' from 'de.common.json' for comparing with 'expected' result
        step
            step number from the test case document
        """
        tile_status = self.get_tile_text(
            test_case, target_screen, self.tiles["title"], target_tile, self.tiles["value"], step)
        self.compare_results(
            test_case, expected_status, tile_status, "Expected status on the tile is wrong with text " + tile_status, step)

    def slide_haus_screen(self, test_case, element_ID, step):
        """
        Slide 'Haus' screen when there are more than one HK (or) Raumregeler in WPM Config

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        element_ID
            An element ID for the particular screen number
        step
            step number from the test case document
        """
        slide = self.findElementsCSS(test_case,
                                     self.Haus_info["border"], step)[element_ID]
        action = ActionChains(_config.browser)
        width = slide.size["width"]
        time.sleep(1)
        action.click_and_hold(slide).move_by_offset(-width /
                                                    2 - 2, 0).release().perform()
        time.sleep(1)

    def click_right_arrow_by_name(self, test_case, name, step):
        """
        Clicks 'right' arrow if **"name"** matches 'div.element-parameter-row' --> 'div.element-text-area.name' 

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        name
            A 'text' from 'de.common.json' which used for searching in browser 
        step
            step number from the test case document
        """
        time.sleep(1)
        try:
            for r in self.findElementsCSS(test_case, self.texT["name"], step):
                if r.text == name:
                    r.click()
                    break
        except:
            self.compare_true_result(
                test_case, False, "' " + name + " ' right arrow option is not available", step)

    def check_option_from_row_text(self, test_case, expected_option, step):
        """
        Verifies if **"expected_options"** are presented in UHI based on 'row.text' search

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        expected_option
            A 'string' row name from 'de.common.json' like Wochenverlauf, Funktionsheizen, ...
        step
            step number from the test case document
        """
        for t in self.findElementsCSS(test_case, self.texT["row"], step):
            if t.text == expected_option:
                break
            else:
                self.compare_true_result(
                    test_case, False, expected_option + " --> option is not available", step)

    def check_option_from_toggle_row_text(self, test_case, expected_option, step):
        """
        Verifies if **"expected_toggles"** are presented in the UHI by searching via 'row.text' options

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        expected_option
            A 'string' row name from 'de.common.json' like Anheizprogramm starten, Warmwasserbereitung ...
        step
            step number from the test case document
        """
        for t in self.findElementsCSS(test_case, self.texT["row"], step):
            if t.text == expected_option:
                break
            else:
                self.compare_true_result(
                    test_case, False, expected_option + " --> option is not available", step)

    def get_toggle_button_status(self, test_case, toggle_option, step):
        """
        Returns True/False if 'toggle button' based on toggle status (ON/OFF)

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        toggle_option
            A 'string' from 'de.common.json' like Anheizprogramm starten, Warmwasserbereitung ...
        step
            step number from the test case document

        Returns
        -------
        type: True / False

        """
        for t in self.findElementsCSS(test_case, self.texT["toggle_row"], step):
            if t.find_element_by_css_selector(self.texT["area"]).text == toggle_option:
                button = t.find_element_by_css_selector(
                    self.buttons["toggle_button"])
                if int(button.value_of_css_property(self.css_prop["margin_left"])[0]) != 0:
                    return True
                else:
                    return False

    def get_UHI_values(self, test_case, element_ID, step):
        """
        Return values from (35) from UHI (35°C) close to '+' and '-' buttons

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document

        Returns
        -------
        type: 'int'

        """
        UHI_value = self.findElementsCSS(
            test_case, self.texT["area_text"], step)[element_ID].text
        UHI_value = UHI_value.strip("°C K h Min.")
        return float(''.join(UHI_value))

    def set_temp_via_plus_minus_buttons(self, test_case, soll_wert, element_ID, step):
        """
        Setup desired temperature via + and - buttons in any screen

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        time.sleep(1)

        if len(self.findElementsCSS(test_case, self.buttons["minus"], step)) > 1:
            minus = self.findElementsCSS(test_case, self.buttons["minus"], step)[-1]
        else:
            minus = self.findElementCSS(test_case, self.buttons["minus"], step)

        if len(self.findElementsCSS(test_case, self.buttons["plus"], step)) > 1:
            plus = self.findElementsCSS(test_case, self.buttons["plus"], step)[-1]
        else:
            plus = self.findElementCSS(test_case, self.buttons["plus"], step)

        # set value to start number
        action = ActionChains(_config.browser)
        for i in range(10):
            action.click_and_hold(minus).move_to_element(
                minus).release().perform()
            time.sleep(1)

        # Setup desired temp from Soll_wert
        while True:
            action.click_and_hold(plus).move_to_element(
                plus).release().perform()
            if self.get_UHI_values(test_case, element_ID, step) >= soll_wert:
                break

    def click_right_arrow(self, test_case, element_ID, step):
        """
        Returns a click on 'right arrow' with particular element ID

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        element_ID
            An element ID for the particular screen number
        step
            step number from the test case document
        """
        time.sleep(1)
        return self.findElementsCSS(test_case, self.buttons["right_arrow"], step)[element_ID].click()

    def click_left_arrow(self, test_case, element_ID, step):
        """
        Returns a click on 'Left arrow' with particular element ID

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        element_ID
            An element ID for the particular screen number (default = -1)
        step
            step number from the test case document
        """
        time.sleep(2)
        return self.findElementsCSS(test_case, self.buttons["left_arrow"], step)[element_ID].click()

    def click_dropdown_by_title1(self, test_case, title1_text, step):
        """
        Returns a click on 'Drop down arrow' like "Automatik, Manuell, Ausgange, Eingange" in different screens

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        title1_text
            A 'string' from 'de.common.json' like Automatik, Manuell, Ausgange,.. 
        step
            step number from the test case document
        """
        time.sleep(1)
        for drop in self.findElementsCSS(test_case, self.buttons["drop_down_title1"], step):
            if drop.text == title1_text:
                drop.click()

    def click_dropdown_by_title2(self, test_case, title2_text, step):
        """
        Returns a click on 'Drop down arrow' like "Heizkries 1, 2 and Rooms 50 to 69" in Betreibsdaten

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        title2_text
            A 'string' from 'de.common.json' like Betreibsdaten -> Rooms 50 to 69
        step
            step number from the test case document
        """
        for drop in self.findElementsCSS(test_case, self.buttons["drop_down_title2"], step):
            if drop.text == title2_text:
                drop.click()
                return True
        return False

    def click_text_area_by_name(self, test_case, name, step):
        """
        Returns a click on 'text area' (click on texts)

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        name
            A 'string' from 'de.common.json'
        step
            step number from the test case document
        """
        time.sleep(1)
        for area in self.findElementsCSS(test_case, self.texT["name"], step):
            if area.text == name:
                area.click()
                break

    def set_date_time_via_up_down_arrow(self, test_case, context, max_clicks, element_ID, step):
        """
        Setup random date and time in UHI via "Up" and "Down" arrows
        and returns the set value 

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        context
            Context here refers to "Urlaub" or "Party" due to different arrow counts
        max_clicks
            Maximum number of random clicks based on requirement
        element_ID
            An element ID for the particular 'UP' arrow index
        step
            step number from the test case document

        Returns
        -------
        type: 'int'
        value: date, month, year, time
        """
        time.sleep(1)
        action = ActionChains(_config.browser)

        # set initial date for continuous testing
        if context == "Urlaub" and element_ID == 0:
            down_arrow = self.findElementsCSS(test_case,
                                              self.buttons["down_arrow"], step)[-1]
            action.click_and_hold(down_arrow).move_to_element(
                down_arrow).release().perform()

        if context == "Party":
            down_arrow = self.findElementsCSS(test_case,
                                              self.buttons["down_arrow"], step)[0]
            for i in range(5):
                action.click_and_hold(down_arrow).move_to_element(
                    down_arrow).release().perform()

        # set value via up arrow
        up_arrow = self.findElementsCSS(test_case,
                                        self.buttons["up_arrow"], step)[element_ID]
        rand = random.randint(0, max_clicks)
        for i in range(rand):
            action.click_and_hold(up_arrow).move_to_element(
                up_arrow).release().perform()
        time.sleep(5)

        if context == "Urlaub" or context == "Party":
            return int(self.findElementsCSS(test_case, self.texT["spin_box"], step)[element_ID].text)

    def get_today_from_ntp(self):
        """
        Returns "current" date from 'ntp' request

        Return
        ------
        type: 'int' tuple
        value: Date, Month, Year

        """
        import ntplib
        from datetime import datetime, date
        client = ntplib.NTPClient()
        response = client.request('de.pool.ntp.org', version=3)
        t = datetime.fromtimestamp(response.tx_time)
        return date(int(t.strftime("%Y")), int(t.strftime("%m")), int(t.strftime("%d")))

    def wait_browser(self, test_case, times_X_5s, step):
        """
        Browser waits for '60' seconds to verify with 'WPM'
        and closes the 'error' popup if any

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        for i in range(int(times_X_5s)):
            time.sleep(5)
            self.close_error_popup(test_case, step)

    def check_internet(self):
        """
        Returns 'True' if 'Network' is active

        Return
        ------
        type: 'boolean'

        """
        response = os.system("ping -n 1 www.google.com >/dev/null 2>&1")
        if response == 0:
            return True
        else:
            return False

    def click_Edit_text(self, test_case, step):
        """
        Returns a click on 'Edit' text in the UHI

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        edit = self.findElementCSS(test_case, self.buttons["Edit_text"], step)
        if edit.text == self._content["EDIT"]:
            return edit.click()

    def click_Anpassen_text(self, test_case, step):
        """
        Returns a click on 'Edit' text in the UHI

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        return self.findElementCSS(test_case, "div.element-text-area.edit.ripple", step).click()

    def click_Edit_without_ripple_text(self, test_case, step):
        """
        Returns a click on 'Edit' text in the `Profiles` screen where `Edit` is not changed to 
        `Fertig` text (No ripple text) 

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        time.sleep(1)
        edit = self.findElementCSS(
            test_case, self.buttons["Edit_text_no_ripple"], step)
        if edit.text == self._content["EDIT"] or edit.text == "An­pas­sen":
            return edit.click()

    def click_Fertig_without_ripple_text(self, test_case, step):
        """
        Returns a click on 'Fertig' text in the `Profiles` screen where `Fertig` is not changed to 
        `Edit` text (No ripple text) 

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        time.sleep(1)
        Done = self.findElementCSS(
            test_case, self.buttons["Done_text_no_ripple"], step)
        if Done.text == self._content["DONE"]:
            return Done.click()

    def click_Edit_icon(self, test_case, element_ID, step):
        """
        Returns a click on 'Pen' button in the UHI

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        element_ID
            Specific 'element' index for this action
        step
            step number from the test case document
        """
        return self.findElementsCSS(test_case, self.buttons["Edit_icon"], step)[element_ID].click()

    def click_Delete_icon(self, test_case, element_ID, step):
        """
        Returns a click on 'Delete' button in the UHI

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        element_ID
            Specific 'element' index for this action
        step
            step number from the test case document
        """
        return self.findElementsCSS(test_case, self.buttons["Delete_icon"], step)[element_ID].click()

    def click_X_icon(self, test_case, element_ID, step):
        """
        Returns a click on 'X' icon after deleting button clicked in the UHI

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        element_ID
            Specific 'element' index for this action
        step
            step number from the test case document
        """
        return self.findElementsCSS(test_case, self.buttons["X_icon"], step)[element_ID].click()

    def Edit_text_box(self, test_case, ID, Name, step):
        """
        Enter text from 'User' input with particular text box 'ID'

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        ID
            Element 'ID' of the 'text box'
        Name
            New name to be entered in the 'text box'
        step
            step number from the test case document
        """
        text_box = self.findElementId(test_case, ID, step)
        text_box.clear()
        text_box.click()
        text_box.send_keys(Name)
        text_box.send_keys(Keys.RETURN)

    def get_click_from_text_area(self, test_case, area_text, step):
        """
        Returns a click from given 'element-text-area' value

        """
        time.sleep(1)
        for tex in self.findElementsCSS(test_case, self.texT["area"], step):
            if tex.text == area_text:
                tex.click()
                break

    def get_screen_saver_clock_status(self, test_case, step):
        """
        Returns 'True' if screen saver `clock` is visible
        """
        clock = self.findElementCSS(test_case, self.elements["clock"], step)
        if clock is not None:
            return True
        else:
            return False

    def get_string_without_soft_hyphen(self, text):
        """
        Returns 'string' without soft hyphens for comaprison
        """
        # text.replace('\u00ad', '')
        # text.replace('\N{SOFT HYPHEN}', '')
        return text.replace('\xad', '')

    def check_if_sensor_broken(self, value):
            return -999.9 if value == 5553.7 else value
