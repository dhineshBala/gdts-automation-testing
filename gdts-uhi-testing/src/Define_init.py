from selenium import webdriver
import os

class Define_init(object):
    """
    This class provokes the **'Selenium'** webdriver for the browser automation testing. In our case,
    we perform this automation with help of 'Chrome' webdriver since the UHI is designed to render on Chromium browser.
    
    .. code-block:: python
       :caption: Define_init.py
        
        def __init__(self):
            option = webdriver.ChromeOptions()
            option.add_argument("--kiosk")
            option.add_argument("--start-maximized")
            option.add_argument("--disable-infobars")
            self.browser = webdriver.Chrome(
                "path\\to\\chromedriver.exe", options=option)
    """
    def __init__(self):
        """
        Setup the *webdriver* with various capabilities for the automation testing.

        """
        # # Only for Sphinx report
        self.path_to_dir = "D:\\Project\\31138_AutomationTesting\\gdts-automation-testing\\gdts-uhi-testing"

        # In general, os.getcwd() works
        # self.path_to_dir = os.getcwd()
        option = webdriver.ChromeOptions()
        option.add_argument("--kiosk")
        option.add_argument("--start-maximized")
        option.add_argument("--disable-infobars")
        # option.add_argument("--headless")
        self.browser = webdriver.Chrome(os.path.join(self.path_to_dir, 'chromedriver.exe'), options=option)

        


       
