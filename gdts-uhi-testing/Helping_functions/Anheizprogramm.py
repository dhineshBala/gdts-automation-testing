from src.General_functions import GeneralFunctions
import time
import json

element_ID = -1

class AnheizProgramm(GeneralFunctions):
    """
    This is a helping class for the test case 'UHI-17 and 18 -- test_Funktionsheizen and test_Belegreifheizen' with get, set and verify functions.

    * :py:meth:`Helping_functions.Anheizprogramm.AnheizProgramm.check_Funktionsheizen_screen`
    * :py:meth:`Helping_functions.Anheizprogramm.AnheizProgramm.activate_WwBereitung`
    * :py:meth:`Helping_functions.Anheizprogramm.AnheizProgramm.start_Anheizprogramm`
    * :py:meth:`Helping_functions.Anheizprogramm.AnheizProgramm.stop_Anheizprogramm`
    * :py:meth:`Helping_functions.Anheizprogramm.AnheizProgramm.check_count_down_screen`
    * :py:meth:`Helping_functions.Anheizprogramm.AnheizProgramm.set_Anheizprogramm_Max_temp`
    * :py:meth:`Helping_functions.Anheizprogramm.AnheizProgramm.get_Max_temp_from_WPM`
    * :py:meth:`Helping_functions.Anheizprogramm.AnheizProgramm.get_status_WwBereitung_from_WPM`
    * :py:meth:`Helping_functions.Anheizprogramm.AnheizProgramm.get_status_Anheizen_status_from_WPM`
    * :py:meth:`Helping_functions.Anheizprogramm.AnheizProgramm.verify_Funktionsheizen_in_WPM`
    * :py:meth:`Helping_functions.Anheizprogramm.AnheizProgramm.check_Belegreifheizen_screen`
    * :py:meth:`Helping_functions.Anheizprogramm.AnheizProgramm.set_TempDiff_Aufheizen`
    * :py:meth:`Helping_functions.Anheizprogramm.AnheizProgramm.set_ZeitDauer_Aufheizen`
    * :py:meth:`Helping_functions.Anheizprogramm.AnheizProgramm.set_ZeitDauer_Halten`
    * :py:meth:`Helping_functions.Anheizprogramm.AnheizProgramm.set_TempDiff_Abheizen`
    * :py:meth:`Helping_functions.Anheizprogramm.AnheizProgramm.set_ZeitDauer_Abheizen`
    * :py:meth:`Helping_functions.Anheizprogramm.AnheizProgramm.setup_Belegreifheizen`
    * :py:meth:`Helping_functions.Anheizprogramm.AnheizProgramm.get_TempDiff_Aufheizen_WPM`
    * :py:meth:`Helping_functions.Anheizprogramm.AnheizProgramm.get_Zeitdauer_Aufheizen_WPM`
    * :py:meth:`Helping_functions.Anheizprogramm.AnheizProgramm.get_Zeitdauer_Halten_WPM`
    * :py:meth:`Helping_functions.Anheizprogramm.AnheizProgramm.get_TempDiff_Abheizen_WPM`
    * :py:meth:`Helping_functions.Anheizprogramm.AnheizProgramm.get_Zeitdauer_Abheizen_WPM`
    * :py:meth:`Helping_functions.Anheizprogramm.AnheizProgramm.get_status_Belegriefheizen_status_from_WPM`
    * :py:meth:`Helping_functions.Anheizprogramm.AnheizProgramm.verify_Belegreifheizen`
    
    """
    genl = GeneralFunctions()
    opt_Funktionsheizen = genl._content["FUNCTION_HEATING"]
    opt_Belegreifheizen = genl._content["READY_FOR_LAYING_HEATING"]
    opt_MaxTemp = genl._content["TITLE_P_AHP_MAX_TEMP"]
    opt_WwBereitung = genl._content["WW_POOL_PREPARATION"]
    opt_AnheizProgramm = genl._content["START_HEATING_PROGRAM"]
    opt_TempDiff_Aufheizen = genl._content["TITLE_P_AHP_DIFF_AUF"]
    opt_ZeitDauer_Aufheizen = genl._content["TITLE_P_AHP_ZEIT_AUF"]
    opt_ZeitDauer_Halten = genl._content["TITLE_P_AHP_ZEIT_HALT"]
    opt_TempDiff_Abheizen = genl._content["TITLE_P_AHP_DIFF_AB"]
    opt_ZeitDauer_Abheizen = genl._content["TITLE_P_AHP_ZEIT_AB"]


    Anheiz_info = genl.user_input["Anheizprogramm"]
    Belegreif_info = Anheiz_info["Belegreifheizen"]

    def check_Funktionsheizen_screen(self, test_case, step):
        """
        Verifies 'Funcktionsheizen' screeen options inside Anheizprogramm

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document

        """
        self.check_option_from_row_text(test_case, self.opt_MaxTemp, step)
        self.check_presence_of_option_from_toggle_row_text(
            test_case, self.opt_WwBereitung, step)
        self.check_presence_of_option_from_toggle_row_text(
            test_case, self.opt_AnheizProgramm, step)

    def activate_WwBereitung(self, test_case, step):
        """
        Activates 'Warmwasserbereitung' inside Funktionsheizen screen

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        if self.get_toggle_button_status(test_case, self.opt_WwBereitung, step):
            pass
        else:
            self.get_click_from_toggle_row(test_case, self.opt_WwBereitung, step)
            time.sleep(2)
            self.get_toggle_button_status(test_case, self.opt_WwBereitung, step)

    def start_Anheizprogramm(self, test_case, step):
        """
        Starts 'Anheizprogramm' inside Funktionsheizen screen

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        if self.get_toggle_button_status(test_case, self.opt_AnheizProgramm, step):
            pass
        else:
            self.get_click_from_toggle_row(test_case, self.opt_AnheizProgramm, step)
            time.sleep(2)
            step += 1

            # 9 Message with 60s timer appears.
            self.check_count_down_screen(
                test_case, self._content["ACTIVATE_PROGRAM"], step)
            time.sleep(80)

    def stop_Anheizprogramm(self, test_case, step):
        """
        Stops 'Anheizprogramm' inside Funktionsheizen screen

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        if self.get_toggle_button_status(test_case, self.opt_AnheizProgramm, step):
            self.get_click_from_toggle_row(test_case, self.opt_AnheizProgramm, step)
            time.sleep(2)

    def check_count_down_screen(self, test_case, info, step):
        """
        Verifies 'count down' timer screen in desired screen

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        info
            Desired 'text' in the count down screen
        step
            step number from the test case document
        """
        for t in self.findElementsCSS(test_case, self.elements["countDown_screen"], step):
            if t.find_element_by_css_selector(self.texT["area"]).text == info:
                break
            else:
                self.compare_true_result(
                    test_case, False, info + " --> information is not available", step)

    def set_Anheizprogramm_Max_temp(self, test_case, step):
        """
        setup the 'Maximum' temp for Anheizprogram using + and - buttons

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        self.set_temp_via_plus_minus_buttons(test_case,
            self.Anheiz_info["Max_temp"], element_ID, step)

    def get_Max_temp_from_WPM(self):
        """
        returns 'Maximum' temperature for Anheizprogramm

        Returns
        -------
        'int'
            temperature
        """
        return self.tcp.get_values_from_integer_registers(self.regs_integer["P_AHP_MAX_TEMP"])

    def get_status_WwBereitung_from_WPM(self):
        """
        returns True/False from WPM registers

        Returns
        -------
        'Boolean'
            True / False
        """
        return self.tcp.get_status_from_coils(self.regs_digital["P_ANHEIZP_WW_OK"])

    def get_status_Anheizen_status_from_WPM(self):
        """
        returns True/False from WPM registers

        Returns
        -------
        'Boolean'
            True / False
        """
        return self.tcp.get_status_from_coils(self.regs_digital["P_ANHEIZP_FUN"])

    def verify_Funktionsheizen_in_WPM(self, test_case, step):
        """
        Verifies 'Funktionsheizen' screen values / status with WPM

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        # verify Maximum temp with WPM
        self.click_right_arrow_by_name(test_case, self.opt_MaxTemp, step)
        UHI_temp = self.get_UHI_values(test_case, element_ID, step)
        WPM_temp = self.get_Max_temp_from_WPM()
        self.compare_results(test_case, WPM_temp, UHI_temp, "Maximum temperature mismatch", step)
        self.click_back_by_header_title(test_case, self.opt_MaxTemp, step)

        # verify Ww.bereitung status with WPM
        UHI_WwBereitung_status = self.get_toggle_button_status(test_case, self.opt_WwBereitung, step)
        WPM_WwBereitung_status = self.get_status_WwBereitung_from_WPM()
        self.compare_results(test_case, WPM_WwBereitung_status, UHI_WwBereitung_status, "Warmwasserbereitung toggle status mismatch", step)
        
        # verify Anheizprogramm status with WPM
        UHI_Anheiz_status = self.get_toggle_button_status(test_case, self.opt_AnheizProgramm, step)
        WPM_Anheiz_status = self.get_status_Anheizen_status_from_WPM()
        self.compare_results(test_case, WPM_Anheiz_status, UHI_Anheiz_status, "Anheizprogramm Starten toggle status mismatch", step)


    def check_Belegreifheizen_screen(self, test_case, step):
        """
        Verifies 'Belegreifheizen' screeen options inside Anheizprogramm

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        self.check_option_from_row_text(test_case, self.opt_MaxTemp, step)
        self.check_option_from_row_text(test_case, self.opt_TempDiff_Aufheizen, step)
        self.check_option_from_row_text(test_case, self.opt_ZeitDauer_Aufheizen, step)
        self.check_option_from_row_text(test_case, self.opt_ZeitDauer_Halten, step)
        self.check_option_from_row_text(test_case, self.opt_TempDiff_Abheizen, step)
        self.check_option_from_row_text(test_case, self.opt_ZeitDauer_Abheizen, step)
        self.check_presence_of_option_from_toggle_row_text(
            test_case, self.opt_WwBereitung, step)
        self.check_presence_of_option_from_toggle_row_text(
            test_case, self.opt_AnheizProgramm, step)

    def set_TempDiff_Aufheizen(self, test_case, step):
        """
        setup the temperature difference of 'Aufheizen' via + and - buttons

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        time.sleep(1)
        self.set_temp_via_plus_minus_buttons(test_case,
            self.Belegreif_info["Aufheizen"]["temp_differenz"], element_ID, step)
    
    def set_ZeitDauer_Aufheizen(self, test_case, step):
        """
        setup the Zeitdauer of 'Aufheizen' via + and - buttons

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        time.sleep(1)
        self.set_temp_via_plus_minus_buttons(test_case,
            self.Belegreif_info["Aufheizen"]["Zeit_dauer"], element_ID, step)

    def set_ZeitDauer_Halten(self, test_case, step):
        """
        setup the Zeitdauer 'Halten' via + and - buttons

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        time.sleep(1)
        self.set_temp_via_plus_minus_buttons(test_case,
            self.Belegreif_info["Zeitdauer_Halten"], element_ID, step)
    
    def set_TempDiff_Abheizen(self, test_case, step):
        """
        setup the temperature difference of 'Abheizen' via + and - buttons

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        time.sleep(1)
        self.set_temp_via_plus_minus_buttons(test_case,
            self.Belegreif_info["Abheizen"]["temp_differenz"], element_ID, step)
    
    def set_ZeitDauer_Abheizen(self, test_case, step):
        """
        setup the Zeitdauer of 'Abheizen' via + and - buttons

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        time.sleep(1)
        self.set_temp_via_plus_minus_buttons(test_case,
            self.Belegreif_info["Abheizen"]["Zeit_dauer"], element_ID, step)

    def setup_Belegreifheizen(self, test_case, step):
        """
        setup the 'Belegreifheizen' options based on 'user_input.json' file 

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        # Maximum temperature
        # self.click_right_arrow_by_name(test_case, self.opt_MaxTemp, step)
        # self.set_Anheizprogramm_Max_temp(test_case, step)
        # self.click_back_by_header_title(test_case, self.opt_MaxTemp, step)

        # Temp Differenz 'Aufheizen'
        self.click_right_arrow_by_name(test_case, self.opt_TempDiff_Aufheizen, step)
        self.set_TempDiff_Aufheizen(test_case, step)
        self.click_back_by_header_title(test_case, self.opt_TempDiff_Aufheizen, step)

        # Zeitdauer 'Aufheizen'
        self.click_right_arrow_by_name(test_case, self.opt_ZeitDauer_Aufheizen, step)
        self.set_ZeitDauer_Aufheizen(test_case, step)
        self.click_back_by_header_title(test_case, self.opt_ZeitDauer_Aufheizen, step)

        # Zeitdauer Halten
        self.click_right_arrow_by_name(test_case, self.opt_ZeitDauer_Halten, step)
        self.set_ZeitDauer_Halten(test_case, step)
        self.click_back_by_header_title(test_case, self.opt_ZeitDauer_Halten, step)

        # Temp Differenz 'Abheizen'
        self.click_right_arrow_by_name(test_case, self.opt_TempDiff_Abheizen, step)
        self.set_TempDiff_Abheizen(test_case, step)
        self.click_back_by_header_title(test_case, self.opt_TempDiff_Abheizen, step)

        # Zeitdauer 'Abheizen'
        self.click_right_arrow_by_name(test_case, self.opt_ZeitDauer_Abheizen, step)
        self.set_ZeitDauer_Abheizen(test_case, step)
        self.click_back_by_header_title(test_case, self.opt_ZeitDauer_Abheizen, step)

        # activate 'Ww Bereitung'
        self.activate_WwBereitung(test_case, step)
        
        # start 'Anheizenprogramm'
        self.start_Anheizprogramm(test_case, step)

    def get_TempDiff_Aufheizen_WPM(self):
        """
        returns 'Aufheizen' temperature differenz for Belegreifheizen

        Returns
        -------
        'int'
            temperature
        """
        return self.tcp.get_values_from_integer_registers(self.regs_integer["P_AHP_DIFF_AUF"])
    
    def get_Zeitdauer_Aufheizen_WPM(self):
        """
        returns 'Aufheizen' Zeitdauer for Belegreifheizen

        Returns
        -------
        'int'
            hours
        """
        return self.tcp.get_values_from_integer_registers(self.regs_integer["P_AHP_ZEIT_AUF"])

    def get_Zeitdauer_Halten_WPM(self):
        """
        returns 'Halten' Zeitdauer for Belegreifheizen

        Returns
        -------
        'int'
            hours
        """
        return self.tcp.get_values_from_integer_registers(self.regs_integer["P_AHP_ZEIT_HALT"])

    def get_TempDiff_Abheizen_WPM(self):
        """
        returns 'Abheizen' temperature differenz for Belegreifheizen

        Returns
        -------
        'int'
            temperature
        """
        return self.tcp.get_values_from_integer_registers(self.regs_integer["P_AHP_DIFF_AB"])
    
    def get_Zeitdauer_Abheizen_WPM(self):
        """
        returns 'Abheizen' Zeitdauer for Belegreifheizen

        Returns
        -------
        'int'
            hours
        """
        return self.tcp.get_values_from_integer_registers(self.regs_integer["P_AHP_ZEIT_AB"])

    def get_status_Belegriefheizen_status_from_WPM(self):
        """
        returns True/False from WPM registers

        Returns
        -------
        'Boolean'
            True / False
        """
        return self.tcp.get_status_from_coils(self.regs_digital["P_ANHEIZP_IND"])

    def verify_Belegreifheizen(self, test_case, step):
        """
        Verifies 'Belegreifheizen' options based on 'user_input.json' file 

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        # Maximum temperature
        self.click_right_arrow_by_name(test_case, self.opt_MaxTemp, step)
        self.compare_results(test_case, self.get_Max_temp_from_WPM(), self.get_UHI_values(test_case, element_ID, step), "Maximum temperature mismatch", step)
        self.click_back_by_header_title(test_case, self.opt_MaxTemp, step)

        # Temp Differenz 'Aufheizen'
        self.click_right_arrow_by_name(test_case, self.opt_TempDiff_Aufheizen, step)
        self.compare_results(test_case, self.get_TempDiff_Aufheizen_WPM(), self.get_UHI_values(test_case, element_ID, step), "Temp Diff 'Aufheizen' mismatch", step)
        self.click_back_by_header_title(test_case, self.opt_TempDiff_Aufheizen, step)

        # Zeitdauer 'Aufheizen'
        self.click_right_arrow_by_name(test_case, self.opt_ZeitDauer_Aufheizen, step)
        self.compare_results(test_case, self.get_Zeitdauer_Aufheizen_WPM(), self.get_UHI_values(test_case, element_ID, step), "Zeitdauer 'Aufheizen' mismatch", step)
        self.click_back_by_header_title(test_case, self.opt_ZeitDauer_Aufheizen, step)

        # Zeitdauer Halten
        self.click_right_arrow_by_name(test_case, self.opt_ZeitDauer_Halten, step)
        self.compare_results(test_case, self.get_Zeitdauer_Halten_WPM(), self.get_UHI_values(test_case, element_ID, step), "Zeitdauer 'Halten' mismatch", step)
        self.click_back_by_header_title(test_case, self.opt_ZeitDauer_Halten, step)

        # Temp Differenz 'Abheizen'
        self.click_right_arrow_by_name(test_case, self.opt_TempDiff_Abheizen, step)
        self.compare_results(test_case, self.get_TempDiff_Abheizen_WPM(), self.get_UHI_values(test_case, element_ID, step), "Temp Diff 'Abheizen' mismatch", step)
        self.click_back_by_header_title(test_case, self.opt_TempDiff_Abheizen, step)

        # Zeitdauer 'Abheizen'
        self.click_right_arrow_by_name(test_case, self.opt_ZeitDauer_Abheizen, step)
        self.compare_results(test_case, self.get_Zeitdauer_Abheizen_WPM(), self.get_UHI_values(test_case, element_ID, step), "Zeitdauer 'Abheizen' mismatch", step)
        self.click_back_by_header_title(test_case, self.opt_ZeitDauer_Abheizen, step)

        # verify Ww.bereitung status with WPM
        UHI_WwBereitung_status = self.get_toggle_button_status(test_case, self.opt_WwBereitung, step)
        WPM_WwBereitung_status = self.get_status_WwBereitung_from_WPM()
        self.compare_results(test_case, WPM_WwBereitung_status, UHI_WwBereitung_status, "Warmwasserbereitung toggle status mismatch", step)
        
        # verify Anheizprogramm status with WPM
        UHI_Anheiz_status = self.get_toggle_button_status(test_case, self.opt_AnheizProgramm, step)
        WPM_Anheiz_status = self.get_status_Belegriefheizen_status_from_WPM()
        self.compare_results(test_case, WPM_Anheiz_status, UHI_Anheiz_status, "Belegriefheizen --> Anheizprogramm Starten toggle status mismatch", step)

        # turn off Anheizprogramm and check with WPM
        self.stop_Anheizprogramm(test_case, step)

        time.sleep(40)

        # verify Anheizprogramm status with WPM
        UHI_Anheiz_status = self.get_toggle_button_status(test_case, self.opt_AnheizProgramm, step)
        WPM_Anheiz_status = self.get_status_Belegriefheizen_status_from_WPM()
        self.compare_results(test_case, WPM_Anheiz_status, UHI_Anheiz_status, "Belegriefheizen --> Anheizprogramm Starten toggle status mismatch", step)
