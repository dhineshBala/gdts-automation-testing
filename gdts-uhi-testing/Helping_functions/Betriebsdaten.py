from src.General_functions import GeneralFunctions
from Profile_functions.warmwasser_setting import WarmwasserSetting
import time

_config = WarmwasserSetting()._config
profile_info = WarmwasserSetting().profile_info

class BetriebsDaten(GeneralFunctions):
    """
    This is a helping class for the test case 'UHI-23 -- Betriebsdaten' with get and verify functions.

    * :py:meth:`Helping_functions.Betriebsdaten.BetriebsDaten.get_Aussen_temp_from_WPM`
    * :py:meth:`Helping_functions.Betriebsdaten.BetriebsDaten.get_Anz_Ww_Soll_temp`
    * :py:meth:`Helping_functions.Betriebsdaten.BetriebsDaten.get_Vorlauf_WP_temp_from_WPM`
    * :py:meth:`Helping_functions.Betriebsdaten.BetriebsDaten.get_Ruecklauf_WP_temp_from_WPM`
    * :py:meth:`Helping_functions.Betriebsdaten.BetriebsDaten.get_HK1_Ist_temp_from_WPM`
    * :py:meth:`Helping_functions.Betriebsdaten.BetriebsDaten.get_HK1_Soll_temp_from_WPM`
    * :py:meth:`Helping_functions.Betriebsdaten.BetriebsDaten.get_HK2_Ist_temp_from_WPM`
    * :py:meth:`Helping_functions.Betriebsdaten.BetriebsDaten.get_HK2_Soll_temp_from_WPM`
    * :py:meth:`Helping_functions.Betriebsdaten.BetriebsDaten.get_other_Betriebsdaten_info_from_WPM`
    * :py:meth:`Helping_functions.Betriebsdaten.BetriebsDaten.get_Werts_values_from_WPM`
    * :py:meth:`Helping_functions.Betriebsdaten.BetriebsDaten.get_Werts_from_WPM`
    * :py:meth:`Helping_functions.Betriebsdaten.BetriebsDaten.Betriebdaten_without_units`
    * :py:meth:`Helping_functions.Betriebsdaten.BetriebsDaten.get_Betriebsdaten_from_UHI`
    * :py:meth:`Helping_functions.Betriebsdaten.BetriebsDaten.verify_Allgemeine_Betriebsdaten_with_WPM`
    * :py:meth:`Helping_functions.Betriebsdaten.BetriebsDaten.verify_specific_HK_Betriebsdaten`

    """
    def get_Aussen_temp_from_WPM(self):
        """
        returns 'Aussentemperatur' from WPM

        Returns
        -------
        'float'
            AussenTemperatur
        """
        reg_index = self.regs_analog["E_Aussen_T"]
        aussen_T = self.tcp.get_values_from_analog_registers(reg_index, 10)
        return aussen_T

    def get_Anz_Ww_Soll_temp(self):
        """
        returns 'Warmwasser' Soll temperatur for Betriebsdaten

        Returns
        -------
        'float'
            Temperatur
        """
        reg_index = self.regs_integer["Ww_Soll_Anz"]
        Ww_Soll_Anz = self.tcp.get_values_from_integer_registers(reg_index)/10
        return Ww_Soll_Anz

    def get_Vorlauf_WP_temp_from_WPM(self):
        """
        returns 'Vorlauf temperatur' of WP from WPM

        Returns
        -------
        'float'
            Temperatur
        """
        reg_index = self.regs_analog["E_Vorl_T_WP"]
        Vorlauf_WP = self.tcp.get_values_from_analog_registers(reg_index, 10)
        return Vorlauf_WP

    def get_Ruecklauf_WP_temp_from_WPM(self):
        """
        returns 'Ruecklauf temperatur' of WP from WPM

        Returns
        -------
        'float'
            Temperatur
        """
        reg_index = self.regs_analog["E_Rueckl_T_WP"]
        Rueckl_WP = self.tcp.get_values_from_analog_registers(reg_index, 10)
        return Rueckl_WP

    def get_HK1_Ist_temp_from_WPM(self):
        """
        returns 'HK1' Ist temperatur for Betriebsdaten

        Returns
        -------
        'float'
            Temperatur
        """
        reg_index = self.regs_integer["Anz_HK1_Ist_Temp"]
        HK1_Ist_temp = self.tcp.get_values_from_integer_registers(reg_index)/10
        return HK1_Ist_temp

    def get_HK1_Soll_temp_from_WPM(self):
        """
        returns 'HK1' Soll temperatur for Betriebsdaten

        Returns
        -------
        'float'
            Temperatur
        """
        reg_index = self.regs_integer["Anz_HK1_Soll_Temp"]
        HK1_Soll_temp = self.tcp.get_values_from_integer_registers(
            reg_index)/10
        return HK1_Soll_temp

    def get_HK2_Ist_temp_from_WPM(self):
        """
        returns 'HK2' Ist temperatur for Betriebsdaten

        Returns
        -------
        'float'
            Temperatur
        """
        reg_index = self.regs_integer["HK2_Ist_Temp"]
        HK2_Ist_temp = self.tcp.get_values_from_integer_registers(reg_index)/10
        return HK2_Ist_temp

    def get_HK2_Soll_temp_from_WPM(self):
        """
        returns 'HK2' Soll temperatur for Betriebsdaten

        Returns
        -------
        'float'
            Temperatur
        """
        reg_index = self.regs_integer["Anz_HK2_Soll_Temp"]
        HK2_Soll_temp = self.tcp.get_values_from_integer_registers(
            reg_index)/10
        return HK2_Soll_temp

    def get_other_Betriebsdaten_info_from_WPM(self):
        """
        returns Anlagendruck, Durchfluss, Hochdruck, Niederdruck infos for Betriebsdaten from WPM

        Returns
        -------
        'float' tuple
            AD, DF, HD, ND values
        """
        Anlagendruck = self.tcp.get_values_from_analog_registers(
            self.regs_analog["E_Sek_Druck"], 10)
        Durchfluss = self.tcp.get_values_from_integer_registers(
            self.regs_integer["E_DfSen_Sek"])
        HD = self.tcp.get_values_from_analog_registers(
            self.regs_analog["E_DRUCK_HD"], 10)
        ND = self.tcp.get_values_from_analog_registers(
            self.regs_analog["E_DRUCK_ND"], 10)
        return Anlagendruck, Durchfluss, HD, ND

    def get_Werts_values_from_WPM(self):
        """
        returns Sperrwert, Stoerwert, Fehlerwert infos for 'Systemzustand' check from WPM

        Returns
        -------
        'string' tuple
            Sperrwert, Stoerwert, Fehlerwert
        """
        Sperre_wert_int = self.tcp.get_values_from_integer_registers(
            self.regs_integer["Sperr_Wp_Wert"])/10
        
        Stoerung_wert_int = self.tcp.get_values_from_integer_registers(
            self.regs_integer["Stoerung_Wert"])/10
        
        Fehler_wert_int = self.tcp.get_values_from_integer_registers(
            self.regs_integer["Fehler_Wert"])/10
        
        return Sperre_wert_int, Stoerung_wert_int, Fehler_wert_int

    def get_Werts_from_WPM(self):
        """
        returns Statuswert, Sperrwert, Stoerwert, Fehlerwert infos for Betriebsdaten from WPM

        Returns
        -------
        'string' tuple
            Statuswert, Sperrwert, Stoerwert, Fehlerwert
        """
        Status_wert_int = self.tcp.get_values_from_integer_registers(
            self.regs_integer["Status_Wert"])/10
        Status_wert = self._content["STATUS_" + str(Status_wert_int)]
        
        Sperre_wert_int = self.tcp.get_values_from_integer_registers(
            self.regs_integer["Sperr_Wp_Wert"])/10
        Sperre_wert = self._content["LOCK_" + str(Sperre_wert_int)]
        
        Stoerung_wert_int = self.tcp.get_values_from_integer_registers(
            self.regs_integer["Stoerung_Wert"])/10
        Stoerung_wert = self._content["FAULT_" + str(Stoerung_wert_int)]
        
        Fehler_wert_int = self.tcp.get_values_from_integer_registers(
            self.regs_integer["Fehler_Wert"])/10
        Fehler_wert = self._content["ERROR_" + str(Fehler_wert_int)]
        
        return Status_wert, Sperre_wert, Stoerung_wert, Fehler_wert

    def Betriebdaten_without_units(self, value, step):
        """
        return values with "° C bar l/h" stripped

        Parameters
        ----------
        value
            Read 'string' with units from UHI
        step
            step number from the test case document
        
        Returns
        -------
        'float'
            All values without units from UHI
        """
        string = value.strip("° C bar l/h % K")
        return float(''.join(string))

    def get_Betriebsdaten_from_UHI(self, test_case, step):
        """
        returns 'Betriebsdaten' in float datatype from UHI

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document

        Returns
        -------
        'float', 'string' tuple
            Betriebsdaten in float types
        """
        UHI_E_Aussen_T = self.Betriebdaten_without_units(
            self.get_text_from_row(test_case, self._content["E_Aussen_T"], step), step)

        UHI_Ww_Ist, UHI_Ww_Soll = 0, 0
        if _config.P_WW():
            UHI_Ww_Ist = self.Betriebdaten_without_units(
                self.get_text_from_row(test_case, self._content["E_Ww_Fuehl"], step), step)
            UHI_Ww_Soll = self.Betriebdaten_without_units(
                self.get_text_from_row(test_case, self._content["Ww_Soll_Anz"], step), step)

        UHI_Vorlauf_Wp = self.Betriebdaten_without_units(
            self.get_text_from_row(test_case, self._content["E_Vorl_T"], step), step)
        UHI_Rucklauf_Wp = self.Betriebdaten_without_units(
            self.get_text_from_row(test_case, self._content["E_Rueckl_T"], step), step)
        
        UHI_HK1_Ist, UHI_HK1_Soll = 0, 0
        if _config.P_HK_1() > 0:
            UHI_HK1_Ist = self.Betriebdaten_without_units(
                self.get_text_from_row(test_case, "HK1 Ist-Tem­pe­ra­tur", step), step)
            UHI_HK1_Soll = self.Betriebdaten_without_units(
                self.get_text_from_row(test_case, "HK1 Soll-Tem­pe­ra­tur", step), step)

        UHI_HK2_Ist, UHI_HK2_Soll = 0, 0
        if _config.P_HK_2() > 0:
            UHI_HK2_Ist = self.Betriebdaten_without_units(
                self.get_text_from_row(test_case, "HK2 Ist-Tem­pe­ra­tur", step), step)
            UHI_HK2_Soll = self.Betriebdaten_without_units(
                self.get_text_from_row(test_case, "HK2 Soll-Tem­pe­ra­tur", step), step)

        UHI_Anlagendruck = self.Betriebdaten_without_units(
            self.get_text_from_row(test_case, self._content["E_Sek_Druck"], step), step)
        UHI_Duruchfluss = self.Betriebdaten_without_units(
            self.get_text_from_row(test_case, self._content["E_DfSen_Sek"], step), step)
        UHI_Hochdruck = self.Betriebdaten_without_units(
            self.get_text_from_row(test_case, self._content["E_DRUCK_HD"], step), step)
        UHI_Niederdruck = self.Betriebdaten_without_units(
            self.get_text_from_row(test_case, self._content["E_DRUCK_ND"], step), step)

        UHI_Statuswert = self.get_text_from_row(test_case,
            self._content["Status_Wert"], step)
        UHI_Sperrewert = self.get_text_from_row(test_case,
            self._content["Sperr_Wp_Wert"], step)
        UHI_Stoerwert = self.get_text_from_row(test_case,
            self._content["Stoerung_Wert"], step)
        UHI_Fehlerwert = self.get_text_from_row(test_case,
            self._content["Fehler_Wert"], step)

        return UHI_E_Aussen_T, UHI_Ww_Ist, UHI_Ww_Soll, UHI_Vorlauf_Wp, UHI_Rucklauf_Wp, UHI_HK1_Ist, UHI_HK1_Soll, UHI_HK2_Ist, UHI_HK2_Soll, UHI_Anlagendruck, UHI_Duruchfluss, UHI_Hochdruck, UHI_Niederdruck, UHI_Statuswert, UHI_Sperrewert, UHI_Stoerwert, UHI_Fehlerwert

    def verify_Allgemeine_Betriebsdaten_with_WPM(self, test_case, step):
        """
        Verifies *Allgemeine* 'Betriebsdaten' from the UHI with WPM by comparing them

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        self.click_right_arrow(test_case, 0, step)
        time.sleep(5)
        self.close_error_popup(test_case, step)
        UHI_E_Aussen_T, UHI_Ww_Ist, UHI_Ww_Soll, UHI_Vorlauf_Wp, UHI_Rucklauf_Wp, UHI_HK1_Ist, UHI_HK1_Soll, UHI_HK2_Ist, UHI_HK2_Soll, UHI_Anlagendruck, UHI_Duruchfluss, UHI_Hochdruck, UHI_Niederdruck, UHI_Statuswert, UHI_Sperrewert, UHI_Stoerwert, UHI_Fehlerwert = self.get_Betriebsdaten_from_UHI(test_case, step)
        WPM_E_Aussen_T = self.get_Aussen_temp_from_WPM()
        WPM_Ww_Ist = WarmwasserSetting().get_Ww_Ist_temp_from_WPM()
        WPM_Ww_Soll = WarmwasserSetting().get_Ww_Soll_Anz_temp_from_WPM()
        WPM_Vorlauf_Wp = self.get_Vorlauf_WP_temp_from_WPM()
        WPM_Rucklauf_Wp = self.get_Ruecklauf_WP_temp_from_WPM()
        WPM_HK1_Ist = self.get_HK1_Ist_temp_from_WPM()
        WPM_HK1_Soll = self.get_HK1_Soll_temp_from_WPM()
        WPM_HK2_Ist = self.get_HK2_Ist_temp_from_WPM()
        WPM_HK2_Soll = self.get_HK2_Soll_temp_from_WPM()
        WPM_Anlagendruck, WPM_Duruchfluss, WPM_Hochdruck, WPM_Niederdruck = self.get_other_Betriebsdaten_info_from_WPM()
        WPM_Statuswert, WPM_Sperrewert, WPM_Stoerwert, WPM_Fehlerwert = self.get_Werts_from_WPM()
        
        self.compare_results(test_case, WPM_E_Aussen_T, UHI_E_Aussen_T, "Aussentemperatur is wrong", step)
        
        if _config.P_WW():
            self.compare_results(test_case, self.check_if_sensor_broken(WPM_Ww_Ist), UHI_Ww_Ist, "Warmwasser 'Ist' value is wrong", step)
            self.compare_results(test_case, WPM_Ww_Soll, int(UHI_Ww_Soll*10), "Warmwasser 'Soll' value is wrong", step)
        
        self.compare_results(test_case, WPM_Vorlauf_Wp, UHI_Vorlauf_Wp, "Warmepumpe 'Vorlauf' value is wrong", step)
        self.compare_results(test_case, WPM_Rucklauf_Wp, UHI_Rucklauf_Wp, "Warmepumpe 'Ruecklauf' value is wrong", step)
        
        if _config.P_HK_1() > 0:
            self.compare_results(test_case, self.check_if_sensor_broken(WPM_HK1_Ist), UHI_HK1_Ist, "1.Heizkreis 'Ist' value is wrong", step)
            self.compare_results(test_case, WPM_HK1_Soll, UHI_HK1_Soll, "1.Heizkreis 'Soll' value is wrong", step)
        
        if _config.P_HK_2() > 0:
            self.compare_results(test_case, self.check_if_sensor_broken(WPM_HK2_Ist), UHI_HK2_Ist, "2.Heizkreis 'Ist' value is wrong", step)
            self.compare_results(test_case, WPM_HK2_Soll, UHI_HK2_Soll, "2.Heizkreis 'Soll' value is wrong", step)
        
        self.compare_results(test_case, WPM_Anlagendruck, UHI_Anlagendruck, "'Anlagendruck' value is wrong", step)
        self.compare_results(test_case, WPM_Duruchfluss, UHI_Duruchfluss, "'Duruchfluss' value is wrong", step)
        self.compare_results(test_case, WPM_Hochdruck, UHI_Hochdruck, "'Hochdrucksensor' value is wrong", step)
        self.compare_results(test_case, WPM_Niederdruck, UHI_Niederdruck, "'Niederdrucksensor' value is wrong", step)

        self.compare_results(test_case, WPM_Statuswert, UHI_Statuswert, "'Statuswert' value is wrong", step)
        self.compare_results(test_case, WPM_Sperrewert, UHI_Sperrewert, "'Sperrewert' value is wrong", step)
        self.compare_results(test_case, WPM_Stoerwert, UHI_Stoerwert, "'Stoerwert' value is wrong", step)
        self.compare_results(test_case, WPM_Fehlerwert, UHI_Fehlerwert, "'Fehlerwert' value is wrong", step)
        
        self.click_left_arrow(test_case, 0, step)

    def verify_specific_HK_Betriebsdaten(self, test_case, step):
        """
        Verifies *Heizkreis* 'Betriebsdaten' from the UHI with WPM by comparing them

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        from Profile_functions.raumregler_setting import RaumreglerSettings
        rtc = RaumreglerSettings()
        time.sleep(5)
        self.close_error_popup(test_case, step)
        
        def Aussentemp_steps(HK, WPM_HK_Ist, step):
            """
            Supporting method for verifying *AussenTemperatur* 'Betriebsdaten'

            Parameters
            ----------
            HK
                Heizkreis number for specific scenrio
            WPM_HK_Ist
                Ist temperature values from Modbus registers
            step
                step number from the test case document
            """
            time.sleep(2)
            HK_name = profile_info["AussenTemp_info"]["HK"+str(HK)]["HK_name"]
            self.click_right_arrow(test_case, HK, step)
            time.sleep(2)

            if not self.click_dropdown_by_title2(test_case, HK_name, step):
                self.findElementsCSS(test_case, self.buttons["drop_down_title2"], step)[HK-1].click()

            time.sleep(2)
            UHI_HK_Ist = self.Betriebdaten_without_units(self.get_text_from_row(test_case, self._content["CURRENT"], step), step)
            self.compare_results(test_case, self.check_if_sensor_broken(WPM_HK_Ist), UHI_HK_Ist, HK_name + " Individual Betriebsdaten --> HK 'Ist' temp is wrong", step)

            WPM_HK_value = WarmwasserSetting().get_Heizkurve_value_from_WPM(HK)
            UHI_HK_value = self.Betriebdaten_without_units(self.get_text_from_row(test_case, self._content["HEATING_CURVE"], step), step)
            self.compare_results(test_case, WPM_HK_value, int(UHI_HK_value), HK_name + " Individual Betriebsdaten --> HK 'Heizkurve' value is wrong", step)
            self.click_left_arrow(test_case, 0, step)

        def Festwert_steps(HK, WPM_HK_Ist, step):
            """
            Supporting method for verifying *Festwert* 'Betriebsdaten'

            Parameters
            ----------
            HK
                Heizkreis number for specific scenrio
            WPM_HK_Ist
                Ist temperature values from Modbus registers
            step
                step number from the test case document
            """
            time.sleep(2)
            HK_name = profile_info["AussenTemp_info"]["HK"+str(HK)]["HK_name"]
            self.click_right_arrow(test_case, HK, step)
            time.sleep(2)

            if not self.click_dropdown_by_title2(test_case, HK_name, step):
                self.findElementsCSS(test_case, self.buttons["drop_down_title2"], step)[HK-1].click()

            time.sleep(2)
            UHI_HK_Ist = self.Betriebdaten_without_units(self.get_text_from_row(test_case, self._content["CURRENT"], step), step)
            self.compare_results(test_case, WPM_HK_Ist, UHI_HK_Ist, HK_name + " Individual Betriebsdaten --> HK 'Ist' temp is wrong", step)

            WPM_HK_Ziel = WarmwasserSetting().get_Festwert_Ref_temp_from_WPM(HK)
            UHI_HK_Ziel = self.Betriebdaten_without_units(self.get_text_from_row(test_case, self._content["TARGET"], step), step)
            self.compare_results(test_case, WPM_HK_Ziel, UHI_HK_Ziel, HK_name + " Individual Betriebsdaten --> HK 'Heizkurve' value is wrong", step)
            self.click_left_arrow(test_case, 0, step)

        def Raumregler_steps(room_no, step):
            """
            Supporting method for verifying *Raumregeler* 'Betriebsdaten'

            Parameters
            ----------
            room_no
                Room number in the format of 50, 51, 52, ... 69
            step
                step number from the test case document
            """
            time.sleep(2)
            self.click_dropdown_by_title1(test_case, str(room_no), step)
            time.sleep(2)
            WPM_Room_Ist = rtc.get_Ist_temp_from_WPM(room_no)
            UHI_Room_Ist = self.Betriebdaten_without_units(self.get_text_from_row(test_case, self._content["CURRENT"], step), step)
            WPM_Room_Ziel = rtc.get_Soll_temp_from_WPM(room_no)
            UHI_Room_Ziel = self.Betriebdaten_without_units(self.get_text_from_row(test_case, self._content["TARGET"], step), step)
            WPM_Humidity = rtc.get_humidity_from_WPM(room_no)
            UHI_Humidity = self.Betriebdaten_without_units(self.get_text_from_row(test_case, self._content["HUMIDITY"], step), step)
            WPM_Ventilstellung = rtc.get_Ventilstellung_from_WPM(room_no)
            UHI_Ventilstellung = self.get_text_from_row(test_case, self._content["VALVE_POSITION"], step)
            WPM_Communiaction = rtc.get_Communication_status_from_WPM(room_no)
            UHI_Communication = self.get_text_from_row(test_case, self._content["COMMUNICATION"], step)
            WPM_Fenster_auf = rtc.get_Fenster_status_from_WPM(room_no)
            UHI_Fenster_auf = self.get_text_from_row(test_case, self._content["WINDOW_IS_OPEN"], step)
            self.compare_results(test_case, WPM_Room_Ist, UHI_Room_Ist, "Individual Betriebsdaten --> Raumregler (" + str(room_no) + ") 'Ist' value is wrong", step)
            self.compare_results(test_case, WPM_Room_Ziel, UHI_Room_Ziel, "Individual Betriebsdaten --> Raumregler (" + str(room_no) + ") 'Ziel' value is wrong", step)
            self.compare_results(test_case, WPM_Humidity, UHI_Humidity, "Individual Betriebsdaten --> Raumregler (" + str(room_no) + ") 'Humidity' value is wrong", step)
            self.compare_results(test_case, WPM_Ventilstellung, UHI_Ventilstellung, "Individual Betriebsdaten --> Raumregler (" + str(room_no) + ") 'Ventilstellung' value is wrong", step)
            self.compare_results(test_case, WPM_Communiaction, UHI_Communication, "Individual Betriebsdaten --> Raumregler (" + str(room_no) + ") 'Communication' value is wrong", step)
            self.compare_results(test_case, WPM_Fenster_auf, UHI_Fenster_auf, "Individual Betriebsdaten --> Raumregler (" + str(room_no) + ") 'Fenster auf' value is wrong", step)
            self.click_dropdown_by_title1(test_case, str(room_no), step)

        if _config.P_HK_1() > 0 and _config.P_HK1_REG() == 0:
            HK = 1
            WPM_HK1_Ist = self.get_HK1_Ist_temp_from_WPM()
            Aussentemp_steps(HK, WPM_HK1_Ist, step)
        if _config.P_HK_2() > 0 and _config.P_HK2_REG() == 0:
            HK = 2
            WPM_HK2_Ist = self.get_HK2_Ist_temp_from_WPM()
            Aussentemp_steps(HK, WPM_HK2_Ist, step)

        if _config.P_HK_1() > 0 and _config.P_HK1_REG() == 1:
            HK = 1
            WPM_HK1_Ist = self.get_HK1_Ist_temp_from_WPM()
            Festwert_steps(HK, WPM_HK1_Ist, step)
        if _config.P_HK_2() > 0 and _config.P_HK2_REG() == 1:
            HK = 2
            WPM_HK2_Ist = self.get_HK2_Ist_temp_from_WPM()
            Festwert_steps(HK, WPM_HK2_Ist, step)

        if _config.P_HK_1() > 0 and _config.P_HK1_REG() == 2:
            room_no = 50
            self.click_right_arrow(test_case, 1, step)
            for HK in range(_config.P_HK1_RT_thT_Anz()):
                Raumregler_steps(room_no, step)
                room_no += 1
            self.click_left_arrow(test_case, 0, step)

        if _config.P_HK_2() > 0 and _config.P_HK2_REG() == 2:
            room_no = 60
            self.click_right_arrow(test_case, 2, step)
            for HK in range(_config.P_HK2_RT_thT_Anz()):
                Raumregler_steps(room_no, step)
                room_no += 1
            self.click_left_arrow(test_case, 0, step)