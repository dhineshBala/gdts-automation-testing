from src.General_functions import GeneralFunctions

OK = "OK"

class TestNetzwerk(GeneralFunctions):
        
    def get_IPv4_UHI(self, test_case, step):
        """ returns `IPv4` from UHI """
        return self.findElementCSS(test_case, self.texT["ipv4_text"], step).text
    
    def get_IPv6_UHI(self, test_case, step):
        """ returns `IPv6` from UHI """
        return self.findElementCSS(test_case, self.texT["ipv6_text"], step).text

    def verify_IPv4(self, test_case, host_ip, step):
        """ verify `IPv4` from UHI against socket """
        UHI_IPv4 = self.get_IPv4_UHI(test_case, step)
        self.compare_results(
            test_case, host_ip, UHI_IPv4, "wrong IPV4", step)

    def verify_IPv6(self, test_case, step):
        """ verify `IPv6` from UHI with no of chars """
        UHI_IPv6 = self.get_IPv6_UHI(test_case, step)
        self.compare_greater_than_results(test_case, int(
            len(UHI_IPv6)), 15, "IPV6 is not presented", step)
    
    def get_status_text_from_row(self, test_case, row_options, step):
        """ Returns status text from given `row_options` """
        for opt in self.findElementsCSS(test_case, "div.row", step):
            if opt.find_element_by_css_selector(self.texT["area_text"]).text == row_options:
                return opt.find_element_by_css_selector(self.texT["netz_status"]).text
    
    def verify_Netzwerk_status(self, test_case, step):
        """
        returns `Netzwerk-Analyse` status in Netwerk screen
        """
        if not self.get_status_text_from_row(test_case, "HTTP", step) == OK:
            self.compare_true_result(test_case, False, "HTTP Status is not OK", step)
        if not self.get_status_text_from_row(test_case, "HTTPS", step) == OK:
            self.compare_true_result(test_case, False, "HTTPS Status is not OK", step)
        if not self.get_status_text_from_row(test_case, "MQTT", step) == OK:
            self.compare_true_result(test_case, False, "MQTT Status is not OK", step)
        if not self.get_status_text_from_row(test_case, "AMQPS", step) == OK:
            self.compare_true_result(test_case, False, "AMQPS Status is not OK", step)
        
            

        