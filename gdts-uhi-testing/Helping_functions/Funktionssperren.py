from src.General_functions import GeneralFunctions
import json, time
from src.logger import fail_log, pass_log

GESPERRT = "gesperrt"
genl = GeneralFunctions()

class FunktionsSperren(GeneralFunctions):
    """
    This is a helping class for the test case 'UHI-20 -- test_Funktionssperren' with get, set and verify functions.

    * :py:meth:`Helping_functions.Funktionssperren.FunktionsSperren.get_Funktionssperren_status_json`
    * :py:meth:`Helping_functions.Funktionssperren.FunktionsSperren.get_Ww_Fkt_Sperre_status_from_WPM`
    * :py:meth:`Helping_functions.Funktionssperren.FunktionsSperren.get_Sw_Fkt_Sperre_status_from_WPM`
    * :py:meth:`Helping_functions.Funktionssperren.FunktionsSperren.get_Biv_Fkt_Sperre_status_from_WPM`
    * :py:meth:`Helping_functions.Funktionssperren.FunktionsSperren.get_Reg_Fkt_Sperre_status_from_WPM`
    * :py:meth:`Helping_functions.Funktionssperren.FunktionsSperren.get_KA_Fkt_Sperre_status_from_WPM`
    * :py:meth:`Helping_functions.Funktionssperren.FunktionsSperren.get_KP_Fkt_Sperre_status_from_WPM`
    * :py:meth:`Helping_functions.Funktionssperren.FunktionsSperren.activate_Funktionssperren_by_option`
    * :py:meth:`Helping_functions.Funktionssperren.FunktionsSperren.deactivate_Funktionssperren_by_option`
    * :py:meth:`Helping_functions.Funktionssperren.FunktionsSperren.activate_Funktionssperren`
    * :py:meth:`Helping_functions.Funktionssperren.FunktionsSperren.deactivate_Funktionssperren`
    * :py:meth:`Helping_functions.Funktionssperren.FunktionsSperren.verify_Funktionssperren_after_activation`
    * :py:meth:`Helping_functions.Funktionssperren.FunktionsSperren.verify_Funktionssperren_after_deactivation`
    * :py:meth:`Helping_functions.Funktionssperren.FunktionsSperren.verify_Funktionssperren_with_WPM_after_activation`
    * :py:meth:`Helping_functions.Funktionssperren.FunktionsSperren.verify_Funktionssperren_with_WPM_after_deactivation`

    """
    FunkSperre_info = genl.user_input["Funktionssperren"]

    opt_Ww = genl._content["WARMWATER"]
    opt_Sw = genl._content["POOL"]
    opt_Bi = genl._content["Biv_Fkt_Sperre"]
    opt_Re = genl._content["Reg_Fkt_Sperre"]
    opt_AK = genl._content["KA_Fkt_Sperre"]
    opt_PK = genl._content["KP_Fkt_Sperre"]

    def get_Funktionssperren_status_json(self):
        """
        Returns 'Funktionssperren' options with status from 'json' file 

        Returns
        -------
        string 'tuple of tuple'
            retruns Ww, Sw, Bi, Re, AK, PK info from 'user_input.json' file
        """
        Ww = self.opt_Ww, self.FunkSperre_info["Warmwasser"]
        Sw = self.opt_Sw, self.FunkSperre_info["Schwimmbad"]
        Bi = self.opt_Bi, self.FunkSperre_info["Bivalent"]
        Re = self.opt_Re, self.FunkSperre_info["Regenerativ"]
        AK = self.opt_AK, self.FunkSperre_info["Aktiv_Kuehlen"]
        PK = self.opt_PK, self.FunkSperre_info["Passiv_Kuehlen"]
        return Ww, Sw, Bi, Re, AK, PK

    def get_Ww_Fkt_Sperre_status_from_WPM(self):
        """
        returns 'True', if 'Ww' Funktionssperre is active

        Returns
        -------
        'Boolean'
            True / False
        """
        reg_index = self.regs_digital["Ww_Fkt_Sperre"]
        return self.tcp.get_status_from_coils(reg_index)

    def get_Sw_Fkt_Sperre_status_from_WPM(self):
        """
        returns 'True', if 'Sw' Funktionssperre is active

        Returns
        -------
        'Boolean'
            True / False
        """
        reg_index = self.regs_digital["Sw_Fkt_Sperre"]
        return self.tcp.get_status_from_coils(reg_index)
    
    def get_Biv_Fkt_Sperre_status_from_WPM(self):
        """
        returns 'True', if 'Bivalent' Funktionssperre is active
        
        Returns
        -------
        'Boolean'
            True / False
        """
        reg_index = self.regs_digital["Biv_Fkt_Sperre"]
        return self.tcp.get_status_from_coils(reg_index)
    
    def get_Reg_Fkt_Sperre_status_from_WPM(self):
        """
        returns 'True', if 'Regenerativ' Funktionssperre is active

        Returns
        -------
        'Boolean'
            True / False
        """
        reg_index = self.regs_digital["Reg_Fkt_Sperre"]
        return self.tcp.get_status_from_coils(reg_index)
    
    def get_KA_Fkt_Sperre_status_from_WPM(self):
        """
        returns 'True', if 'Aktiv Kuehlen' Funktionssperre is active

        Returns
        -------
        'Boolean'
            True / False
        """
        reg_index = self.regs_digital["KA_Fkt_Sperre"]
        return self.tcp.get_status_from_coils(reg_index)
    
    def get_KP_Fkt_Sperre_status_from_WPM(self):
        """
        returns 'True', if 'Passiv Kuehlen' Funktionssperre is active

        Returns
        -------
        'Boolean'
            True / False
        """
        reg_index = self.regs_digital["KP_Fkt_Sperre"]
        return self.tcp.get_status_from_coils(reg_index)

    def activate_Funktionssperren_by_option(self, test_case, option, step):
        """
        Activate 'Funktionssperren' based on passed 'option' like (Warmwasser, Schwimmbad,..)

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        option
            Targeted text *option* from 'de.common.json' file 
        step
            step number from the test case document
        """
        time.sleep(1)
        for opt in self.findElementsCSS(test_case, self.texT["toggle_row_2"], step):
            if opt.find_element_by_css_selector(self.texT["area"]).text == option:
                if opt.find_element_by_css_selector(self.buttons["toggle_text_btn"]).text != self._content["FUNCTION_DISABLED"]:
                    opt.find_element_by_css_selector(self.buttons["toggle_text_btn"]).click()

    def deactivate_Funktionssperren_by_option(self, test_case, option, step):
        """
        Deactivate 'Funktionssperren' based on passed 'option' like (Warmwasser, Schwimmbad,..)

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        option
            Targeted text *option* from 'de.common.json' file 
        step
            step number from the test case document
        """
        time.sleep(1)
        for opt in self.findElementsCSS(test_case, self.texT["toggle_row_2"], step):
            if opt.find_element_by_css_selector(self.texT["area"]).text == option:
                if opt.find_element_by_css_selector(self.buttons["toggle_text_btn"]).text != self._content["FUNCTION_ENABLED"]:
                    opt.find_element_by_css_selector(self.buttons["toggle_text_btn"]).click()
    
    def activate_Funktionssperren(self, test_case, step):
        """
        Activate 'Funktionssperren' based on passed 'option' like (Warmwasser, Schwimmbad,..) from 'tuple'

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        Ww, Sw, Bi, Re, AK, PK = self.get_Funktionssperren_status_json()
        self.close_error_popup(test_case, step)
        if self._config.Fun_Code_Ww():
            if Ww[1] == GESPERRT:
                self.activate_Funktionssperren_by_option(test_case, Ww[0], step)
        # else:
        #     pass_log.info("[Invalid Config] 'Warmwasser' is not available in WPM config at step = " + str(step))

        self.close_error_popup(test_case, step)
        if self._config.Fun_Code_Sw(): 
            if Sw[1] == GESPERRT:
                self.activate_Funktionssperren_by_option(test_case, Sw[0], step)
        # else:
        #     pass_log.info("[Invalid Config] 'Schwimmbad' is not available in WPM config at step = " + str(step))
        
        self.close_error_popup(test_case, step)
        if self._config.Fun_Code_Biv(): 
            if Bi[1] == GESPERRT:
                self.activate_Funktionssperren_by_option(test_case, Bi[0], step)
        # else:
        #     pass_log.info("[Invalid Config] 'Bivalent' is not available in WPM config at step = " + str(step))

        self.close_error_popup(test_case, step)
        if self._config.Fun_Code_Reg(): 
            if Re[1] == GESPERRT:
                self.activate_Funktionssperren_by_option(test_case, Re[0], step)
        # else:
        #     pass_log.info("[Invalid Config] 'Regenerativ' is not available in WPM config at step = " + str(step))
        
        self.close_error_popup(test_case, step)
        if self._config.Fun_Code_KuehlAktiv():
            if AK[1] == GESPERRT:
                self.activate_Funktionssperren_by_option(test_case, AK[0], step)
        # else:
        #     pass_log.info("[Invalid Config] 'Aktiv Kühlen' is not available in WPM config at step = " + str(step))
        
        self.close_error_popup(test_case, step)
        if self._config.Fun_Code_KuehlPass():
            if PK[1] == GESPERRT:
                self.activate_Funktionssperren_by_option(test_case, PK[0], step)
        # else:
        #     pass_log.info("[Invalid Config] 'Passiv Kühlen' is not available in WPM config at step = " + str(step))
        

    def deactivate_Funktionssperren(self, test_case, step):
        """
        Deactivate 'Funktionssperren' based on passed 'option' like (Warmwasser, Schwimmbad,..) from 'tuple'

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        Ww, Sw, Bi, Re, AK, PK = self.get_Funktionssperren_status_json()
        if self._config.Fun_Code_Ww():
            if Ww[1] == GESPERRT:
                self.deactivate_Funktionssperren_by_option(test_case, Ww[0], step)
        if self._config.Fun_Code_Sw(): 
            if Sw[1] == GESPERRT:
                self.deactivate_Funktionssperren_by_option(test_case, Sw[0], step)
        if self._config.Fun_Code_Biv(): 
            if Bi[1] == GESPERRT:
                self.deactivate_Funktionssperren_by_option(test_case, Bi[0], step)
        if self._config.Fun_Code_Reg(): 
            if Re[1] == GESPERRT:
                self.deactivate_Funktionssperren_by_option(test_case, Re[0], step)
        if self._config.Fun_Code_KuehlAktiv():
            if AK[1] == GESPERRT:
                self.deactivate_Funktionssperren_by_option(test_case, AK[0], step)
        if self._config.Fun_Code_KuehlPass():
            if PK[1] == GESPERRT:
                self.deactivate_Funktionssperren_by_option(test_case, PK[0], step)

    def verify_Funktionssperren_after_activation(self, test_case, option, step):
        """
        Verify 'Funktionssperren' in WPM after activated in UHI. This function verify, if some option from UHI is 'LOCKED' then the same from WPM

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        option
            Targeted text *option* from 'de.common.json' file 
        step
            step number from the test case document
        """
        if option == self.opt_Ww:
            self.compare_true_result(test_case, self.get_Ww_Fkt_Sperre_status_from_WPM(), "'Warmwasser' Funktionssperre is not activated in WPM, i.e., Ww is still active in WPM", step)
        
        if option == self.opt_Sw:
            self.compare_true_result(test_case, self.get_Sw_Fkt_Sperre_status_from_WPM(), "'Schwimmbad' Funktionssperre is not activated in WPM, i.e., Sw is still active in WPM", step)
        
        if option == self.opt_Bi:
            self.compare_true_result(test_case, self.get_Biv_Fkt_Sperre_status_from_WPM(), "'Bivalent' Funktionssperre is not activated in WPM, i.e., Bivalent is still active in WPM", step)

        if option == self.opt_Re:
            self.compare_true_result(test_case, self.get_Reg_Fkt_Sperre_status_from_WPM(), "'Regenerativ' Funktionssperre is not activated in WPM, i.e., Regenerativ is still active in WPM", step)
        
        if option == self.opt_AK:
            self.compare_true_result(test_case, self.get_KA_Fkt_Sperre_status_from_WPM(), "'Aktiv Kühlen' Funktionssperre is not activated in WPM, i.e., Aktiv Kuhlen is still active in WPM", step)

        if option == self.opt_PK:
            self.compare_true_result(test_case, self.get_KP_Fkt_Sperre_status_from_WPM(), "'Passiv Kühlen' Funktionssperre is not activated in WPM, i.e., Passiv Kuhlen is still active in WPM", step)
        
    def verify_Funktionssperren_after_deactivation(self, test_case, option, step):
        """
        Verify 'Funktionssperren' in WPM after deactivated in UHI. This function verify, if some option from UHI is 'FREED' then the same from WPM

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        option
            Targeted text *option* from 'de.common.json' file 
        step
            step number from the test case document
        """
        if option == self.opt_Ww:
            self.compare_false_result(test_case, self.get_Ww_Fkt_Sperre_status_from_WPM(), "'Warmwasser' Funktionssperre is still activated in WPM, i.e., Ww is still deactivated in WPM", step)

        if option == self.opt_Sw:
            self.compare_false_result(test_case, self.get_Sw_Fkt_Sperre_status_from_WPM(), "'Schwimmbad' Funktionssperre is still activated in WPM, i.e., Sw is still deactivated in WPM", step)
        
        if option == self.opt_Bi:
            self.compare_false_result(test_case, self.get_Biv_Fkt_Sperre_status_from_WPM(), "'Bivalent' Funktionssperre is still activated in WPM, i.e., Bi is still deactivated in WPM", step)

        if option == self.opt_Re:
            self.compare_false_result(test_case, self.get_Reg_Fkt_Sperre_status_from_WPM(), "'Regenerativ' Funktionssperre is still activated in WPM, i.e., Reg is still deactivated in WPM", step)
        
        if option == self.opt_AK:
            self.compare_false_result(test_case, self.get_KA_Fkt_Sperre_status_from_WPM(), "'Aktiv Kühlen' Funktionssperre is still activated in WPM, i.e., Aktiv Kuhlen is still deactivated in WPM", step)
        
        if option == self.opt_PK:
            self.compare_false_result(test_case, self.get_KP_Fkt_Sperre_status_from_WPM(), "'Passiv Kühlen' Funktionssperre is still activated in WPM, i.e., Passiv Kuhlen is still deactivated in WPM", step)
        

    def verify_Funktionssperren_with_WPM_after_activation(self, test_case, step):
        """
        Get the 'options' from the function 'get_Funktionssperren_status_json' and pass this to 'verify_Funktionssperren_after_activation' to verify with WPM

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        Ww, Sw, Bi, Re, AK, PK = self.get_Funktionssperren_status_json()
        if self._config.Fun_Code_Ww():
            if Ww[1] == GESPERRT:
                self.verify_Funktionssperren_after_activation(test_case, Ww[0], step)
        if self._config.Fun_Code_Sw(): 
            if Sw[1] == GESPERRT:
                self.verify_Funktionssperren_after_activation(test_case, Sw[0], step)
        if self._config.Fun_Code_Biv(): 
            if Bi[1] == GESPERRT:
                self.verify_Funktionssperren_after_activation(test_case, Bi[0], step)
        if self._config.Fun_Code_Reg():
            if Re[1] == GESPERRT:
                self.verify_Funktionssperren_after_activation(test_case, Re[0], step)
        if self._config.Fun_Code_KuehlAktiv():
            if AK[1] == GESPERRT:
                self.verify_Funktionssperren_after_activation(test_case, AK[0], step)
        if self._config.Fun_Code_KuehlPass():
            if PK[1] == GESPERRT:
                self.verify_Funktionssperren_after_activation(test_case, PK[0], step)

    def verify_Funktionssperren_with_WPM_after_deactivation(self, test_case, step):
        """
        Get the 'options' from the function 'get_Funktionssperren_status_json' and pass this to 'verify_Funktionssperren_after_deactivation' to verify with WPM

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        Ww, Sw, Bi, Re, AK, PK = self.get_Funktionssperren_status_json()
        if self._config.Fun_Code_Ww():
            if Ww[1] == GESPERRT:
                self.verify_Funktionssperren_after_deactivation(test_case, Ww[0], step)
        if self._config.Fun_Code_Sw(): 
            if Sw[1] == GESPERRT:
                self.verify_Funktionssperren_after_deactivation(test_case, Sw[0], step)
        if self._config.Fun_Code_Biv(): 
            if Bi[1] == GESPERRT:
                self.verify_Funktionssperren_after_deactivation(test_case, Bi[0], step)
        if self._config.Fun_Code_Reg():
            if Re[1] == GESPERRT:
                self.verify_Funktionssperren_after_deactivation(test_case, Re[0], step)
        if self._config.Fun_Code_KuehlAktiv():
            if AK[1] == GESPERRT:
                self.verify_Funktionssperren_after_deactivation(test_case, AK[0], step)
        if self._config.Fun_Code_KuehlPass():
            if PK[1] == GESPERRT:
                self.verify_Funktionssperren_after_deactivation(test_case, PK[0], step)



    