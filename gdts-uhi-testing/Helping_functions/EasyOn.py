from src.General_functions import GeneralFunctions
import time, json, os

class EasyOn(GeneralFunctions):
    """
    This is a helping class for the test case 'UHI-35 -- EasyOn' with get, set and verify functions.

    """
    with open(os.path.join(GeneralFunctions().json_dir, 'de.easyon.json'), encoding='utf-8') as content:
        _content_EOn = json.load(content)

    with open(os.path.join(GeneralFunctions().json_dir, 'de.easyon.info.json'), encoding='utf-8') as content:
        _content_EOn_info = json.load(content)

    with open(os.path.join(GeneralFunctions().json_dir, 'EasyOn_info.json'), encoding='utf-8') as info:
        EOn_info = json.load(info)

    def get_active_EasyOn(self, test_case, step):
        """
        Returns `Current/Active` EasyOn setup which is setup in UHI

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document

        Returns
        -------
        'string'
            EasyOn with Date and Time / No Active EasyOn
        """
        time.sleep(1)
        temp = self.findElementCSS(test_case, "table.easyon-conf-table", step)
        info = temp.find_elements_by_css_selector(self.texT["area"])
        active_config = info[-3].text
        active_since = info[-2].text
        activated_by = info[-1].text
        return active_config, active_since, activated_by

    def verify_active_config_timestamp(self, test_case, expected_name, step):
        """
        """

    def select_advanced_option_by_name(self, test_case, name, step):
        """
        Select `Advanced options` icon in the options available in EasyOn screens

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        name
            Name of the target option from 'de.easyon.json'
        step
            step number from the test case document
        """
        time.sleep(1)
        ele = 0
        for opt in self.findElementsCSS(test_case, "div.button-row.ripple", step):
            if opt.find_element_by_css_selector("div.element-text-area.text").text == name:
                self.findElementsCSS(test_case, "div.advanced-option-button.ripple", step)[ele].click()
                break
            ele += 1

    def create_new_EasyOn(self, test_case, step):
        """
        """
        self.click_right_arrow_by_name(test_case, self._content_EOn["MANAGE_CONFIGURATIONS"], step)
        self.get_click_from_text_area(test_case, self._content_EOn["ARCHIVE_AND_CREATE_CONFIGURATION"], step)
        self.waitByCss(test_case, self.texT["area_desc"], 20, step)

        ############## Betriebsweise ##############
        Betriebsweise_info = self.EOn_info["Betriebsweise"]
        
        ### Monovalent ###
        Mo_info = Betriebsweise_info["Monovalent"]
        self.select_advanced_option_by_name(test_case, "monoenergetisch" , step)

        # Grenztemp Parallel
        self.click_right_arrow_by_name(test_case, self._content_EOn["P_ESG_ZWE"], step)
        self.set_temp_via_plus_minus_buttons(test_case, Mo_info["Grenztemperatur-Parallel"], -1, step)
        self.click_back_by_header_title(test_case, self._content_EOn["P_ESG_ZWE"], step)

        time.sleep(5)

        ### Bivalent ###
        Bi_info = Betriebsweise_info["Bivalent"]
        self.select_advanced_option_by_name(test_case, self._content_EOn["P_BETR_WEISE.2"] , step)
        
        # Grenztemp Parallel
        self.click_right_arrow_by_name(test_case, self._content_EOn["P_ESG_ZWE"], step)
        self.set_temp_via_plus_minus_buttons(test_case, Bi_info["Grenztemperatur-Parallel"], -1, step)
        self.click_back_by_header_title(test_case, self._content_EOn["P_ESG_ZWE"], step)

        # Grenztemp Alternativ
        self.click_right_arrow_by_name(test_case, self._content_EOn["P_ESG_WP"], step)
        self.set_temp_via_plus_minus_buttons(test_case, Bi_info["Grenztemperatur-Alternativ"], -1, step)
        self.click_back_by_header_title(test_case, self._content_EOn["P_ESG_WP"], step)

        # Betriebsweise
        self.click_right_arrow_by_name(test_case, self._content_EOn["P_BIV_BW"], step)
        self.click_text_area_by_name(test_case, self._content_EOn[Bi_info["Betriebsweise"]], step)
        self.click_back_by_header_title(test_case, self._content_EOn["P_BIV_BW"], step)

        # Mischerlaufzeit
        self.click_right_arrow_by_name(test_case, self._content_EOn["P_HK1_MILZ"], step)
        self.set_temp_via_plus_minus_buttons(test_case, Bi_info["Mischerlaufzeit"], -1, step)
        self.click_back_by_header_title(test_case, self._content_EOn["P_HK1_MILZ"], step)

        # Mischerhysterese
        self.click_right_arrow_by_name(test_case, self._content_EOn["P_Biv_MIHY"], step)
        self.set_temp_via_plus_minus_buttons(test_case, Bi_info["Mischerhysterese"], -1, step)
        self.click_back_by_header_title(test_case, self._content_EOn["P_Biv_MIHY"], step)

        # EVU Sperre
        self.click_right_arrow_by_name(test_case, self._content_EOn["P_EVS"], step)
        self.click_text_area_by_name(test_case, self._content_EOn[Bi_info["EVU-Sperre"]], step)
        self.click_back_by_header_title(test_case, self._content_EOn["P_EVS"], step)

        # EVU Sperre Grenztemperatur
        self.click_right_arrow_by_name(test_case, self._content_EOn["P_EVSGT"], step)
        self.set_temp_via_plus_minus_buttons(test_case, Bi_info["EVU-Sperre-Grenztemperatur"], -1, step)
        self.click_back_by_header_title(test_case, self._content_EOn["P_EVSGT"], step)

        # Sonderprogramm
        self.click_right_arrow_by_name(test_case, self._content_EOn["P_BIV_STD_SOPO"], step)
        self.set_temp_via_plus_minus_buttons(test_case, Bi_info["Sonderprogramm"], -1, step)
        self.click_back_by_header_title(test_case, self._content_EOn["P_BIV_STD_SOPO"], step)

        # M16 2.Warmeerzeuger
        self.click_right_arrow_by_name(test_case, self._content_EOn["P_ZUP_BIV"], step)
        self.click_text_area_by_name(test_case, self._content_EOn[Bi_info["M16-2.Waermeerzeuger"]], step)
        self.click_back_by_header_title(test_case, self._content_EOn["P_ZUP_BIV"], step)


