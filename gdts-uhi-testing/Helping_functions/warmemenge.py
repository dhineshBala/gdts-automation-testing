from src.General_functions import GeneralFunctions

class Warmemenge(GeneralFunctions):
    """
    This is a helping class for the test case 'UHI-19 -- test_Waermemenge' with get and verify functions.

    * :py:meth:`Helping_functions.warmemenge.Warmemenge.get_Warmwasser_value_from_WPM`
    * :py:meth:`Helping_functions.warmemenge.Warmemenge.get_Heizen_value_from_WPM`
    
    """
    def get_Warmwasser_value_from_WPM(self):
        """
        returns 'Warmwasser' Warmenmenge value from WPM registers

        Returns
        -------
        'string'
            Ww Warmenmenge
        """
        WPM_Ww_1 = self.tcp.get_values_from_integer_registers(self.regs_integer["WMZ_WW_ST1bis4"])
        WPM_Ww_2 = self.tcp.get_values_from_integer_registers(self.regs_integer["WMZ_WW_ST5bis8"])
        WPM_Ww_3 = self.tcp.get_values_from_integer_registers(self.regs_integer["WMZ_WW_ST9bis12"])
        return str(WPM_Ww_1 + WPM_Ww_2 * 10**4 + WPM_Ww_3 * 10**8)

    def get_Heizen_value_from_WPM(self):
        """
        returns 'Heizen' Warmenmenge value from WPM registers

        Returns
        -------
        'string'
            Heizen Warmenmenge
        """
        WPM_Hz_1 = self.tcp.get_values_from_integer_registers(self.regs_integer["WMZ_H_ST1bis4"])
        WPM_Hz_2 = self.tcp.get_values_from_integer_registers(self.regs_integer["WMZ_H_ST5bis8"])
        WPM_Hz_3 = self.tcp.get_values_from_integer_registers(self.regs_integer["WMZ_H_ST9bis12"])
        return str(WPM_Hz_1 + WPM_Hz_2 * 10**4 + WPM_Hz_3 * 10**8)

