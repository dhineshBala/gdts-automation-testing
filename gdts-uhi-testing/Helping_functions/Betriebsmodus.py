from src.General_functions import GeneralFunctions
from datetime import date, datetime, timedelta
import time
from src.logger import pass_log
from src.logger import fail_log

FAIL = fail_log.error
PASS = pass_log.info
genl = GeneralFunctions()

user_info = genl.user_input["Home"]["Betriebsmodus"]
AUTOMATIK = "Automatik"
MANUELL = "Manuell"
WINTER = "Winter"
SOMMER = "Sommer"
HEIZSTAB = "Heizstab"
KUEHLEN = "Kuehlen"
URLAUB = "Urlaub"
PARTY = "Party"
ALL = "All"
TEMP = "Temperatur"
TIME = "time"
action = genl.action
wait = time.sleep(2)


class BetriebsModus(GeneralFunctions):
    """
    This is a helping class for the test case 'UHI-21 -- test_Betriebsmodus' with set, get and verify functions.

    * :py:meth:`Helping_functions.Betriebsmodus.BetriebsModus.get_Betriebsmodus_from_json`
    * :py:meth:`Helping_functions.Betriebsmodus.BetriebsModus.click_area_title_by_name`
    * :py:meth:`Helping_functions.Betriebsmodus.BetriebsModus.click_right_arrow_by_option`
    * :py:meth:`Helping_functions.Betriebsmodus.BetriebsModus.get_Grenz_values_from_UHI`
    * :py:meth:`Helping_functions.Betriebsmodus.BetriebsModus.set_Grenz_temperature`
    * :py:meth:`Helping_functions.Betriebsmodus.BetriebsModus.set_Grenz_time`
    * :py:meth:`Helping_functions.Betriebsmodus.BetriebsModus.get_Automatik_status_from_WPM`
    * :py:meth:`Helping_functions.Betriebsmodus.BetriebsModus.get_Automatik_Heating_limit_from_WPM`
    * :py:meth:`Helping_functions.Betriebsmodus.BetriebsModus.get_Automatik_Cooling_limit_from_WPM`
    * :py:meth:`Helping_functions.Betriebsmodus.BetriebsModus.get_Automatik_time_limit_from_WPM`
    * :py:meth:`Helping_functions.Betriebsmodus.BetriebsModus.get_Urlaub_days_from_WPM`
    * :py:meth:`Helping_functions.Betriebsmodus.BetriebsModus.get_Party_hours_from_WPM`
    * :py:meth:`Helping_functions.Betriebsmodus.BetriebsModus.verify_Manuell_modus_with_WPM`
    * :py:meth:`Helping_functions.Betriebsmodus.BetriebsModus.verify_Urlaub_modus_with_WPM`
    * :py:meth:`Helping_functions.Betriebsmodus.BetriebsModus.verify_Party_modus_with_WPM`
    * :py:meth:`Helping_functions.Betriebsmodus.BetriebsModus.test_Betriebsmodus_in_UHI`

    """
    opt_Automatik = genl._content["AUTO"]
    opt_Urlaub = genl._content["OPERATION_MODE_2"]
    opt_Party = genl._content["OPERATION_MODE_3"]
    opt_Manuell = genl._content["MANUAL"]
    opt_Heat_limit = genl._content["HEATING_TO_LIMIT"]
    opt_Cool_limit = genl._content["COOLING_TO_LIMIT"]
    opt_Winter = genl._content["OPERATION_MODE_1.1"]
    opt_Sommer = genl._content["OPERATION_MODE_0.1"]
    opt_Heizstab = genl._content["OPERATION_MODE_4.1"]
    opt_Kuehlen = genl._content["OPERATION_MODE_5.1"]

    def get_Betriebsmodus_from_json(self):
        """
        Returns 'Betriebsmodus' with information from 'user_input.json' file

        """
        Auto = user_info[AUTOMATIK]
        if Auto["Mode_status"] == "On":
            Operating_info = AUTOMATIK, Auto

        Manuell = user_info[MANUELL]
        if Manuell["Mode_status"] == "On":
            Operating_info = MANUELL, Manuell

        return Operating_info

    def click_area_title_by_name(self, test_case, name, step):
        """
        returns a click on 'div.element-text-area.title' based on given *option* from 'de.common.json'

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        option
            Targeted text *option* from 'de.common.json' file 
        step
            step number from the test case document
        """
        for title in self.findElementsCSS(test_case, self.texT["title"], step):
            if title.text == name:
                title.click()

    def click_right_arrow_by_option(self, test_case, option, step):
        """
        returns a click on 'div.chevron-right-arrow' based on given *option* from 'de.common.json'

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        option
            Targeted text *option* from 'de.common.json' file 
        step
            step number from the test case document
        """
        for row in self.findElementsCSS(test_case, self.texT["param_row"], step):
            if row.find_element_by_css_selector(self.texT["name"]).text == option:
                row.find_element_by_css_selector("div.right").find_element_by_css_selector(
                    self.buttons["right_arrow"]).click()

    def get_Grenz_values_from_UHI(self, test_case, header, element_ID, step):
        """
        returns "values" from 'Grenzwert' screen like temp, time, ... without 'units'

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        header
            Page header from 'de.common.json' for verification
        element_ID
            Targeted element number for particular action
        step
            step number from the test case document

        Returns
        -------
        'int'
            Temperature
        """
        for screen in self.findElementsCSS(test_case, "div.widget-status--view-set-temp-and-time", step):
            if screen.find_element_by_css_selector(self.texT["title"]).text == header:
                temp = screen.find_elements_by_css_selector(
                    "div.row")[element_ID].find_element_by_css_selector(self.texT["area"]).text
                value = temp.strip(" ° C h")
        return int(''.join(value))

    def set_Grenz_temperature(self, test_case, screen_name, soll_wert, element_ID, step):
        """
        Setup and returns desired temperature via + and - buttons in 'Grenzwert' screen in 'Betriebsmodus' screen

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        screen_name
            Nmae of the screen from 'de.common.json' for results
        soll_wert
            Target *Soll* temperature from 'user_input.json' file
        element_ID
            Targeted element number for particular action
        step
            step number from the test case document

        Returns
        -------
        'int'
            Temperature
        """
        minus = self.findElementsCSS(test_case,
            self.buttons["minus"], step)[element_ID]
        plus = self.findElementsCSS(test_case,
            self.buttons["plus"], step)[element_ID]
        try:
            # set value to start number
            for i in range(3):
                action.click_and_hold(minus).move_to_element(
                    minus).release(minus).perform()
                wait
        except:
            FAIL("[Automation Fail] 'Grenztemperatur' Minus button failed for " +
                 screen_name + " in step = " + str(step))

        # Setup desired temp from Soll_wert
        try:
            while True:
                time.sleep(4)
                UHI_temp = self.get_Grenz_values_from_UHI(test_case,
                    screen_name, element_ID, step)
                if UHI_temp >= soll_wert:
                    break
                action.click_and_hold(plus).move_to_element(
                    plus).release(plus).perform()
                wait

        except:
            FAIL("[Automation Fail] Setting 'Grenztemperatur' failed for " +
                 screen_name + " in step = " + str(step))

        return UHI_temp

    def set_Grenz_time(self, test_case, screen_name, _time, element_ID, step):
        """
        Setup and returns desired time via + and - buttons in 'Grenzwert' screen in 'Betriebsmodus' screen

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        screen_name
            Nmae of the screen from 'de.common.json' for results
        _time
            Desired *Time* limit from 'user_input.json' file
        element_ID
            Targeted element number for particular action
        step
            step number from the test case document
        
        Returns
        -------
        'int'
            Grenzwert times
        """
        minus = self.findElementsCSS(test_case,
            self.buttons["minus"], step)[element_ID]
        plus = self.findElementsCSS(test_case,
            self.buttons["plus"], step)[element_ID]
        try:
            # set value to start number
            for i in range(3):
                action.click_and_hold(minus).move_to_element(
                    minus).release(minus).perform()
                wait
        except:
            FAIL("[Automation Fail] 'Grenztime' Minus button failed for " +
                 screen_name + " in step = " + str(step))

        # Setup desired temp from Soll_wert
        try:
            while True:
                UHI_time = self.get_Grenz_values_from_UHI(test_case,
                    screen_name, element_ID, step)
                if UHI_time >= _time:
                    break
                action.click_and_hold(plus).move_to_element(
                    plus).release(plus).perform()
                wait
        except:
            FAIL("[Automation Fail] Setting 'Grenztime' failed for " +
                 screen_name + " in step = " + str(step))

        return UHI_time

    def get_Automatik_status_from_WPM(self):
        """
        returns 'True', if Automatik mode is active in WPM Betriebsmodus

        Returns
        -------
        'Boolean'
            True / False
        """
        coil_index = self.regs_digital["P_TBaUs"]
        return self.tcp.get_status_from_coils(coil_index)

    def get_Automatik_Heating_limit_from_WPM(self):
        """
        returns 'Heizen bis Grenzwert' for Automatik mode from WPM Betriebsmodus

        Returns
        -------
        'int'
            Temperature
        """
        reg_index = self.regs_integer["P_TBaUs_2"]
        return self.tcp.get_values_from_integer_registers(reg_index)

    def get_Automatik_Cooling_limit_from_WPM(self):
        """
        returns 'Kuehlen bis Grenzwert' for Automatik mode from WPM Betriebsmodus

        Returns
        -------
        'int'
            Temperature
        """
        reg_index = self.regs_integer["P_TBaUs_1"]
        return self.tcp.get_values_from_integer_registers(reg_index)

    def get_Automatik_time_limit_from_WPM(self):
        """
        returns 'Time' for Automatik mode from WPM Betriebsmodus

        Returns
        -------
        'int'
            Grenzwert times
        """
        reg_index = self.regs_integer["P_TBaUs_Zeit"]
        return self.tcp.get_values_from_integer_registers(reg_index)

    def get_Urlaub_days_from_WPM(self):
        """
        returns 'Urlaub' days from WPM Betriebsmodus

        Returns
        -------
        'int'
            Urlaub days
        """
        reg_index = self.regs_integer["P_URLAUB_TAGE"]
        return self.tcp.get_values_from_integer_registers(reg_index)

    def get_Party_hours_from_WPM(self):
        """
        returns 'Party' hours from WPM Betriebsmodus

        Returns
        -------
        'int'
            Party hours
        """
        reg_index = self.regs_integer["P_PARTY_HOUR"]
        return self.tcp.get_values_from_integer_registers(reg_index)

    def verify_Manuell_modus_with_WPM(self, test_case, UHI_mode, step):
        """
        verifies 'Manuell' Betriebsmodus with WPM

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        UHI_mode
            UHI Betriebsmodus in the form of 'text' for verfication
        step
            step number from the test case document
        """
        Betrieb_info = self.Wpm_info["Betriebsmodus"]
        mode_int = self.tcp.get_values_from_integer_registers(
            self.regs_integer["BA_aktiv"])
        mode = Betrieb_info[str(mode_int)]
        self.compare_results(test_case, mode, UHI_mode,
                             "'Manuell' -> Betriebsmodus is wrong", step)
        time.sleep(3)

    def verify_Urlaub_modus_with_WPM(self, test_case, Mode, step):
        """
        verifies 'Urlaub' Betriebsmodus with WPM

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        Mode
            UHI Betriebsmodus (Automatik / Manuell) for printing result
        step
            step number from the test case document
        """
        self.click_right_arrow(test_case, -2, step)
        time.sleep(5)
        self.close_error_popup(test_case, step)
        # set 'Date' in Urlaub
        Date = self.set_date_time_via_up_down_arrow(test_case, URLAUB, 5, 0, step)
        # set 'Month' in Urlaub
        Month = self.set_date_time_via_up_down_arrow(test_case, URLAUB, 2, 1, step)
        # set 'year' in Urlaub
        Year = self.set_date_time_via_up_down_arrow(test_case, URLAUB, 0, 2, step)
        today = self.get_today_from_ntp()
        end_day = date(Year, Month, Date)
        UHI_Urlaub_days = (end_day - today).days

        self.wait_browser(test_case, 6, step)

        # verify with WPM
        self.compare_results(test_case, self.get_Urlaub_days_from_WPM(
        ), UHI_Urlaub_days, Mode + " -> Urlaub days is wrong", step)

    def verify_Party_modus_with_WPM(self, test_case, Mode, step):
        """
        verifies 'Party' Betriebsmodus with WPM

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        Mode
            UHI Betriebsmodus (Automatik / Manuell) for printing result
        step
            step number from the test case document
        """
        self.click_right_arrow(test_case, -1, step)
        time.sleep(5)
        self.close_error_popup(test_case, step)
        # set 'time' in Party
        UHI_Party_hours = self.set_date_time_via_up_down_arrow(test_case,
            PARTY, 5, 0, step)
        time.sleep(2)
        UHI_Party_end_time = self.findElementCSS(test_case,
            self.texT["area_desc"], step).text
        ntp_Party_end_time = self.opt_Party + " " + self._content["UNTIL"] + ": " + (
            datetime.now() + timedelta(hours=UHI_Party_hours, seconds=-30)).strftime("%H:%M (%d.%m.%Y)")
        self.compare_results(test_case, ntp_Party_end_time, UHI_Party_end_time,
                             "[Expected Fail] Party end time in 'UHI' mismatched with 'ntp' time", step)

        self.wait_browser(test_case, 10, step)

        # verify with WPM
        self.compare_results(test_case, self.get_Party_hours_from_WPM(
        ), UHI_Party_hours, Mode + " -> Party hours is wrong", step)

    def test_Betriebsmodus_in_UHI(self, test_case, target_screen, target_tile, step):
        """
        setup 'Betriebsmodus' in UHI based on 'user_input.json' input

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        target_screen
            The screen where the action is performed (Home, Settings, EasyOn, Analysis)
        target_tile
            The tile where the test steps are performed (Warmwasser, Haus, Netzwerk, Software update and so on.)
        step
            step number from the test case document
        """
        Mode, info = self.get_Betriebsmodus_from_json()
        if Mode == AUTOMATIK:
            self.click_dropdown_by_title1(test_case, self.opt_Automatik, step)
            time.sleep(5)
            try:
                active_mode = self.findElementCSS(test_case,
                    "div.element-text-area.current-mode", step)
                if not active_mode.is_displayed():
                    pass
            except:
                self.click_area_title_by_name(test_case, self.opt_Automatik, step)
                self.wait_browser(test_case, 1, step)

            if info["Operating_mode"] == AUTOMATIK or info["Operating_mode"] == ALL:
                # if active_mode.text == self.opt_Urlaub:
                #     self.click_text_area_by_name(test_case, self.opt_Urlaub, step)
                #     time.sleep(5)
                # if active_mode.text == self.opt_Party:
                #     self.click_text_area_by_name(test_case, self.opt_Party, step)
                #     time.sleep(5)
                element_ID = 0
                self.click_right_arrow(test_case, element_ID, step)
                wait
                UHI_heat_limit = self.set_Grenz_temperature(test_case,
                    self.opt_Heat_limit, info["Heating_limit"], element_ID, step)
                wait
                UHI_time_limit = self.set_Grenz_time(test_case,
                    self.opt_Heat_limit, info["time_limit"], element_ID+1, step)
                self.click_back_by_header_title(test_case, self.opt_Heat_limit, step)
                time.sleep(5)

                # check if 'Kuehlen' is in any HK
                if self._config.Kuehlen_in_HK():
                    element_ID = 0
                    self.browser.get(self.uhi_url)
                    self.initial_steps(test_case, target_screen, target_tile)
                    self.click_dropdown_by_title1(test_case, self.opt_Automatik, step)
                    wait
                    try:
                        self.click_right_arrow(test_case, element_ID+1, step)
                    except:
                        self.compare_true_result(
                            test_case, False, "'Kuehlen bis Grenzwert' right arrow is not available", step)
                    wait
                    UHI_cool_limit = self.set_Grenz_temperature(test_case,
                        self.opt_Cool_limit, info["Cooling_limit"], element_ID, step)
                    wait
                    UHI_time_limit = self.set_Grenz_time(test_case,
                        self.opt_Cool_limit, info["time_limit"], element_ID+1, step)
                    wait
                    self.click_back_by_header_title(test_case, self.opt_Cool_limit, step)
                    time.sleep(5)

                self.wait_browser(test_case, 6, step)

                # verify with WPM
                self.compare_true_result(test_case, self.get_Automatik_status_from_WPM(
                ), "'Automatik' Betriebsmodus is not activated in WPM", step)
                self.compare_results(test_case, self.get_Automatik_Heating_limit_from_WPM(
                ), UHI_heat_limit, "[Expected Fail] Automatik -> 'Heizen bis Grenzwert' is wrong", step)
                self.compare_results(test_case, self.get_Automatik_time_limit_from_WPM(
                ), UHI_time_limit, "Automatik -> 'Time' limit is wrong", step)
                
                # check if 'Kuehlen' is in any HK
                if self._config.Kuehlen_in_HK():
                    self.compare_results(test_case, self.get_Automatik_Cooling_limit_from_WPM(
                    ), UHI_cool_limit, "Automatik -> 'Kuehlen bis Grenzwert' is wrong", step)
                
            # Urlaub deprecated in UHI 2.4.1
            if info["Operating_mode"] == URLAUB: # or info["Operating_mode"] == ALL:
                if active_mode.text == self.opt_Urlaub:
                    for i in range(2):
                        self.click_text_area_by_name(test_case, self.opt_Urlaub, step)
                        time.sleep(5)
                if not active_mode.text == self.opt_Urlaub:
                    self.click_text_area_by_name(test_case, self.opt_Urlaub, step)
                    time.sleep(5)
                self.verify_Urlaub_modus_with_WPM(test_case, Mode, step)
                self.compare_true_result(test_case, self.get_Automatik_status_from_WPM(
                ), "'Automatik' Betriebsmodus is not activated in WPM", step)
                self.click_back_by_header_title(test_case, self.opt_Urlaub, step)
                time.sleep(5)

            if info["Operating_mode"] == PARTY: # or info["Operating_mode"] == ALL:
                if active_mode.text == self.opt_Party:
                    for i in range(2):
                        self.click_text_area_by_name(test_case, self.opt_Party, step)
                        time.sleep(5)
                if not active_mode.text == self.opt_Party:
                    self.click_text_area_by_name(test_case, self.opt_Party, step)
                    time.sleep(5)
                self.verify_Party_modus_with_WPM(test_case, Mode, step)
                self.compare_true_result(test_case, self.get_Automatik_status_from_WPM(
                ), "'Automatik' Betriebsmodus is not activated in WPM", step)
                self.click_back_by_header_title(test_case, self.opt_Party, step)
                time.sleep(5)

        # Manuell 'Betriebsmodus'
        if Mode == MANUELL:
            if info["Operating_mode"] == WINTER or info["Operating_mode"] == ALL:
                self.click_text_area_by_name(test_case, self.opt_Winter, step)
                self.wait_browser(test_case, 6, step)
                self.verify_Manuell_modus_with_WPM(test_case, WINTER, step)
            if info["Operating_mode"] == SOMMER or info["Operating_mode"] == ALL:
                self.click_text_area_by_name(test_case, self.opt_Sommer, step)
                self.wait_browser(test_case, 6, step)
                self.verify_Manuell_modus_with_WPM(test_case, SOMMER, step)
            if info["Operating_mode"] == HEIZSTAB or info["Operating_mode"] == ALL:
                self.click_text_area_by_name(test_case, self.opt_Heizstab, step)
                self.wait_browser(test_case, 6, step)
                self.verify_Manuell_modus_with_WPM(test_case, HEIZSTAB, step)
            if info["Operating_mode"] == KUEHLEN or info["Operating_mode"] == ALL:
                if self._config.Kuehlen_in_HK():
                    self.click_text_area_by_name(test_case, self.opt_Kuehlen, step)
                    self.wait_browser(test_case, 6, step)
                    self.verify_Manuell_modus_with_WPM(test_case, KUEHLEN, step)
                else:
                    PASS("[Invalid Config] Kuehlen is not available in any 'Heizkreise'")
            # Urlaub deprecated in UHI 2.4.1
            if info["Operating_mode"] == URLAUB: # or info["Operating_mode"] == ALL:
                self.click_text_area_by_name(test_case, self.opt_Urlaub, step)
                self.verify_Urlaub_modus_with_WPM(test_case, Mode, step)
            if info["Operating_mode"] == PARTY: # or info["Operating_mode"] == ALL:
                self.click_text_area_by_name(test_case, self.opt_Party, step)
                self.verify_Party_modus_with_WPM(test_case, Mode, step)
