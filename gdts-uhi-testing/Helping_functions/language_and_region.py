from src.General_functions import GeneralFunctions
from src.logger import fail_log, pass_log
import time

FAIL = fail_log.error
PASS = pass_log.info


class LanguageAndRegion(GeneralFunctions):
    """
    This is a helping class for the test case 'UHI-03 -- Language und Region' with get, 
    set and verify functions.

    * :py:meth:`Helping_functions.language_and_region.LanguageAndRegion.select_SpracheUndRegion_tile`
    * :py:meth:`Helping_functions.language_and_region.LanguageAndRegion.verify_language_on_tile`
    * :py:meth:`Helping_functions.language_and_region.LanguageAndRegion.select_languages_from_list`

    """
    lang_und_regio = GeneralFunctions().screen_info["lang_regio"]

    def select_SpracheUndRegion_tile(self, test_case, step):
        """
        Select 'Sprache und Region' tile in all languages for testing.

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        for t in self.findElementsCSS(test_case, self.texT["title"], step):
            if t.text == "Spra­che, Re­gi­on und Er­schei­nung" or t.text == "Lan­guage, Re­gion and Ap­pear­ance" or t.text == "Langue et ré­gion" or t.text == "Lin­gua e re­gio­ne" \
                or t.text == "LAN­GU­AGE_RE­GION_AND_AP­PE­ARAN­CE" or t.text == "LAN­GU­AGE_RE­GION_AND_AP­PE­ARAN­CE" or t.text == "LAN­GUAGE_RE­GION_AND_APPEA­RAN­CE":
                t.click()
                break
        time.sleep(2)
        active = self.get_text_from_element(test_case,
                                            self.lang_und_regio["active"], step)
        PASS("Active language: " + str(active))
        inactive = self.get_size_of_elements(test_case,
                                             self.lang_und_regio["inactive"], step)
        self.compare_results(
            test_case, 5, inactive, "Total available languages are not equal to 5", step)

    def verify_language_on_tile(self, test_case, activate, step):
        """
        Verify displayed 'language' on tile after it is selected from the list of available languages.

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        activate
            The language that user selected from the list
        step
            step number from the test case document
        """
        self.click_close_icon(test_case, step)
        time.sleep(2)
        for lang in self.findElementsCSS(test_case, self.elements["tiles"]["value"], step):
            if lang.text == activate:
                self.compare_results(
                    test_case, lang.text, activate, activate + " is not UHI language", step)

    def select_languages_from_list(self, test_case, element, activate, step):
        """
        Select 'languages' from the list of available languages

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        activate
            The language that user wanted to activate in the UHI from the list
        step
            step number from the test case document
        """
        selected_lang = self.findElementsCSS(
            test_case, self.lang_und_regio["inactive"], step)[element]
        if selected_lang.text == activate:
            selected_lang.click()
            time.sleep(2)
            self.verify_language_on_tile(test_case,
                                         activate, step)
            self.select_SpracheUndRegion_tile(test_case, step)
