from src.General_functions import GeneralFunctions

class FunctionControl(GeneralFunctions):
    """
    This is a helping class for the test case 'UHI-04, 05, 06 -- Kontrollefunktionen' with get, set and verify functions.

    * :py:meth:`Helping_functions.function_control.FunctionControl.check_options_from_config`
    * :py:meth:`Helping_functions.function_control.FunctionControl.verify_results`
    * :py:meth:`Helping_functions.function_control.FunctionControl.verify_toggle_after_time`
    * :py:meth:`Helping_functions.function_control.FunctionControl.P_FK_Zeit_Ein`
    * :py:meth:`Helping_functions.function_control.FunctionControl.Funkt_Kontr_Aktiv`
    * :py:meth:`Helping_functions.function_control.FunctionControl.Fun_Code_Biv`
    * :py:meth:`Helping_functions.function_control.FunctionControl.P_FK_ZWE`
    * :py:meth:`Helping_functions.function_control.FunctionControl.P_FK_Mi_Biv`
    * :py:meth:`Helping_functions.function_control.FunctionControl.P_FK_Mi_Hk1`
    * :py:meth:`Helping_functions.function_control.FunctionControl.HK1_Msicher_status`
    * :py:meth:`Helping_functions.function_control.FunctionControl.HK2_Msicher_status`
    * :py:meth:`Helping_functions.function_control.FunctionControl.P_FK_Mi_Hk2`
    * :py:meth:`Helping_functions.function_control.FunctionControl.P_FK_ZUP`
    * :py:meth:`Helping_functions.function_control.FunctionControl.P_FK_HUP1`
    * :py:meth:`Helping_functions.function_control.FunctionControl.P_FK_HUP2`
    * :py:meth:`Helping_functions.function_control.FunctionControl.P_FK_WUP`
    * :py:meth:`Helping_functions.function_control.FunctionControl.P_FK_ZWUP`
    * :py:meth:`Helping_functions.function_control.FunctionControl.P_FK_VEN`
    * :py:meth:`Helping_functions.function_control.FunctionControl.Biv_Mischer_status`

    """
    def check_options_from_config(self, test_case, _config, _option, step):
        """
        Check if the 'toggle' buttons are presented in the 'Funktionskontroll' based on WPM config

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        _config
            WPM config based on *Funktionscode*
        _option
            Options 'text' from 'de.common.json'
        step
            step number from the test case document
        """
        if _config:
            self.check_presence_of_option_from_toggle_row_text(test_case, _option, step)
            self.get_click_from_toggle_row(test_case, _option, step)

    def verify_results(self, test_case, _config, expected, error, step):
        """
        verify 'expected' results against 'actual' result for 'Funktionskontroll' based on WPM config

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        _config
            WPM config based on *Funktionscode*
        expected
            Expected result to verify with actual result from UHI
        error
            Reason of error to be displayed in *Failed.log*
        step
            step number from the test case document
        """
        if _config:
            self.compare_true_result(
                test_case, expected, error, step)

    def verify_toggle_after_time(self, test_case, step):
        """
        Verify if 'toggle' button turned back off after the end time of 'Funktionkontroll'

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        for t in self.findElementsCSS(test_case, self.texT["toggle_row"], step):
            button = t.find_element_by_css_selector(
                self.buttons["toggle_button"])
            if not int(button.value_of_css_property(self.css_prop["margin_left"])[0]) == 0:
                opt = t.find_element_by_css_selector(self.texT["area"]).text
                self.compare_true_result(test_case, False, opt + " toggle failed to turn back off", step)

    def P_FK_Zeit_Ein(self):
        """
        Returns 'Funktionkontroll' time from registers

        Returns
        -------
        'int'
            'Systemkontroll' time from WPM
        """
        return self.tcp.get_values_from_integer_registers(self.regs_integer["P_FK_Zeit_Ein"])
    
    def Funkt_Kontr_Aktiv(self):
        """
        Returns 'True', if 'Funktionkontroll' is activated

        Returns
        -------
        'Boolean'
            True / False
        """
        return self.tcp.get_status_from_coils(self.regs_digital["Funkt_Kontr_Aktiv"])

    def Fun_Code_Biv(self):
        """
        Returns 'True', if 'Bivalent' is presented in Funktionscode

        Returns
        -------
        'Boolean'
            True / False
        
        """
        return self.tcp.get_status_from_coils(self.regs_digital["Fun_Code_Biv"])

    def P_FK_ZWE(self):
        """
        Returns 'True', if '2.Warmeerzeuger' is activated

        Returns
        -------
        'Boolean'
            True / False
        """
        return self.tcp.get_status_from_coils(self.regs_digital["P_FK_ZWE"])

    #### Funktionskontroll Mischer ####
    def P_FK_Mi_Biv(self):
        """
        Returns 'True', if 'Bivalent' Mischer is activated 

        Returns
        -------
        'Boolean'
            True / False
        """
        return self.tcp.get_status_from_coils(self.regs_digital["P_FK_Mi_Biv"])
    
    def P_FK_Mi_Hk1(self):
        """
        Returns 'True', if 'HK1' Mischer is Presented 

        Returns
        -------
        'Boolean'
            True / False
        """
        return self.tcp.get_status_from_coils(self.regs_digital["P_FK_Mi_Hk1"])

    def HK1_Msicher_status(self):
        """
        Returns "True", if 'HK1' Mischer is Open

        Returns
        -------
        'Boolean'
            True / False
        """
        return self.tcp.get_status_from_coils(self.regs_digital["A_Hk1_MiA"])

    def HK2_Msicher_status(self):
        """
        Returns "True", if 'HK2' Mischer is Open

        Returns
        -------
        'Boolean'
            True / False
        """
        return self.tcp.get_status_from_coils(self.regs_digital["A_Hk2_MiA"])

    def P_FK_Mi_Hk2(self):
        """
        Returns 'True', if 'HK2' Mischer is activated 

        Returns
        -------
        'Boolean'
            True / False
        """
        return self.tcp.get_status_from_coils(self.regs_digital["P_FK_Mi_Hk2"])

    #### Funktionskontroll Pumpe ####
    def P_FK_ZUP(self):
        """
        Returns 'True', if 'Zusatz' Pump is activated 

        Returns
        -------
        'Boolean'
            True / False
        """
        return self.tcp.get_status_from_coils(self.regs_digital["P_FK_ZUP"])

    def P_FK_HUP1(self):
        """
        Returns 'True', if 'HK1' Pump is activated 

        Returns
        -------
        'Boolean'
            True / False
        """
        return self.tcp.get_status_from_coils(self.regs_digital["P_FK_HUP1"])

    def P_FK_HUP2(self):
        """
        Returns 'True', if 'HK2' Pump is activated 

        Returns
        -------
        'Boolean'
            True / False
        """
        return self.tcp.get_status_from_coils(self.regs_digital["P_FK_HUP2"])

    def P_FK_WUP(self):
        """
        Returns 'True', if 'Warmwasser' Pump is activated 

        Returns
        -------
        'Boolean'
            True / False
        """
        return self.tcp.get_status_from_coils(self.regs_digital["P_FK_WUP"])

    def P_FK_ZWUP(self):
        """
        Returns 'True', if 'Schwimmbad' Pump is activated 

        Returns
        -------
        'Boolean'
            True / False
        """
        return self.tcp.get_status_from_coils(self.regs_digital["P_FK_ZWUP"])

    def P_FK_VEN(self):
        """
        Returns 'True', if 'Ventilator M2' is activated 

        Returns
        -------
        'Boolean'
            True / False
        """
        return self.tcp.get_status_from_coils(self.regs_digital["P_FK_VEN"])

    def Biv_Mischer_status(self):
        """
        Returns "True", if 'Bivalent' Mischer is Open

        Returns
        -------
        'Boolean'
            True / False
        """
        return self.tcp.get_status_from_coils(self.regs_digital["A_Biv_MiA"])

    def Reg_Mischer_status(self):
        """
        Returns "True", if 'Regenerativ' Mischer is Open

        Returns
        -------
        'Boolean'
            True / False
        """
        return self.tcp.get_status_from_coils(self.regs_digital["A_Reg_MiA"])

    def get_max_Mischerlaufzeit_WPM(self):
        """ Returns maximum of `Mischerlaufzeit` from modbus registers """
        MischerLZ_HK1 = self.get_values_from_integer_registers(self.regs_integer["P_HK1_MILZ"])
        MischerLZ_HK2 = self.get_values_from_integer_registers(self.regs_integer["P_HK2_MILZ"])
        MischerLZ_Biv = self.get_values_from_integer_registers(self.regs_integer["P_Biv_MILZ"])
        MischerLZ_Reg = self.get_values_from_integer_registers(self.regs_integer["P_Reg_MILZ"])
        return max(MischerLZ_HK1, MischerLZ_HK2, MischerLZ_Biv, MischerLZ_Reg)