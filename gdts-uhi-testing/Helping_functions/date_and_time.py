from src.General_functions import GeneralFunctions
import time
import datetime
import ntplib
import re

date_time = GeneralFunctions().screen_info["Date_Time"]


class DateAndTime(GeneralFunctions):

    """
    This is a helping class for the test case 'UHI-02 -- test_date_and_time' with get, 
    set and verify functions.

    * :py:meth:`Helping_functions.date_and_time.DateAndTime.get_uhi_date`
    * :py:meth:`Helping_functions.date_and_time.DateAndTime.get_uhi_time`
    * :py:meth:`Helping_functions.date_and_time.DateAndTime.verify_WPM_date`
    * :py:meth:`Helping_functions.date_and_time.DateAndTime.verify_WPM_time`
    * :py:meth:`Helping_functions.date_and_time.DateAndTime.set_and_verify_Manuell_time`

    """

    def get_uhi_date(self, test_case, step):
        """
        Returns 'Date' from UHI screen in the format of Date, Month, Year

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document

        Returns
        -------
        string 'tuple'
            Returns Date, Month, Year from UHI frontend
        """
        date = self.get_text_from_spinbox(test_case,
                                          date_time["date"], date_time["value"], 0, step)
        month = self.get_text_from_spinbox(test_case,
                                           date_time["date"], date_time["value"], 1, step)
        year = self.get_text_from_spinbox(test_case,
                                          date_time["date"], date_time["value"], 2, step).strip("20")
        return date, month, year

    def get_uhi_time(self, test_case, step):
        """
        Returns 'Time' from UHI screen in the format of Hour, Mins

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document

        Returns
        -------
        string 'tuple'
            Returns Hours, Minutes from UHI frontend
        """
        hour = self.get_text_from_spinbox(test_case,
                                          date_time["time"], date_time["value"], 0, step)
        mins = self.get_text_from_spinbox(test_case,
                                          date_time["time"], date_time["value"], 1, step)
        return hour, mins

    def verify_WPM_date(self, test_case, mode, step):
        """
        Verify 'Date' from UHI with WPM registers

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        mode
            Automatik (or) Manuell clock mode from UHI
        step
            step number from the test case document
        """
        date, month, year = self.get_uhi_date(test_case, step)
        self.compare_results(mode, self.tcp.get_values_from_integer_registers(
            self.regs_integer["NEW_DAY"]), int(date), "Date mismatch", step)
        self.compare_results(mode, self.tcp.get_values_from_integer_registers(
            self.regs_integer["NEW_MONTH"]), int(month), "Month mismatch", step)
        self.compare_results(mode, self.tcp.get_values_from_integer_registers(
            self.regs_integer["NEW_YEAR"]), int(year[-2:]), "Year mismatch", step)

    def verify_WPM_time(self, test_case, mode, step):
        """
        Verify 'Time' from UHI with WPM registers

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        mode
            Automatik (or) Manuell clock mode from UHI
        step
            step number from the test case document
        """
        hour, mins = self.get_uhi_time(test_case, step)
        self.compare_results(mode, self.tcp.get_values_from_integer_registers(
            self.regs_integer["NEW_HOUR"]), int(hour), "Hour mismatch", step)
        self.compare_greater_than_results(mode, int(mins), self.tcp.get_values_from_integer_registers(
            self.regs_integer["NEW_MINUTE"]), "Minutes mismatch", step)

    def set_and_verify_Manuell_time(self, test_case, mode):
        """
        This function performs the setup of *'Manuell'* Date and Time, then verifies with WPM values.
        Once the testing is Done, then it switches back to 'Automatik' for further testing.

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        mode
            Automatik (or) Manuell clock mode from UHI
        """
        # 4 Click on Manuel mode
        step = 4
        try:
            for ele in self.findElementCSS(test_case, date_time["date"], step).find_elements_by_css_selector(date_time["up_arrow"]):
                self.assertEqual(ele.is_displayed(), True)
            for ele in self.findElementCSS(test_case, date_time["time"], step).find_elements_by_css_selector(date_time["up_arrow"]):
                self.assertEqual(ele.is_displayed(), True)
        except:
            self.compare_true_result(
                test_case, False, "Manuell time mode is not enabled", step)
        step += 1

        # Change date randomly
        self.set_date_time_via_up_down_arrow(test_case, None, 5, 0, step)
        self.set_date_time_via_up_down_arrow(test_case, None, 2, 1, step)

        # Change time randomly
        self.set_date_time_via_up_down_arrow(test_case, None, 5, 3, step)
        self.set_date_time_via_up_down_arrow(test_case, None, 10, 4, step)

        self.wait_browser(test_case, 25, step)

        self.verify_WPM_date(test_case, mode, step)
        self.verify_WPM_time(test_case, mode, step)

        # changing back to Automatik for further execution
        self.switch_to_Automatik_date_time_mode(test_case, step)

    def get_Automatik_date_time_from_ntp(self, test_case, step):
        """ Get `Automatik` Date and Time with **ntp** library"""
        client = ntplib.NTPClient()
        response = client.request('de.pool.ntp.org', version=3)
        t = datetime.datetime.fromtimestamp(response.tx_time)
        date = t.strftime("%d"), t.strftime("%m"), t.strftime("%y")
        _time = t.strftime("%H"), t.strftime("%M")

        self.compare_results(test_case, date, self.get_uhi_date(test_case,
                                                                step), "Automatik date is wrong in UHI with ntp", step)
        self.compare_results(test_case, _time, self.get_uhi_time(test_case,
                                                                 step), "Automatik time is wrong in UHI with ntp", step)

    def get_inactive_element_from_UHI(self, test_case, step):
        """ returns `inactive` element Date and Time mode from UHI 
        Returns
        -------
        inactive mode
            string
        """
        inactive_mode = self.findElementCSS(test_case,
                                            self.texT["active"], step)
        return inactive_mode

    def get_active_element_from_UHI(self, test_case, step):
        """ returns `active` element Date and Time mode from UHI 
        Returns
        -------
        active mode
            string
        """
        active_mode = self.findElementCSS(test_case,
                                          self.texT["inactive"], step)
        return active_mode

    def switch_to_Manuell_date_time_mode(self, test_case, step):
        """ Switch to `Manuell` Date and Time mode in UHI """
        # switching to "Manuell" mode
        if self.get_inactive_element_from_UHI(test_case, step).text == self._content["MANUAL"]:
            self.get_inactive_element_from_UHI(test_case, step).click()
            self.wait_browser(test_case, 2, step)
            self.compare_results(
                test_case, self._content["MANUAL"], self.get_active_element_from_UHI(test_case, step).text, "Switching to 'Manuell' mode failed", step)

    def switch_to_Automatik_date_time_mode(self, test_case, step):
        """ Switch to `Automatik` Date and Time mode in UHI """
        # switching to "Automatik" mode
        if self.get_inactive_element_from_UHI(test_case, step).text == self._content["AUTO"]:
            self.get_inactive_element_from_UHI(test_case, step).click()
            self.wait_browser(test_case, 2, step)
            self.compare_results(
                test_case, self._content["AUTO"], self.get_active_element_from_UHI(test_case, step).text, "Switching to 'Automatik' mode failed", step)

    def test_Automatik_timezone(self, test_case, step):
        """ Changes Automatik timezone in the UHI """

        def select_zone(timeZone):
            self.wait_browser(test_case, 1, step)
            self.findElementCSS(test_case, self.buttons["Edit_icon"], step).click()
            self.wait_browser(test_case, 1, step)

            for zone in self.findElementsCSS(test_case, self.texT["area"], step):
                try:
                    if zone.text == timeZone:
                        zone.click()
                except:
                    pass

            self.wait_browser(test_case, 1, step)

        def get_UHI_date_time(separator):
            for row in self.findElementsCSS(test_case, "div.row", step):
                if(bool(re.search(separator, row.text))):
                    return tuple(row.text.split(separator))

        def verify_date_and_time_with_WPM():
            date, month, year = get_UHI_date_time(".")
            hour, mins = get_UHI_date_time(":")
            mode = "Automatik"
            
            self.compare_results(mode, self.tcp.get_values_from_integer_registers(
                self.regs_integer["NEW_DAY"]), int(date), "Date mismatch", step)
            self.compare_results(mode, self.tcp.get_values_from_integer_registers(
                self.regs_integer["NEW_MONTH"]), int(month), "Month mismatch", step)
            self.compare_results(mode, self.tcp.get_values_from_integer_registers(
                self.regs_integer["NEW_YEAR"]), int(year[-2:]), "Year mismatch", step)
            
            self.compare_results(mode, self.tcp.get_values_from_integer_registers(
                self.regs_integer["NEW_HOUR"]), int(hour), "Hour mismatch", step)
            self.compare_greater_than_results(mode, int(mins), self.tcp.get_values_from_integer_registers(
                self.regs_integer["NEW_MINUTE"]), "Minutes mismatch", step)

        select_zone("Australia/Sydney")
        self.wait_browser(test_case, 12, step)
        verify_date_and_time_with_WPM()
        
        select_zone("Europe/Amsterdam")
        self.wait_browser(test_case, 12, step)
        verify_date_and_time_with_WPM()
