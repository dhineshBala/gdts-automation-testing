from src.General_functions import GeneralFunctions

class LaufzeitUndTaktungen(GeneralFunctions):
    """
    This is a helping class for the test case 'UHI-11 -- Laufzeiten und Taktungen' with get and verify functions.

    * :py:meth:`Helping_functions.laufzeit_and_taktungen.LaufzeitUndTaktungen.BS_Vd1`
    * :py:meth:`Helping_functions.laufzeit_and_taktungen.LaufzeitUndTaktungen.BS_ZWE`
    * :py:meth:`Helping_functions.laufzeit_and_taktungen.LaufzeitUndTaktungen.BS_PupVen`
    * :py:meth:`Helping_functions.laufzeit_and_taktungen.LaufzeitUndTaktungen.BS_WUP`
    * :py:meth:`Helping_functions.laufzeit_and_taktungen.LaufzeitUndTaktungen.BS_Vd1_SV_H`
    * :py:meth:`Helping_functions.laufzeit_and_taktungen.LaufzeitUndTaktungen.BS_Vd1_SV_WW`
    * :py:meth:`Helping_functions.laufzeit_and_taktungen.LaufzeitUndTaktungen.BS_Vd1_SV_K`
    * :py:meth:`Helping_functions.laufzeit_and_taktungen.LaufzeitUndTaktungen.get_uhi_value`
    * :py:meth:`Helping_functions.laufzeit_and_taktungen.LaufzeitUndTaktungen.verify_laufzeit_taktungen`
    * :py:meth:`Helping_functions.laufzeit_and_taktungen.LaufzeitUndTaktungen.verify_sameName_laufzeit`

    """
    # Laufzeiten
    def BS_Vd1(self):
        """
        Returns 'Verdichter' laufzeit value from WPM registers

        Returns
        -------
        'int'
            hours
        """
        return self.tcp.get_values_from_integer_registers(self.regs_integer["BS_Vd1"])

    def BS_ZWE(self):
        """
        Returns '2.Waermeerzeuger' laufzeit value from WPM registers

        Returns
        -------
        'int'
            hours
        """
        return self.tcp.get_values_from_integer_registers(self.regs_integer["BS_ZWE"])

    def BS_PupVen(self):
        """
        Returns 'Ventilator' laufzeit value from WPM registers

        Returns
        -------
        'int'
            hours
        """
        return self.tcp.get_values_from_integer_registers(self.regs_integer["BS_PupVen"])

    def BS_WUP(self):
        """
        Returns 'Warmwasser' laufzeit value from WPM registers

        Returns
        -------
        'int'
            hours
        """
        return self.tcp.get_values_from_integer_registers(self.regs_integer["BS_WUP"])

    # Taktungen
    def BS_Vd1_SV_H(self):
        """
        Returns 'Heizen' Taktungen value from WPM registers

        Returns
        -------
        'int'
        """
        return self.tcp.get_values_from_integer_registers(self.regs_integer["BS_Vd1_SV_H"])

    def BS_Vd1_SV_WW(self):
        """
        Returns 'Warmwasser' Taktungen value from WPM registers

        Returns
        -------
        'int'
        """
        return self.tcp.get_values_from_integer_registers(self.regs_integer["BS_Vd1_SV_WW"])

    def BS_Vd1_SV_K(self):
        """
        Returns 'Kuehlen' Taktungen value from WPM registers

        Returns
        -------
        'int'
        """
        return self.tcp.get_values_from_integer_registers(self.regs_integer["BS_Vd1_SV_K"])

    # read value from UHI
    def get_uhi_value(self, test_case, option, step):
        """
        Returns 'UHI' *'Value'* without units at the end for verification

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        option
            Text *option* from 'de.common.json' file for search in UHI
        step
            step number from the test case document
        
        Returns
        -------
        'int'
            Laufzeit and Taktungen values
        """
        value = self.get_text_from_row(test_case, option, step)
        if value[-1] == "h":
            value = value.strip(" h")
        return int(value)

    def verify_laufzeit_taktungen(self, test_case, option, expected, step):
        """
        Verifies 'UHI' *'Laufzeit and Taktungen'* values with WPM by comparing them

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        option
            Text *option* from 'de.common.json' file for search in UHI
        expected
            An *expected* result for comprison
        step
            step number from the test case document
        """
        actual = self.get_uhi_value(test_case, option, step)
        self.compare_results(test_case, expected, actual, option + " value is wrong", step)

    def verify_sameName_laufzeit(self, test_case, option, expected, step):
        """
        Verifies 'UHI' *'Laufzeit'* values with WPM when the **option** texts are same like Warmwasser, Heizen in Laufzeit

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        option
            Text *option* from 'de.common.json' file for search in UHI
        expected
            An *expected* result for comprison
        step
            step number from the test case document
        """
        for i in self.findElementsCSS(test_case, self.texT["row"], step):
            opt = i.find_element_by_css_selector(self.texT["title"]).text
            val = i.find_element_by_css_selector(self.texT["value"]).text
            if  opt == option and val[-1] == "h":
                self.compare_results(test_case, expected, int(val.strip(" h")), option + " Laufzeit is wrong", step)
                break

    def verify_sameName_taktungen(self, test_case, option, expected, step):
        """
        Verifies 'UHI' *'Taktungen'* values with WPM when the **option** texts are same like Warmwasser, Heizen in Taktungen

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        option
            Text *option* from 'de.common.json' file for search in UHI
        expected
            An *expected* result for comprison
        step
            step number from the test case document
        """
        for i in self.findElementsCSS(test_case, self.texT["row"], step):
            opt = i.find_element_by_css_selector(self.texT["title"]).text
            val = i.find_element_by_css_selector(self.texT["value"]).text
            if  opt == option and val[-1] != "h":
                self.compare_results(test_case, expected, int(val), option + " Taktungen is wrong", step)
                break
