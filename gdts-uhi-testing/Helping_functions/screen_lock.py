from src.General_functions import GeneralFunctions
import time

class ScreenLock(GeneralFunctions):
    """
    This is a helping class for the test case 'UHI-09 -- Bildschrim Sperren' with get, set and verify functions.

    * :py:meth:`Helping_functions.screen_lock..check_auto_lock_time`
    * :py:meth:`Helping_functions.screen_lock.ScreenLock._4th_step`
    * :py:meth:`Helping_functions.screen_lock.ScreenLock.action_slider`
    * :py:meth:`Helping_functions.screen_lock.ScreenLock.select_lock_time`

    """
    screen_lock = GeneralFunctions().user_input["Bildschrim_Sperre"]

    def check_auto_lock_time(self, test_case, lock_time, step):
        """
        Verifies 'Automatik' lock time in the UHI based on user chosen time in *'user_input.json'* file

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        lock_time
            Lock time in '1 min, 15 min .. ' format to identify the lock time
        step
            step number from the test case document
        """
        lock_time = int(lock_time[0:2]) * 60
        t_start = time.clock()
        self.waitByCss(test_case, self.elements["clock"], lock_time + 5, step)
        t_end = time.clock()
        uhi_lock_time = t_end - t_start
        self.compare_greater_than_results(test_case, uhi_lock_time, lock_time - 3,
                                               "UHI not locked at " + str(lock_time) + "s, actual lock time is " +
                                               str(uhi_lock_time) + "s", step)
        self.compare_greater_than_results(test_case, lock_time + 3, uhi_lock_time,
                                               "UHI not locked at " + str(lock_time) + "s, actual lock time is " +
                                               str(uhi_lock_time) + "s", step)

    def _4th_step(self, test_case, target_screen, target_tile, step):
        """
        Executes 4th step from the test case *'Bildschrim Sperre'* from the document

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        target_screen
            The screen where the action is performed (Home, Settings, EasyOn, Analysis)
        target_tile
            The tile where the test steps are performed (Warmwasser, Haus, Netzwerk, Software update and so on.)
        step
            step number from the test case document
        """
        self._1st_step(test_case, step)
        step += 1

        # 6 open settings
        self._2nd_step(test_case, target_screen, step)
        step += 1

        # 7 click on screen lock time
        self._3rd_step(test_case, target_tile, step)
        scale = self.findElementsCSS(test_case,
            self.elements["lock_slider"]["label"], step)
        self.compare_results(test_case, 4, int(
            len(scale)), "Times index are not 1, 3, 5, 15 mins", step)
        step += 1

    def action_slider(self, slider, offset_x, step):
        """
        This is supporting function for setting up the Auto lock time slider in UHI

        Parameters
        ----------
        slider
            Slider element from Web browser
        offset_x
            offset in x direction since the time slider is horizantel
        step
            step number from the test case document
        """
        action = self.action
        action.click_and_hold(slider).move_by_offset(
                offset_x, 0).release().perform()
        time.sleep(3)

    def select_lock_time(self, test_case, step):
        """
        Setup *Automatik* lock time from 'user_input.json' file and returns the setup time for further verification

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document

        Returns
        -------
        'string'
            value: 1 min, 2 min, ...
        """
        lock_time = self.screen_lock["Zeit"]
        slider = self.findElementCSS(test_case,
            self.lock_slider["slider"], step)
        length = slider.size["width"]
        
        self.action_slider(slider, -(length), step)

        if lock_time == "3 min":
            self.action_slider(slider, 0, step)
        if lock_time == "5 min":
            self.action_slider(slider, 10, step)
        if lock_time == "15 min":
            self.action_slider(slider, length+100, step)
        return lock_time
