from src.General_functions import GeneralFunctions
from src.logger import fail_log
import json, requests

class AnlagenDaten(GeneralFunctions):
    """
    This is a helping class for the test case 'UHI-10 -- Anlagendaten' with get and verify functions.

    * :py:meth:`Helping_functions.anlagen_daten.AnlagenDaten.get_uhi_versions_info`
    * :py:meth:`Helping_functions.anlagen_daten.AnlagenDaten.get_uhi_version_from_api`
    * :py:meth:`Helping_functions.anlagen_daten.AnlagenDaten.get_serial_no_from_WPM`
    * :py:meth:`Helping_functions.anlagen_daten.AnlagenDaten.get_WPM_ver_from_WPM`
    * :py:meth:`Helping_functions.anlagen_daten.AnlagenDaten.get_KKR_ver_from_WPM`
    * :py:meth:`Helping_functions.anlagen_daten.AnlagenDaten.get_WQMF_ver_from_WPM`
    """

    def get_uhi_versions_info(self, test_case, step):
        """
        Returns *'Versions'* information from UHI 'Anlagendaten' screen

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        
        Returns
        -------
        'string' tuple
            returns UHI_ver, serial_no, WPM_ver, KKR_ver, WQMF_ver
        """
        versions = self.findElementsCSS(test_case, self.texT["value"], step)

        UHI_ver = versions[1].text
        serial_no = versions[2].text
        WPM_ver = versions[3].text
        KKR_ver = "0.0"
        WQMF_ver = "0.0"

        for title in self.findElementsCSS(test_case, "div.element-text-area.section-title", step):
            if self.get_string_without_soft_hyphen(title.text) == "Kältekreismodul":
                KKR_ver = versions[4].text
            
            if self.get_string_without_soft_hyphen(title.text) == "Wärmequellenmodul":
                WQMF_ver = versions[5].text

        return UHI_ver, serial_no, WPM_ver, KKR_ver, WQMF_ver

    def get_uhi_version_from_api(self, step):
        """
        Returns 'UHI' version from API -- *'http://ip-address/api/system/version'* 

        Parameters
        ----------
        step
            step number from the test case document
        
        Returns
        -------
        'string' 
            returns UHI_version
        """
        uhi_ver = self._data["APIservices"]["uhi_ver"]
        response = requests.get(
            self.Url_info["prefix"] + self.Url_info["Url"] + uhi_ver)
        if not response.status_code == 200:
            fail_log.error(uhi_ver + " -> Request failed with code " +
                    response.status_code + " in step " + str(step))
        try:
            json_data = json.loads(response.text)
            uhi_ver_from_api = json_data["uhi"]["version"]
        except:
            fail_log.error(
                "UHI version from API is not available in step = " + str(step))
        return uhi_ver_from_api

    def get_serial_no_from_WPM(self):
        """
        Returns 'WPM' serial number from Modbus registers

        Returns
        -------
        'int' tuple 
            returns Serien_Nummer_1, Serien_Nummer_2, Serien_Nummer_3
        """
        sn1 = self.tcp.get_values_from_integer_registers(self.regs_integer["Serien_Nummer_1"])
        sn2 = self.tcp.get_values_from_integer_registers(self.regs_integer["Serien_Nummer_2"])
        sn3 = self.tcp.get_values_from_integer_registers(self.regs_integer["Serien_Nummer_3"])
        return sn1, sn2, sn3

    def get_WPM_ver_from_WPM(self):
        """
        Returns 'WPM' software version from Modbus registers

        Returns
        -------
        'string'
            returns WPM version (e.g., M1.3a)
        """
        wpm_index = self.Wpm_info["WPM_index"]
        SWa_Version = (wpm_index[str(int(self.tcp.get_values_from_analog_registers(self.regs_analog["SWa_Version"], 1)))]).upper()
        SWa_Nummer = int(self.tcp.get_values_from_analog_registers(self.regs_analog["SWa_Nummer"], 1))
        SWa_Index = int(self.tcp.get_values_from_analog_registers(self.regs_analog["SWa_Index"], 1))
        SWa_Kd_Version = wpm_index[str(int(self.tcp.get_values_from_integer_registers(self.regs_integer["SWa_Kd_Version"])))]
        return SWa_Version + str(SWa_Nummer) + "." + str(SWa_Index) + SWa_Kd_Version

    def get_KKR_ver_from_WPM(self):
        """
        Returns 'KKR' software version from Modbus registers

        Returns
        -------
        'string'
            returns KKR version (e.g., 2.18e)
        """
        KKR_index = self.Wpm_info["WPM_index"]
        r_SW_Ver_1 = int(self.tcp.get_values_from_analog_registers(self.regs_analog["r_SW_Ver_1"], 1))
        r_SW_Ver_2 = int(self.tcp.get_values_from_analog_registers(self.regs_analog["r_SW_Ver_2"], 1))
        r_SW_Ver_3 = KKR_index[str(int(self.tcp.get_values_from_analog_registers(self.regs_analog["r_SW_Ver_3"], 1)))]
        return str(r_SW_Ver_1) + "." + str(r_SW_Ver_2) + r_SW_Ver_3

    def get_WQMF_ver_from_WPM(self):
        """
        Returns 'WQMF' software version from Modbus registers

        Returns
        -------
        'float'
            returns WQMF version (e.g., 1.2)
        """
        r_WQMF_PrgVersion = self.tcp.get_values_from_analog_registers(self.regs_analog["r_WQMF_PrgVersion"], 10)
        return r_WQMF_PrgVersion

