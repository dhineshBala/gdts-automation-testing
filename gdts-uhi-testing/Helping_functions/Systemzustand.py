from src.General_functions import GeneralFunctions
from Helping_functions.Anheizprogramm import AnheizProgramm
import time
Anheiz = AnheizProgramm()

class SystemZustand(GeneralFunctions):
    """
    This is a helping class for the test case 'UHI-24 -- Systemzustand' with get, set and verify functions.

        * :py:meth:`Helping_functions.Systemzustand.SystemZustand.activate_Funktionsheizen`
        * :py:meth:`Helping_functions.Systemzustand.SystemZustand.get_status_of_Funktionsheizen`
        * :py:meth:`Helping_functions.Systemzustand.SystemZustand.verify_Systemzustand_tile`

    """
    def activate_Funktionsheizen(self):
        """
        Activate 'Funktionsheizen' for testing 'Systemzustand' tile. Funktionsheizen will be activated if its not running already and if its running, this method will become invalid.
        """
        test_case = "UHI-24: 'Systemzustand'"
        target_screen = self._content["EASYON"]
        target_tile = self._content["HEATING_PROGRAM"]

        step = self.initial_steps(test_case, target_screen, target_tile)
        Anheiz.click_right_arrow(test_case, 0, step)
        Anheiz.start_Anheizprogramm(test_case, step)
        self.click_left_arrow(test_case, 0, step)
        time.sleep(1)
        self.click_close_icon(test_case, step)
        time.sleep(1)
        self.findElementCSS(test_case, self.buttons["bottom_back"], step).click()
        time.sleep(1)

    def get_status_of_Funktionsheizen(self):
        """
        returns 'True' if 'Funktionsheizen' is ON

        Returns
        -------
        'Boolean'
            True / False
        """
        return Anheiz.get_status_Anheizen_status_from_WPM()


    def verify_Systemzustand_tile(self, test_case, step):
        """
        verifies 'Systemzustand' tile with WPM results

        Parameters
        ----------
        test_case
            The targeted 'test case' ID with Description
        step
            step number from the test case document
        """
        from Helping_functions.Betriebsdaten import BetriebsDaten
        Status_wert, Sperre_wert, Stoerung_wert, Fehler_wert = BetriebsDaten().get_Werts_from_WPM()
        Sperre_wert_int, Stoerung_wert_int, Fehler_wert_int = BetriebsDaten().get_Werts_values_from_WPM()

        # verify 'System information' in tile
        for area in self.findElementsCSS(test_case, self.texT["area"], step):
            if self.get_status_of_Funktionsheizen():
                if area.text == self._content["PROGRAM"]:
                    # 4 tiles valid config
                    try:
                        if self.browser.find_element_by_css_selector("div.border-vertical").is_displayed():
                            programm = self.findElementsCSS(test_case, self.texT["area_bold"], step)[0].text    
                    except:
                        # valid only for `9` tiles config
                        programm = self.findElementCSS(test_case, self.texT["area_margin_bold"], step).text
                    self.compare_results(test_case, self._content["FUNCTION_HEATING"], programm, "'Funktionsheizen' is not displayed in the 'Systemzustand' tile", step)
            if area.text == self._content["HEAT_GENERATING"]:
                WP_status = self.findElementCSS(test_case, self.texT["row_heat_gen"], step).find_element_by_css_selector(self.texT["area_bold"]).text
                self.compare_results(test_case, Status_wert, WP_status, "'Wärmeerzeugung' status is wrong in 'Statuszustand' tile", step)

        # verify 'Warning' icon in tile
        if Sperre_wert_int != 0 or Stoerung_wert_int != 0 or Fehler_wert_int != 0:
            warning = self.findElementCSS(test_case, self.icon["status_Warning"], step)
            self.compare_true_result(test_case, warning.is_displayed(), "Warning icon is not displayed", step)
        else:
            ok = self.findElementCSS(test_case, self.icon["status_OK"], step)
            self.compare_true_result(test_case, ok.is_displayed(), "'OK' icon is not displayed", step)
        if self.check_internet():
            internet = self.findElementCSS(test_case, self.icon["internet_OK"], step)
            self.compare_true_result(test_case, internet.is_displayed(), "'Online' icon is not displayed", step)






