from func_testCases.Demomodus import DemoModus

class DefTestCases(DemoModus):
    """ Defining test cases based on testing order """
    
    def setUp(self):
        self.startApp()
    
    def tearDown(self):
        self.driver.quit()

    def test_LoginWithRegisteredUser(self):
        self._test_login()

    def test_DemoModus(self):
        self._test_login()
        step = 2
        self._test_demo_modus(step)
    
    def test_Netzwerk(self):
        from func_testCases.netzwerk import Netzwerk
        self._test_login()
        step = 2
        self._select_1st_device(step)
        step += 1
        self._click_ssaver_clock(step)
        step += 1
        self._unlock_userM(step)
        step += 1
        self._click_settings(step)
        step += 1
        Netzwerk()._cont_network(step)

