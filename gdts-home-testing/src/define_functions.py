from appium import webdriver
import src.exceptions as ex
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from src.logger import fail_log
import unittest
import json
import os, sys

class DefineFunctions(unittest.TestCase):
    """
    This class defines the most used common functions in this test automation project like check result, tapping, and so on.
    """
    # Get current work DIR
    current_dir = os.getcwd()
    json_dir = os.path.join(current_dir, 'json-files')
    app_path = os.path.join(current_dir, 'app-staging-debug.apk')

    # Read 'elements.json' file
    with open(os.path.join(json_dir, 'elements.json'), encoding='utf-8') as ele:
        __ele = json.load(ele)
        ele_xpath = __ele["XPath"]
        ele_ID = __ele["ID"]

    with open(os.path.join(json_dir, 'de.common.json'), encoding='utf-8') as info:
        content = json.load(info)

    def startApp(self):
        """
        Launch the 'Android' app for each test cases.
        """
        desired_cap = {}
        desired_cap['platformName'] = 'Android'
        desired_cap['platformVersion'] = '7.0'
        desired_cap['deviceName'] = 'Android Tablet'
        desired_cap['deviceId'] = "192.168.16.238:5555"
        desired_cap['app'] = self.app_path
        desired_cap["noReset"]: True
        desired_cap["fullReset"]: False
        self.driver = webdriver.Remote(
            'http://127.0.0.1:4723/wd/hub', desired_cap)

    def _assertEqual(self, expected, actual, error, step):
        """
        Compare 'expected' result against 'actual' result with an exception of errors.

        Parameters
        ----------
        expected
            The expected result for the particular step
        actual
            The actual result for the particular step
        error
            The error description for the `failed.log`
        step
            The step number at which the test case is running
        """
        try:
            self.assertEqual(expected, actual)
        except KeyboardInterrupt:
            print("Cancelled the test execution.")
        except:
            fail_log.error("[AssertEqualFailed] at step: " + str(step) + " [ Expected: " + str(
                expected) + ", Actual: " + str(actual) + ", Error: " + str(error) + " ]")

    def _assertTrue(self, expected, error, step):
        """
        Compare 'expected' result against 'True' result with an exception of errors.

        Parameters
        ----------
        expected
            The expected result for the particular step
        error
            The error description for the `failed.log`
        step
            The step number at which the test case is running
        """
        try:
            self.assertTrue(expected)
        except KeyboardInterrupt:
            print("Cancelled the test execution.")
        except:
            fail_log.error("[AssertTrueFailed] at step: " + str(step) + " [ Expected: " + str(
                expected) + ", Actual: " + str(True) + ", Error: " + str(error) + " ]")


    def findElementXpath(self, xpath, step):
        """
        Find element using `xpath`.
        Parameters
        ----------
        xpath
            The `xpath` of desired element in the device
        error
            The error description for the `failed.log`
        step
            The step number at which the test case is running
        """
        try:
            return self.driver.find_element_by_xpath(xpath)
        except KeyboardInterrupt:
            print("Cancelled the test execution.")
        except:
            fail_log.error("[XpathNotFound] at step: " + str(step) +
                     " [ Xpath: " + str(xpath) + " ]")

    def findElementID(self, ID, step):
        """
        Find element using `xpath`.
        
        Parameters
        ----------
        xpath
            The `xpath` of desired element in the device
        error
            The error description for the `failed.log`
        step
            The step number at which the test case is running
        """
        try:
            return self.driver.find_element_by_id(ID)
        except KeyboardInterrupt:
            print("Cancelled the test execution.")
        except:
            fail_log.error("[IdNotFound] at step: " +
                     str(step) + " [ ID: " + str(ID) + " ]")

    def waitAndFindElementXpath(self, xpath, step):
        """
        Wait until specified `Xpath` is visible and returns the "Xpath" element

        Parameters
        ----------
        xpath
            The `xpath` of desired element in the device
        step
            The step number at which the test case is running
        """
        try:
            return WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.XPATH, xpath)))
        except:
            fail_log.error("[WaitByXpathFailed] at step: " +
                     str(step) + " [ Xpath: " + str(xpath) + " ]")

    def waitAndFindElementID(self, ele_ID, step):
        """
        Wait until specified `ID` is visible and returns the "ID" element

        Parameters
        ----------
        ID
            The `ID` of desired element in the device
        step
            The step number at which the test case is running
        """
        try:
            return WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.ID, ele_ID)))
        except:
            fail_log.error("[WaitByIDFailed] at step: " +
                     str(step) + " [ ID: " + str(ele_ID) + " ]")

    def assertElementPresented(self, error, step, ID=None, xpath=None):
        """ 
        Returns `True`, if described element with ID (or) xpath presented

        Parameters
        ----------
        ID
            The `ID` of desired element in the device
        xpath
            The `xpath` of desired element in the device
        step
            The step number at which the test case is running
        
        Returns
        -------
        Boolean
            If the mentioned element is presented
        """
        if not ID == None:
            return self._assertTrue(self.findElementID(ID, step).is_displayed(), error, step)
        if not xpath == None:
            return self._assertTrue(self.findElementXpath(xpath, step).is_displayed(), error, step)

    def findElementByViewXpathAndText(self, ele_text, step):
        """ Find elements by xpath `//android.view.View[@text='ele_text']` and returns 
        
        Returns:
        -------
        element: 'reference of the element'"""
        
        xpath_view = self.ele_xpath["xpath_android.view"] + "[@text='" + ele_text + "']"
        return self.waitAndFindElementXpath(xpath_view, step)

if __name__ == "__main__":
    call  = DefineFunctions()