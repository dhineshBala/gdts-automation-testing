import logging, json
import datetime

now = datetime.datetime.now()
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')


def setup_logger(name, log_file, level=logging.INFO):
    handler = logging.FileHandler(log_file)
    handler.setFormatter(formatter)

    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)
    return logger

pass_log = setup_logger("Passed", ".\\logs\\Passed_" + str(now.strftime("%Y-%m-%d_%H-%M-%S")) + ".log")
pass_log.info(
    "*************************** Test execution started ***************************")
fail_log = setup_logger("Failed", ".\\logs\\Failed_" + str(now.strftime("%Y-%m-%d_%H-%M-%S")) + ".log")
fail_log.error(
    "*************************** Test execution started ***************************")