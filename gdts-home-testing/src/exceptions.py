class Error(Exception):
    """Base class for other exceptions"""
    pass


class ElementNotFound(Error):
    """ Raise when the element is not found """
    pass