from src.test_cases import DefTestCases
import os, unittest
import time
import xmlrunner

def test_suite():
    suite = unittest.TestSuite()
    suite.addTest(DefTestCases('test_DemoModus'))
    return suite

if __name__ == "__main__":
    runner = xmlrunner.XMLTestRunner(output='Test_Reports', verbosity=2)
    runner.run(test_suite())

