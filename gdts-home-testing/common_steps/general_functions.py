from src.define_functions import DefineFunctions
from selenium.webdriver.common.action_chains import ActionChains
from appium.webdriver.common.touch_action import TouchAction
import time

class GeneralFunctions(DefineFunctions):

    def _select_1st_device(self, step):
        """ select `1st System` from the list of available devices """
        el_1st_UHI = self.waitAndFindElementXpath(
            self.ele_xpath["btn_1st_UHI"], step)
        el_1st_UHI.click()

    def _click_settings(self, step):
        """ cliks on `settings` icon in the bottom navigation """
        el_settings = self.waitAndFindElementXpath(
            self.ele_xpath["settings"]["btn_settings"], step)
        print(self.ele_xpath["settings"]["btn_settings"])
        el_settings.click()

    def _click_ssaver_clock(self, step):
        """ cliks on `Analog clock` on the screen saver """
        el_clock = self.waitAndFindElementXpath(
            self.ele_xpath["btn_ssaver_clock"], step)
        el_clock.click()
        

    def _unlock_userM(self, step):
        """ unlocks 'System M User` with pattern 'M'"""
        ele_dots = self.ele_ID["unlock_pattern"]
        action = TouchAction(self.driver)
        loc_dot_6 = self.findElementID(ele_dots["dot_6"], step).location
        action.press(x=loc_dot_6['x'], y=loc_dot_6['y'])
        action.wait(100)
        loc_dot_3 = self.findElementID(ele_dots["dot_3"], step).location
        action.move_to(x=loc_dot_3['x'], y=loc_dot_3['y'])
        action.wait(100)
        loc_dot_0 = self.findElementID(ele_dots["dot_0"], step).location
        action.move_to(x=loc_dot_0['x'], y=loc_dot_0['y'])
        action.wait(100)
        loc_dot_4 = self.findElementID(ele_dots["dot_4"], step).location
        action.move_to(x=loc_dot_4['x'], y=loc_dot_4['y'])
        action.wait(100)
        loc_dot_2 = self.findElementID(ele_dots["dot_2"], step).location
        action.move_to(x=loc_dot_2['x'], y=loc_dot_2['y'])
        action.wait(100)
        loc_dot_5 = self.findElementID(ele_dots["dot_5"], step).location
        action.move_to(x=loc_dot_5['x'], y=loc_dot_5['y'])
        action.wait(100)
        loc_dot_8 = self.findElementID(ele_dots["dot_8"], step).location
        action.move_to(x=loc_dot_8['x'], y=loc_dot_8['y'])
        action.wait(100)
        action.release().perform()
