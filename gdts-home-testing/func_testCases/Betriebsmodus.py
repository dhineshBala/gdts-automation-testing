from Demomodus import DemoModus

class BetriebsModus(DemoModus):
    """ support class for `Betriebsmodus` test case"""
    
    def __click_BM_tile(self):
        """ click on `Betriebsmodus` tile in the **Haus** screen """