from common_steps.general_functions import GeneralFunctions
import os
import json

class LoginWithRegisteredUser(GeneralFunctions):

    step = 1

    with open(os.path.join(GeneralFunctions.json_dir, 'login.info.json'), encoding='utf-8') as info:
        info = json.load(info)
    info_ek_email = info["end_user"]["Email"]
    info_ek_pwd = info["end_user"]["pwd"]

    def _click_home(self):
        """Clicks on `Home Heat` screen after app startups"""
        el_heat_home = self.waitAndFindElementXpath(self.ele_xpath["btn_heat_home"], self.step)
        el_heat_home.click()
        self.step += 1

    def __enter_login_details(self):
        """Enters `Login` details from *login.info.json* file"""
        el_email = self.waitAndFindElementXpath(self.ele_xpath["tbx_email"], self.step)
        el_pwd = self.waitAndFindElementXpath(self.ele_xpath["tbx_pwd"], self.step)
        el_email.send_keys(self.info_ek_email)
        el_pwd.send_keys(self.info_ek_pwd)
        self.step += 1

    def __click_login(self):
        """ Clicks `Login` button in the app screen """
        el_login = self.findElementXpath(self.ele_xpath["btn_login"], self.step)
        el_login.click()
        self.step += 1
    
    def __verify_login_success(self):
        """Verifies `Login` username after successfull login"""
        el_your_devices = self.waitAndFindElementXpath(self.ele_xpath["txt_your_devices"], self.step)
        self._assertEqual(self.content["LINKED_DEVICES"], el_your_devices.text, "Login info mismatch", self.step)

    def _test_login(self):
        """ Steps to execute `Home-03 Login with registered user` test case"""
        self._click_home()
        self.__enter_login_details()
        self.__click_login()
        self.__verify_login_success()
