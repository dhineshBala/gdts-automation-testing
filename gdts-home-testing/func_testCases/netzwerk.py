from func_testCases.login import LoginWithRegisteredUser

class Netzwerk(LoginWithRegisteredUser):
    """ This class contains `Home-10 Netzwerk` functions """
    
    ele_settings = LoginWithRegisteredUser.ele_xpath["settings"]
    
    def __click_network_tile(self, step):
        """ clicks on `Netzwerk` tile in the setting screen """
        el_network = self.waitAndFindElementXpath(self.ele_settings["tile_Network"], step)
        el_network.click()

    def __verify_IPvs(self, step):
        """ verify IPv4 and IPv6 rows for data """
        self.assertElementPresented("IPv4 is empty", step, xpath=self.ele_settings["txt_IPv4"])
        self.assertElementPresented("IPv6 is empty", step, xpath=self.ele_settings["txt_IPv6"])

    def _cont_network(self, step):
        """ 
        continue `Netwerk` test case with
            - click Netwerk tile
            - verify IPvs info
        """
        self.__click_network_tile(step)
        step += 1
        self.__verify_IPvs(step)
