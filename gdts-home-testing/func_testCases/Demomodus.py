from func_testCases.login import LoginWithRegisteredUser

class DemoModus(LoginWithRegisteredUser):
    """ This class will execute `Home-06 Demo Modus` test case """
    
    def __click_add_system(self, step):
        """ clicks on `Add System` button in the **Welcome** screen """
        el_add_system = self.findElementByViewXpathAndText(self.content["ADD_DEVICE"], step)
        el_add_system.click()
    
    def __click_activate_demo_systemm(self, step):
        """ clicks in `Activate the Demo Mode` in the **System M** screen """
        el_activate_demo = self.findElementByViewXpathAndText(self.content["ACTIVATE_DEMO_MODE"], step)
        el_activate_demo.click()
    
    def __click_activate_demo_demomode(self, step):
        """ clicks in `Activate the Demo Mode` in the **Demo Mode** screen """
        self.driver.implicitly_wait(5)
        el_activate_demo = self.findElementByViewXpathAndText(self.content["Demo-Mo­dus ak­ti­vie­ren"], step)
        el_activate_demo.click()

    def __verify_demo_modus(self, step):
        """ verifies if `Bottom navigation` (Pretzel, Home,..) icons are presented in the demo mode screen """
        el_bottom_navi = self.waitAndFindElementXpath(self.ele_xpath["btn_bottom_navi"], step)
        self.assertElementPresented("Demo Mode bottom navigation is not available", step, xpath=el_bottom_navi)

    def _test_demo_modus(self, step):
        """ steps to reach `Demo Modus` and verify """
        self.__click_add_system(step)
        self.__click_activate_demo_systemm(step)
        self.__click_activate_demo_demomode(step)
